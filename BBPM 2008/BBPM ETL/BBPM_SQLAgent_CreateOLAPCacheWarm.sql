/************************************************  FRDW Cache Warming Agent job  ************************************************************

	1. Search/Replace NT AUTHORITY\LOCAL SERVICE with your assigned user - e.g. DOMAIN\BIRefresh
	2. Search/Replace	localhost with your SSAS Servername
	3. Search/Replace	BBPM_OLAP with your SSAS db name
	4. Run this script

*******************************************************************************************************************************************************/

USE [msdb]
GO

/****** Object:  Job [BBPM SSAS Cache Warming]    Script Date: 03/23/2012 18:15:04 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Blackbaud BI]    Script Date: 03/23/2012 18:15:04 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Blackbaud BI' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Blackbaud BI'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'BBPM SSAS Cache Warming', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Runs multiple MDX queries against SSAS Cubes to Warm the Cache to make OLAP Queries run faster', 
		@category_name=N'Blackbaud BI', 
		@owner_login_name=N'NT AUTHORITY\LOCAL SERVICE', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ClearCache]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ClearCache', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISCOMMAND', 
		@command=N'<Batch xmlns="http://schemas.microsoft.com/analysisservices/2003/engine"> 

  <ClearCache> 

    <Object> 

      <DatabaseID>BBPM_OLAP</DatabaseID> 

      <CubeID>Fundraising</CubeID> 

    </Object> 

  </ClearCache> 

</Batch>

', 
		@server=N'localhost', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Fiscal Year Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Fiscal Year Query', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].Members} ON COLUMNS,
CROSSJOIN
(
{[Date].[Fiscal Date].[All], [Date].[Fiscal Date].[Fiscal Year].Members, [Date].[Fiscal Date].[Fiscal Quarter].Members},
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Fiscal Month Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Fiscal Month Query', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].Members} ON COLUMNS,
CROSSJOIN
(
{[Date].[Fiscal Date].[Fiscal Month].Members},
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Fund Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Fund Query', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].[Gift Split Amount], [Measures].[Highest Gift Split Amount], [Measures].[Gift Count], [Measures].[Gift Constituent Count]} ON columns,
CROSSJOIN
(
{[Fund].[Fund Category].Members}, 
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Campaign Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Campaign Query', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].[Gift Split Amount], [Measures].[Highest Gift Split Amount], [Measures].[Gift Count], [Measures].[Gift Constituent Count]} ON columns,
CROSSJOIN
(
{[Campaign].[Campaign].Members}, 
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Appeal Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Appeal Query', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].[Gift Split Amount], [Measures].[Highest Gift Split Amount], [Measures].[Gift Count], [Measures].[Gift Constituent Count]} ON columns,
CROSSJOIN
(
{[Appeal].[Appeal Category].Members}, 
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Constituent Code Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Constituent Code Query', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].Members} ON columns,
CROSSJOIN
(
{[Constituent Code].[Constituent Code].Members},
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Constituent Code by Fiscal Date Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Constituent Code by Fiscal Date Query', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].Members} ON columns,
CROSSJOIN
(
{[Constituent Code].[Constituent Code].[Constituent Code Long].Members},
{[Date].[Fiscal Date].[Fiscal Year].Members},
{
[Gift Type].[Gift Type].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Pledge], 
[Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property]
}
)
ON ROWS
FROM
Fundraising', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Action Category Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Action Category Query', 
		@step_id=9, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].[Action Count], [Measures].[Action Constituent Count]} ON columns,
CROSSJOIN(
{[Action].[Action Category].Members}, 
{[Date].[Fiscal Date].[Fiscal Year].Members, [Date].[Fiscal Date].[Fiscal Quarter].Members, [Date].[Fiscal Date].[Fiscal Month].Members} 
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Action Type Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Action Type Query', 
		@step_id=10, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].[Action Count], [Measures].[Action Constituent Count]} ON columns,
CROSSJOIN(
{[Action].[Action Type].Members}, 
{[Date].[Fiscal Date].[Fiscal Year].Members, [Date].[Fiscal Date].[Fiscal Quarter].Members, [Date].[Fiscal Date].[Fiscal Month].Members} 
)
ON ROWS
FROM
Fundraising
', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Team Query]    Script Date: 03/23/2012 18:15:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Solicitor Query', 
		@step_id=12, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'ANALYSISQUERY', 
		@command=N'SELECT
{[Measures].Members} ON COLUMNS,
CROSSJOIN
(
{[Solicitor].[Solicitor].[Solicitor Name].Members},
{[Date].[Fiscal Date].[All], [Date].[Fiscal Date].[Fiscal Year].Members},
{[Gift Type].[Gift Type].[Cash], [Gift Type].[Gift Type].[Pledge], [Gift Type].[Gift Type].[Gift Type Name].[Cash], 
[Gift Type].[Gift Type].[Gift Type Name].[Pay-Cash], [Gift Type].[Gift Type].[Gift Type Name].[Pledge],
[Gift Type].[Gift Type].[Gift Type Name].[Recurring Gift Pay-Cash],
[Gift Type].[Gift Type].[Gift Type Group].[Stock/Property],
[Gift Type].[Gift Type].[Gift Type Name].[Stock/Property (Sold)]}
)
ON ROWS
FROM
Fundraising', 
		@server=N'localhost', 
		@database_name=N'BBPM_OLAP', 
		@flags=0, 
		@proxy_name=N'BIRefresh'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=62, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20120323, 
		@active_end_date=99991231, 
		@active_start_time=70000, 
		@active_end_time=235959, 
		@schedule_uid=N'0a7a4c01-e0d0-476a-bc30-33edf3aaa0c4'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

