/*****************************************************************************************

Release Notes
Fundraising Data Warehouse

*****************************************************************************************/
Date		By	Module				Change			
04/24/08	AE	FACT_GIFT_LOAD		Branch at end to retain Fact Keys in Incremental Load
04/28/08	AE	FACT_GIFT_LOAD		Added Acknowledgement fields into ETL - DoNotAcknowledge & DoNotReceipt
									and AcknowledgeDate with Fuzzy Date Processing
									
05/05/08	AE	FACT_GIFT_LOAD		Bug fixes in FACT_GIFT_LOAD for Amount/Split and UserFilter1 assigned fields and added logic
									to bring together UserFilter1 fields from other dimensions
05/06/08	AE	DIM_CONSTITUENT		Put in Additional Lookups for Code Tables to speed up performance
			AE	FACT_GIFT			Acknowledgement Bug in GiftDateToAcknowledgementDate
05/09/08	AE	DIM_APPEAL			Last Gift Date and Date Dim foreign keys
05/09/08	AE	DIM_CAMPAIGN		Last Gift Date and Date Dim foreign keys
05/09/08	AE	DIM_FUND			Last Gift Date and Date Dim foreign keys
05/09/08	AE	DIM_PACKAGE			Last Gift Date and Date Dim foreign keys
05/09/08	AE	DIM_CONSTITUENTRELATIONSHIP		Added Date Dim foreign keys
05/09/08	AE	DIM_CONSTITUENTFULRELATIONSHIP	Added Date Dim foreign keys
05/09/08	AE	DIM_CONSITUTENT_DETAILS			Added Date Dim foreign keys
05/09/08	AE	DIM_CONSTITUENT_SALUTATION		Generecized the package to bring in all Saluations
05/13/08	AE	FACT_ACTION			Put in New/Existing load split on incremental to retain surrogate keys
05/13/08	AE	DIM_CONSTITUENT		Modified "Lookup Head of Household Flag" so duplicate ConstituentSystemIDs were
									not yeilded - put in MIN(IS_HEADOFHOUSEHOLD) to return -1 if a HeadOfHousehold is returned
08/08/08	AE	CONTROL_ETL_MASTER	Set FACT_GIFT_LOAD and FACT_GIFT_TRIBUTES_LOAD property ExecuteOutOfProcess set to false
09/04/08	AE	FACT_SOLICITORASSIGNMENT and FACT_GIFTCOMMITMENT - Bug fixes Incremental load and InstallmentBalance
09/08/08	CH	DIM_CONSTITUENTADDRESS		New table and logic
09/08/08	CH	DIM_CONSTITUENT			Mapped Address5
09/08/08	CH	DIM_RANGE			Change RangeDescription for Frequency Count from 13-14 to 13-15
09/08/08	CH	FACT_CONSTITUENTLTGIVING	Last and First Gift fields both pulling First info
09/15/08	AE	DIM_PROPOSAL_LOAD	Added the loading of Proposal Attributes, with new table in MasterScript.sql
09/16/08	AE	FACT_Gift	Added population of AcknolwedgementDateDimID
09/16/08	AE	FACT_GiftWriteOff	Various fields not being populated and removed unnecessary fields
09/16/08	AE	FACT_GiftChange		Various fields not being populated and removed unnecessary fields
09/16/08	AE	SUMMARY_DONOR_ANALYTICS	FACT_ConstituentLTAppeal fixed population of FirstAppealDateDimID and LastAppealDateDimID
09/17/08	AE	FACT_EVENTPARTICIPANT	Lookup of ConstitituentSystemID for Sponsor and Guest not catching NULL records - fixed to -1
										Also ensured EventStartDateDimID was properly populated
09/17/08	AE	DIM_EVENT	Added population of EventStartDateDimID and EventEndDateDimID
09/17/08	AE	FACT_AppealExpense	Ensured ExpenseDateDimID was populated propery and ExpenseTypeDimID
09/17/08	AE	FACT_Action	Ensured ProposalDimID was returning a -1 rather than NULL
09/17/08	AE	MODULE_MEMBERSHIP	Ensured that FACT_MembershipTransaction returned default ActivityDateDimID and ExpiredDateDimID
09/17/08	AE	SUMMARY_DONOR_ANALYTICS		Ensured that RankingHighestToLowest returns default GiftDateDimID if NULL
09/17/08	AE	SUMMARY_MEMBER_ANALYTICS	Ensured that RankingHighestToLowest returns default ActivityDateDimID if NULL
09/27/08	CH	SUMMARY_DONOR_ANALYTICS		ConsecutiveYears Calc fix
09/27/08	CH	FACT_GIFTCOMMITMENT			
09/27/08	CH	FACT_INSTALLMENTS			Fixed Installment Balances
09/27/08	CH	FACT_INSTALLMENTPAYMENTS
09/29/08	AE	FACT_GIFTWRITEOFF_LOAD	Bug fix to Integer Date Conversions
10/04/08	AE	FACT_EVENTPARTICIPANTGIFT	Bug Fix on Gift Lookup, added Event to Gift Date Interval Calculation
11/05/08	CH 	Added deletion package, FACT_EVENTEXPENSE, and FACT_EVENTPARTICIPANTFEE
01/02/09	AE	FACT_ACTION		Bug fix to add proper loading of AppealSystemID from ACTIONS table rather than Attribute.  Removal
				of Package ID and Lookup
01/16/09	AE	FACT_GIFT	Removed the SplitGiftId's removal part - it is now in WAREHOUSE_DELETIONS
01/16/09	AE	DIM_APPEAL	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_CAMPAIGN	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_EVENT	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_FUND	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_PACKAGE	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_PROPOSAL	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_CONSTITUENT_DETAIL	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	DIM_CONSTITUENTEDUCATION	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	FACT_ACTION	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	FACT_GIFT	Added logic to add AttributeDateDimID to loading Attribute
01/16/09	AE	FACT_EVENTPARTICIPANT	Added New Container to load Attribute
01/16/09	AE	WAREHOUSE_DELETIONS	Improved performance with use of Lookups and made Deletions tables generic
01/16/09	AE	FACT_GIFTPROPOSAL	New Package Added
01/16/09	AE	DIM_CAMPAIGNGIVINGLEVELS	New Package Added
01/16/09	AE	DIM_GIVINGHIERARCHY			New Package Added
01/20/09	AE	FACT_GIFTCOMMITMENT		Improved Performance, introduced Lookups to Replace Merge Join processes
01/21/09	AE	DIM_STATIC_RE			Added population of DIM_Time Dimension
01/21/09	AE	CONTROL_ETL_MASTER		Added SQL Task to update DIM_Date dimension with current date info
01/21/09	AE	DIM_CONSTITUENT		Removed BirthDate and Deceased Date VB Script - replaced with SQL
02/04/09	AE	DIM_CONSTITUENT_DETAILS	Changed ConstituentPhones to load all phones rather than just preferred and
										the same with addresses to load all addresses other than just preferred
03/25/09	AE	FACT_GIFTCOMMITMENT		Bug in calculation Installment Amount - changed from MAX to SUM
04/02/09	AE	FACT_SOLICITOR_DETAILS	Added two new columns to FACT_SolicitorGift table and changed ETL to populate SolicitorSplitRatio
										and TotalGiftAmount
05/04/09	AE	FACT_SOLICITORACTIVITYGOAL	New Package Added to load data in from Spreadsheet for Solicitor Action goals
05/05/09	AE	DIM_PROSPECTS			Added Sequencing and Latency Calculations
05/07/09	AE	DIM_PROPOSALSTATUSHISTORY	New Package to load Historical Status on Proposals
05/12/09	AE	DIM_SOLICITOR			New Package to load Solicitor Information
05/12/09	AE	FACT_SOLICITORASSIGNMENT		Added Fields DateAddedDimID and DateChangedDimID
05/27/09	AE	FACT_GIFTCOMMITMENT Bug in calculating Write-Offs & BalanceAmount
05/27/09	AE	FACT_EVENTPARTICIPANTGIFT	Added additional Gift fields in - FundDimID, AppealDimID, CampaignDimID, PackageDimID and others
06/03/09	AE	New Packages DIM_LETTERCODE, DIM_FINANCIALINFO, DIM_PHILANTHROPICINTEREST, FACT_GIFTOTHER, FACT_GIFTCOMMITMENTBYPAYSPLIT
06/03/09	AE	FACT_GIFTWRITEOFF		Added new column to  - GiftCommitmentSystemID.  
06/03/09	AE	FACT_Gift				Added LetterCodeDimID
06/05/09	AE	SUMMARY_DONOR_ANALYTICS	Added new Table - FACT_GiftRank.  Removed existing tables of
				FACT_GiftRankingOldestToNewest, FACT_GiftRankingNewestToOldest, FACT_GiftRankingHighestToLowest
06/05/09	AE	SUMMARY_MEMBER_ANALYTICS	Added new Tables - FACT_MembershipRank, FACT_LTMembershipRank.Removed existing tables of
				FACT_MembershipRankingOldestToNewest, FACT_MembershipRankingNewestToOldest
				FACT_LTMembershipRankingOldestToNewest, FACT_LTMembershipRankingNewestToOldest
06/05/09	AE	SUMMARY_DONOR_ANALYTICS		Addition of new Table FACT_ConstituentLTProposal.  New Stored Proc BBBI_ProposalSummaryUpdates
06/11/09	AE	DIM_PROPOSAL	Addition of RFM Fields to DIM_Proposal
06/16/09	AE	DIM_CONSTITUENT	Changed DIM_Constituent - field Surname changed to LastName
06/18/09	AE	DIM_PROPOSAL	Added new DIM_Proposal Fields IsExpected, IsFunded, HasCloseDays
06/22/09	AE	FACT_SOLICITOR_DETAILS	New fields added to DIM_Proposal and FACT_SolicitorProposal for Action Count Analysis.  SQL added
				to this package to update the fields in those two tables
06/22/09	AE	New Package FACT_GIFT_PLANNED to Load extended Planned Giving info from Gift2Fields from RE into FACT_GiftPlanned
08/28/09	AE	FACT_INSTALLMENTS	Modifications to FACT_GiftInstallment and FACT_GiftInstallmentByPaySplit with InstallmentFullStatus field added
08/31/09	AE	FACT_GIFTWRITEOFF	Added WriteOffDate and WriteOffDateDimID to FACT_GiftWriteOff
09/01/09	AE	FACT_GIFTCOMMITMENT	Added PostStatusDimID and PaymentTypeDimID to FACT_GiftCommitment and FACT_GiftCommitmentByPaySplit
09/04/09	AE	FACT_INSTALLMENTPAYMENT	New fields added to FACT_GiftInstallmentPayment - PaymentStatus, InstallmentToPaymentDays, CommitmentToPaymentDays
09/29/09	AE	DIM_CONSTITUENT_DETAILS bug fix to properly load RatingDescription
10/07/09	AE	FACT_GIFT Added IsPlannedGiftPayment and IsTribute to FACT_Gift
10/15/09	AE	DIM_RE_STATIC	Added DIM_TransactionType table and PostStatusFESystemID to DIM_PostStatus
10/16/09	AE	FACT_EVENTPARTICIPANT	Bug fix for flags on IsCoordinator, IsSponsor, etc flag check set from 1 to -1
10/21/09	AE	DIM_Fund added processing of new flag fields IsRE, IsFE, IsGE
10/22/09	AE	SUMMARY_DONOR_ANALYTICS	Added FACT_ConstituentAnnualGivingUpgrade table processing
10/22/09	AE	CONTROL_ETL_MASTER	Bug Fix - calling wrong Package in TEMP_INSTALLMENTS_LOAD process








