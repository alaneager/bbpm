/************************************ NULL Column Utility Stored Proc ************************************************/
USE [master]
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckNullsInDBFields]    Script Date: 10/27/2008 12:02:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
IF EXISTS(SELECT name FROM sysobjects WHERE name = 'sp_CheckNullsInDBFields' AND type = 'P')  
	DROP PROCEDURE sp_CheckNullsInDBFields

CREATE PROCEDURE [dbo].[sp_CheckNullsInDBFields] 
	-- parameters for the stored procedure 
	@DatabaseName VARCHAR(100) , 
	@TableName VARCHAR(200)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --  procedure code

	IF OBJECT_ID(N'tempdb..#ColumnTable') IS NOT NULL
		DROP TABLE #ColumnTable

	CREATE TABLE #ColumnTable ( RowID INT PRIMARY KEY IDENTITY
		, TableName VARCHAR(60)
		, ColumnName VARCHAR(80)
		, TableCount INT
		, ColumnNullCount INT
		, ColumnPctNullValues NUMERIC(5,2)  )

	DECLARE @TableHolder TABLE (TableCount int)

	DECLARE @TableCount INT
	DECLARE @n INT, @Max INT
	DECLARE @SQL NVARCHAR(255)
	DECLARE @ColumnName VARCHAR(80)
	DECLARE @ColumnNullCount INT

	SET @SQL = 'SELECT COUNT(*) FROM ' + @DatabaseName + '..[' + @TableName + ']'

	INSERT INTO @TableHolder (TableCount)
	EXEC sp_executesql @SQL

	--- PRINT @SQL 

	SET @SQL = 'INSERT INTO #ColumnTable (TableName, ColumnName) 
	SELECT DISTINCT Table_Name, Column_Name FROM ' + @DatabaseName + '.INFORMATION_SCHEMA.COLUMNS
	WHERE table_name = ''' + @TableName + ''''

	 --- PRINT @SQL

	EXEC sp_executesql @SQL

	--INSERT INTO #ColumnTable (TableName, ColumnName)
	--SELECT DISTINCT Table_Name, Column_Name FROM PITS.INFORMATION_SCHEMA.COLUMNS
	--WHERE table_name =  @TableName


	-- update has to come after the insert
	UPDATE #ColumnTable
	SET	TableCount = (SELECT TableCount FROM @TableHolder )

	SET @n = (SELECT MIN(RowID) FROM #ColumnTable)
	SET @Max = (SELECT MAX(RowID) FROM #ColumnTable)

	-- SELECT * FROM #ColumnTable 

	WHILE @n <= @Max
	BEGIN
		
		
		SET @ColumnName = (SELECT ColumnName FROM #ColumnTable WHERE RowID = @n)
		SET @SQL = 'UPDATE #ColumnTable SET ColumnNullCount = (SELECT Count(*) FROM ' + @DatabaseName 
			+ '..[' + @TableName + '] WHERE [' + @ColumnName + '] IS NULL OR LEN(LTRIM(RTRIM([' + @ColumnName + ']))) < 1 ) WHERE RowID = ' + CAST(@n AS VARCHAR(4) ) 

	---- WHERE [' + @ColumnName + '] IS NULL AND RowID = ' + CAST(@n AS VARCHAR(4) ) + ')'

	--	SELECT Count(' + @ColumnName + ') FROM ' + @TableName + ' WHERE ' + @ColumnName + 
	--			' IS NULL OR ' + @ColumnName + ' = '''
	--	SET @ColumnNullCount = ( EXEC '@SQL' )

	-- PRINT @n	
	-- PRINT @SQL
	-- PRINT @Max 
	-- PRINT @ColumnName	
		
		EXEC sp_executesql @SQL

		SET @SQL = ''
		
		--- now calc the percent
		SET @SQL = 'UPDATE #ColumnTable SET ColumnPctNullValues  = ( SELECT ((CAST(ColumnNullCount AS NUMERIC(9,2)) / CAST(TableCount AS NUMERIC(9,2)) * 100 ) ) FROM #ColumnTable WHERE RowID = ' 
		 + CAST(@n AS VARCHAR(4) )  + ') WHERE RowID = ' + CAST(@n AS VARCHAR(4) ) 

	-- PRINT @SQL	

		EXEC sp_executesql @SQL

		-- increment
		SET @n = @n + 1
		
	END
	
	-- Output the results
	SELECT RowID, ColumnName, ColumnNullCount, ColumnPctNullValues
	  FROM #ColumnTable 
	ORDER BY ColumnPctNullValues ,ColumnNullCount
	
END

GO

