/*******************************************************************************************************************************************************
		Test Queries
*******************************************************************************************************************************************************/
-- Gift Join against all related tables

SELECT   
Count(*) AS GiftSplitCount
FROM         
FACT_Gift AS g INNER JOIN
                      DIM_Appeal AS a ON g.AppealDimID = a.AppealDimID INNER JOIN
                      DIM_Campaign AS cp ON g.CampaignDimID = cp.CampaignDimID INNER JOIN
                      DIM_Constituent AS c ON g.ConstituentDimID = c.ConstituentDimID INNER JOIN
                      DIM_ConstituentCode AS cc ON g.ConstituentCodeDimID = cc.ConstituentCodeDimID INNER JOIN
                      DIM_Fund AS f ON g.FundDimID = f.FundDimID INNER JOIN
                      DIM_GiftCode AS gc ON g.GiftCodeDimID = gc.GiftCodeDimID INNER JOIN
                      DIM_GiftSubtype AS gst ON g.GiftSubtypeDimID = gst.GiftSubtypeDimID INNER JOIN
                      DIM_GiftType AS gt ON g.GiftTypeDimID = gt.GiftTypeDimID INNER JOIN
                      DIM_LetterCode AS lc ON g.LetterCodeDimID = lc.LetterCodeDimID INNER JOIN
                      DIM_Package AS p ON g.PackageDimID = p.PackageDimID INNER JOIN
                      DIM_PaymentType AS pt ON g.PaymentTypeDimID = pt.PaymentTypeDimID INNER JOIN
                      DIM_PostStatus AS ps ON g.PostStatusDimID = ps.PostStatusDimID INNER JOIN
                      DIM_Date AS gd ON g.GiftDateDimID = gd.DateDimID



