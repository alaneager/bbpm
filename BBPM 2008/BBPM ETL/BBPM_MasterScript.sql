/********************************************************************************************************************************************************************************
Version Log
Version		Date					Comments
5.0				12/1/2011			Merge of RE/FE/GE into one DW

********************************************************************************************************************************************************************************/

/********************************************************************************************************************************************************************************

		***********  IMPORTANT DO THIS FIRST!! ********************
		Main DB Create
		Modify File Locations of File groups to give correct disk location
		Search/Replace 
				C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA
		with your DB Location directory where BBPM_DW database files will reside
********************************************************************************************************************************************************************************/

USE [master]
GO
CREATE DATABASE BBPM_DW ON  PRIMARY 
( NAME = N'BBPM_PRIM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW.mdf' , SIZE = 10240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 2048KB ), 
 FILEGROUP [BBPM_DEFGROUP]  DEFAULT 
( NAME = N'BBPM_DEF1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_DEF1.ndf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 5120KB ), 
( NAME = N'BBPM_DEF2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_DEF2.ndf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 5120KB ), 
 FILEGROUP [BBPM_DIMGROUP] 
( NAME = N'BBPM_DIM1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_DIM1.ndf' , SIZE = 25600KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB ), 
( NAME = N'BBPM_DIM2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_DIM2.ndf' , SIZE = 25600KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB ), 
 FILEGROUP [BBPM_FACTGROUP] 
( NAME = N'BBPM_FACT1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_FACT1.ndf' , SIZE = 25600KB , MAXSIZE = UNLIMITED, FILEGROWTH = 20480KB ), 
( NAME = N'BBPM_FACT2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_FACT2.ndf' , SIZE = 25600KB , MAXSIZE = UNLIMITED, FILEGROWTH = 20480KB ), 
( NAME = N'BBPM_FACT3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_FACT3.ndf' , SIZE = 25600KB , MAXSIZE = UNLIMITED, FILEGROWTH = 20480KB ), 
( NAME = N'BBPM_FACT4', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_FACT4.ndf' , SIZE = 25600KB , MAXSIZE = UNLIMITED, FILEGROWTH = 20480KB ), 
 FILEGROUP [BBPM_IDXGROUP] 
( NAME = N'BBPM_IDX1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_IDX1.ndf' , SIZE = 230400KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB ), 
( NAME = N'BBPM_IDX2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_IDX2.ndf' , SIZE = 220160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB ), 
 FILEGROUP [BBPM_STAGEGROUP] 
( NAME = N'BBPM_STAGE1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_STAGE1.ndf' , SIZE = 189440KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB ), 
( NAME = N'BBPM_STAGE2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_STAGE2.ndf' , SIZE = 189440KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'BBPM_LOG1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQL2008R2\MSSQL\DATA\BBPM_DW_LOG1.ldf' , SIZE = 230400KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO

ALTER DATABASE [BBPM_DW] SET COMPATIBILITY_LEVEL = 100
GO

PRINT 'Database Created'

USE [BBPM_DW]

/********************************************************************************************************************************************************************************
		Main Table Creates
********************************************************************************************************************************************************************************/

/****** Object:  Table [dbo].[DIM_Range]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Range](
	[RangeDimID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[Unit] [varchar](50) NULL,
	[RangeDescription] [varchar](50) NULL,
	[LowerRange] [decimal](22, 4) NULL,
	[UpperRange] [decimal](22, 4) NULL,
	[Sequence] [smallint] NULL,
	[IsCustomRange] bit NULL,
 CONSTRAINT [PK_DIM_Range_RangeDimID] PRIMARY KEY CLUSTERED 
(
	[RangeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ConstituentAge]    Script Date: 12/05/2011 12:28:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_ConstituentAge] ( @IsDeceased varchar(3), @Birthday datetime , @DeceasedDate datetime )  
RETURNS Int AS  
BEGIN 
-- Arguments @IsDeceased = 
-- Arguments @Birthday = 
-- Arguments @DeceasedDate = 

DECLARE @Age Int

SET @Age = (SELECT case when @IsDeceased='No' then 
	case WHEN Year(@Birthday) < 1800 THEN NULL 
	else 
		case when isdate(@Birthday)=(1) 
			then datediff(year,@Birthday,getdate())
		end
	end
else  --- IsDeceased = 'Yes'
	case when isdate(@DeceasedDate)=(1) and Year(@DeceasedDate) > 1800 and Year(@Birthday) > 1800 then datediff(year,@Birthday,@DeceasedDate)	
	end
end)
RETURN (@Age)

END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ReturnRange]    Script Date: 12/05/2011 12:28:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_ReturnRange] (@RangeType Varchar(50), @Value Numeric(18, 4))  
RETURNS Varchar(50) AS  
BEGIN 
DECLARE @RangeName AS Varchar(50)

SET @RangeName = 
(
SELECT     TOP 1
r.RangeDescription
FROM  DIM_Range r 
WHERE 
r.Type = @RangeType AND 
@Value >= r.LowerRange AND
@Value <= r.UpperRange
)
RETURN (@RangeName)
END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ReturnRangeID]    Script Date: 12/05/2011 12:28:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_ReturnRangeID] (@RangeType Varchar(50), @Value Numeric(18, 4))  
RETURNS Varchar(50) AS  
BEGIN 
DECLARE @RangeName AS Varchar(50)

SET @RangeName = 
(
SELECT     TOP 1
r.RangeDimID
FROM  DIM_Range r 
WHERE 
r.Type = @RangeType AND 
@Value >= r.LowerRange AND
@Value <= r.UpperRange
)
RETURN (@RangeName)
END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ReturnRangeSequence]    Script Date: 12/05/2011 12:28:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_ReturnRangeSequence] (@RangeType Varchar(50), @Value Numeric(18, 4))  
RETURNS Varchar(50) AS  
BEGIN 
DECLARE @RangeName AS Varchar(50)

SET @RangeName = 
(
SELECT     TOP 1
r.Sequence
FROM  DIM_Range r 
WHERE 
r.Type = @RangeType AND 
@Value >= r.LowerRange AND
@Value <= r.UpperRange
)
RETURN (@RangeName)
END

GO

/****** Object:  Table [dbo].[CTL_Config]    Script Date: 04/22/2013 11:26:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CTL_Config](
	[ConfigCTLID] [int] IDENTITY(1,1) NOT NULL,
	[VariableName] [varchar](50) NULL,
	[VariableValue] [varchar](255) NULL,
	[VariableDataType] [varchar](25) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_CTL_Config] PRIMARY KEY CLUSTERED 
(
	[ConfigCTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DEFGROUP]
) ON [BBPM_DEFGROUP]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[CTL_DWTableStats]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CTL_DWTableStats](
	[ParentObjectName] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[ObjectType] [varchar](50) NULL,
	[StatsType] [varchar](50) NULL,
	[Value] [int] NULL,
	[ValueNull] [int] NULL,
	[Date] [datetime] NULL,
	[HasIndex] [bit] NULL,
	[IsPrimary] [bit] NULL,
	[Sequence] [int] NULL,
	[SourceName] [varchar](25) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_CTL_DWTableStats] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DEFGROUP]
) ON [BBPM_DEFGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[CTL_ETLHistory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CTL_ETLHistory](
	[ETLControlID] [int] IDENTITY(1,1) NOT NULL,
	[ParentETLControlID] [int] NULL,
	[IsParent] [bit] NULL,
	[SSISPackageName] [nvarchar](255) NULL,
	[DataWindowOpen] [datetimeoffset](7) NULL,
	[DataWindowClose] [datetimeoffset](7) NULL,
	[ETLStartTime] [datetimeoffset](7) NULL,
	[ETLEndTime] [datetimeoffset](7) NULL,
	[LoadIsComplete] [bit] NULL,
	[SSISPackagePath] [nvarchar](255) NULL,
	[SSISStartTime] [datetimeoffset](7) NULL,
	[SSISUserName] [nvarchar](500) NULL,
	[SSISMachineName] [nvarchar](500) NULL,
	[SSISExecutionInstanceGUID] [uniqueidentifier] NULL,
	[NumRowsAdded] [bigint] NULL,
	[NumRowsUpdated] [bigint] NULL,
 CONSTRAINT [PK_ETLControl] PRIMARY KEY CLUSTERED 
(
	[ETLControlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DEFGROUP]
) ON [BBPM_DEFGROUP]

GO


GO

/****** Object:  Table [dbo].[CTL_IndexHistory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CTL_IndexHistory](
	[TableSystemID] [int] NULL,
	[IndexSystemID] [int] NULL,
	[ColumnSystemID] [int] NULL,
	[IndexColumnPosition] [smallint] NULL,
	[IndexType] [varchar](15) NULL,
	[TableName] [varchar](50) NULL,
	[IndexName] [varchar](100) NULL,
	[ColumnName] [varchar](50) NULL,
	[IncludeColumns] [varchar](500) NULL,
	[IndexInclude] [smallint] NULL,
	[NoColumns] [smallint] NULL
) ON [BBPM_DEFGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[CTL_SourceTableStats]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CTL_SourceTableStats](
	[ParentObjectName] [varchar](100) NULL,
	[ObjectName] [varchar](100) NULL,
	[ObjectType] [varchar](50) NULL,
	[StatsType] [varchar](50) NULL,
	[Value] [int] NULL,
	[ValueNull] [int] NULL,
	[Date] [datetime] NULL,
	[HasIndex] [bit] NULL,
	[IsPrimary] [bit] NULL,
	[Sequence] [int] NULL,
	[SourceName] [varchar](25) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_CTL_SourceTableStats] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DEFGROUP]
) ON [BBPM_DEFGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[CTL_SourceToTargetMapping]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CTL_SourceToTargetMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceDatabaseName] [varchar](100) NULL,
	[SourceTableName] [varchar](100) NULL,
	[SourceColumnName] [varchar](100) NULL,
	[TargetDatabaseName] [varchar](100) NULL,
	[TargetTableName] [varchar](100) NULL,
	[TargetColumnName] [varchar](100) NULL,
	[Description] [varchar](2000) NULL,
 CONSTRAINT [PK_CTL_SourceToTargetMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DEFGROUP]
) ON [BBPM_DEFGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Account]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Account](
	[AccountDimID] [int] NOT NULL,
	[AccountSystemID] [int] NOT NULL,
	[AccountNumber] [varchar](30) NULL,
	[AccountCode] [varchar](30) NULL,
	[AccountCodeDescription] [varchar](60) NULL,
	[AccountCodeSystemID] [int] NULL,
	[AccountDescription] [varchar](60) NULL,
	[AccountCategorySystemID] [smallint] NULL,
	[ReportType] [varchar](20) NULL,
	[ReportTypeSystemID] [smallint] NULL,
	[AccountCategoryDescription] [varchar](10) NULL,
	[ClassDimID] [int] NULL,
	[ClassSystemID] [int] NULL,
	[ClassDescription] [varchar](60) NULL,
	[FundSystemID] [int] NULL,
	[FundID] [varchar](10) NULL,
	[FundDescription] [varchar](60) NULL,
	[WorkingCapitalSystemID] [int] NULL,
	[WorkingCapital] [varchar](60) NULL,
	[CashFlowSystemID] [int] NULL,
	[CashFlow] [varchar](60) NULL,
	[AccountStatusSystemID] [smallint] NULL,
	[AccountStatusDescription] [varchar](30) NULL,
	[PreventPostDate] [datetime] NULL,
	[GeneralInfoSystemID] [int] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[ContraAccount] [varchar](1) NULL,
	[ControlAccount] [varchar](1) NULL,
	[CashAccount] [varchar](1) NULL,
	[Segment1SystemID] [int] NULL,
	[Segment2SystemID] [int] NULL,
	[AnnotationText] [text] NULL,
	[SubAccountCode] [varchar](10) NULL,
	[SubAccountName] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Account] PRIMARY KEY CLUSTERED 
(
	[AccountDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP] TEXTIMAGE_ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AccountAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AccountAttribute](
	[AccountAttributeDimID] [int] NOT NULL,
	[AccountAttributeSystemID] [int] NOT NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_AccountAttribute] PRIMARY KEY CLUSTERED 
(
	[AccountAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AccountSegment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AccountSegment](
	[AccountSegmentDimID] [int] NOT NULL,
	[AccountSegmentSystemID] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[SegmentSystemID] [int] NULL,
	[AccountCodeSystemID] [int] NULL,
	[FundSystemID] [int] NULL,
	[TableEntrySystemID] [int] NULL,
	[SegmentCode] [varchar](30) NULL,
	[SegmentName] [varchar](60) NULL,
	[SegmentCategoryName] [varchar](60) NULL,
	[SegmentType] [varchar](12) NULL,
	[SegmentLength] [smallint] NULL,
	[Separator] [smallint] NULL,
	[CodeTableSystemID] [int] NULL,
	[Sequence] [smallint] NULL,
	[GeneralInfoSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_AccountSegment] PRIMARY KEY CLUSTERED 
(
	[AccountSegmentDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ActionCategory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[DIM_ActionCategory](
	[ActionCategoryDimID] [int] NOT NULL,
	[ActionCategorySystemID] [smallint] NULL,
	[ActionCategory] [varchar](25) NULL,
 CONSTRAINT [PK_DIM_ActionCategory_ActionCategoryDimID] PRIMARY KEY CLUSTERED 
(
	[ActionCategoryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ActionLocation]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ActionLocation](
	[ActionLocationDimID] [int] NOT NULL,
	[ActionLocationSystemID] [int] NULL,
	[Description] [varchar](255) NULL,
	[ShortDescription] [varchar](100) NULL,
	[IsActive] [varchar](3) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ActionLocation_ActionLocationDimID] PRIMARY KEY CLUSTERED 
(
	[ActionLocationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ActionNotepad]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ActionNotepad](
	[ActionNotepadDimID] [int] NOT NULL,
	[ActionNotepadSystemID] [int] NULL,
	[ActionFactID] [int] NULL,
	[ActionSystemID] [int] NULL,
	[NoteType] [varchar](100) NULL,
	[NotepadDateDimID] [int] NULL,
	[NotepadDate] [datetime] NULL,
	[NotepadTitle] [varchar](50) NULL,
	[NotepadDescription] [varchar](255) NULL,
	[Author] [varchar](50) NULL,
	[ActualNotesText] [text] NULL,
	[ActualNotes] [nvarchar](2000) NULL,
	[Sequence] [smallint] NULL,
	[NotepadImportID] [varchar](20) NULL,
	[DateAddedDimID] [int] NULL,
	[DateChangedDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[AllFirstSequence] [int] NULL,
	[AllLastSequence] [int] NULL,
	[ConstituentDimID] [int] NULL,
 CONSTRAINT [PK_DIM_ActionNotepad] PRIMARY KEY CLUSTERED 
(
	[ActionNotepadDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP] TEXTIMAGE_ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ActionPriority]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ActionPriority](
	[ActionPriorityDimID] [int] NOT NULL,
	[ActionPrioritySystemID] [smallint] NULL,
	[ActionPriority] [varchar](25) NULL,
 CONSTRAINT [PK_DIM_ActionPriority_ActionPriorityDimID] PRIMARY KEY CLUSTERED 
(
	[ActionPriorityDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ActionStatus]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ActionStatus](
	[ActionStatusDimID] [int] NOT NULL,
	[ActionStatusSystemID] [int] NULL,
	[Description] [varchar](255) NULL,
	[ShortDescription] [varchar](100) NULL,
	[IsActive] [varchar](3) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ActionStatus_ActionStatusDimID] PRIMARY KEY CLUSTERED 
(
	[ActionStatusDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ActionType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ActionType](
	[ActionTypeDimID] [int] NOT NULL,
	[ActionTypeSystemID] [int] NULL,
	[Description] [varchar](255) NULL,
	[ShortDescription] [varchar](100) NULL,
	[IsActive] [varchar](3) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ActionType_ActionTypeDimID] PRIMARY KEY CLUSTERED 
(
	[ActionTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AddressAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AddressAttribute](
	[AddressAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[AddressAttributeSystemID] [int] NULL,
	[ConstituentAddressSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[IsPreferred] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_AddressAttribute_AddressAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[AddressAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AddressAttribute_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AddressAttribute_Stage](
	[AddressAttributeSystemID] [int] NULL,
	[ConstituentAddressSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[IsPreferred] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AnnualClassification]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AnnualClassification](
	[ClassificationCode] [varchar](4) NOT NULL,
	[ClassificationDescription] [varchar](100) NULL,
	[ClassificationType] [varchar](50) NULL,
	[ClassificationLength] [int] NULL,
	[ClassificationGroup] [varchar](50) NULL,
	[Sequence] [smallint] NULL,
	[GroupSequence] [smallint] NULL,
 CONSTRAINT [PK_DIM_AnnualClassification] PRIMARY KEY CLUSTERED 
(
	[ClassificationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Appeal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Appeal](
	[AppealDimID] [int] NOT NULL,
	[AppealSystemID] [int] NULL,
	[AppealIdentifier] [varchar](20) NULL,
	[AppealDescription] [varchar](255) NULL,
	[AppealGoal] [money] NULL,
	[AppealNumberSolicited] [int] NULL,
	[AppealCategorySystemID] [int] NULL,
	[AppealCategoryDescription] [varchar](100) NULL,
	[IsInactive] [varchar](3) NULL,
	[AppealStartDate] [datetime] NULL,
	[AppealEndDate] [datetime] NULL,
	[AppealDateChanged] [datetime] NULL,
	[CampaignDimID] [int] NULL,
	[CampaignSystemID] [int] NULL,
	[CampaignIdentifier] [varchar](20) NULL,
	[CampaignDescription] [varchar](100) NULL,
	[FirstGiftDate] [datetime] NULL,
	[LastGiftDate] [datetime] NULL,
	[StartDateDimID] [int] NULL,
	[EndDateDimID] [int] NULL,
	[FirstGiftDateDimID] [int] NULL,
	[LastGiftDateDimID] [int] NULL,
	[IsDM] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_Appeal_AppealDimID] PRIMARY KEY CLUSTERED 
(
	[AppealDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AppealAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AppealAttribute](
	[AppealAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[AppealAttributeSystemID] [int] NULL,
	[AppealDimID] [int] NULL,
	[AppealSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_AppealAttribute_AppealAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[AppealAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AppealExpenseType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AppealExpenseType](
	[AppealExpenseTypeDimID] [int] NOT NULL,
	[AppealExpenseTypeSystemID] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_AppealExpenseType_AppealExpenseTypeDimID] PRIMARY KEY CLUSTERED 
(
	[AppealExpenseTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_AppealResponse]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_AppealResponse](
	[AppealResponseDimID] [int] NOT NULL,
	[AppealResponseSystemID] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_AppealResponse_AppealResponseDimID] PRIMARY KEY CLUSTERED 
(
	[AppealResponseDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Campaign]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Campaign](
	[CampaignDimID] [int] NOT NULL,
	[CampaignSystemID] [int] NULL,
	[CampaignIdentifier] [varchar](20) NULL,
	[CampaignDescription] [varchar](100) NULL,
	[CampaignCategorySystemID] [int] NULL,
	[CampaignCategoryDescription] [varchar](100) NULL,
	[IsInactive] [varchar](3) NULL,
	[CampaignGoal] [money] NULL,
	[CampaignStartDate] [datetime] NULL,
	[CampaignEndDate] [datetime] NULL,
	[FirstGiftDate] [datetime] NULL,
	[LastGiftDate] [datetime] NULL,
	[StartDateDimID] [int] NULL,
	[EndDateDimID] [int] NULL,
	[FirstGiftDateDimID] [int] NULL,
	[LastGiftDateDimID] [int] NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_Campaign_CampaignDimID] PRIMARY KEY CLUSTERED 
(
	[CampaignDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_CampaignAppeal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DIM_CampaignAppeal](
	[CampaignAppealDimID] [int] NOT NULL,
	[CampaignAppealSystemID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[CampaignSequence] [int] NULL,
	[AppealSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_CampaignAppeal] PRIMARY KEY CLUSTERED 
(
	[CampaignAppealDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO


GO

/****** Object:  Table [dbo].[DIM_CampaignAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_CampaignAttribute](
	[CampaignAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[CampaignAttributeSystemID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[CampaignSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_CampaignAttribute_CampaignAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[CampaignAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_CampaignGivingLevel]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_CampaignGivingLevel](
	[CampaignGivingLevelDimID] [int] NOT NULL,
	[CampaignGivingLevelSystemID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[Sequence] [int] NULL,
	[Gifts] [int] NULL,
	[EachGift] [numeric](20, 4) NULL,
	[EachGiftPercent] [numeric](20, 4) NULL,
	[TotalGifts] [numeric](20, 4) NULL,
	[TotalGiftsPercent] [numeric](20, 4) NULL,
	[Prospects] [int] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChangedDimID] [int] NULL,
	[CampaignGivingLevelImportID] [varchar](20) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_CampaignGivingLevel] PRIMARY KEY CLUSTERED 
(
	[CampaignGivingLevelDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ChargeAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ChargeAttribute](
	[ChargeAttributeDimID] [int] NOT NULL,
	[ChargeAttributeSystemID] [int] NOT NULL,
	[ChargeSystemID] [int] NULL,
	[ChargeFactID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_ChargeAttribute] PRIMARY KEY CLUSTERED 
(
	[ChargeAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Class]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Class](
	[ClassDimID] [int] NOT NULL,
	[ClassSystemID] [int] NULL,
	[ClassCode] [int] NULL,
	[ClassName] [varchar](60) NULL,
	[ClassType] [varchar](60) NULL,
	[Sequence] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_Class] PRIMARY KEY CLUSTERED 
(
	[ClassDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Client]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Client](
	[ClientDimID] [int] NOT NULL,
	[ClientSystemID] [int] NULL,
	[UserDefinedID] [varchar](20) NULL,
	[ClientRecordType] [smallint] NULL,
	[TaxIDNumber] [varchar](64) NULL,
	[TaxExemptNumber] [varchar](64) NULL,
	[FEImportID] [varchar](50) NULL,
	[ClientName] [varchar](255) NULL,
	[ClientNameID] [int] NULL,
	[ClientDisplayName] [varchar](255) NULL,
	[ClientSearchName] [varchar](255) NULL,
	[AccountStatus] [varchar](8) NULL,
	[HasCreditLimit] [varchar](1) NULL,
	[CreditLimitAmount] [numeric](19, 4) NULL,
	[CurrentBalance] [numeric](19, 4) NULL,
	[CurrentAvailable] [numeric](19, 4) NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[TotalPurgedInvoicesAmount] [numeric](19, 4) NULL,
	[TotalPurgedChargeAmount] [numeric](19, 4) NULL,
	[TotalPurgedPaymentAmount] [numeric](19, 4) NULL,
	[TotalPurgedRefundAmount] [numeric](19, 4) NULL,
	[TotalPurgedReturnAmount] [numeric](19, 4) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Client] PRIMARY KEY CLUSTERED 
(
	[ClientDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Constituent]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Constituent](
	[ConstituentDimID] [int] NOT NULL,
	[ConstituentSystemID] [int] NULL,
	[UniqueConstituentID] [int] NULL,
	[ConstituentID] [varchar](20) NULL,
	[ImportID] [varchar](50) NULL,
	[KeyIndicator] [varchar](2) NULL,
	[FullName] [varchar](110) NULL,
	[Title] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Suffix] [varchar](100) NULL,
	[Birthday] [datetime] NULL,
	[BirthdayFuzzy] [varchar](8) NULL,
	[Addressee] [varchar](500) NULL,
	[Salutation] [varchar](500) NULL,
	[Gender] [varchar](7) NULL,
	[MaritalStatus] [varchar](100) NULL,
	[IsAConstituent] [varchar](3) NULL,
	[IsDeceased] [varchar](3) NULL,
	[IsAnonymous] [varchar](3) NULL,
	[IsInactive] [varchar](3) NULL,
	[IsASolicitor] [varchar](3) NULL,
	[IsSolicitorInactive] [varchar](3) NULL,
	[IsHonorMemorial] [varchar](3) NULL,
	[IsProspect] [varchar](3) NULL,
	[HasProspectStatus] [varchar](3) NULL,
	[DeceasedDate] [datetime] NULL,
	[DeceasedDateFuzzy] [varchar](8) NULL,
	[Income] [varchar](100) NULL,
	[Industry] [varchar](100) NULL,
	[NoValidAddress] [varchar](3) NULL,
	[NoEMail] [varchar](3) NULL,
	[Address] [varchar](150) NULL,
	[Address1] [varchar](150) NULL,
	[Address2] [varchar](150) NULL,
	[Address3] [varchar](150) NULL,
	[Address4] [varchar](150) NULL,
	[Address5] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [char](6) NULL,
	[County] [varchar](100) NULL,
	[PostCode] [varchar](12) NULL,
	[Country] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Target] [varchar](100) NULL,
	[EmailAddress] [varchar](100) NULL,
	[HomePhone] [varchar](100) NULL,
	[BusinessPhone] [varchar](100) NULL,
	[SendMail] [char](3) NULL,
	[PaysTax] [varchar](7) NULL,
	[Ethnicity] [varchar](100) NULL,
	[Religion] [varchar](100) NULL,
	[SocialSecurityNumber] [varchar](64) NULL,
	[DateAdded] [datetime] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChanged] [datetime] NULL,
	[EmployerConstituentSystemID] [int] NULL,
	[SpouseConstituentSystemID] [int] NULL,
	[PrimaryContactConstituentSystemID] [int] NULL,
	[ProspectStatus] [varchar](100) NULL,
	[ProspectClassification] [varchar](100) NULL,
	[IsHeadOfHousehold] [varchar](3) NULL,
	[IsHeadOfHouseholdBit] [bit] NULL,
	[HouseholdID] [varchar](20) NULL,
	[HouseholdSystemID] [int] NULL,
	[Age]  AS ([dbo].[BBBI_ConstituentAge]([IsDeceased],[Birthday],[DeceasedDate])),
	[IsRE] [bit] NULL,
	[IsFE] [bit] NULL,
	[IsGE] [bit] NULL,
	[IsVendor] [bit] NULL,
	[IsRecipient] [bit] NULL,
	[IsPayee] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Constituent_ConstituentDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Constituent_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Constituent_Stage](
	[ConstituentSystemID] [int] NULL,
	[UniqueConstituentID] [int] NULL,
	[ConstituentID] [varchar](20) NULL,
	[ImportID] [varchar](50) NULL,
	[KeyIndicator] [varchar](2) NULL,
	[FullName] [varchar](110) NULL,
	[Title] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Suffix] [varchar](100) NULL,
	[Birthday] [datetime] NULL,
	[BirthdayFuzzy] [varchar](8) NULL,
	[Addressee] [varchar](500) NULL,
	[Salutation] [varchar](500) NULL,
	[Gender] [varchar](7) NULL,
	[MaritalStatus] [varchar](100) NULL,
	[IsAConstituent] [varchar](3) NULL,
	[IsDeceased] [varchar](3) NULL,
	[IsAnonymous] [varchar](3) NULL,
	[IsInactive] [varchar](3) NULL,
	[IsASolicitor] [varchar](3) NULL,
	[IsSolicitorInactive] [varchar](3) NULL,
	[IsHonorMemorial] [varchar](3) NULL,
	[IsProspect] [varchar](3) NULL,
	[HasProspectStatus] [varchar](3) NULL,
	[DeceasedDate] [datetime] NULL,
	[DeceasedDateFuzzy] [varchar](8) NULL,
	[Income] [varchar](100) NULL,
	[Industry] [varchar](100) NULL,
	[NoValidAddress] [varchar](3) NULL,
	[NoEMail] [varchar](3) NULL,
	[Address] [varchar](150) NULL,
	[Address1] [varchar](150) NULL,
	[Address2] [varchar](150) NULL,
	[Address3] [varchar](150) NULL,
	[Address4] [varchar](150) NULL,
	[Address5] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [char](6) NULL,
	[County] [varchar](100) NULL,
	[PostCode] [varchar](12) NULL,
	[Country] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Target] [varchar](100) NULL,
	[EmailAddress] [varchar](100) NULL,
	[HomePhone] [varchar](100) NULL,
	[BusinessPhone] [varchar](100) NULL,
	[SendMail] [char](3) NULL,
	[PaysTax] [varchar](7) NULL,
	[Ethnicity] [varchar](100) NULL,
	[Religion] [varchar](100) NULL,
	[SocialSecurityNumber] [varchar](64) NULL,
	[DateAdded] [datetime] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChanged] [datetime] NULL,
	[EmployerConstituentSystemID] [int] NULL,
	[SpouseConstituentSystemID] [int] NULL,
	[PrimaryContactConstituentSystemID] [int] NULL,
	[ProspectStatus] [varchar](100) NULL,
	[ProspectClassification] [varchar](100) NULL,
	[IsHeadOfHousehold] [varchar](3) NULL,
	[IsHeadOfHouseholdBit] [bit] NULL,
	[HouseholdID] [varchar](20) NULL,
	[HouseholdSystemID] [int] NULL,
	[IsRE] [bit] NULL,
	[IsFE] [bit] NULL,
	[IsGE] [bit] NULL,
	[IsVendor] [bit] NULL,
	[IsRecipient] [bit] NULL,
	[IsPayee] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentAddress]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentAddress](
	[ConstituentAddressDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentAddressSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[Preferred] [varchar](3) NULL,
	[Seasonal] [varchar](3) NULL,
	[SendMail] [varchar](3) NULL,
	[SeasonalDateFrom] [datetime] NULL,
	[SeasonalDateTo] [datetime] NULL,
	[AddressType] [varchar](100) NULL,
	[Address] [varchar](150) NULL,
	[Address1] [varchar](150) NULL,
	[Address2] [varchar](150) NULL,
	[Address3] [varchar](150) NULL,
	[Address4] [varchar](150) NULL,
	[Address5] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[PostCode] [varchar](12) NULL,
	[County] [varchar](100) NULL,
	[Country] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [nchar](10) NULL,
 CONSTRAINT [PK_DIM_ConstituentAddress] PRIMARY KEY CLUSTERED 
(
	[ConstituentAddressDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentAddress_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentAddress_Stage](
	[ConstituentAddressSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[Preferred] [varchar](3) NULL,
	[Seasonal] [varchar](3) NULL,
	[SendMail] [varchar](3) NULL,
	[SeasonalDateFrom] [datetime] NULL,
	[SeasonalDateTo] [datetime] NULL,
	[AddressType] [varchar](100) NULL,
	[Address] [varchar](150) NULL,
	[Address1] [varchar](150) NULL,
	[Address2] [varchar](150) NULL,
	[Address3] [varchar](150) NULL,
	[Address4] [varchar](150) NULL,
	[Address5] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[PostCode] [varchar](12) NULL,
	[County] [varchar](100) NULL,
	[Country] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [nchar](10) NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentAttribute](
	[ConstituentAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentAttributeSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentAttribute_ConstituentAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentAttribute_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentAttribute_Stage](
	[ConstituentAttributeSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentCode](
	[ConstituentCodeDimID] [int] NOT NULL,
	[ConstituentCodeSystemID] [int] NULL,
	[Description] [varchar](100) NULL,
	[ShortDescription] [varchar](6) NULL,
	[ConstituentCodeGroup] [varchar](100) NULL,
	[IsActive] [varchar](3) NULL,
	[CodeSequence] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentCode_ConstituentCodeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentConstitCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentConstitCode](
	[ConstituentConstitCodeDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentConstitCodeSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentCodeDimID] [int] NULL,
	[ConstituentCode] [varchar](100) NULL,
	[ConstituentCodeShort] [varchar](6) NULL,
	[ConstituentCodeGroup] [varchar](100) NULL,
	[IsPrimary] [bit] NULL,
	[FuzzyDateFrom] [varchar](8) NULL,
	[FuzzyDateTo] [varchar](8) NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateFromDimID] [int] NULL,
	[DateToDimID] [int] NULL,
	[DateFromIsBlank] [bit] NULL,
	[DateToIsBlank] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[Sequence] [smallint] NULL,
	[NativeSequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentConstitCode_ConstituentConstitCodeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentConstitCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentConstitCode_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentConstitCode_Stage](
	[ConstituentConstitCodeSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentCodeDimID] [int] NULL,
	[ConstituentCode] [varchar](100) NULL,
	[ConstituentCodeShort] [varchar](6) NULL,
	[ConstituentCodeGroup] [varchar](100) NULL,
	[IsPrimary] [bit] NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateFromDimID] [int] NULL,
	[DateToDimID] [int] NULL,
	[DateFromIsBlank] [bit] NULL,
	[DateToIsBlank] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[Sequence] [smallint] NULL,
	[NativeSequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[FuzzyDateFrom] [varchar](8) NULL,
	[FuzzyDateTo] [varchar](8) NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentEducation]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentEducation](
	[ConstituentEducationDimID] [int] NOT NULL,
	[ConstituentEducationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[IsPrimary] [varchar](3) NULL,
	[ClassYear] [smallint] NULL,
	[Campus] [varchar](50) NULL,
	[Degree] [varchar](100) NULL,
	[FuzzyDateEntered] [varchar](8) NULL,
	[DateEntered] [datetime] NULL,
	[DateEnteredDimID] [int] NULL,
	[FuzzyDateGraduated] [varchar](8) NULL,
	[DateGraduated] [datetime] NULL,
	[DateGraduatedDimID] [int] NULL,
	[FuzzyDateLeft] [varchar](8) NULL,
	[DateLeft] [datetime] NULL,
	[DateLeftDimID] [int] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[GPA] [decimal](30, 6) NULL,
	[KnownName] [varchar](50) NULL,
	[RegistrationNumber] [varchar](50) NULL,
	[ClassOfDegree] [varchar](100) NULL,
	[SchoolName] [varchar](100) NULL,
	[SchoolType] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[SubjectOfStudy] [varchar](100) NULL,
	[Department] [varchar](100) NULL,
	[Faculty] [varchar](100) NULL,
	[Fraternity] [varchar](50) NULL,
	[Sequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentEducation_ConstituentEducationDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentEducationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentEducationAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentEducationAttribute](
	[ConstituentEducationAttributeDimID] [int] NOT NULL,
	[ConstituentEducationAttributeSystemID] [int] NULL,
	[ConstituentEducationDimID] [int] NULL,
	[ConstituentEducationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ConstituentSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentAttribute_ConstituentEducationAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentEducationAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentEducationMajor]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentEducationMajor](
	[ConstituentEducationMajorDimID] [int] NOT NULL,
	[ConstituentEducationMajorSystemID] [int] NULL,
	[ConstituentEducationDimID] [int] NULL,
	[ConstituentEducationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[Major] [varchar](255) NULL,
	[MajorCode] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentEducationMajor_ConstituentEducationMajorDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentEducationMajorDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentEducationMinor]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentEducationMinor](
	[ConstituentEducationMinorDimID] [int] NOT NULL,
	[ConstituentEducationMinorSystemID] [int] NULL,
	[ConstituentEducationDimID] [int] NULL,
	[ConstituentEducationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[Minor] [varchar](255) NULL,
	[MinorCode] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentEducationMinor_ConstituentEducationMinorDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentEducationMinorDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentFundRelationship]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentFundRelationship](
	[ConstituentFundRelationshipDimID] [int] NOT NULL,
	[ConstituentFundRelationshipSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[FuzzyDateFrom] [varchar](8) NULL,
	[FuzzyDateTo] [varchar](8) NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateFromDimID] [int] NULL,
	[DateToDimID] [int] NULL,
	[DateFromIsBlank] [bit] NULL,
	[DateToIsBlank] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[KeyIndicator] [varchar](2) NULL,
	[ConstituentDimID] [int] NULL,
	[FundSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[Notes] [text] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[FundSequence] [smallint] NULL,
	[Reciprocal] [varchar](100) NULL,
	[Relationship] [varchar](100) NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentFundRelationship_ConstituentFundRelationshipDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentFundRelationshipDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP] TEXTIMAGE_ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentMedia]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentMedia](
	[ConstituentMediaDimID] [int] NOT NULL,
	[ConstituentMediaSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[MediaType] [varchar](100) NULL,
	[MediaDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[MediaDate] [datetime] NULL,
	[MediaDateDimID] [int] NULL,
	[Author] [varchar](50) NULL,
	[MediaTitle] [varchar](50) NULL,
	[ProposalDimID] [int] NULL,
	[ProposalSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentMedia] PRIMARY KEY CLUSTERED 
(
	[ConstituentMediaDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentNotepad]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentNotepad](
	[ConstituentNotepadDimID] [int] NOT NULL,
	[ConstituentNotepadSystemID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NOT NULL,
	[NoteType] [varchar](100) NOT NULL,
	[NotepadDateDimID] [int] NULL,
	[NotepadDate] [datetime] NULL,
	[NotepadTitle] [varchar](50) NULL,
	[NotepadDescription] [varchar](255) NULL,
	[Author] [varchar](50) NULL,
	[ActualNotes] [text] NULL,
	[Sequence] [smallint] NULL,
	[NotepadImportID] [varchar](20) NULL,
	[DateAddedDimID] [int] NOT NULL,
	[DateChangedDimID] [int] NOT NULL,
	[ETLControlID] [int] NOT NULL,
	[SourceID] [int] NOT NULL,
	[AllFirstSequence] [int] NULL,
	[AllLastSequence] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentNotepad] PRIMARY KEY CLUSTERED 
(
	[ConstituentNotepadDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP] TEXTIMAGE_ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentPhone]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentPhone](
	[ConstituentPhoneDimID] [int] NOT NULL,
	[ConstituentPhoneSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[PhoneType] [varchar](100) NULL,
	[PhoneNumber] [varchar](100) NULL,
	[PreferredAddress] [varchar](3) NULL,
	[DoNotCall] [varchar](3) NULL,
	[DoNotShare] [varchar](3) NULL,
	[TextMsg] [varchar](3) NULL,
	[PhoneDateChanged] [datetime] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentPhone_ConstituentPhoneDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentPhoneDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentPhone_Stage]    Script Date: 12/08/2011 14:09:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentPhone_Stage](
	[ConstituentPhoneSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[PhoneType] [varchar](100) NULL,
	[PhoneNumber] [varchar](100) NULL,
	[PreferredAddress] [varchar](3) NULL,
	[DoNotCall] [varchar](3) NULL,
	[DoNotShare] [varchar](3) NULL,
	[TextMsg] [varchar](3) NULL,
	[PhoneDateChanged] [datetime] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DIM_ConstituentProspect]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentProspect](
	[ConstituentProspectDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentProspectSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ClassificationSystemID] [int] NULL,
	[Classification] [varchar](100) NULL,
	[StatusSystemID] [int] NULL,
	[Status] [varchar](100) NULL,
	[TotalFinancialAmount] [numeric](38, 6) NULL,
	[Sequence] [int] NULL,
	[DateChanged] [datetime] NULL,
	[DateChangedDimID] [int] NULL,
	[ClassificationSequence] [int] NULL,
	[StatusSequence] [int] NULL,
	[LatencyDays] [int] NULL,
	[IsCurrent] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[LatencyRange]  AS ([dbo].[BBBI_ReturnRange]('Prospect Latency',[LatencyDays])),
	[LatencyRangeSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Prospect Latency',[LatencyDays])),
 CONSTRAINT [PK_DIM_ConstituentProspect_ConstituentProspectDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentProspectDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentRating]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentRating](
	[ConstituentRatingDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentRatingSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[Source] [varchar](100) NULL,
	[Category] [varchar](100) NULL,
	[RatingDescription] [varchar](255) NULL,
	[RatingDate] [datetime] NULL,
	[RatingDateDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentRating_ConstituentRatingDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentRatingDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentRelationship]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentRelationship](
	[ConstituentRelationshipDimID] [int] NOT NULL,
	[ConstituentRelationshipSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[RelatedConstituentSystemID] [int] NULL,
	[RelatedConstituentDimID] [int] NULL,
	[RelationshipSystemID] [int] NULL,
	[Relationship] [varchar](100) NULL,
	[RelationshipType] [varchar](100) NULL,
	[ReciprocalSystemID] [int] NULL,
	[Reciprocal] [varchar](100) NULL,
	[ReciprocalType] [varchar](100) NULL,
	[KeyIndicator] [varchar](2) NULL,
	[IsHeadOfHousehold] [char](3) NULL,
	[IsEmployee] [char](3) NULL,
	[IsContact] [char](3) NULL,
	[IsPrimary] [char](3) NULL,
	[IsSpouse] [char](3) NULL,
	[Income] [varchar](100) NULL,
	[Industry] [varchar](100) NULL,
	[Position] [varchar](100) NULL,
	[Profession] [varchar](100) NULL,
	[FuzzyDateFrom] [varchar](8) NULL,
	[FuzzyDateTo] [varchar](8) NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateFromDimID] [int] NULL,
	[DateToDimID] [int] NULL,
	[DateFromIsBlank] [bit] NULL,
	[DateToIsBlank] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[ContactType] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_ConstituentRelationship_ConstituentRelationshipFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentRelationshipDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentRelationshipAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentRelationshipAttribute](
	[ConstituentRelationshipAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentRelationshipAttributeSystemID] [int] NULL,
	[ConstituentRelationshipDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[ConstituentSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentAttribute_ConstituentRelationshipAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentRelationshipAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentSalutation]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentSalutation](
	[ConstituentSalutationDimID] [int] NOT NULL,
	[ConstituentSalutationSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[SalutationType] [varchar](100) NULL,
	[Salutation] [varchar](2000) NULL,
	[IsEditable] [varchar](3) NULL,
	[IsPrimaryAddressee] [varchar](3) NULL,
	[IsPrimarySalutation] [varchar](3) NULL,
	[RESequence] [int] NULL,
	[Sequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentSalutation_ConstituentSalutationDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentSalutationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentSolicitCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentSolicitCode](
	[ConstituentSolicitCodeDimID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentSolicitCodeSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[SolicitCode] [varchar](100) NULL,
	[Sequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentSolicitCode_ConstituentSolicitCodeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentSolicitCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentSolicitCode_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentSolicitCode_Stage](
	[ConstituentSolicitCodeSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[SolicitCode] [varchar](100) NULL,
	[Sequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentTaxDeclaration]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentTaxDeclaration](
	[ConstituentTaxDeclarationDimID] [int] NOT NULL,
	[ConstituentTaxDeclarationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[DeclarationMadeDate] [datetime] NULL,
	[DeclarationStartDate] [datetime] NULL,
	[DeclarationEndDate] [datetime] NULL,
	[ConfirmationSentDate] [datetime] NULL,
	[ConfirmationReturnedDate] [datetime] NULL,
	[DeclarationMadeDateDimID] [int] NULL,
	[DeclarationStartDateDimID] [int] NULL,
	[DeclarationEndDateDimID] [int] NULL,
	[ConfirmationSentDateDimID] [int] NULL,
	[ConfirmationReturnedDateDimID] [int] NULL,
	[DeclarationIndicator] [varchar](100) NULL,
	[DeclarationSource] [varchar](100) NULL,
	[Sequence] [int] NULL,
	[IsPrimary] [bit] NULL,
	[PaysTax] [varchar](7) NULL,
	[TaxDeclarationStatus] [varchar](100) NULL,
	[ScannedDocumentsExist] [bit] NULL,
	[TaxNotes] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentTaxDeclaration] PRIMARY KEY CLUSTERED 
(
	[ConstituentTaxDeclarationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentTaxDeclaration_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentTaxDeclaration_Stage](
	[ConstituentTaxDeclarationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[DeclarationMadeDate] [datetime] NULL,
	[DeclarationStartDate] [datetime] NULL,
	[DeclarationEndDate] [datetime] NULL,
	[ConfirmationSentDate] [datetime] NULL,
	[ConfirmationReturnedDate] [datetime] NULL,
	[DeclarationMadeDateDimID] [int] NULL,
	[DeclarationStartDateDimID] [int] NULL,
	[DeclarationEndDateDimID] [int] NULL,
	[ConfirmationSentDateDimID] [int] NULL,
	[ConfirmationReturnedDateDimID] [int] NULL,
	[DeclarationIndicator] [varchar](100) NULL,
	[DeclarationSource] [varchar](100) NULL,
	[Sequence] [int] NULL,
	[IsPrimary] [bit] NULL,
	[PaysTax] [varchar](7) NULL,
	[TaxDeclarationStatus] [varchar](100) NULL,
	[ScannedDocumentsExist] [bit] NULL,
	[TaxNotes] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentUser]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentUser](
	[ConstituentUserDimID] [int] NOT NULL,
	[ConstituentDimID] [int] NOT NULL,
	[ClientUsersID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[EMail] [varchar](100) NULL,
	[ForwardingAddress] [varchar](100) NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentUser] PRIMARY KEY CLUSTERED 
(
	[ConstituentUserDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ConstituentVolunteerType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ConstituentVolunteerType](
	[ConstituentVolunteerTypeDimID] [int] NOT NULL,
	[ConstituentVolunteerTypeSystemID] [int] NULL,
	[VolunteerDimID] [int] NULL,
	[VolunteerSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[VolunteerTypeDimID] [int] NULL,
	[VolunteerType] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[DateStarted] [datetime] NULL,
	[DateFinished] [datetime] NULL,
	[DateStartedDimID] [int] NULL,
	[DateFinishedDimID] [int] NULL,
	[ReasonFinished] [varchar](50) NULL,
	[Sequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ConstituentVolunteerType_ConstituentVolunteerTypeDimID] PRIMARY KEY CLUSTERED 
(
	[ConstituentVolunteerTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Contact]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Contact](
	[ContactDimID] [int] NOT NULL,
	[ContactSystemID] [int] NULL,
	[KeyIndicator] [varchar](1) NULL,
	[ContactType] [varchar](10) NULL,
	[FullName] [varchar](255) NULL,
	[KeyName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[MaidenName] [varchar](50) NULL,
	[NickName] [varchar](50) NULL,
	[Gender] [varchar](7) NULL,
	[Deceased] [varchar](3) NULL,
	[DeceasedDate] [datetime] NULL,
	[AddedDate] [datetime] NULL,
	[AddedDateDimID] [int] NULL,
	[ChangedDate] [datetime] NULL,
	[ChangedDateDimID] [int] NULL,
	[Birthdate] [datetime] NULL,
	[BirthCity] [varchar](50) NULL,
	[BirthState] [varchar](3) NULL,
	[ClassOf] [smallint] NULL,
	[SpouseContactSystemID] [int] NULL,
	[BusinessContactSystemID] [int] NULL,
	[Title1] [varchar](60) NULL,
	[Title2] [varchar](60) NULL,
	[Suffix1] [varchar](60) NULL,
	[Suffix2] [varchar](60) NULL,
	[BirthCountry] [varchar](60) NULL,
	[Ethnicity] [varchar](60) NULL,
	[Religion] [varchar](60) NULL,
	[ChurchAffiliation] [varchar](60) NULL,
	[GradeLevel] [varchar](60) NULL,
	[Industry] [varchar](60) NULL,
	[IsVendorContact] [bit] NULL,
	[IsGrantContact] [bit] NULL,
	[IsProjectContact] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Date]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Date](
	[DateDimID] [int] NOT NULL,
	[CalendarYear] [smallint] NULL,
	[CalendarHalf] [smallint] NULL,
	[CalendarHalfName] [char](2) NULL,
	[CalendarYearHalf] [int] NULL,
	[CalendarHalfYearName] [varchar](8) NULL,
	[CalendarFullHalfYearName] [varchar](15) NULL,
	[CalendarQuarter] [smallint] NULL,
	[CalendarQuarterName] [char](2) NULL,
	[CalendarYearQuarter] [int] NULL,
	[CalendarQuarterYearName] [varchar](8) NULL,
	[CalendarFullQuarterYearName] [varchar](15) NULL,
	[CalendarMonth] [smallint] NULL,
	[CalendarMonthName] [varchar](12) NULL,
	[CalendarWeek] [smallint] NULL,
	[CalendarYearWeek] [int] NULL,
	[CalendarWeekYearName] [varchar](15) NULL,
	[CalendarFullWeekYearName] [varchar](20) NULL,
	[CalendarDayofYear] [smallint] NULL,
	[CalendarYearMonth] [int] NULL,
	[CalendarMonthYearName] [varchar](15) NULL,
	[CalendarFullMonthYearName] [varchar](20) NULL,
	[FiscalYear] [smallint] NULL,
	[FiscalHalf] [smallint] NULL,
	[FiscalYearHalf] [int] NULL,
	[FiscalHalfName] [char](2) NULL,
	[FiscalHalfYearName] [varchar](8) NULL,
	[FiscalFullHalfYearName] [varchar](15) NULL,
	[FiscalQuarter] [smallint] NULL,
	[FiscalQuarterName] [char](2) NULL,
	[FiscalYearQuarter] [int] NULL,
	[FiscalQuarterYearName] [varchar](8) NULL,
	[FiscalFullQuarterYearName] [varchar](15) NULL,
	[FiscalPeriod] [smallint] NULL,
	[FiscalPeriodYearName] [varchar](15) NULL,
	[FiscalYearPeriod] [int] NULL,
	[FiscalMonth] [smallint] NULL,
	[FiscalMonthName] [varchar](12) NULL,
	[FiscalYearMonth] [int] NULL,
	[FiscalMonthYearName] [varchar](15) NULL,
	[FiscalFullMonthYearName] [varchar](20) NULL,
	[FiscalWeek] [smallint] NULL,
	[FiscalYearWeek] [int] NULL,
	[FiscalWeekYearName] [varchar](15) NULL,
	[FiscalFullWeekYearName] [varchar](20) NULL,
	[FiscalDayofYear] [smallint] NULL,
	[AcademicYear] [smallint] NULL,
	[AcademicSemesterName] [varchar](20) NULL,
	[AcademicSemester] [smallint] NULL,
	[AcademicMonth] [smallint] NULL,
	[AcademicYearSemester] [int] NULL,
	[AcademicSemesterYearName] [varchar](20) NULL,
	[AcademicYearMonth] [int] NULL,
	[AcademicMonthYearName] [varchar](20) NULL,
	[SeasonName] [varchar](20) NULL,
	[DayofMonth] [smallint] NULL,
	[DayofWeek] [smallint] NULL,
	[DayName] [varchar](12) NULL,
	[IsWeekend] [bit] NULL,
	[IsWeekDay] [bit] NULL,
	[IsHoliday] [bit] NULL,
	[IsLeapYear] [bit] NULL,
	[IsCurrentDate] [bit] NULL,
	[IsCurrentFiscalWeek] [bit] NULL,
	[IsCurrentCalendarWeek] [bit] NULL,
	[IsCurrentMonth] [bit] NULL,
	[IsCurrentPeriod] [bit] NULL,
	[IsCurrentFiscalQuarter] [bit] NULL,
	[IsCurrentCalendarQuarter] [bit] NULL,
	[IsCurrentFiscalYear] [bit] NULL,
	[IsCurrentCalendarYear] [bit] NULL,
	[InFYStartToCurrent] [bit] NULL,
	[InCYStartToCurrent] [bit] NULL,
	[IsClosed] [bit] NULL,
	[IsLastClosedDate] [bit] NULL,
	[Filter] [bit] NULL,
	[HolidayName] [varchar](100) NULL,
	[ActualDate] [datetime] NULL,
	[ActualDateString] [varchar](20) NULL,
	[ActualDateStringDMY] [varchar](25) NULL,
	[ActualDateStringMDY] [varchar](25) NULL,
	[FiscalYearStartDate] [datetime] NULL,
	[FiscalYearEndDate] [datetime] NULL,
	[FiscalQuarterStartDate] [datetime] NULL,
	[FiscalQuarterEndDate] [datetime] NULL,
	[FiscalWeekStartDate] [datetime] NULL,
	[FiscalWeekEndDate] [datetime] NULL,
	[CalendarQuarterStartDate] [datetime] NULL,
	[CalendarQuarterEndDate] [datetime] NULL,
	[CalendarWeekStartDate] [datetime] NULL,
	[CalendarWeekEndDate] [datetime] NULL,
	[PeriodStartDate] [datetime] NULL,
	[PeriodEndDate] [datetime] NULL,
	[SemesterStartDate] [datetime] NULL,
	[SemesterEndDate] [datetime] NULL,
	[AcademicYearStartDate] [datetime] NULL,
	[AcademicYearEndDate] [datetime] NULL,
	[SeasonStartDate] [datetime] NULL,
	[SeasonEndDate] [datetime] NULL,
	[MonSequence] [smallint] NULL,
	[TueSequence] [smallint] NULL,
	[WedSequence] [smallint] NULL,
	[ThuSequence] [smallint] NULL,
	[FriSequence] [smallint] NULL,
	[SatSequence] [smallint] NULL,
	[SunSequence] [smallint] NULL,
	[FE_FiscalYear] [varchar](12) NULL,
	[FE_FiscalYearDescription] [varchar](50) NULL,
	[FE_FiscalYearSequence] [int] NULL,
	[FE_FiscalPeriodStartDate] [datetime] NULL,
	[FE_FiscalPeriodEndDate] [datetime] NULL,
	[FE_FiscalPeriodIsClosed] [bit] NULL,
	[FE_FiscalYearIsSummarized] [bit] NULL,
	[FE_FiscalYearNoPeriods] [smallint] NULL,
	[FE_FiscalPeriodSequence] [smallint] NULL,
	[FE_CloseLastRunDate] [datetime] NULL,
	[FE_SummarizedLastRunDate] [datetime] NULL,
	[FE_PurgeLastRunDate] [datetime] NULL,
	[DaysSince] [int] NULL,
	[WeeksSince] [int] NULL,
	[MonthsSince] [int] NULL,
	[YearsSince] [int] NULL,
	[YearsMonthSince] [varchar](30) NULL,
	[RecencyGroupDays]  AS ([dbo].[BBBI_ReturnRange]('Recency Days',[DaysSince])),
	[RecencyGroupDaysSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency Days',[DaysSince])),
	[RecencyGroupWeeks]  AS ([dbo].[BBBI_ReturnRange]('Recency Weeks',[WeeksSince])),
	[RecencyGroupWeeksSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency Weeks',[WeeksSince])),
	[RecencyGroupMonths]  AS ([dbo].[BBBI_ReturnRange]('Recency',[MonthsSince])),
	[RecencyGroupMonthsSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',[MonthsSince])),
	[RecencyGroupYears]  AS ([dbo].[BBBI_ReturnRange]('Recency Years',[YearsSince])),
	[RecencyGroupYearsSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency Years',[YearsSince])),
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_DIM_Date_DateDimID] PRIMARY KEY CLUSTERED 
(
	[DateDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_EMailGroup]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_EMailGroup](
	[EMailGroupDimID] [int] NOT NULL,
	[EMailKeyName] [varchar](100) NULL,
	[EMailKeySubject] [varchar](100) NULL,
	[EMailGroupName] [varchar](100) NULL,
 CONSTRAINT [PK_DIM_EMailGroup] PRIMARY KEY CLUSTERED 
(
	[EMailGroupDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_EMailJob]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_EMailJob](
	[EMailJobDimID] [int] NOT NULL,
	[JobName] [varchar](100) NULL,
	[JobGroup] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[JobCreateDate] [datetime] NULL,
	[JobCreateDateDimID] [int] NULL,
	[SendAfterDate] [datetime] NULL,
	[SendAfterDateDimID] [int] NULL,
	[JobFromAddress] [varchar](100) NULL,
	[JobFromDisplayName] [varchar](100) NULL,
	[JobSubject] [varchar](100) NULL,
	[JobPriority] [int] NULL,
	[JobStatus] [tinyint] NULL,
	[NumberRequested] [int] NULL,
	[NumberAbsentAddress] [int] NULL,
	[NumberOptedOut] [int] NULL,
	[ReturnReceipt] [bit] NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_DIM_EMailJob] PRIMARY KEY CLUSTERED 
(
	[EMailJobDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Event]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Event](
	[EventDimID] [int] NOT NULL,
	[EventSystemID] [int] NULL,
	[EventIdentifier] [varchar](50) NULL,
	[EventName] [varchar](255) NULL,
	[EventStartDate] [datetime] NULL,
	[EventStartDateDimID] [int] NULL,
	[EventEndDate] [datetime] NULL,
	[EventEndDateDimID] [int] NULL,
	[EventCategorySystemID] [int] NULL,
	[EventCategory] [varchar](255) NULL,
	[EventGroupSystemID] [int] NULL,
	[EventGroup] [varchar](255) NULL,
	[EventTypeSystemID] [int] NULL,
	[EventType] [varchar](255) NULL,
	[EventGoal] [money] NULL,
	[NumberInvited] [int] NULL,
	[LocationName] [varchar](255) NULL,
	[LocationCountrySystemID] [int] NULL,
	[LocationCountry] [varchar](100) NULL,
	[LocationCountySystemID] [int] NULL,
	[LocationCounty] [varchar](100) NULL,
	[LocationNZCitySystemID] [int] NULL,
	[LocationNZCity] [varchar](100) NULL,
	[LocationNZCitySuburbSystemID] [int] NULL,
	[LocationNZCitySuburb] [varchar](100) NULL,
	[LocationState] [varchar](100) NULL,
	[LocationCity] [varchar](100) NULL,
	[LocationPostCode] [varchar](25) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Event_EventDimID] PRIMARY KEY CLUSTERED 
(
	[EventDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_EventAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_EventAttribute](
	[EventAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[EventAttributeSystemID] [int] NULL,
	[EventDimID] [int] NULL,
	[EventSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ConstituentSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_EventAttribute_EventAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[EventAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_FEConstituent]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_FEConstituent](
	[FEConstituentDimID] [int] NOT NULL,
	[FEGroupConstituentDimID] [int] NULL,
	[FEConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ContactDimID] [int] NULL,
	[VendorDimID] [int] NULL,
	[AddressSystemID] [int] NULL,
	[ImportID] [varchar](50) NULL,
	[KeyIndicator] [varchar](2) NULL,
	[FullName] [varchar](110) NULL,
	[Title] [varchar](100) NULL,
	[Title2] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Suffix] [varchar](100) NULL,
	[Suffix2] [varchar](100) NULL,
	[ContactType] [varchar](20) NULL,
	[Birthday] [datetime] NULL,
	[BirthState] [varchar](3) NULL,
	[BirthCity] [varchar](50) NULL,
	[Gender] [varchar](7) NULL,
	[MaritalStatus] [varchar](100) NULL,
	[IsAContact] [varchar](3) NULL,
	[IsDeceased] [varchar](3) NULL,
	[IsVendor] [bit] NULL,
	[IsVendorContact] [bit] NULL,
	[IsClient] [bit] NULL,
	[IsClientContact] [bit] NULL,
	[IsGrantContact] [bit] NULL,
	[IsProjectContact] [bit] NULL,
	[IsDuplicate] [bit] NULL,
	[DeceasedDate] [datetime] NULL,
	[Income] [varchar](100) NULL,
	[Industry] [varchar](100) NULL,
	[NoValidAddress] [varchar](3) NULL,
	[NoEMail] [varchar](3) NULL,
	[AddressFull] [varchar](500) NULL,
	[Address] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [char](6) NULL,
	[County] [varchar](100) NULL,
	[PostCode] [varchar](12) NULL,
	[Country] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Suburb] [varchar](100) NULL,
	[EmailAddress] [varchar](100) NULL,
	[HomePhone] [varchar](100) NULL,
	[BusinessPhone] [varchar](100) NULL,
	[Ethnicity] [varchar](100) NULL,
	[Religion] [varchar](100) NULL,
	[ChurchAffiliation] [varchar](100) NULL,
	[ClassOf] [smallint] NULL,
	[GradeLevel] [varchar](100) NULL,
	[DateAdded] [datetime] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChanged] [datetime] NULL,
	[DateChangedDimID] [int] NULL,
	[IsHeadOfHousehold] [varchar](3) NULL,
	[IsHeadOfHouseholdBit] [bit] NULL,
	[HouseholdID] [varchar](20) NULL,
	[HouseholdSystemID] [int] NULL,
	[Age]  AS ([dbo].[BBBI_ConstituentAge]([IsDeceased],[Birthday],[DeceasedDate])),
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_FEConstituent] PRIMARY KEY CLUSTERED 
(
	[FEConstituentDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_FinancialInformation]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_FinancialInformation](
	[FinancialInformationDimID] [int] NOT NULL,
	[FinancialInformationSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ProspectSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[FinancialAmount] [numeric](30, 6) NULL,
	[FinancialNotes] [varchar](1000) NULL,
	[DateAcquiredDimID] [int] NULL,
	[DateAssessedDimID] [int] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChangedDimID] [int] NULL,
	[FinancialInfoImportID] [varchar](20) NULL,
	[Sequence] [smallint] NULL,
	[WealthFinderHash] [varchar](255) NULL,
	[WealthFinderPartialHash] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_FinancialInformation] PRIMARY KEY CLUSTERED 
(
	[FinancialInformationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Fund]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Fund](
	[FundDimID] [int] NOT NULL,
	[FundSystemID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[GEFundDimID] [int] NULL,
	[FundIdentifier] [varchar](20) NULL,
	[ImportID] [varchar](20) NULL,
	[FundCategorySystemID] [int] NULL,
	[FundCategoryDescription] [varchar](100) NULL,
	[FundDescription] [varchar](100) NULL,
	[FundTypeDescription] [varchar](100) NULL,
	[ProjectTypeDescription] [varchar](100) NULL,
	[IsInactive] [varchar](3) NULL,
	[IsRestricted] [varchar](3) NULL,
	[CampaignDimID] [int] NULL,
	[FundGoal] [money] NULL,
	[FundStartDate] [datetime] NULL,
	[FundEndDate] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[FirstGiftDate] [datetime] NULL,
	[LastGiftDate] [datetime] NULL,
	[StartDateDimID] [int] NULL,
	[EndDateDimID] [int] NULL,
	[FirstGiftDateDimID] [int] NULL,
	[LastGiftDateDimID] [int] NULL,
	[IsRE] [bit] NULL,
	[IsFE] [bit] NULL,
	[IsGE] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Fund_FundDimID] PRIMARY KEY CLUSTERED 
(
	[FundDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_FundAppeal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DIM_FundAppeal](
	[FundAppealDimID] [int] NOT NULL,
	[FundAppealSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[FundSequence] [int] NULL,
	[AppealSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_FundAppeal] PRIMARY KEY CLUSTERED 
(
	[FundAppealDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO


GO

/****** Object:  Table [dbo].[DIM_FundAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_FundAttribute](
	[FundAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[FundAttributeSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[FundSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[IsRE] [bit] NULL,
	[IsFE] [bit] NULL,
	[IsGE] [bit] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_FundAttribute_FundAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[FundAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEAccountCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEAccountCode](
	[GEAccountCodeDimID] [int] NOT NULL,
	[AccountCodeID] [int] NULL,
	[AccountCode] [varchar](255) NULL,
	[AccountCodeDescription] [varchar](255) NULL,
	[AccountType] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEAccountCode] PRIMARY KEY CLUSTERED 
(
	[GEAccountCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEAllocation]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DIM_GEAllocation](
	[GEAllocationDimID] [int] NOT NULL,
	[GEProgramAreaDimID] [int] NULL,
	[GEPortfolioDimID] [int] NULL,
	[GEProposalDimID] [int] NULL,
	[GEFundDimID] [int] NULL,
	[Amount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEAllocation] PRIMARY KEY CLUSTERED 
(
	[GEAllocationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO


GO

/****** Object:  Table [dbo].[DIM_GEAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEAttribute](
	[GEAttributeDimID] [int] NOT NULL,
	[AttributeCategorySystemID] [int] NULL,
	[AttributeSystemID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeDescription] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[REAttribute] [varchar](50) NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeDescriptionLevel] [int] NULL,
	[ParentGEAttributeDimID] [int] NULL,
 CONSTRAINT [PK_DIM_GEAttribute] PRIMARY KEY CLUSTERED 
(
	[GEAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GECheckRun]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GECheckRun](
	[GECheckRunDimID] [int] NOT NULL,
	[CheckRunSystemID] [int] NULL,
	[GrantSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[PaymentSystemID] [int] NOT NULL,
	[GrantPaymentFactID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Description] [varchar](255) NULL,
	[Status] [varchar](50) NULL,
	[Comments] [varchar](255) NULL,
	[DateSent] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IssueNumber] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GECheckRun] PRIMARY KEY CLUSTERED 
(
	[GECheckRunDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEConstituent]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEConstituent](
	[GEConstituentDimID] [int] NOT NULL,
	[GEConstituentSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[FEVendorSystemID] [int] NULL,
	[FEVendorDimID] [int] NULL,
	[FullName] [varchar](255) NULL,
	[RecSalutation] [varchar](255) NULL,
	[RecContact] [varchar](255) NULL,
	[RecPosition] [varchar](255) NULL,
	[RecAddrType] [varchar](255) NULL,
	[RecAddressBlock] [varchar](255) NULL,
	[RecCity] [varchar](255) NULL,
	[RecState] [varchar](255) NULL,
	[RecZip] [varchar](255) NULL,
	[RecPhone] [varchar](255) NULL,
	[RecEmail] [varchar](255) NULL,
	[RecURL] [varchar](255) NULL,
	[PayContact] [varchar](255) NULL,
	[PayAddressBlock] [varchar](255) NULL,
	[PaySalutation] [varchar](255) NULL,
	[PayAddrType] [varchar](255) NULL,
	[PayCity] [varchar](255) NULL,
	[PayState] [varchar](255) NULL,
	[PayZip] [varchar](255) NULL,
	[OrganizationID] [varchar](20) NULL,
	[TaxID] [varchar](30) NULL,
	[IRSClassCode] [char](3) NULL,
	[PayCheckName] [varchar](255) NULL,
	[VendorName] [varchar](255) NULL,
	[Notes] [text] NULL,
	[IsOrphan] [bit] NULL,
	[IsRecipient] [bit] NULL,
	[IsPayee] [bit] NULL,
	[IsStudent] [bit] NULL,
	[IsInstitution] [bit] NULL,
	[IsCommittee] [bit] NULL,
	[IsAdvisor] [bit] NULL,
	[Is1099] [bit] NULL,
	[RequiresFiscalSponsor] [bit] NULL,
	[SSN] [varchar](20) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [nchar](10) NULL,
 CONSTRAINT [PK_DIM_GEConstituent] PRIMARY KEY CLUSTERED 
(
	[GEConstituentDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP] TEXTIMAGE_ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEConstituentPayee]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEConstituentPayee](
	[GEConstituentPayeeDimID] [int] NOT NULL,
	[GEConstituentPayeeSystemID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[FullName] [varchar](255) NULL,
	[AddressBlock] [varchar](255) NULL,
	[AddrType] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [varchar](255) NULL,
	[Zip] [varchar](255) NULL,
	[Contact] [varchar](255) NULL,
	[Notes] [nvarchar](255) NULL,
	[Salutation] [varchar](255) NULL,
	[TaxID] [varchar](255) NULL,
	[CheckName] [varchar](255) NULL,
	[VendorName] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEConstituentPayee] PRIMARY KEY CLUSTERED 
(
	[GEConstituentPayeeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEConstituentRecipient]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEConstituentRecipient](
	[GEConstituentRecipientDimID] [int] NOT NULL,
	[GEConstituentRecipientSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[FullName] [varchar](255) NULL,
	[AddressBlock] [varchar](255) NULL,
	[AddrType] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [varchar](255) NULL,
	[Zip] [varchar](255) NULL,
	[Notes] [nvarchar](255) NULL,
	[Contact] [varchar](255) NULL,
	[Salutation] [varchar](255) NULL,
	[Position] [varchar](255) NULL,
	[URL] [varchar](255) NULL,
	[TaxID] [varchar](255) NULL,
	[SSN] [varchar](20) NULL,
	[Phone] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEConstituentRecipient] PRIMARY KEY CLUSTERED 
(
	[GEConstituentRecipientDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEFund]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEFund](
	[GEFundDimID] [int] NOT NULL,
	[GEFundSystemID] [int] NULL,
	[FEProjectDimID] [int] NULL,
	[FEProjectSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[REFundSystemID] [int] NULL,
	[FEProjectID] [varchar](255) NULL,
	[FundNumber] [varchar](50) NULL,
	[FundName] [varchar](255) NULL,
	[FundType] [varchar](50) NULL,
	[SignaturesRequired] [int] NULL,
	[Notes] [nvarchar](1000) NULL,
	[FundCategory] [varchar](50) NULL,
	[FEGrantsPayableAccountNumber] [varchar](50) NULL,
	[FEAccountFund] [varchar](255) NULL,
	[DefaultDRAccountCode] [varchar](255) NULL,
	[DefaultCRAccountCode] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEFund] PRIMARY KEY CLUSTERED 
(
	[GEFundDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEFundAdvisor]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEFundAdvisor](
	[GEFundAdvisorDimID] [int] NOT NULL,
	[FundAdvisorSystemID] [int] NULL,
	[GEFundSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[GEConstituentSystemID] [int] NULL,
	[AdvisorType] [varchar](50) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEFundAdvisor] PRIMARY KEY CLUSTERED 
(
	[GEFundAdvisorDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEFundAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEFundAttribute](
	[GEFundAttributeDimID] [int] NOT NULL,
	[GEFundAttributeSystemID] [int] NULL,
	[GEFundDimID] [int] NULL,
	[GEFundSystemID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Comments] [varchar](255) NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeLevel] [int] NULL,
	[Required] [varchar](3) NULL,
	[IsActive] [varchar](3) NULL,
	[ParentGEFundAttributeDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEFundAttribute] PRIMARY KEY CLUSTERED 
(
	[GEFundAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEGrant]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEGrant](
	[GEGrantDimID] [int] NOT NULL,
	[GEGrantSystemID] [int] NULL,
	[ParentGEGrantSystemID] [int] NULL,
	[ParentGEGrantDimID] [int] NULL,
	[GEGrantFundDimID] [int] NULL,
	[GEFundSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[GrantNumber] [varchar](255) NULL,
	[CheckInfoSystemID] [int] NULL,
	[RecipientSystemID] [int] NULL,
	[RecipientConstituentDimID] [int] NULL,
	[PayeeSystemID] [int] NULL,
	[PayeeConstituentDimID] [int] NULL,
	[RequestorSystemID] [int] NULL,
	[RequestorConstituentDimID] [int] NULL,
	[Requestor2SystemID] [int] NULL,
	[Requestor2ConstituentDimID] [int] NULL,
	[TargetGrantFundDImID] [int] NULL,
	[TargetFundSystemID] [int] NULL,
	[EthnicityCategory] [varchar](255) NULL,
	[DemographicCategory] [varchar](255) NULL,
	[ContactMethod] [char](1) NULL,
	[IRS990Code] [char](3) NULL,
	[IRS990Description] [varchar](255) NULL,
	[GrantType] [varchar](4) NULL,
	[GrantTypeDescription] [varchar](255) NULL,
	[ReceivedDate] [datetime] NULL,
	[ReceivedDateDimID] [int] NULL,
	[EnteredDate] [datetime] NULL,
	[EnteredDateDimID] [int] NULL,
	[GrantStatus] [varchar](50) NULL,
	[GrantDescription] [varchar](255) NULL,
	[IsAnonymous] [bit] NULL,
	[IsReceiptLetter] [bit] NULL,
	[HasRestrictions] [bit] NULL,
	[PaymentAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEGrant] PRIMARY KEY CLUSTERED 
(
	[GEGrantDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEGrantAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEGrantAttribute](
	[GEGrantAttributeDimID] [int] NOT NULL,
	[GEGrantAttributeSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeDescription] [varchar](255) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeLevel] [int] NULL,
	[Comments] [varchar](255) NULL,
	[Required] [varchar](3) NULL,
	[IsActive] [varchar](3) NULL,
	[ParentGEGrantAttributeDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEGrantAttribute] PRIMARY KEY CLUSTERED 
(
	[GEGrantAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEGrantCategory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEGrantCategory](
	[GrantCategoryDimID] [int] NOT NULL,
	[GEGrantDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[NTEECodeID] [int] NULL,
	[NTEECategory] [varchar](255) NULL,
	[ActiveFlag] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEGrantCategory] PRIMARY KEY CLUSTERED 
(
	[GrantCategoryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DIM_GEGrantFund]    Script Date: 05/17/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEGrantFund](
	[GEGrantFundDimID] [int] NOT NULL,
	[GEGrantSystemID] [int] NULL,
	[ParentGrantDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[CheckInfoID] [int] NULL,
	[RequestorConstituentDimID] [int] NULL,
	[Requestor2ConstituentDimID] [int] NULL,
	[FundSystemID] [int] NULL,
	[TargetFundSystemID] [int] NULL,
	[EthnicityCategoryID] [int] NULL,
	[DemographicCategoryID] [int] NULL,
	[FEProjectSystemID] [int] NULL,
	[GEFundSystemID] [int] NULL,
	[FEProjectID] [varchar](255) NULL,
	[ContactMethod] [char](1) NULL,
	[IRS990Code] [char](3) NULL,
	[GrantType] [varchar](4) NULL,
	[ReceivedDate] [datetime] NULL,
	[ReceivedDateDimID] [int] NULL,
	[EnteredDate] [datetime] NULL,
	[EnteredDateDimID] [nchar](10) NULL,
	[GrantStatus] [varchar](50) NULL,
	[GrantDescription] [varchar](255) NULL,
	[GrantTypeDescription] [varchar](255) NULL,
	[FundCategory] [varchar](50) NULL,
	[FundNumber] [varchar](50) NULL,
	[FundName] [varchar](255) NULL,
	[FundType] [varchar](50) NULL,
	[IRS990Description] [varchar](255) NULL,
	[IsAnonymous] [bit] NULL,
	[IsReceiptLetter] [bit] NULL,
	[HasRestrictions] [bit] NULL,
	[PaymentAmount] [money] NULL,
 CONSTRAINT [PK_DIM_GEGrantFund] PRIMARY KEY CLUSTERED 
(
	[GEGrantFundDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DIM_GEGrantNote]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DIM_GEGrantNote](
	[GEGrantNoteDimID] [int] NOT NULL,
	[GEGrantDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[Designation] [nvarchar](1000) NULL,
	[SpecialInstructions] [nvarchar](1000) NULL,
	[NatureOfGrant] [nvarchar](1000) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEGrantNote] PRIMARY KEY CLUSTERED 
(
	[GEGrantNoteDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO


GO

/****** Object:  Table [dbo].[DIM_GEGrantProfile]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEGrantProfile](
	[IsActive] [varchar](3) NULL,
	[ProfileType] [varchar](30) NULL,
	[GrantSystemID] [int] NULL,
	[ProfileDetailID] [int] NULL,
	[ProfileDetailName] [varchar](255) NULL,
	[ProfileDetailValue] [varchar](255) NULL,
	[ProfileDetailTextValue] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[GrantProfileDimID] [int] NULL,
	[GEGrantDimID] [int] NULL
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEGrantRegion]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEGrantRegion](
	[GEGrantRegionDimID] [int] NOT NULL,
	[GEGrantDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[RegionSystemID] [int] NULL,
	[ParentID] [int] NULL,
	[Region] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEGrantRegion] PRIMARY KEY CLUSTERED 
(
	[GEGrantRegionDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEPortfolio]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEPortfolio](
	[GEPortfolioDimID] [int] NOT NULL,
	[GEPortfolioSystemID] [int] NULL,
	[GEProgramAreaSystemID] [int] NULL,
	[GEProgramAreaDimID] [int] NULL,
	[PortfolioName] [varchar](255) NULL,
	[PortfolioDescription] [varchar](255) NULL,
	[StartDate] [datetime] NULL,
	[StartDateDimID] [int] NULL,
	[EndDate] [datetime] NULL,
	[EndDateDimID] [int] NULL,
	[OwnerUserSystemID] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[TargetBudget] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEPortfolio] PRIMARY KEY CLUSTERED 
(
	[GEPortfolioDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEPortfolioAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEPortfolioAttribute](
	[GEPortfolioAttributeDimID] [int] NOT NULL,
	[GEPortfolioAttributeSystemID] [int] NULL,
	[GEPortfolioDimID] [int] NULL,
	[GEPortfolioSystemID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Comments] [varchar](255) NULL,
	[Required] [varchar](3) NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeLevel] [int] NULL,
	[GEProgramAreaSystemID] [int] NULL,
	[GEProgramAreaDimID] [nchar](10) NULL,
	[IsActive] [varchar](3) NULL,
	[ParentGEPortfolioAttributeDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEPortfolioAttribute] PRIMARY KEY CLUSTERED 
(
	[GEPortfolioAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEProgramArea]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEProgramArea](
	[GEProgramAreaDimID] [int] NOT NULL,
	[ProgramAreaSystemID] [int] NULL,
	[OwnerUserSystemID] [int] NULL,
	[ProgramAreaName] [varchar](255) NULL,
	[ProgramAreaDescription] [varchar](255) NULL,
	[TotalBudget] [money] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsActive] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEProgramArea] PRIMARY KEY CLUSTERED 
(
	[GEProgramAreaDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEProgramAreaAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEProgramAreaAttribute](
	[GEProgramAreaAttributeDimID] [int] NOT NULL,
	[GEProgramAreaAttributeSystemID] [int] NULL,
	[GEProgramAreaSystemID] [int] NULL,
	[GEProgramAreaDimID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeLevel] [int] NULL,
	[Comments] [varchar](255) NULL,
	[Required] [varchar](3) NULL,
	[IsActive] [varchar](3) NULL,
	[ParentGEProgramAreaAttributeDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEProgramAreaAttribute] PRIMARY KEY CLUSTERED 
(
	[GEProgramAreaAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEProposal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEProposal](
	[GEProposalDimID] [int] NOT NULL,
	[GEProposalSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[GEPortfolioDimID] [int] NULL,
	[GEPortfolioSystemID] [int] NULL,
	[RequestAmount] [money] NULL,
	[ReceiveDate] [datetime] NULL,
	[ReceiveDateDimID] [int] NULL,
	[RecipientSystemID] [int] NULL,
	[RecipientConstituentDimID] [int] NULL,
	[PayeeSystemID] [int] NULL,
	[PayeeConstituentDimID] [int] NULL,
	[ProposalStatus] [varchar](50) NULL,
	[ProposalDescription] [varchar](255) NULL,
	[ProposalType] [varchar](255) NULL,
	[GrantTypeCode] [varchar](50) NULL,
	[IsActive] [varchar](3) NULL,
	[TotalProjectAmount] [money] NULL,
	[ApprovedAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEProposal] PRIMARY KEY CLUSTERED 
(
	[GEProposalDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEProposalAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEProposalAttribute](
	[GEProposalAttributeDimID] [int] NOT NULL,
	[GEProposalAttributeSystemID] [int] NULL,
	[GEProposalDimID] [int] NULL,
	[GEProposalSystemID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Comments] [varchar](255) NULL,
	[Required] [varchar](3) NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeLevel] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[ParentGEProposalAttributeDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEProposalAttribute] PRIMARY KEY CLUSTERED 
(
	[GEProposalAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEProposalOtherFundingSource]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEProposalOtherFundingSource](
	[GEProposalOtherFundingSourceDimID] [int] NOT NULL,
	[GEProposalOtherFundingSourceSystemID] [int] NULL,
	[GEProposalDimID] [int] NULL,
	[OrganizationName] [varchar](255) NULL,
	[ContactPerson] [varchar](255) NULL,
	[ContactNumber] [varchar](255) NULL,
	[AmountFunded] [money] NULL,
	[GEProposalSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEProposalOtherFundingSource] PRIMARY KEY CLUSTERED 
(
	[GEProposalOtherFundingSourceDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GEProposalTableValue]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GEProposalTableValue](
	[GEProposalTableValueDimID] [int] NOT NULL,
	[GEProposalTableSystemID] [int] NULL,
	[GEProposalTableMemberSystemID] [int] NULL,
	[GEProposalDimID] [int] NULL,
	[GEProposalSystemID] [int] NULL,
	[GEProposalTableName] [varchar](255) NULL,
	[GEProposalTableMemberName] [varchar](255) NULL,
	[GEProposalTableMemberValue] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GEProposalTableValue] PRIMARY KEY CLUSTERED 
(
	[GEProposalTableValueDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GERecipientAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GERecipientAttribute](
	[GERecipientAttributeDimID] [int] NOT NULL,
	[GERecipientAttributeSystemID] [int] NULL,
	[GEConstituentRecipientDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[GERecipientSystemID] [int] NULL,
	[AttributeCategory] [varchar](255) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Comments] [varchar](255) NULL,
	[AttributeCategoryLevel] [int] NULL,
	[AttributeLevel] [int] NULL,
	[Required] [varchar](3) NULL,
	[IsActive] [varchar](3) NULL,
	[ParentGERecipientAttributeID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GERecipientAttribute] PRIMARY KEY CLUSTERED 
(
	[GERecipientAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GiftCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GiftCode](
	[GiftCodeDimID] [int] NOT NULL,
	[GiftCodeSystemID] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[GiftCodeGroup] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GiftCode_GiftCodeDimID] PRIMARY KEY CLUSTERED 
(
	[GiftCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GiftStatus]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GiftStatus](
	[GiftStatusDimID] [int] NOT NULL,
	[GiftStatusSystemID] [int] NULL,
	[GiftStatus] [varchar](50) NULL,
 CONSTRAINT [PK_DIM_GiftStatus_GiftStatusDimID] PRIMARY KEY CLUSTERED 
(
	[GiftStatusDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GiftSubType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GiftSubType](
	[GiftSubTypeDimID] [int] NOT NULL,
	[GiftSubTypeSystemID] [int] NULL,
	[GiftSubType] [varchar](100) NULL,
	[GiftSubTypeGroup] [varchar](50) NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GiftSubType_GiftSubTypeDimID] PRIMARY KEY CLUSTERED 
(
	[GiftSubTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GiftType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GiftType](
	[GiftTypeDimID] [int] NOT NULL,
	[GiftTypeSystemID] [int] NULL,
	[GiftType] [varchar](100) NULL,
	[GiftTypeUK] [varchar](100) NULL,
	[GiftTypeGroup] [varchar](50) NULL,
	[IsCash] [varchar](3) NULL,
	[IsCommitment] [varchar](3) NULL,
	[IsPay] [varchar](3) NULL,
	[IsCashAndPay] [varchar](3) NULL,
	[IsAccounting] [varchar](3) NULL,
	[IsStockProperty] [varchar](3) NULL,
	[IsRecurring] [varchar](3) NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GiftType_GiftTypeDimID] PRIMARY KEY CLUSTERED 
(
	[GiftTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GivingHierarchy]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GivingHierarchy](
	[GivingHierarchyDimID] [int] NOT NULL,
	[GivingHierarchySystemID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[FundSystemID] [int] NULL,
	[Sequence] [smallint] NULL,
	[GivingHierarchyParentSystemID] [int] NULL,
	[GivingHierarchyParentDimID] [int] NULL,
	[IsRoot] [varchar](3) NULL,
	[CampaignSequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_GivingHierarchy] PRIMARY KEY CLUSTERED 
(
	[GivingHierarchyDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GLSourceType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[DIM_GLSourceType](
	[SourceTypeDimID] [int] IDENTITY(1,1) NOT NULL,
	[SourceTypeSystemID] [int] NOT NULL,
	[SourceType] [varchar](50) NOT NULL,
	[SourceTypeGroup] [varchar](25) NULL,
 CONSTRAINT [PK_DIM_GLSourceType] PRIMARY KEY CLUSTERED 
(
	[SourceTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Grant]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Grant](
	[GrantDimID] [int] NOT NULL,
	[GrantSystemID] [int] NULL,
	[GrantID] [varchar](12) NULL,
	[GrantDescription] [varchar](60) NULL,
	[GrantStatusSystemID] [int] NULL,
	[GrantStatusDescription] [varchar](60) NULL,
	[GrantTypeSystemID] [int] NULL,
	[GrantTypeDescription] [varchar](60) NULL,
	[GrantTCodeID] [int] NULL,
	[GrantTCodeDescription] [varchar](60) NULL,
	[GrantStartDate] [datetime] NULL,
	[GrantEndDate] [datetime] NULL,
	[GrantStartDateDimID] [int] NULL,
	[GrantEndDateDimID] [int] NULL,
	[GrantAmount] [numeric](19, 4) NULL,
	[GrantAR7ClientsID] [int] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[AnnotationText] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Grant] PRIMARY KEY CLUSTERED 
(
	[GrantDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_GrantAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_GrantAttribute](
	[GrantAttributeDimID] [int] NOT NULL,
	[GrantAttributeSystemID] [int] NOT NULL,
	[GrantDimID] [int] NULL,
	[GrantSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_GrantAttribute] PRIMARY KEY CLUSTERED 
(
	[GrantAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_InstallmentFrequency]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_InstallmentFrequency](
	[InstallmentFrequencyDimID] [int] NOT NULL,
	[InstallmentFrequencySystemID] [int] NULL,
	[InstallmentFrequency] [varchar](50) NULL,
 CONSTRAINT [PK_DIM_InstallmentFrequency_InstallmentFrequencyDimID] PRIMARY KEY CLUSTERED 
(
	[InstallmentFrequencyDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_InvoiceAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_InvoiceAttribute](
	[InvoiceAttributeDimID] [int] NOT NULL,
	[InvoiceAttributeSystemID] [int] NOT NULL,
	[InvoiceSystemID] [int] NULL,
	[InvoiceFactID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_InvoiceAttribute] PRIMARY KEY CLUSTERED 
(
	[InvoiceAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_JointMembership]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_JointMembership](
	[JointMembershipDimID] [int] NOT NULL,
	[MembershipDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[JointMembershipSystemID] [int] NULL,
	[MembershipSystemID] [int] NOT NULL,
	[MembershipCardSystemID] [int] NULL,
	[ConstituentSystemID] [int] NOT NULL,
	[IsPrimaryMember] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK__DIM_JointMembership_JointMembershipDimID] PRIMARY KEY CLUSTERED 
(
	[JointMembershipDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Journal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Journal](
	[JournalDimID] [int] NOT NULL,
	[JournalSystemID] [int] NULL,
	[JournalLabel] [varchar](255) NULL,
	[JournalName] [varchar](60) NULL,
	[JournalShortName] [varchar](10) NULL,
	[TableSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Journal] PRIMARY KEY CLUSTERED 
(
	[JournalDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_LetterCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_LetterCode](
	[LetterCodeDimID] [int] NOT NULL,
	[LetterCodeSystemID] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_LetterCode_LetterCodeDimID] PRIMARY KEY CLUSTERED 
(
	[LetterCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Location]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Location](
	[LocationDimID] [int] NOT NULL,
	[Country] [varchar](100) NULL,
	[County] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[PostCode] [varchar](20) NULL,
	[Region] [varchar](100) NULL,
	[LocationChecksum] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Location_LocationDimID] PRIMARY KEY CLUSTERED 
(
	[LocationDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Membership]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DIM_Membership](
	[MembershipDimID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[MembershipSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[TotalMembers] [int] NULL,
	[TotalChildren] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK__DIM_Membership__MembershipDimID] PRIMARY KEY CLUSTERED 
(
	[MembershipDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO


GO

/****** Object:  Table [dbo].[DIM_MembershipAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_MembershipAttribute](
	[MembershipAttributeDimID] [int] NOT NULL,
	[MembershipAttributeSystemID] [int] NULL,
	[MembershipDimID] [int] NOT NULL,
	[MembershipSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[SourceID] [int] NOT NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_MembershipAttribute_MembershipAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[MembershipAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_MembershipCategory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_MembershipCategory](
	[MembershipCategoryDimID] [int] NOT NULL,
	[MembershipCategorySystemID] [int] NULL,
	[Category] [varchar](100) NULL,
	[Program] [varchar](100) NULL,
	[LifetimeMembership] [varchar](3) NULL,
	[MaximumMembers] [smallint] NULL,
	[DuesLevel] [varchar](50) NULL,
	[MaximumDues] [money] NULL,
	[MinimumDues] [money] NULL,
	[Inactive] [varchar](3) NULL,
	[DefaultRenewCardStatus] [varchar](15) NULL,
	[PrintRenewals] [varchar](3) NULL,
	[DefaultExpires] [varchar](50) NULL,
	[DefaultExpiresDate] [datetime] NULL,
	[NewMemExpiresValue] [smallint] NULL,
	[NewMemExpiresInterval] [varchar](10) NULL,
	[RenewMemExpiresValue] [smallint] NULL,
	[RenewMemExpiresInterval] [varchar](10) NULL,
	[RejoinedMemExpiresValue] [smallint] NULL,
	[RejoinedMemExpiresInterval] [varchar](10) NULL,
	[DefaultReceiptType] [varchar](50) NULL,
	[DefaultReceiptAmt] [money] NULL,
	[DropAfterValue] [smallint] NULL,
	[DropAfterInterval] [varchar](10) NULL,
	[SendBenefitsTo] [varchar](15) NULL,
	[GiftDefaultsID] [int] NULL,
	[DefaultNumCardsToPrint] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK__DIM_MembershipCategory_MembershipCategoryDimID] PRIMARY KEY CLUSTERED 
(
	[MembershipCategoryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_MembershipCategoryBenefit]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[DIM_MembershipCategoryBenefit](
	[MembershipCategoryBenefitDimID] [int] NOT NULL,
	[MembershipCategoryDimID] [int] NULL,
	[MembershipCategoryBenefitSystemID] [int] NOT NULL,
	[MembershipCategorySystemID] [int] NULL,
	[Benefit] [varchar](100) NULL,
	[Count] [smallint] NULL,
	[UnitCost] [money] NOT NULL,
	[TotalBenefitValue] [money] NOT NULL,
	[SentDate] [datetime] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_MembershipCategoryBenefit_MembershipCategoryBenefitDimID] PRIMARY KEY CLUSTERED 
(
	[MembershipCategoryBenefitDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_MembershipRenewal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_MembershipRenewal](
	[MembershipRenewalDimID] [int] NOT NULL,
	[MembershipRenewalSystemID] [int] NOT NULL,
	[LastMembershipRenewalDimID] [int] NULL,
	[LastMembershipRenewalSystemID] [int] NULL,
	[MembershipDimID] [int] NOT NULL,
	[MembershipSystemID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[LastRenewalDate] [datetime] NOT NULL,
	[NameOnRenewal] [varchar](100) NULL,
	[SentTo] [varchar](25) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_MembershipRenewal] PRIMARY KEY CLUSTERED 
(
	[MembershipRenewalDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_MembershipSubcategory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_MembershipSubcategory](
	[MembershipSubcategoryDimID] [int] NOT NULL,
	[MembershipCategoryDimID] [int] NOT NULL,
	[MembershipSubcategorySystemID] [int] NOT NULL,
	[MembershipCategorySystemID] [int] NOT NULL,
	[Subcategory] [varchar](100) NOT NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_MembershipSubcategory_MembershipSubcategoryDimID] PRIMARY KEY CLUSTERED 
(
	[MembershipSubcategoryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Package]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Package](
	[PackageDimID] [int] NOT NULL,
	[PackageSystemID] [int] NULL,
	[PackageIdentifier] [varchar](20) NULL,
	[PackageDescription] [varchar](50) NULL,
	[PackageCategorySystemID] [int] NULL,
	[PackageCategoryDescription] [varchar](100) NULL,
	[IsInactive] [varchar](3) NULL,
	[PackageGoal] [money] NULL,
	[PackageNumberSolicited] [int] NULL,
	[PackageStartDate] [datetime] NULL,
	[PackageEndDate] [datetime] NULL,
	[PackageDateChanged] [datetime] NULL,
	[AppealSystemID] [int] NULL,
	[AppealDimID] [int] NULL,
	[CAPCode] [varchar](60) NULL,
	[FirstGiftDate] [datetime] NULL,
	[LastGiftDate] [datetime] NULL,
	[StartDateDimID] [int] NULL,
	[EndDateDimID] [int] NULL,
	[FirstGiftDateDimID] [int] NULL,
	[LastGiftDateDimID] [int] NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_DIM_Package_PackageDimID] PRIMARY KEY CLUSTERED 
(
	[PackageDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_PackageAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_PackageAttribute](
	[PackageAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[PackageAttributeSystemID] [int] NULL,
	[PackageDimID] [int] NULL,
	[PackageSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_PackageAttribute_PackageAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[PackageAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_PaymentType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_PaymentType](
	[PaymentTypeDimID] [int] NOT NULL,
	[PaymentTypeSystemID] [int] NULL,
	[PaymentType] [varchar](50) NULL,
	[PaymentTypeUK] [varchar](50) NULL,
 CONSTRAINT [PK_DIM_PaymentType_PaymentTypeDimID] PRIMARY KEY CLUSTERED 
(
	[PaymentTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_PhilanthropicInterest]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_PhilanthropicInterest](
	[PhilanthropicInterestDimID] [int] NOT NULL,
	[PhilanthropicInterestSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[HasInterest] [varchar](3) NULL,
	[Interest] [varchar](100) NULL,
	[Sequence] [smallint] NULL,
	[ProspectSystemID] [int] NULL,
	[PhilanthropicComments] [varchar](1000) NULL,
	[PhilanthropicInterestImportID] [varchar](20) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_PhilanthropicInterest] PRIMARY KEY CLUSTERED 
(
	[PhilanthropicInterestDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_PostStatus]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_PostStatus](
	[PostStatusDimID] [smallint] NOT NULL,
	[PostStatusSystemID] [int] NOT NULL,
	[PostStatusFESystemID] [int] NOT NULL,
	[PostStatus] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DIM_PostStatus_PostStatusDimID] PRIMARY KEY CLUSTERED 
(
	[PostStatusDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Project]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Project](
	[ProjectDimID] [int] NOT NULL,
	[FundDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[ProjectID] [varchar](12) NULL,
	[ProjectDescription] [varchar](60) NULL,
	[ProjectStatusSystemID] [int] NULL,
	[ProjectStatusDescription] [varchar](60) NULL,
	[ProjectTypeSystemID] [int] NULL,
	[ProjectTypeDescription] [varchar](60) NULL,
	[ProjectStartDate] [datetime] NULL,
	[ProjectEndDate] [datetime] NULL,
	[ProjectStartDateDimID] [int] NULL,
	[ProjectEndDateDimID] [int] NULL,
	[ProjectActive] [varchar](50) NULL,
	[PreventPostDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[AnnotationText] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[ParentProjectSystemID] [int] NULL,
	[ParentProjectDimID] [int] NULL,
 CONSTRAINT [PK_DIM_Projects] PRIMARY KEY CLUSTERED 
(
	[ProjectDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ProjectAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ProjectAttribute](
	[ProjectAttributeDimID] [int] NOT NULL,
	[ProjectAttributeSystemID] [int] NOT NULL,
	[ProjectDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_ProjectAttribute] PRIMARY KEY CLUSTERED 
(
	[ProjectAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Proposal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Proposal](
	[ProposalDimID] [int] NOT NULL,
	[ProposalSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentProspectSystemID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[CampaignSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[FundSystemID] [int] NULL,
	[OriginalAmountAsked] [money] NULL,
	[AmountAsked] [money] NULL,
	[AmountExpected] [money] NULL,
	[AmountFunded] [money] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChangedDimID] [int] NULL,
	[OriginalDateAsked] [datetime] NULL,
	[DateAsked] [datetime] NULL,
	[DateExpected] [datetime] NULL,
	[DateFunded] [datetime] NULL,
	[DateRated] [datetime] NULL,
	[DateDeadline] [datetime] NULL,
	[DateConfirmation] [datetime] NULL,
	[OriginalDateAskedDimID] [int] NULL,
	[DateAskedDimID] [int] NULL,
	[DateExpectedDimID] [int] NULL,
	[DateFundedDimID] [int] NULL,
	[DateRatedDimID] [int] NULL,
	[DateDeadlineDimID] [int] NULL,
	[DateConfirmationDimID] [int] NULL,
	[PrimaryAssignedSolicitorSystemID] [int] NULL,
	[PrimaryAssignedSolicitorDimID] [int] NULL,
	[ContactSystemID] [int] NULL,
	[ContactConstituentDimID] [int] NULL,
	[InstrumentType] [varchar](100) NULL,
	[Purpose] [varchar](100) NULL,
	[Instrument] [varchar](100) NULL,
	[Rating] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[ProposalName] [varchar](255) NULL,
	[IsInactive] [varchar](3) NULL,
	[Reason] [varchar](100) NULL,
	[Notes] [varchar](1000) NULL,
	[IsExpected] [bit] NULL,
	[IsFunded] [bit] NULL,
	[HasCloseDays] [bit] NULL,
	[AskToFundedDays] [int] NULL,
	[ExpectedToFundedDays] [int] NULL,
	[TotalActionCount] [int] NULL,
	[TotalProposalActionCount] [int] NULL,
	[TotalProposalPreFundedActionCount] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[MonetaryAsked]  AS ([dbo].[BBBI_ReturnRange]('Monetary Major Gift',[AmountAsked])),
	[MonetaryAskedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Major Gift',[AmountAsked])),
	[DaysSinceAsked]  AS (datediff(day,[DateAsked],getdate())),
	[RecencyAsked]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[DateAsked],getdate()))),
	[RecencyAskedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[DateAsked],getdate()))),
	[MonetaryExpected]  AS ([dbo].[BBBI_ReturnRange]('Monetary Major Gift',[AmountExpected])),
	[MonetaryExpectedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Major Gift',[AmountExpected])),
	[DaysSinceExpected]  AS (datediff(day,[DateExpected],getdate())),
	[RecencyExpected]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[DateExpected],getdate()))),
	[RecencyExpectedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[DateExpected],getdate()))),
	[MonetaryFunded]  AS ([dbo].[BBBI_ReturnRange]('Monetary Major Gift',[AmountFunded])),
	[MonetaryFundedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Major Gift',[AmountFunded])),
	[DaysSinceFunded]  AS (datediff(day,[DateFunded],getdate())),
	[RecencyFunded]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[DateFunded],getdate()))),
	[RecencyFundedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[DateFunded],getdate()))),
 CONSTRAINT [PK_DIM_Proposal_ProposalDimID_ProposalDimID] PRIMARY KEY CLUSTERED 
(
	[ProposalDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ProposalAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ProposalAttribute](
	[ProposalAttributeDimID] [int] IDENTITY(1,1) NOT NULL,
	[ProposalAttributeSystemID] [int] NULL,
	[ProposalDimID] [int] NULL,
	[ProposalSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_ProposalAttribute_ProposalAttributeDimID] PRIMARY KEY CLUSTERED 
(
	[ProposalAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_ProposalRank]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DIM_ProposalRank](
	[ProposalDimID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[CreateFirstSequence] [int] NULL,
	[ChangeFirstSequence] [int] NULL,
	[OriginalAskFirstSequence] [int] NULL,
	[AskFirstSequence] [int] NULL,
	[ExpectedFirstSequence] [int] NULL,
	[FundedFirstSequence] [int] NULL,
	[CreateLastSequence] [int] NULL,
	[ChangeLastSequence] [int] NULL,
	[OriginalAskLastSequence] [int] NULL,
	[AskLastSequence] [int] NULL,
	[ExpectedLastSequence] [int] NULL,
	[FundedLastSequence] [int] NULL,
	[AskHighestSequence] [int] NULL,
	[ExpectedHighestSequence] [int] NULL,
	[FundedHighestSequence] [int] NULL,
 CONSTRAINT [PK_DIM_ProposalRank] PRIMARY KEY CLUSTERED 
(
	[ProposalDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO


GO

/****** Object:  Table [dbo].[DIM_ProposalStatusHistory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_ProposalStatusHistory](
	[ProposalStatusHistoryDimID] [int] IDENTITY(1,1) NOT NULL,
	[ProposalDimID] [int] NULL,
	[ProposalSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[Status] [varchar](100) NULL,
	[StatusSystemID] [int] NULL,
	[PriorStatus] [varchar](100) NULL,
	[NextStatus] [varchar](100) NULL,
	[DateChanged] [datetime] NULL,
	[DateChangedDimID] [int] NULL,
	[ProposalDateChanged] [datetime] NULL,
	[ProposalDateChangedDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[StatusSequence] [smallint] NULL,
	[IsCurrent] [bit] NULL,
	[HasLatency] [bit] NULL,
	[LatencyDays] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[LatencyRange]  AS ([dbo].[BBBI_ReturnRange]('Proposal Latency',[LatencyDays])),
	[LatencyRangeSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Proposal Latency',[LatencyDays])),
 CONSTRAINT [PK_DIM_ProposalStatusHistory] PRIMARY KEY CLUSTERED 
(
	[ProposalStatusHistoryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Scenario]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Scenario](
	[ScenarioDimID] [int] NOT NULL,
	[ScenarioSystemID] [int] NULL,
	[ScenarioID] [int] NULL,
	[ScenarioName] [varchar](60) NULL,
	[Status] [varchar](8) NULL,
	[IsLocked] [varchar](1) NULL,
	[FiscalYearDescription] [varchar](60) NULL,
	[NoFiscalPeriods] [smallint] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Scenario] PRIMARY KEY CLUSTERED 
(
	[ScenarioDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Solicitor]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Solicitor](
	[SolicitorDimID] [int] NOT NULL,
	[ConstituentID] [varchar](20) NULL,
	[ConstituentImportID] [varchar](50) NULL,
	[FullName] [varchar](110) NULL,
	[StaffSolicitorName] [varchar](110) NULL,
	[KeyIndicator] [varchar](1) NULL,
	[PrimaryConstituentCode] [varchar](100) NULL,
	[LatestSolicitorType] [varchar](100) NULL,
	[PrimarySolicitorType] [varchar](100) NULL,
	[IsStaffSolicitor] [bit] NULL,
	[IsPrimarySolicitor] [bit] NULL,
	[IsASolicitor] [bit] NULL,
	[IsSolicitorInactive] [bit] NULL,
	[IsDeceased] [bit] NULL,
	[HasGift] [bit] NULL,
	[HasGoal] [bit] NULL,
	[HasAction] [bit] NULL,
	[HasProposal] [bit] NULL,
	[HasAssignment] [bit] NULL,
	[HasActivityGoal] [bit] NULL,
	[LastSolicitorGiftDateDimID] [int] NULL,
	[LastSolicitorActionDateDimID] [int] NULL,
	[LastSolicitorProposalAskDateDimID] [int] NULL,
	[LastSolicitorProposalFundedDateDimID] [int] NULL,
	[LastAssignmentDateDimID] [int] NULL,
	[TotalSolicitorGiftAmount] [money] NULL,
	[TotalProposalFundedAmount] [money] NULL,
	[TotalActiveAssignmentCount] [int] NULL,
	[TotalRelationshipCount] [int] NULL,
	[TotalActionCount] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Solicitor_SolicitorDimID] PRIMARY KEY CLUSTERED 
(
	[SolicitorDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Source]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Source](
	[SourceDimID] [int] NOT NULL,
	[OrganizationName] [varchar](100) NULL,
	[Country] [varchar](50) NULL,
	[CurrentDateDimID] [int] NULL,
	[CurrentDate] [datetime] NULL,
	[FiscalYearStart] [varchar](10) NULL,
	[FiscalYearEnd] [varchar](10) NULL,
	[FiscalMonthStart] [tinyint] NULL,
	[RefreshDate] [datetime] NULL,
	[CurrentFiscalYear] [smallint] NULL,
	[CurrentCalendarYear] [smallint] NULL,
	[CurrentFiscalYearStartDate] [datetime] NULL,
	[CurrentFiscalYearEndDate] [datetime] NULL,
	[PriorFiscalYearStartDate] [datetime] NULL,
	[PriorFiscalYearEndDate] [datetime] NULL,
	[CurrentFiscalQtrStartDate] [datetime] NULL,
	[CurrentFiscalQtrEndDate] [datetime] NULL,
	[CurrentCalendarQtrStartDate] [datetime] NULL,
	[CurrentCalendarQtrEndDate] [datetime] NULL,
	[PriorFiscalQtrStartDate] [datetime] NULL,
	[PriorFiscalQtrEndDate] [datetime] NULL,
	[PriorCalendarQtrStartDate] [datetime] NULL,
	[PriorCalendarQtrEndDate] [datetime] NULL,
	[CurrentPeriodStartDate] [datetime] NULL,
	[CurrentPeriodEndDate] [datetime] NULL,
	[PriorPeriodStartDate] [datetime] NULL,
	[PriorPeriodEndDate] [datetime] NULL,
	[FirstFiscalYearStartDate] [datetime] NULL,
	[FirstFiscalYearEndDate] [datetime] NULL,
	[LastFiscalYearStartDate] [datetime] NULL,
	[LastFiscalYearEndDate] [datetime] NULL,
	[FirstGiftDate] [datetime] NULL,
	[LastGiftDate] [datetime] NULL,
	[FirstPostDate] [datetime] NULL,
	[LastPostDate] [datetime] NULL,
	[OpenYearStartDate] [datetime] NULL,
	[ClosedYearEndDate] [datetime] NULL,
	[ClosedPeriodEndDate] [datetime] NULL,
	[OpenYearFirstPeriodEndDate] [datetime] NULL,
	[NoSPPeriods] [int] NULL,
	[SPBalanceStartDate] [datetime] NULL,
	[SPBalanceEndDate] [datetime] NULL,
	[VersionNumberRE] [varchar](12) NULL,
	[SourceName] [varchar](50) NULL,
	[SourceShortName] [varchar](10) NULL,
	[VersionNumberSource] [varchar](12) NULL,
 CONSTRAINT [PK_DIM_Source_SourceDimID] PRIMARY KEY CLUSTERED 
(
	[SourceDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Summary]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Summary](
	[SummaryDimID] [int] NOT NULL,
	[SummaryDescription] [varchar](50) NULL,
	[SummaryTypeDescription] [varchar](50) NULL,
	[SummaryFilterDescription] [varchar](50) NULL,
 CONSTRAINT [PK_DIM_Summary_SummaryDimID] PRIMARY KEY CLUSTERED 
(
	[SummaryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Time]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Time](
	[TimeDimID] [int] NOT NULL,
	[TimeFull] [varchar](8) NULL,
	[TimeDescription] [varchar](10) NULL,
	[TimeDescription24] [varchar](10) NULL,
	[Hour] [smallint] NULL,
	[HourFull] [varchar](2) NULL,
	[Hour24] [smallint] NULL,
	[Hour24Full] [varchar](2) NULL,
	[Minute] [smallint] NULL,
	[MinuteFull] [varchar](2) NULL,
	[AMorPM] [varchar](2) NULL,
	[PerformanceTime] [varchar](15) NULL,
	[PerformanceTimeGroup] [varchar](15) NULL,
	[NearestMealTime] [varchar](20) NULL,
	[TimeGroupDescription] [varchar](20) NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_DIM_Time] PRIMARY KEY CLUSTERED 
(
	[TimeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_TransactionAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_TransactionAttribute](
	[TransactionAttributeDimID] [int] NOT NULL,
	[TransactionAttributeSystemID] [int] NOT NULL,
	[TransactionSystemID] [int] NULL,
	[TransactionDistributionFactID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_TransactionAttribute] PRIMARY KEY CLUSTERED 
(
	[TransactionAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_TransactionCode]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_TransactionCode](
	[TransactionCodeDimID] [int] NOT NULL,
	[TransactionCodeSystemID] [int] NULL,
	[TransactionCode] [varchar](60) NULL,
	[TableSequence] [int] NULL,
	[TransactionCodeName] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_TransactionCode] PRIMARY KEY CLUSTERED 
(
	[TransactionCodeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_TransactionType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_TransactionType](
	[TransactionTypeDimID] [smallint] NOT NULL,
	[TransactionTypeSystemID] [int] NULL,
	[TransactionType] [varchar](10) NULL,
 CONSTRAINT [PK_DIM_TransactionType] PRIMARY KEY CLUSTERED 
(
	[TransactionTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VCO]    Script Date: 06/15/2012 18:33:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_VCO](
	[VCODimID] [int] IDENTITY(1,1) NOT NULL,
	[ChartTemplateSystemID] [int] NULL,
	[ChartTemplateID] [varchar](50) NULL,
	[ChartTemplateName] [varchar](100) NULL,
	[TemplateDetailSystemID] [int] NULL,
	[TemplateDetailFilterSystemID] [int] NULL,
	[RecordType] [smallint] NULL,
	[Level] [smallint] NULL,
	[Sequence] [int] NULL,
	[Caption] [varchar](100) NULL,
	[TotalCaption] [varchar](60) NULL,
	[AccountCategory] [varchar](9) NULL,
	[AccountCodeSystemID] [int] NULL,
	[AccountCode] [varchar](30) NULL,
	[AccountSystemID] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountNumber] [varchar](100) NULL,
	[AccountDescription] [varchar](60) NULL,
	[ParentKey] [varchar](20) NULL,
	[ParentVCODimID] [int] NULL,
	[TemplateStatus] [varchar](8) NULL,
	[FilterList] [varchar](500) NULL,
	[IsGroup] [bit] NULL,
	[IsAdvancedFilter] [bit] NULL,
	[IsAccountRange] [bit] NULL,
	[Include] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_VCO] PRIMARY KEY CLUSTERED 
(
	[VCODimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DIM_Vendor]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[DIM_Vendor](
	[VendorDimID] [int] NOT NULL,
	[VendorSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[UserDefinedID] [varchar](20) NULL,
	[TaxIDNumber] [varchar](20) NULL,
	[FEImportID] [varchar](50) NULL,
	[VendorName] [varchar](60) NULL,
	[VendorSearchName] [varchar](60) NULL,
	[VendorDisplayName] [varchar](60) NULL,
	[CustomerNumber] [varchar](20) NULL,
	[PaymentOption] [smallint] NULL,
	[Status] [varchar](8) NULL,
	[HasCreditLimit] [varchar](1) NULL,
	[CreditLimitAmount] [numeric](19, 4) NULL,
	[DefaultPaymentMethod] [varchar](10) NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[AddedDateDimID] [int] NULL,
	[ChangedDateDimID] [int] NULL,
	[VendorBalance] [numeric](19, 4) NULL,
	[HighestBalance] [numeric](19, 4) NULL,
	[CheckNotes] [varchar](95) NULL,
	[DistributedDiscount] [varchar](1) NULL,
	[IsVendor1099] [varchar](1) NULL,
	[TotalPurgedInvoicesAmount] [numeric](19, 4) NULL,
	[TotalPurgedCreditMemosAmount] [numeric](19, 4) NULL,
	[TotalPurgedPOAmount] [numeric](19, 4) NULL,
	[BankName] [varchar](60) NULL,
	[ContactDimID] [int] NULL,
	[ContactSystemID] [int] NULL,
	[KeyName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[VendorContactSequence] [smallint] NULL,
	[VendorContactImportID] [varchar](50) NULL,
	[KeyIndicator] [varchar](1) NULL,
	[ContactType] [varchar](10) NULL,
	[AddressSystemID] [int] NULL,
	[Address] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](3) NULL,
	[PostCode] [varchar](12) NULL,
	[County] [varchar](60) NULL,
	[Country] [varchar](60) NULL,
	[Region] [varchar](60) NULL,
	[Suburb] [varchar](60) NULL,
	[IsPrimaryAddress] [bit] NULL,
	[IsDefaultInvoiceAddress] [bit] NULL,
	[IsDefaultPOAddress] [bit] NULL,
	[IsDefault1099Address] [bit] NULL,
	[NameOne1099] [varchar](40) NULL,
	[NameTwo1099] [varchar](40) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Vendor] PRIMARY KEY CLUSTERED 
(
	[VendorDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VendorAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[DIM_VendorAttribute](
	[VendorAttributeDimID] [int] NOT NULL,
	[VendorAttributeSystemID] [int] NOT NULL,
	[VendorDimID] [int] NULL,
	[VendorSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_DIM_VendorAttribute] PRIMARY KEY CLUSTERED 
(
	[VendorAttributeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VendorContact]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[DIM_VendorContact](
	[VendorContactDimID] [int] NOT NULL,
	[VendorContactSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[VendorDimID] [int] NULL,
	[VendorSystemID] [int] NULL,
	[NameSystemID] [int] NULL,
	[AddressSystemID] [int] NULL,
	[VendorContactDescription] [varchar](60) NULL,
	[VendorContactName] [varchar](60) NULL,
	[AddressBlock] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](3) NULL,
	[PostCode] [varchar](12) NULL,
	[County] [varchar](60) NULL,
	[Country] [varchar](60) NULL,
	[Region] [varchar](60) NULL,
	[Suburb] [varchar](60) NULL,
	[Gender] [varchar](10) NULL,
	[Deceased] [bit] NULL,
	[DeceasedDate] [datetime] NULL,
	[FullName] [varchar](255) NULL,
	[Alias] [varchar](60) NULL,
	[IsPrimaryAddress] [bit] NULL,
	[IsDefaultInvoiceAddress] [bit] NULL,
	[IsDefaultPOAddress] [bit] NULL,
	[IsDefault1099Address] [bit] NULL,
	[NameOne1099] [varchar](40) NULL,
	[NameTwo1099] [varchar](40) NULL,
	[VendorContactSequence] [smallint] NULL,
	[VendorContactImportID] [varchar](50) NULL,
	[KeyIndicator] [varchar](1) NULL,
	[ContactType] [varchar](10) NULL,
	[KeyName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_VendorContact] PRIMARY KEY CLUSTERED 
(
	[VendorContactDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_Volunteer]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_Volunteer](
	[VolunteerDimID] [int] NOT NULL,
	[VolunteerSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[VehicleType] [varchar](100) NULL,
	[FutureAward] [varchar](100) NULL,
	[EmergencyName] [varchar](50) NULL,
	[EmergencyPhone] [varchar](50) NULL,
	[IsVehicleAvailable] [varchar](3) NULL,
	[EmergencyRelation] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_Volunteer_VolunteerDimID] PRIMARY KEY CLUSTERED 
(
	[VolunteerDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VolunteerDepartment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_VolunteerDepartment](
	[VolunteerDepartmentDimID] [int] NOT NULL,
	[VolunteerDepartmentSystemID] [int] NOT NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_VolunteerDepartment_VolunteerDepartmentDimID] PRIMARY KEY CLUSTERED 
(
	[VolunteerDepartmentDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VolunteerJob]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_VolunteerJob](
	[VolunteerJobDimID] [int] NOT NULL,
	[VolunteerJobSystemID] [int] NULL,
	[JobName] [varchar](50) NULL,
	[VolunteerJobCategoryDimID] [int] NULL,
	[AssignmentCategory] [varchar](100) NULL,
	[VolunteerTypeDimID] [int] NULL,
	[VolunteerType] [varchar](100) NULL,
	[VolunteerPosition] [varchar](50) NULL,
	[MinAge] [int] NULL,
	[JobStartDateDimID] [int] NULL,
	[JobEndDateDimID] [int] NULL,
	[JobStartDate] [datetime] NULL,
	[JobEndDate] [datetime] NULL,
	[AllowMandate] [varchar](3) NULL,
	[Status] [varchar](15) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_VolunteerJob_VolunteerJobDimID] PRIMARY KEY CLUSTERED 
(
	[VolunteerJobDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VolunteerJobCategory]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_VolunteerJobCategory](
	[VolunteerJobCategoryDimID] [int] NOT NULL,
	[VolunteerJobCategorySystemID] [int] NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_VolunteerJobCategory_VolunteerJobCategoryDimID] PRIMARY KEY CLUSTERED 
(
	[VolunteerJobCategoryDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[DIM_VolunteerType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_VolunteerType](
	[VolunteerTypeDimID] [int] NOT NULL,
	[VolunteerTypeSystemID] [int] NOT NULL,
	[IsActive] [varchar](3) NULL,
	[Description] [varchar](100) NULL,
	[CodeSequence] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_VolunteerType_VolunteerTypeDimID] PRIMARY KEY CLUSTERED 
(
	[VolunteerTypeDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[DIM_WebPage]    Script Date: 10/17/2012 13:53:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_WebPage](
	[WebPageDimID] [int] NOT NULL,
	[WebPageSystemID] [int] NULL,
	[WebPageName] [varchar](100) NULL,
	[WebPageDisplayName] [varchar](100) NULL,
	[WebPageURL] [nvarchar](255) NULL,
	[WebPageFolder] [varchar](100) NULL,
	[WebPageParentFolder] [varchar](100) NULL,
	[WebPageType] [varchar](100) NULL,
	[WebPageCategory] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_DIM_WebPage] PRIMARY KEY CLUSTERED 
(
	[WebPageDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[DIM_WebSite]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DIM_WebSite](
	[WebSiteDimID] [int] NOT NULL,
	[WebSiteName] [varchar](50) NULL,
	[WebSiteURL] [varchar](100) NULL,
	[WebSiteType] [varchar](100) NULL,
	[WebSiteSearch] [varchar](100) NULL,
	[WebSiteCategory] [varchar](100) NULL,
	[WebSiteGroup] [varchar](100) NULL,
 CONSTRAINT [PK_DIM_WebSite] PRIMARY KEY CLUSTERED 
(
	[WebSiteDimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_DIMGROUP]
) ON [BBPM_DIMGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_AccountBudget]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_AccountBudget](
	[AccountBudgetFactID] [int] NOT NULL,
	[AccountBudgetSystemID] [int] NOT NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[ScenarioDimID] [int] NULL,
	[ScenarioSystemID] [int] NULL,
	[ScenarioID] [int] NULL,
	[ScenarioDescription] [varchar](60) NULL,
	[ScenarioSequence] [smallint] NULL,
	[PeriodStartDate] [datetime] NULL,
	[PeriodStartDateDimID] [int] NULL,
	[PeriodEndDate] [datetime] NULL,
	[PeriodEndDateDimID] [int] NULL,
	[Closed] [smallint] NULL,
	[PeriodSequence] [smallint] NULL,
	[FiscalYearDesc] [varchar](60) NULL,
	[YearID] [varchar](12) NULL,
	[FiscalYearSequence] [int] NULL,
	[FiscalPeriods] [smallint] NULL,
	[FiscalYearStatus] [smallint] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[Amount] [money] NULL,
	[NaturalAmount] [money] NULL,
	[Percent] [decimal](20, 4) NULL,
	[Notes] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_AccountBudget] PRIMARY KEY CLUSTERED 
(
	[AccountBudgetFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_Action]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_Action](
	[ActionFactID] [int] NOT NULL,
	[ActionSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ActionDateDimID] [int] NULL,
	[ActionDate] [datetime] NULL,
	[ActionCategoryDimID] [int] NULL,
	[ActionPriorityDimID] [int] NULL,
	[IsActionCompleted] [char](3) NULL,
	[CompletedDateDimID] [int] NULL,
	[ContactConstituentDimID] [int] NULL,
	[ChangedDateDimID] [int] NULL,
	[ActionStatusDimID] [int] NULL,
	[ActionTypeDimID] [int] NULL,
	[ActionLocationDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[ProposalDimID] [int] NULL,
	[ProposalSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_Action_ActionFactID] PRIMARY KEY CLUSTERED 
(
	[ActionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_Action_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_Action_Stage](
	[ActionFactID] [int] NOT NULL,
	[ActionSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ActionDateDimID] [int] NULL,
	[ActionDate] [datetime] NULL,
	[ActionCategoryDimID] [int] NULL,
	[ActionPriorityDimID] [int] NULL,
	[IsActionCompleted] [char](3) NULL,
	[CompletedDateDimID] [int] NULL,
	[ContactConstituentDimID] [int] NULL,
	[ChangedDateDimID] [int] NULL,
	[ActionStatusDimID] [int] NULL,
	[ActionTypeDimID] [int] NULL,
	[ActionLocationDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[ProposalDimID] [int] NULL,
	[ProposalSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ActionAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ActionAttribute](
	[ActionAttributeFactID] [int] IDENTITY(1,1) NOT NULL,
	[ActionAttributeSystemID] [int] NULL,
	[ActionFactID] [int] NULL,
	[ActionSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [int] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](25) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_ActionAttribute_ActionAttributeFactID] PRIMARY KEY CLUSTERED 
(
	[ActionAttributeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ActionAttribute_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ActionAttribute_Stage](
	[ActionAttributeFactID] [int] NULL,
	[ActionAttributeSystemID] [int] NULL,
	[ActionFactID] [int] NULL,
	[ActionSystemID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [int] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](25) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_APCreditMemo]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[FACT_APCreditMemo](
	[APCreditMemoFactID] [int] NULL,
	[CreditMemoSystemID] [int] NULL,
	[InvoiceFactID] [int] NULL,
	[InvoiceSystemID] [int] NULL,
	[InvoiceNumber] [varchar](20) NULL,
	[ConstituentDimID] [int] NULL,
	[VendorDimID] [int] NULL,
	[InvoiceDateDimID] [int] NULL,
	[CreditMemoNumber] [varchar](20) NULL,
	[CreditMemoDate] [datetime] NULL,
	[CreditMemoDateDimID] [int] NULL,
	[CreditMemoDescription] [varchar](60) NULL,
	[CreditMemoStatus] [varchar](17) NULL,
	[PostDate] [datetime] NULL,
	[PostDateDimID] [int] NULL,
	[PostStatus] [varchar](20) NULL,
	[ReversePostStatus] [varchar](14) NULL,
	[ReverseDate] [datetime] NULL,
	[ReverseDateDimID] [int] NULL,
	[IsDeleted] [varchar](3) NULL,
	[Amount] [numeric](19, 4) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_AppealExpense]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_AppealExpense](
	[AppealExpenseFactID] [int] NOT NULL,
	[AppealExpenseSystemID] [int] NULL,
	[AppealDimID] [int] NULL,
	[AppealSystemID] [int] NULL,
	[PackageDimID] [int] NULL,
	[PackageSystemID] [int] NULL,
	[AppealExpenseTypeDimID] [int] NULL,
	[AppealExpenseTypeSystemID] [int] NULL,
	[AppealExpenseType] [varchar](100) NULL,
	[AppealExpenseDate] [datetime] NULL,
	[AppealExpenseDateDimID] [int] NULL,
	[Amount] [money] NULL,
	[BudgetedAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_AppealExpense_AppealExpenseFactID] PRIMARY KEY CLUSTERED 
(
	[AppealExpenseFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_APTransactionDistribution]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_APTransactionDistribution](
	[TransactionDistributionFactID] [int] NOT NULL,
	[APTransDistributionSystemID] [int] NULL,
	[APDistributionSystemID] [int] NULL,
	[InvoiceFactID] [int] NULL,
	[InvoiceNumber] [varchar](20) NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceDateDimID] [int] NULL,
	[VendorSystemID] [int] NULL,
	[VendorDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[AccountCode] [varchar](30) NULL,
	[AccountNumber] [varchar](30) NULL,
	[AccountDescription] [varchar](60) NULL,
	[FundDimID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[ProjectID] [varchar](20) NULL,
	[ProjectDescription] [varchar](60) NULL,
	[GLTransactionSystemID] [int] NULL,
	[GLTransactionDistributionSystemID] [int] NULL,
	[GLTransactionDistributionFactID] [int] NULL,
	[TransactionType] [varchar](6) NULL,
	[TransactionTypeDimID] [int] NULL,
	[DistributionType] [varchar](18) NULL,
	[Percent] [decimal](20, 4) NULL,
	[LinkKey] [int] NULL,
	[ParentObjectType] [int] NULL,
	[RowNumber] [int] NULL,
	[Interfund] [smallint] NULL,
	[MiscEntry] [smallint] NULL,
	[TaxEntityID] [int] NULL,
	[TransactionCode1DimID] [int] NULL,
	[TransactionCode2DimID] [int] NULL,
	[TransactionCode3DimID] [int] NULL,
	[TransactionCode4DimID] [int] NULL,
	[TransactionCode5DimID] [int] NULL,
	[ClassDimID] [int] NULL,
	[ClassSystemID] [int] NULL,
	[ClassName] [varchar](60) NULL,
	[Sequence] [int] NULL,
	[Amount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_APTransactionDistribution] PRIMARY KEY CLUSTERED 
(
	[TransactionDistributionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_Charge]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_Charge](
	[ChargeFactID] [int] NOT NULL,
	[ChargeSystemID] [int] NULL,
	[ChargeNumber] [varchar](20) NULL,
	[Category] [smallint] NULL,
	[BillingItemID] [int] NULL,
	[ChargeDescription] [varchar](60) NULL,
	[ChargeDate] [datetime] NULL,
	[ChargeDateDimID] [int] NULL,
	[DueDate] [datetime] NULL,
	[DueDateDimID] [int] NULL,
	[PostDate] [datetime] NULL,
	[PostDateDimID] [int] NULL,
	[PostStatus] [varchar](19) NULL,
	[Status] [varchar](14) NULL,
	[ClientDimID] [int] NULL,
	[ClientSystemID] [int] NULL,
	[ClientName] [varchar](60) NULL,
	[ReversePostStatus] [varchar](14) NULL,
	[ReversePostDate] [datetime] NULL,
	[ReversePostDateDimID] [int] NULL,
	[PostStatusDimID] [smallint] NULL,
	[ReversePostStatusDimID] [smallint] NULL,
	[ChargeAmount] [numeric](19, 4) NULL,
	[ChargeBalance] [numeric](19, 4) NULL,
	[TaxAmount] [numeric](19, 4) NULL,
	[CSRID] [int] NULL,
	[PatientID] [int] NULL,
	[AssistanceInstanceID] [int] NULL,
	[AddressID] [int] NULL,
	[AssistanceType] [varchar](100) NULL,
	[CSRName] [varchar](100) NULL,
	[PatientName] [varchar](150) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_Charge] PRIMARY KEY CLUSTERED 
(
	[ChargeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentAnnualClassification]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentAnnualClassification](
	[ConstituentAnnualClassificationFactID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentAnnualGivingFactID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[FiscalYear] [smallint] NULL,
	[Sequence] [smallint] NULL,
	[Classification] [varchar](5) NULL,
	[SummaryDimID] [int] NULL,
 CONSTRAINT [PK_FACT_ConstituentAnnualClassification] PRIMARY KEY CLUSTERED 
(
	[ConstituentAnnualClassificationFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentAnnualGiving]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentAnnualGiving](
	[ConstituentAnnualGivingFactID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentDimID] [int] NULL,
	[RelativeYear] [varchar](15) NULL,
	[TotalGiftAmount] [money] NULL,
	[TotalNumberGifts] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[HighestGiftAmount] [money] NULL,
	[FiscalYear] [smallint] NULL,
	[FiscalYearPrior] [smallint] NULL,
	[FiscalYearNext] [smallint] NULL,
	[FiscalYearStartDateDimID] [int] NULL,
	[YearAgo] [int] NULL,
	[LowestGiftAmount] [money] NULL,
	[AvgGiftAmount] [money] NULL,
	[FirstGiftDate] [datetime] NULL,
	[LastGiftDate] [datetime] NULL,
	[UpgradeType] [varchar](20) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TotalNumberGifts])),
	[MonetaryTotal]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalGiftAmount])),
	[MonetaryMax]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[HighestGiftAmount])),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TotalNumberGifts])),
	[MonetaryTotalSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalGiftAmount])),
	[MonetaryMaxSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[HighestGiftAmount])),
	[SummaryDimID] [int] NULL,
	[GivingSequence] [smallint] NULL,
 CONSTRAINT [PK_FACT_ConstituentAnnualGiving_ConstituentAnnualGivingFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentAnnualGivingFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentAnnualGivingUpgrade]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentAnnualGivingUpgrade](
	[ConstituentAnnualGivingUpgradeFactID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentAnnualGivingFactID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[FiscalYear] [smallint] NULL,
	[TotalUpgradeType] [varchar](10) NULL,
	[MaxUpgradeType] [varchar](10) NULL,
	[MinUpgradeType] [varchar](10) NULL,
	[AvgUpgradeType] [varchar](10) NULL,
	[FrequencyUpgradeType] [varchar](10) NULL,
	[SummaryDimID] [int] NULL,
 CONSTRAINT [PK_FACT_ConstituentAnnualGivingUpgrade] PRIMARY KEY CLUSTERED 
(
	[ConstituentAnnualGivingUpgradeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentAnnualMembership]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentAnnualMembership](
	[ConstituentAnnualMembershipFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[RelativeYear] [varchar](15) NULL,
	[FiscalYear] [smallint] NULL,
	[FiscalYearStartDateDimID] [int] NULL,
	[TotalAmount] [money] NULL,
	[TotalCount] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[HighestAmount] [money] NULL,
	[LowestAmount] [money] NULL,
	[YearAgo] [int] NULL,
	[AvgAmount] [money] NULL,
	[FirstDate] [datetime] NULL,
	[LastDate] [datetime] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TotalCount])),
	[MonetaryTotal]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalAmount])),
	[MonetaryMax]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[HighestAmount])),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TotalCount])),
	[MonetaryTotalSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalAmount])),
	[MonetaryMaxSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[HighestAmount])),
 CONSTRAINT [PK_FACT_ConstituentAnnualMemb_ConstituentAnnualMembFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentAnnualMembershipFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentAppeal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentAppeal](
	[ConstituentAppealFactID] [int] NOT NULL,
	[ConstituentAppealSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[AppealSystemID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageSystemID] [int] NULL,
	[PackageDimID] [int] NULL,
	[AppealDateDimID] [int] NULL,
	[AppealResponseDimID] [int] NULL,
	[MarketingSegment] [varchar](255) NULL,
	[MarketingSourceCode] [varchar](60) NULL,
	[MarketingFinderNumber] [numeric](19, 0) NULL,
	[MarketingMailingID] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_ConstituentAppeal_ConstituentAppealFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentAppealFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentGivingSummary]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentGivingSummary](
	[ConstituentGivingSummaryFactID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[DateAddedDimID] [int] NULL,
	[Amt0] [money] NULL,
	[Amt1] [money] NULL,
	[Amt2] [money] NULL,
	[Amt3] [money] NULL,
	[Amt4] [money] NULL,
	[Amt5] [money] NULL,
	[Amt6] [money] NULL,
	[Amt7] [money] NULL,
	[Amt8] [money] NULL,
	[Amt9] [money] NULL,
	[Amt10] [money] NULL,
	[Amt11] [money] NULL,
	[Amt12] [money] NULL,
	[Amt13] [money] NULL,
	[Amt14] [money] NULL,
	[Amt15] [money] NULL,
	[Amt16] [money] NULL,
	[Amt17] [money] NULL,
	[Amt18] [money] NULL,
	[Amt19] [money] NULL,
	[Amt20] [money] NULL,
	[Count0] [int] NULL,
	[Count1] [int] NULL,
	[Count2] [int] NULL,
	[Count3] [int] NULL,
	[Count4] [int] NULL,
	[Count5] [int] NULL,
	[Count6] [int] NULL,
	[Count7] [int] NULL,
	[Count8] [int] NULL,
	[Count9] [int] NULL,
	[Count10] [int] NULL,
	[Count11] [int] NULL,
	[Count12] [int] NULL,
	[Count13] [int] NULL,
	[Count14] [int] NULL,
	[Count15] [int] NULL,
	[Count16] [int] NULL,
	[Count17] [int] NULL,
	[Count18] [int] NULL,
	[Count19] [int] NULL,
	[Count20] [int] NULL,
	[Count20Plus] [int] NULL,
	[AcquisitionAgo] [smallint] NULL,
	[MaxAmt0] [money] NULL,
	[MaxAmt1] [money] NULL,
	[MaxAmt2] [money] NULL,
	[MaxAmt3] [money] NULL,
	[MaxAmt4] [money] NULL,
	[MaxAmt5] [money] NULL,
	[MaxAmt6] [money] NULL,
	[MaxAmt7] [money] NULL,
	[MaxAmt8] [money] NULL,
	[MaxAmt9] [money] NULL,
	[MaxAmt10] [money] NULL,
	[MaxAmt11] [money] NULL,
	[MaxAmt12] [money] NULL,
	[MaxAmt13] [money] NULL,
	[MaxAmt14] [money] NULL,
	[MaxAmt15] [money] NULL,
	[MaxAmt16] [money] NULL,
	[MaxAmt17] [money] NULL,
	[MaxAmt18] [money] NULL,
	[MaxAmt19] [money] NULL,
	[MaxAmt20] [money] NULL,
	[MaxAmt20Plus] [money] NULL,
	[Amt0Plus] [money] NULL,
	[Amt1Plus] [money] NULL,
	[Amt2Plus] [money] NULL,
	[Amt3Plus] [money] NULL,
	[Amt4Plus] [money] NULL,
	[Amt5Plus] [money] NULL,
	[Amt6Plus] [money] NULL,
	[Amt7Plus] [money] NULL,
	[Amt8Plus] [money] NULL,
	[Amt9Plus] [money] NULL,
	[Amt10Plus] [money] NULL,
	[Amt11Plus] [money] NULL,
	[Amt12Plus] [money] NULL,
	[Amt13Plus] [money] NULL,
	[Amt14Plus] [money] NULL,
	[Amt15Plus] [money] NULL,
	[Amt16Plus] [money] NULL,
	[Amt17Plus] [money] NULL,
	[Amt18Plus] [money] NULL,
	[Amt19Plus] [money] NULL,
	[Amt20Plus] [money] NULL,
	[Amt21Plus] [money] NULL,
	[Column] [int] NULL,
	[Flag] [varchar](10) NULL,
	[LYBUNT] [varchar](3) NULL,
	[SYBUNT] [varchar](3) NULL,
	[NewDMDonor] [varchar](3) NULL,
	[LifetimeGiftAmount] [money] NULL,
	[LifetimeGiftCount] [int] NULL,
	[LifetimeGiftAverage] [money] NULL,
	[ConsecutiveRenewal] [int] NULL,
	[TenYearRenewal] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[SummaryDimID] [int] NULL,
 CONSTRAINT [PK_FACT_ConstituentGivingSummary_ConstituentGivingSummaryFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentGivingSummaryFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTAction]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTAction](
	[ConstituentActionFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NOT NULL,
	[FirstActionDate] [datetime] NULL,
	[FirstActionDateDimID] [int] NULL,
	[LastActionDate] [datetime] NULL,
	[LastActionDateDimID] [int] NULL,
	[TotalNumberActions] [int] NULL,
	[TotalNumberActionsPrior12Months] [int] NULL,
	[YearsSinceAction] [int] NULL,
	[MonthsSinceAction] [int] NULL,
	[DistinctCalendarYears] [int] NULL,
	[DistinctFiscalYears] [int] NULL,
	[ActionYears] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[LastActionDate],getdate()))),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TotalNumberActions])),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[LastActionDate],getdate()))),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TotalNumberActions])),
 CONSTRAINT [PK_FACT_ConstituentLTAction_ConstituentActionFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentActionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTAppeal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTAppeal](
	[ConstituentLTAppealFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NOT NULL,
	[FirstAppealDate] [datetime] NULL,
	[FirstAppealDateDimID] [int] NULL,
	[LastAppealDate] [datetime] NULL,
	[LastAppealDateDimID] [int] NULL,
	[DistinctCalendarYears] [int] NULL,
	[DistinctFiscalYears] [int] NULL,
	[YearsSinceAppeal] [int] NULL,
	[MonthsSinceAppeal] [int] NULL,
	[DaysSinceAppeal] [int] NULL,
	[AppealYears] [int] NULL,
	[AppealCount] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[LastAppealDate],getdate()))),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[AppealCount])),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[LastAppealDate],getdate()))),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[AppealCount])),
 CONSTRAINT [PK_FACT_ConstituentLTAppeal_ConstituentLTAppealFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentLTAppealFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTEvent]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTEvent](
	[ConstituentLTEventFactID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentDimID] [int] NULL,
	[FirstEventDate] [datetime] NULL,
	[FirstEventDateDimID] [int] NULL,
	[LastEventDate] [datetime] NULL,
	[LastEventDateDimID] [int] NULL,
	[DistinctCalendarYears] [int] NULL,
	[DistinctFiscalYears] [int] NULL,
	[YearsSinceEvent] [int] NULL,
	[MonthsSinceEvent] [int] NULL,
	[DaysSinceEvent] [int] NULL,
	[EventYears] [int] NULL,
	[EventCount] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[LastEventDate],getdate()))),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[EventCount])),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[LastEventDate],getdate()))),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[EventCount])),
 CONSTRAINT [PK_FACT_ConstituentLTEvent_ConstituentLTEventFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentLTEventFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTGiving]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTGiving](
	[ConstituentDimID] [int] NULL,
	[FirstGiftFactID] [int] NULL,
	[LastGiftFactID] [int] NULL,
	[TotalAmount] [money] NULL,
	[AvgAmount] [money] NULL,
	[MaxAmount] [money] NULL,
	[MinAmount] [money] NULL,
	[TotalMaxAmount] [money] NULL,
	[TotalMinAmount] [money] NULL,
	[Count] [int] NULL,
	[TotalCount] [int] NULL,
	[FirstGiftDate] [datetime] NULL,
	[FirstGiftDateDimID] [int] NULL,
	[LastGiftDate] [datetime] NULL,
	[LastGiftDateDimID] [int] NULL,
	[LastGiftTypeDimID] [int] NULL,
	[GivingYears] [smallint] NULL,
	[GivingMonths] [int] NULL,
	[GivingDays] [int] NULL,
	[YearsSinceGiven] [smallint] NULL,
	[MonthsSinceGiven] [int] NULL,
	[DaysSinceGiven] [int] NULL,
	[ConsecutiveYears] [smallint] NULL,
	[DistinctGivingYears] [smallint] NULL,
	[DistinctGivingFiscalYears] [smallint] NULL,
	[AnnualRenewer] [varchar](1) NULL,
	[AnnualFiscalRenewer] [varchar](1) NULL,
	[FrequencySM] [varchar](20) NULL,
	[LastGiftAmount] [money] NULL,
	[FirstGiftAmount] [money] NULL,
	[FirstFundDimID] [int] NULL,
	[LastFundDimID] [int] NULL,
	[FirstCampaignDimID] [int] NULL,
	[LastCampaignDimID] [int] NULL,
	[FirstAppealDimID] [int] NULL,
	[LastAppealDimID] [int] NULL,
	[FirstPackageDimID] [int] NULL,
	[LastPackageDimID] [int] NULL,
	[MaxGiftDate] [datetime] NULL,
	[MaxGiftDateDimID] [int] NULL,
	[MaxGiftSplitDate] [datetime] NULL,
	[MaxGiftSplitDateDimID] [int] NULL,
	[FirstGiftSplitAmount] [money] NULL,
	[LastGiftSplitAmount] [money] NULL,
	[AvgLatency] [int] NULL,
	[MaxLatency] [int] NULL,
	[MinLatency] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',[MonthsSinceGiven])),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TotalCount])),
	[MonetaryTotal]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalAmount])),
	[MonetaryMax]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[MaxAmount])),
	[MonetaryLast]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[LastGiftAmount])),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',[MonthsSinceGiven])),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TotalCount])),
	[MonetaryTotalSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalAmount])),
	[MonearyMaxSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[MaxAmount])),
	[MonetaryLastSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[LastGiftAmount])),
	[SummaryDimID] [int] NULL,
	[ConstituentLTGivingFactID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_FACT_ConstituentLTGiving_ConstituentLTGivingFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentLTGivingFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTMembership]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTMembership](
	[ConstituentLTMembershipFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[TotalAmount] [money] NULL,
	[AvgAmount] [money] NULL,
	[MaxAmount] [money] NULL,
	[MinAmount] [money] NULL,
	[TransactionCount] [int] NULL,
	[FirstTransactionDate] [datetime] NULL,
	[FirstTransactionDateDimID] [int] NULL,
	[LastTransactionDate] [datetime] NULL,
	[LastTransactionDateDimID] [int] NULL,
	[TransactionYears] [smallint] NULL,
	[TransactionMonths] [int] NULL,
	[TransactionDays] [int] NULL,
	[YearsSinceTransaction] [smallint] NULL,
	[MonthsSinceTransaction] [int] NULL,
	[DaysSinceTransaction] [int] NULL,
	[DistinctTransactionYears] [smallint] NULL,
	[DistinctTransactionFiscalYears] [smallint] NULL,
	[ConsecutiveYears] [smallint] NULL,
	[AnnualRenewer] [varchar](1) NULL,
	[AnnualFiscalRenewer] [varchar](1) NULL,
	[FrequencySM] [varchar](20) NULL,
	[LastTransactionAmount] [money] NULL,
	[FirstTransactionAmount] [money] NULL,
	[MaxTransactionDate] [datetime] NULL,
	[MaxTransactionDateDimID] [int] NULL,
	[AvgLatency] [int] NULL,
	[MaxLatency] [int] NULL,
	[MinLatency] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',[MonthsSinceTransaction])),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TransactionCount])),
	[MonetaryTotal]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalAmount])),
	[MonetaryMax]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[MaxAmount])),
	[MonetaryLast]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[LastTransactionAmount])),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',[MonthsSinceTransaction])),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TransactionCount])),
	[MonetaryTotalSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalAmount])),
	[MonetaryMaxSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[MaxAmount])),
	[MonetaryLastSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[LastTransactionAmount])),
 CONSTRAINT [PK_FACT_ConstituentLTMembership_ConstituentLTMembershipFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentLTMembershipFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTProposal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTProposal](
	[ConstituentLTProposalFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[FirstCreateProposalDimID] [int] NULL,
	[LastCreateProposalDimID] [int] NULL,
	[FirstChangeProposalDimID] [int] NULL,
	[LastChangeProposalDimID] [int] NULL,
	[FirstAskProposalDimID] [int] NULL,
	[LastAskProposalDimID] [int] NULL,
	[FirstFundedProposalDimID] [int] NULL,
	[LastFundedProposalDimID] [int] NULL,
	[TotalAskAmount] [money] NULL,
	[AvgAskAmount] [money] NULL,
	[MaxAskAmount] [money] NULL,
	[MinAskAmount] [money] NULL,
	[TotalFundedAmount] [money] NULL,
	[AvgFundedAmount] [money] NULL,
	[MaxFundedAmount] [money] NULL,
	[MinFundedAmount] [money] NULL,
	[ProposalCount] [int] NULL,
	[FirstProposalCreateDate] [datetime] NULL,
	[FirstProposalCreateDateDimID] [int] NULL,
	[LastProposalCreateDate] [datetime] NULL,
	[LastProposalCreateDateDimID] [int] NULL,
	[FirstProposalChangeDate] [datetime] NULL,
	[FirstProposalChangeDateDimID] [int] NULL,
	[LastProposalChangeDate] [datetime] NULL,
	[LastProposalChangeDateDimID] [int] NULL,
	[FirstProposalAskDate] [datetime] NULL,
	[FirstProposalAskDateDimID] [int] NULL,
	[LastProposalAskDate] [datetime] NULL,
	[LastProposalAskDateDimID] [int] NULL,
	[FirstProposalExpectedDate] [datetime] NULL,
	[FirstProposalExpectedDateDimID] [int] NULL,
	[LastProposalExpectedDate] [datetime] NULL,
	[LastProposalExpectedDateDimID] [int] NULL,
	[FirstProposalFundedDate] [datetime] NULL,
	[FirstProposalFundedDateDimID] [int] NULL,
	[LastProposalFundedDate] [datetime] NULL,
	[LastProposalFundedDateDimID] [int] NULL,
	[FundedYears] [smallint] NULL,
	[FundedMonths] [int] NULL,
	[FundedDays] [int] NULL,
	[YearsSinceFunded] [smallint] NULL,
	[MonthsSinceFunded] [int] NULL,
	[DaysSinceFunded] [int] NULL,
	[YearsSinceCreated] [int] NULL,
	[MonthsSinceCreated] [int] NULL,
	[DaysSinceCreated] [int] NULL,
	[YearsSinceChanged] [int] NULL,
	[MonthsSinceChanged] [int] NULL,
	[DaysSinceChanged] [int] NULL,
	[ConsecutiveFundedYears] [smallint] NULL,
	[DistinctFundedYears] [smallint] NULL,
	[DistinctFundedFiscalYears] [smallint] NULL,
	[AnnualRenewer] [varchar](1) NULL,
	[AnnualFiscalRenewer] [varchar](1) NULL,
	[FrequencySM] [varchar](20) NULL,
	[LastAskAmount] [money] NULL,
	[FirstAskAmount] [money] NULL,
	[FirstFundedAmount] [money] NULL,
	[LastFundedAmount] [money] NULL,
	[MaxAskCreateDate] [datetime] NULL,
	[MaxAskCreateDateDimID] [int] NULL,
	[MaxFundedCreateDate] [datetime] NULL,
	[MaxFundedCreateDateDimID] [int] NULL,
	[AvgLatency] [int] NULL,
	[MaxLatency] [int] NULL,
	[MinLatency] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[RecencyFunded]  AS ([dbo].[BBBI_ReturnRange]('Recency',[MonthsSinceFunded])),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[ProposalCount])),
	[MonetaryTotalFunded]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalFundedAmount])),
	[MonetaryMaxFunded]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[MaxFundedAmount])),
	[MonetaryLastFunded]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[LastAskAmount])),
	[RecencyFundedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',[MonthsSinceFunded])),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[ProposalCount])),
	[MonetaryTotalFundedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalFundedAmount])),
	[MonearyMaxFundedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[MaxFundedAmount])),
	[MonetaryLastFundedSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[LastFundedAmount])),
 CONSTRAINT [PK_FACT_ConstituentLTProposal] PRIMARY KEY CLUSTERED 
(
	[ConstituentLTProposalFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ConstituentLTRestricted]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ConstituentLTRestricted](
	[ConstituentLTRestrictedFactID] [int] IDENTITY(1,1) NOT NULL,
	[ConstituentDimID] [int] NULL,
	[Restricted] [varchar](14) NULL,
	[TotalAmount] [money] NULL,
	[AvgAmount] [money] NULL,
	[MaxAmount] [money] NULL,
	[MinAmount] [money] NULL,
	[TotalMaxAmount] [money] NULL,
	[TotalMinAmount] [money] NULL,
	[Count] [int] NULL,
	[TotalCount] [int] NULL,
	[FirstGiftDate] [datetime] NULL,
	[FirstGiftDateDimID] [int] NULL,
	[LastGiftDate] [datetime] NULL,
	[LastGiftDateDimID] [int] NULL,
	[GivingYears] [smallint] NULL,
	[GivingMonths] [int] NULL,
	[GivingDays] [int] NULL,
	[YearsSinceGiven] [smallint] NULL,
	[MonthsSinceGiven] [int] NULL,
	[DaysSinceGiven] [int] NULL,
	[DistinctGivingYears] [smallint] NULL,
	[DistinctGivingFiscalYears] [smallint] NULL,
	[FrequencySM] [varchar](20) NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',[MonthsSinceGiven])),
	[Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TotalCount])),
	[MonetaryTotal]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalAmount])),
	[MonetaryMax]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[MaxAmount])),
	[MonetaryAvg]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[AvgAmount])),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',[MonthsSinceGiven])),
	[FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TotalCount])),
	[MonetaryTotalSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalAmount])),
	[MonetaryMaxSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[MaxAmount])),
	[MonetaryAvgSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[AvgAmount])),
	[SummaryDimID] [int] NULL,
 CONSTRAINT [PK_FACT_ConstituentLTRestricted_ConstituentLTRestrictedFactID] PRIMARY KEY CLUSTERED 
(
	[ConstituentLTRestrictedFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_CRDeposit]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_CRDeposit](
	[CRDepositFactID] [int] NOT NULL,
	[DepositID] [varchar](12) NULL,
	[DepositDate] [datetime] NULL,
	[DepositNumber] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[CreditDescription] [varchar](60) NULL,
	[Status] [varchar](15) NULL,
	[CRDepositType] [varchar](20) NULL,
	[EntryDate] [datetime] NULL,
	[EntryDateDimID] [int] NULL,
	[BankName] [varchar](70) NULL,
	[BankSystemID] [int] NULL,
	[PostDate] [datetime] NULL,
	[PostDateDimID] [int] NULL,
	[PostStatus] [varchar](19) NULL,
	[IsDepositCleared] [varchar](3) NULL,
	[DepositClearedDate] [datetime] NULL,
	[DepositClearedDateDimID] [int] NULL,
	[IsDepositReconciled] [varchar](3) NULL,
	[DepositReconciledDate] [datetime] NULL,
	[DepositReconciledDateDimID] [int] NULL,
	[ActualTotalAmount] [numeric](19, 4) NULL,
	[ProjectedTotalAmount] [numeric](19, 4) NULL,
	[ActualNoReceipts] [int] NULL,
	[ProjectedNoReceipts] [int] NULL,
	[IsVoided] [varchar](3) NULL,
	[VoidedDate] [datetime] NULL,
	[VoidedDateDimID] [int] NULL,
	[CRDepositSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_CRDeposit] PRIMARY KEY CLUSTERED 
(
	[CRDepositFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_EMailStats]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_EMailStats](
	[EMailStatsFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleInitial] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[EMail] [varchar](100) NULL,
	[DisplayName] [varchar](100) NULL,
	[EMailJobDimID] [int] NULL,
	[JobName] [varchar](100) NULL,
	[RecipientSubject] [varchar](100) NULL,
	[EMailType] [tinyint] NULL,
	[FromDisplayName] [varchar](100) NULL,
	[ReturnReceiptAddress] [varchar](100) NULL,
	[EMailSubject] [varchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[MessageDate] [datetime] NULL,
	[SentDateDimID] [int] NULL,
	[SentDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[JobReturnReceipt] [bit] NULL,
	[ReturnReceipt] [bit] NULL,
	[Sent] [bit] NULL,
	[Opened] [bit] NULL,
	[OpenedDate] [datetime] NULL,
	[Completed] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[OptOut] [bit] NULL,
	[InvalidAddress] [bit] NULL,
	[ClickedThrough] [bit] NULL,
	[AddressBookID] [int] NULL,
	[ERREID] [int] NULL,
	[EMailAddress] [varchar](100) NULL,
	[EMailFromAddress] [varchar](100) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[JobNameGroup] [varchar](100) NULL,
	[EMailGroupDimID] [int] NULL,
	[EMailJobRecipientSystemID] [int] NULL,
	[OnlineDonationTransactionFactID] [int] NULL,
 CONSTRAINT [PK_FACT_EMailStats] PRIMARY KEY CLUSTERED 
(
	[EMailStatsFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_EventExpense]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_EventExpense](
	[EventExpenseFactID] [int] IDENTITY(1,1) NOT NULL,
	[EventExpenseSystemID] [int] NOT NULL,
	[EventDimID] [int] NULL,
	[EventSystemID] [int] NULL,
	[AmountPaid] [money] NULL,
	[BudgetedAmount] [money] NULL,
	[ExpenseAmount] [money] NULL,
	[ExpenseDate] [datetime] NULL,
	[ExpenseDateDimID] [int] NULL,
	[ExpenseType] [varchar](100) NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_EventExpense] PRIMARY KEY CLUSTERED 
(
	[EventExpenseFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_EventParticipant]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_EventParticipant](
	[EventParticipantFactID] [int] NOT NULL,
	[EventParticipantSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[SponsorConstituentDimID] [int] NULL,
	[SponsorConstituentSystemID] [int] NULL,
	[GuestOfConstituentDimID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[EventDimID] [int] NULL,
	[EventSystemID] [int] NULL,
	[Attended] [char](3) NOT NULL,
	[Participation] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[Registration] [varchar](30) NULL,
	[Invitation] [varchar](30) NULL,
	[Response] [varchar](100) NULL,
	[EventStartDateDimID] [int] NULL,
	[InvitationDate] [datetime] NULL,
	[InvitationDateDimID] [int] NULL,
	[RegistrationDate] [datetime] NULL,
	[RegistrationDateDimID] [int] NULL,
	[ResponseDate] [datetime] NULL,
	[ResponseDateDimID] [int] NULL,
	[Type] [varchar](100) NULL,
	[IsCoordinator] [varchar](3) NULL,
	[IsInstructor] [varchar](3) NULL,
	[IsOnlineReg] [varchar](3) NULL,
	[IsParticipant] [varchar](3) NULL,
	[IsSpeaker] [varchar](3) NULL,
	[IsVendor] [varchar](3) NULL,
	[IsVolunteer] [varchar](3) NULL,
	[NumberOfGuests] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_EventParticipant_EventParticipantFactID] PRIMARY KEY CLUSTERED 
(
	[EventParticipantFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_EventParticipantAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_EventParticipantAttribute](
	[EventParticipantAttributeFactID] [int] NOT NULL,
	[AttributeSystemID] [int] NOT NULL,
	[EventDimID] [int] NULL,
	[EventParticipantFactID] [int] NOT NULL,
	[EventParticipantSystemID] [int] NOT NULL,
	[AttributeCategory] [varchar](50) NOT NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[Sequence] [smallint] NOT NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NOT NULL,
	[MustBeUnique] [varchar](3) NOT NULL,
	[ETLControlID] [int] NOT NULL,
	[SourceID] [int] NOT NULL,
 CONSTRAINT [PK_FACT_EventParticipantAttribute] PRIMARY KEY CLUSTERED 
(
	[EventParticipantAttributeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_EventParticipantFee]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_EventParticipantFee](
	[EventParticipantFeeFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[EventDimID] [int] NULL,
	[EventParticipantFactID] [int] NULL,
	[FeeDateDimID] [int] NULL,
	[FuzzyFeeDate] [varchar](8) NULL,
	[Fee] [numeric](20, 4) NULL,
	[NumberOfUnits] [int] NULL,
	[ReceiptAmount] [numeric](20, 4) NULL,
	[Sequence] [int] NULL,
	[EventParticipantSystemID] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_EventParticipantFee] PRIMARY KEY CLUSTERED 
(
	[EventParticipantFeeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_EventParticipantGift]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_EventParticipantGift](
	[EventParticipantGiftFactID] [int] NOT NULL,
	[EventParticipantFactID] [int] NULL,
	[EventParticipantSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[EventDimID] [int] NULL,
	[ParticipantConstituentDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[PaymentTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[ConstituentCodeDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[EventStartDateDimID] [int] NULL,
	[ParticipantGiftType] [varchar](50) NULL,
	[AppliedAmount] [money] NULL,
	[EventToGiftLagDays] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_EventParticipant_EventParticipantGiftFactID] PRIMARY KEY CLUSTERED 
(
	[EventParticipantGiftFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GEGrantAdjustment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GEGrantAdjustment](
	[GEGrantAdjustmentFactID] [int] NOT NULL,
	[GrantAdjustmentSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[GEGrantSystemID] [int] NULL,
	[GEFundSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[GEGrantPaymentFactID] [int] NULL,
	[InvoiceFactID] [int] NULL,
	[FEInvoiceSystemID] [int] NULL,
	[VendorDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[PaymentSystemID] [int] NULL,
	[PaymentComponentSystemID] [int] NULL,
	[AdjustmentType] [varchar](50) NULL,
	[AdjustmentDate] [datetime] NULL,
	[AdjustmentDateDimID] [int] NULL,
	[OldAmount] [money] NULL,
	[AdjustmentAmount] [money] NULL,
	[Notes] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GEGrantAdjustment] PRIMARY KEY CLUSTERED 
(
	[GEGrantAdjustmentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GEGrantPayment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GEGrantPayment](
	[GEGrantPaymentFactID] [int] NOT NULL,
	[GrantPaymentSystemID] [int] NULL,
	[GEGrantDimID] [int] NULL,
	[GrantSystemID] [int] NULL,
	[GEFundDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[GEFundSystemID] [int] NULL,
	[InvoiceFactID] [int] NULL,
	[FEInvoiceSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[PayeeConstituentDimID] [int] NULL,
	[VendorDimID] [int] NULL,
	[PaymentNumber] [int] NULL,
	[Amount] [money] NULL,
	[PayDate] [datetime] NULL,
	[PayDateDimID] [int] NULL,
	[PaymentStatus] [varchar](50) NULL,
	[DatePaid] [datetime] NULL,
	[DatePaidDimID] [int] NULL,
	[CheckNumber] [varchar](50) NULL,
	[IssueNumber] [int] NULL,
	[RefundAmount] [money] NULL,
	[DateVoided] [datetime] NULL,
	[DateVoidedDimID] [int] NULL,
	[Notes] [nvarchar](255) NULL,
	[PaymentComponentAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GEGrantPayment] PRIMARY KEY CLUSTERED 
(
	[GEGrantPaymentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GEGrantRequest]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GEGrantRequest](
	[GrantRequestFactID] [int] NOT NULL,
	[GrantRequestSystemID] [int] NULL,
	[Status] [varchar](20) NULL,
	[DateCreated] [datetime] NULL,
	[DateSubmitted] [datetime] NULL,
	[DateProcessed] [datetime] NULL,
	[ProcessUserSystemID] [int] NULL,
	[DateReviewed] [datetime] NULL,
	[ReviewUserSystemID] [int] NULL,
	[DateDeclined] [datetime] NULL,
	[DeclineUserSystemID] [int] NULL,
	[DeclineReason] [varchar](255) NULL,
	[RecipientREConstituentSystemID] [int] NULL,
	[RecipientName] [varchar](255) NULL,
	[RecipientAddress1] [varchar](255) NULL,
	[RecipientAddress2] [varchar](255) NULL,
	[RecipientCity] [varchar](255) NULL,
	[RecipientState] [varchar](255) NULL,
	[RecipientZip] [varchar](255) NULL,
	[RecipientContact] [varchar](255) NULL,
	[RecipientTitle] [varchar](255) NULL,
	[RecipientPhone] [varchar](255) NULL,
	[RecipientEmail] [varchar](255) NULL,
	[REConstituentSystemID] [int] NULL,
	[DonorEmail] [varchar](255) NULL,
	[EmailCC] [varchar](255) NULL,
	[AckName] [varchar](255) NULL,
	[AckAddress1] [varchar](255) NULL,
	[AckAddress2] [varchar](255) NULL,
	[AckCity] [varchar](255) NULL,
	[AckState] [varchar](255) NULL,
	[AckZip] [varchar](255) NULL,
	[FundAnonFlag] [bit] NULL,
	[DonorAnonFlag] [bit] NULL,
	[TotalAmount] [money] NULL,
	[NumberOfPayments] [smallint] NULL,
	[PaymentFrequency] [varchar](20) NULL,
	[Designation] [text] NULL,
	[Comments] [text] NULL,
	[AgreeFlag] [bit] NULL,
	[GEFundSystemID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[DateCreatedDimID] [int] NULL,
	[DateSubmittedDimID] [int] NULL,
	[DateProcessedDimID] [int] NULL,
	[DateReviewedDimID] [int] NULL,
	[DateDeclinedDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[RecipientConstituentDimID] [int] NULL,
	[FundDimID] [int] NULL,
 CONSTRAINT [PK_FACT_GEGrantRequest] PRIMARY KEY CLUSTERED 
(
	[GrantRequestFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP] TEXTIMAGE_ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_Gift]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_Gift](
	[GiftFactID] [int] NOT NULL,
	[GiftSystemID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSoftCreditSystemID] [int] NULL,
	[Amount] [money] NULL,
	[SplitRatio] [decimal](24, 10) NULL,
	[ReceiptAmount] [money] NULL,
	[IsAnonymous] [bit] NULL,
	[PostStatusDimID] [smallint] NULL,
	[ConstituentDimID] [int] NULL,
	[PaymentTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[ConstituentCodeDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[PostDateDimID] [int] NULL,
	[AcknowledgeDateDimID] [int] NULL,
	[ChangedDateDimID] [int] NULL,
	[AddedDateDimID] [int] NULL,
	[AppealWeek] [int] NULL,
	[Issuer] [varchar](50) NULL,
	[IssuerNumberUnits] [int] NULL,
	[IssuerSymbol] [varchar](4) NULL,
	[StockSaleDate] [datetime] NULL,
	[Reference] [varchar](255) NULL,
	[TotalGiftAmount] [money] NULL,
	[MGConstituentDimID] [int] NULL,
	[FinderNumber] [varchar](50) NULL,
	[MailingID] [varchar](50) NULL,
	[SourceCode] [varchar](50) NULL,
	[GiftAidEligible] [bit] NULL,
	[VATValue] [money] NULL,
	[IsAcknowledged] [bit] NULL,
	[DoNotAcknowledge] [bit] NULL,
	[IsReceipted] [bit] NULL,
	[DoNotReceipt] [bit] NULL,
	[IsPlannedGiftPayment] [bit] NULL,
	[IsTribute] [bit] NULL,
	[Monetary]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalGiftAmount])),
	[MonetarySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalGiftAmount])),
	[MonetarySplit]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[Amount])),
	[MonetarySplitSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[Amount])),
	[DaysSinceGiven]  AS (datediff(day,[GiftDate],getdate())),
	[Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[GiftDate],getdate()))),
	[RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[GiftDate],getdate()))),
	[OnlineDonationTransactionFactID] [int] NULL,
	[EMailJobDimID] [int] NULL,
	[EMailJobGroupDimID] [int] NULL,
	[WebPageDimID] [int] NULL,
	[IsSC] [bit] NULL,
	[CreditType] [varchar](2) NULL,
	[IsOnline] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[GiftAidClaimed] [int] NULL,
	[GiftAidAmount] [money] NULL,
	[GiftAidClaimNumber] [varchar](50) NULL,
	[GiftAidQualificationStatus] [varchar](50) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_FACTGROUP]

SET ANSI_PADDING OFF

/****** Object:  Index [PK_FACT_Gift_GiftFactID]    Script Date: 12/05/2011 12:04:37 ******/
ALTER TABLE [dbo].[FACT_Gift] ADD  CONSTRAINT [PK_FACT_Gift_GiftFactID] PRIMARY KEY CLUSTERED 
(
	[GiftFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_Gift_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_Gift_Stage](
	[GiftFactID] [int] NOT NULL,
	[GiftSystemID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSoftCreditSystemID] [int] NULL,
	[Amount] [money] NULL,
	[SplitRatio] [decimal](24, 10) NULL,
	[ReceiptAmount] [money] NULL,
	[IsAnonymous] [bit] NULL,
	[PostStatusDimID] [smallint] NULL,
	[ConstituentDimID] [int] NULL,
	[PaymentTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[ConstituentCodeDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[PostDateDimID] [int] NULL,
	[AcknowledgeDateDimID] [int] NULL,
	[ChangedDateDimID] [int] NULL,
	[AddedDateDimID] [int] NULL,
	[AppealWeek] [int] NULL,
	[Issuer] [varchar](50) NULL,
	[IssuerNumberUnits] [int] NULL,
	[IssuerSymbol] [varchar](4) NULL,
	[StockSaleDate] [datetime] NULL,
	[Reference] [varchar](255) NULL,
	[TotalGiftAmount] [money] NULL,
	[MGConstituentDimID] [int] NULL,
	[FinderNumber] [varchar](50) NULL,
	[MailingID] [varchar](50) NULL,
	[SourceCode] [varchar](50) NULL,
	[GiftAidEligible] [bit] NULL,
	[VATValue] [money] NULL,
	[IsAcknowledged] [bit] NULL,
	[DoNotAcknowledge] [bit] NULL,
	[IsReceipted] [bit] NULL,
	[DoNotReceipt] [bit] NULL,
	[IsPlannedGiftPayment] [bit] NULL,
	[IsTribute] [bit] NULL,
	[OnlineDonationTransactionFactID] [int] NULL,
	[EMailJobDimID] [int] NULL,
	[EMailJobGroupDimID] [int] NULL,
	[WebPageDimID] [int] NULL,
	[IsSC] [bit] NULL,
	[CreditType] [varchar](2) NULL,
	[IsOnline] [bit] NULL,
	[IsIncluded] [bit] NULL,
	[UserFilter1] [bit] NULL,
	[UserFilter2] [bit] NULL,
	[UserFilter3] [bit] NULL,
	[UserFilter4] [bit] NULL,
	[UserFilter5] [bit] NULL,
	[UserFilter6] [bit] NULL,
	[UserFilter7] [bit] NULL,
	[UserFilter8] [bit] NULL,
	[UserFilter9] [bit] NULL,
	[UserFilter10] [bit] NULL,
	[GiftAidClaimed] [int] NULL,
	[GiftAidAmount] [money] NULL,
	[GiftAidClaimNumber] [varchar](50) NULL,
	[GiftAidQualificationStatus] [varchar](50) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

SET ANSI_PADDING OFF

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftAttribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftAttribute](
	[GiftAttributeFactID] [int] IDENTITY(1,1) NOT NULL,
	[GiftAttributeSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftAttribute_GiftAttributeFactID] PRIMARY KEY CLUSTERED 
(
	[GiftAttributeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftAttribute_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftAttribute_Stage](
	[GiftAttributeSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[AttributeCategory] [varchar](50) NULL,
	[AttributeGroupDescription] [varchar](255) NULL,
	[AttributeGroupSequence] [smallint] NULL,
	[AttributeDescription] [varchar](255) NULL,
	[GiftSystemID] [int] NULL,
	[Sequence] [smallint] NULL,
	[Comments] [varchar](255) NULL,
	[AttributeDate] [datetime] NULL,
	[AttributeDateDimID] [int] NULL,
	[TypeOfData] [varchar](11) NULL,
	[Required] [varchar](3) NULL,
	[MustBeUnique] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftChange]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GiftChange](
	[GiftChangeFactID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[PreviousGiftSplitSystemID] [int] NULL,
	[ChangedGiftSystemID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftTypeSystemID] [int] NULL,
	[GiftSubTypeSystemID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[PreviousGiftTypeDimID] [int] NULL,
	[PreviousGiftTypeSystemID] [int] NULL,
	[PreviousGiftSubTypeSystemID] [int] NULL,
	[PreviousGiftSubTypeDimID] [int] NULL,
	[ChangedGiftTypeDimID] [int] NULL,
	[ChangedGiftTypeSystemID] [int] NULL,
	[ChangedDate] [datetime] NULL,
	[ChangedDateDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[GiftDateDimID] [int] NULL,
	[PreviousFundDimID] [int] NULL,
	[PreviousCampaignDimID] [int] NULL,
	[PreviousAppealDimID] [int] NULL,
	[PreviousFundSystemID] [int] NULL,
	[PreviousCampaignSystemID] [int] NULL,
	[PreviousAppealSystemID] [int] NULL,
	[PreviousPackageSystemID] [int] NULL,
	[PreviousAmount] [numeric](20, 4) NULL,
	[PreviousTotalAmount] [numeric](20, 4) NULL,
	[PreviousSplitPercent] [numeric](38, 18) NULL,
	[ChangedTotalAmount] [numeric](20, 4) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftChange_GiftChangeFactID] PRIMARY KEY CLUSTERED 
(
	[GiftChangeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_GiftCommitment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftCommitment](
	[GiftCommitmentFactID] [int] NOT NULL,
	[GiftFactID] [int] NOT NULL,
	[GiftSystemID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[Amount] [money] NULL,
	[BalanceAmount] [money] NULL,
	[SplitRatio] [numeric](24, 10) NULL,
	[ConstituentDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[PostStatusDimID] [int] NULL,
	[PaymentTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[NextTransactionDate] [datetime] NULL,
	[NextTransactionDateDimID] [int] NULL,
	[ExpiresOnDate] [datetime] NULL,
	[ExpiresOnDateDimID] [int] NULL,
	[InstallmentFrequencyDimID] [int] NULL,
	[GiftStatusDimID] [int] NULL,
	[TotalPaymentAmount] [money] NULL,
	[InstallmentAmount] [money] NULL,
	[InstallmentCount] [int] NULL,
	[PaymentAmount] [money] NULL,
	[PaymentCount] [int] NULL,
	[GiftCount] [int] NULL,
	[InstallmentToPaymentDays] [int] NULL,
	[CommitmentToPaymentDays] [int] NULL,
	[FirstInstallmentDate] [datetime] NULL,
	[FirstInstallmentDateDimID] [int] NULL,
	[LastInstallmentDate] [datetime] NULL,
	[LastInstallmentDateDimID] [int] NULL,
	[FirstPaymentDate] [datetime] NULL,
	[FirstPaymentDateDimID] [int] NULL,
	[LastPaymentDate] [datetime] NULL,
	[LastPaymentDateDimID] [int] NULL,
	[MissedPaymentAmount] [money] NULL,
	[MissedPaymentCount] [int] NULL,
	[FutureInstallmentAmount] [money] NULL,
	[FutureInstallmentCount] [int] NULL,
	[WriteOffAmount] [money] NULL,
	[WriteOffCount] [int] NULL,
	[IsBalancePaid] [char](3) NULL,
	[IsNPV] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[CommitmentYears]  AS (datediff(year,[GiftDate],[LastInstallmentDate])),
	[MonetaryAmount]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[Amount])),
	[MonetaryAmountSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[Amount])),
	[MonetaryPayment]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[PaymentAmount])),
	[MonetaryPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[PaymentAmount])),
	[FrequencyPayment]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[PaymentCount])),
	[FrequencyPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[PaymentCount])),
	[FrequencyInstallment]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[InstallmentCount])),
	[FrequencyInstallmentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[InstallmentCount])),
	[MonetaryWriteOff]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[WriteOffAmount])),
	[MonetaryWriteOffSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[WriteOffAmount])),
	[MonetaryBalance]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[BalanceAmount])),
	[MonetaryBalanceSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[BalanceAmount])),
	[RecencyLastPayment]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[LastPaymentDate],getdate()))),
	[RecencyLastPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[LastPaymentDate],getdate()))),
	[RecencyNextPayment]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[NextTransactionDate],getdate()))),
	[RecencyNextPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[ExpiresOnDate],getdate()))),
	[RecencyExpiration]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[NextTransactionDate],getdate()))),
	[RecencyExpirationSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[ExpiresOnDate],getdate()))),
 CONSTRAINT [PK_FACT_GiftCommitment_GiftCommitmentFactID] PRIMARY KEY CLUSTERED 
(
	[GiftCommitmentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftCommitmentByPaySplit]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftCommitmentByPaySplit](
	[GiftCommitmentByPaySplitFactID] [int] IDENTITY(1,1) NOT NULL,
	[GiftCommitmentFactID] [int] NULL,
	[GiftFactID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[Amount] [money] NULL,
	[BalanceAmount] [money] NULL,
	[SplitRatio] [numeric](24, 10) NULL,
	[ConstituentDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[PostStatusDimID] [int] NULL,
	[PaymentTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[NextTransactionDate] [datetime] NULL,
	[NextTransactionDateDimID] [int] NULL,
	[ExpiresOnDate] [datetime] NULL,
	[ExpiresOnDateDimID] [int] NULL,
	[InstallmentFrequencyDimID] [int] NULL,
	[GiftStatusDimID] [int] NULL,
	[TotalPaymentAmount] [money] NULL,
	[InstallmentAmount] [money] NULL,
	[InstallmentCount] [int] NULL,
	[PaymentAmount] [money] NULL,
	[PaymentCount] [int] NULL,
	[GiftCount] [int] NULL,
	[InstallmentToPaymentDays] [int] NULL,
	[CommitmentToPaymentDays] [int] NULL,
	[FirstInstallmentDate] [datetime] NULL,
	[FirstInstallmentDateDimID] [int] NULL,
	[LastInstallmentDate] [datetime] NULL,
	[LastInstallmentDateDimID] [int] NULL,
	[FirstPaymentDate] [datetime] NULL,
	[FirstPaymentDateDimID] [int] NULL,
	[LastPaymentDate] [datetime] NULL,
	[LastPaymentDateDimID] [int] NULL,
	[MissedPaymentAmount] [money] NULL,
	[MissedPaymentCount] [int] NULL,
	[FutureInstallmentAmount] [money] NULL,
	[FutureInstallmentCount] [int] NULL,
	[WriteOffAmount] [money] NULL,
	[WriteOffCount] [int] NULL,
	[IsBalancePaid] [char](3) NULL,
	[IsNPV] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[CommitmentYears]  AS (datediff(year,[GiftDate],[LastInstallmentDate])),
	[MonetaryAmount]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[Amount])),
	[MonetaryAmountSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[Amount])),
	[MonetaryPayment]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[PaymentAmount])),
	[MonetaryPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[PaymentAmount])),
	[FrequencyPayment]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[PaymentCount])),
	[FrequencyPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[PaymentCount])),
	[FrequencyInstallment]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[InstallmentCount])),
	[FrequencyInstallmentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[InstallmentCount])),
	[MonetaryWriteOff]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[WriteOffAmount])),
	[MonetaryWriteOffSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[WriteOffAmount])),
	[MonetaryBalance]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[BalanceAmount])),
	[MonetaryBalanceSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[BalanceAmount])),
	[RecencyLastPayment]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[LastPaymentDate],getdate()))),
	[RecencyLastPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[LastPaymentDate],getdate()))),
	[RecencyNextPayment]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[NextTransactionDate],getdate()))),
	[RecencyNextPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[ExpiresOnDate],getdate()))),
	[RecencyExpiration]  AS ([dbo].[BBBI_ReturnRange]('Recency',datediff(month,[NextTransactionDate],getdate()))),
	[RecencyExpirationSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',datediff(month,[ExpiresOnDate],getdate()))),
 CONSTRAINT [PK_FACT_GiftCommitmentByPaySplit] PRIMARY KEY CLUSTERED 
(
	[GiftCommitmentByPaySplitFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftInstallment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftInstallment](
	[GiftInstallmentFactID] [int] IDENTITY(1,1) NOT NULL,
	[GiftFactID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[SplitRatio] [numeric](24, 10) NULL,
	[FundDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[GiftCommitmentDateDimID] [int] NULL,
	[InstallmentDate] [datetime] NULL,
	[InstallmentDateDimID] [int] NULL,
	[InstallmentAmount] [money] NULL,
	[InstallmentBalance] [money] NULL,
	[GiftInstallmentSystemID] [int] NULL,
	[FirstPaymentDate] [datetime] NULL,
	[LastPaymentDate] [datetime] NULL,
	[LastWriteOffDate] [datetime] NULL,
	[PaymentDays] [int] NULL,
	[PaymentAmount] [money] NULL,
	[PaymentCount] [int] NULL,
	[WriteOffAmount] [money] NULL,
	[WriteOffCount] [int] NULL,
	[InstallmentStatus] [varchar](20) NULL,
	[InstallmentFullStatus] [varchar](200) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[InstallmentsDue]  AS ([dbo].[BBBI_ReturnRange]('Due Months',datediff(month,getdate(),[InstallmentDate]))),
	[InstallmentsDueSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Due Months',datediff(month,getdate(),[InstallmentDate]))),
	[MonetaryInstallment]  AS ([dbo].[BBBI_ReturnRange]('Monetary Balance',[InstallmentAmount])),
	[MonetaryInstallmentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Balance',[InstallmentAmount])),
	[MonetaryBalance]  AS ([dbo].[BBBI_ReturnRange]('Monetary Balance',[InstallmentBalance])),
	[MonetaryBalanceSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Balance',[InstallmentBalance])),
	[SplitRatioLastFY] [numeric](24, 10) NULL,
	[IsNPV] [bit] NULL,
	[InterestRate] [numeric](18, 10) NULL,
	[DueMonths] [int] NULL,
	[DueYears] [int] NULL,
	[InstallmentAdjustedDate] [datetime] NULL,
	[InstallmentAdjustedDateDimID] [int] NULL,
	[DueMonthsAdjusted] [int] NULL,
	[DueYearsAdjusted] [int] NULL,
	[InstallmentLastFYBalance] [money] NULL,
	[NPVDiscountRateApplied] [numeric](18, 10) NULL,
	[NPVDiscountAmount] [money] NULL,
	[DoubtfullAllowanceRate] [numeric](18, 10) NULL,
	[DoubtfullAllowanceAmount] [money] NULL,
	[NPVNetAmount] [money] NULL,
	[InstallmentsAdjustedDue]  AS ([dbo].[BBBI_ReturnRange]('Installments Due',[DueYearsAdjusted])),
	[InstallmentsAdjustedDueSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Installments Due',[DueYearsAdjusted])),
 CONSTRAINT [PK_FACT_GiftInstallment_GiftInstallmentFactID] PRIMARY KEY CLUSTERED 
(
	[GiftInstallmentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftInstallment_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftInstallment_Stage](
	[GiftFactID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[SplitRatio] [numeric](24, 10) NULL,
	[FundDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[GiftCommitmentDateDimID] [int] NULL,
	[InstallmentDate] [datetime] NULL,
	[InstallmentDateDimID] [int] NULL,
	[InstallmentAmount] [money] NULL,
	[InstallmentBalance] [money] NULL,
	[GiftInstallmentSystemID] [int] NULL,
	[FirstPaymentDate] [datetime] NULL,
	[LastPaymentDate] [datetime] NULL,
	[LastWriteOffDate] [datetime] NULL,
	[PaymentDays] [int] NULL,
	[PaymentAmount] [money] NULL,
	[PaymentCount] [int] NULL,
	[WriteOffAmount] [money] NULL,
	[WriteOffCount] [int] NULL,
	[InstallmentStatus] [varchar](20) NULL,
	[InstallmentFullStatus] [varchar](200) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[SplitRatioLastFY] [numeric](24, 10) NULL,
	[IsNPV] [bit] NULL,
	[InterestRate] [numeric](18, 10) NULL,
	[DueMonths] [int] NULL,
	[DueYears] [int] NULL,
	[InstallmentAdjustedDate] [datetime] NULL,
	[InstallmentAdjustedDateDimID] [int] NULL,
	[DueMonthsAdjusted] [int] NULL,
	[DueYearsAdjusted] [int] NULL,
	[InstallmentLastFYBalance] [money] NULL,
	[NPVDiscountRateApplied] [numeric](18, 10) NULL,
	[NPVDiscountAmount] [money] NULL,
	[DoubtfullAllowanceRate] [numeric](18, 10) NULL,
	[DoubtfullAllowanceAmount] [money] NULL,
	[NPVNetAmount] [money] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftInstallmentByPaySplit]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftInstallmentByPaySplit](
	[GiftInstallmentByPaySplitFactID] [int] IDENTITY(1,1) NOT NULL,
	[GiftInstallmentFactID] [int] NULL,
	[GiftFactID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[SplitRatio] [numeric](24, 10) NULL,
	[FundDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[GiftCodeDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[GiftCommitmentDateDimID] [int] NULL,
	[InstallmentDate] [datetime] NULL,
	[InstallmentDateDimID] [int] NULL,
	[InstallmentAmount] [money] NULL,
	[InstallmentBalance] [money] NULL,
	[GiftInstallmentSystemID] [int] NULL,
	[FirstPaymentDate] [datetime] NULL,
	[LastPaymentDate] [datetime] NULL,
	[PaymentDays] [int] NULL,
	[PaymentAmount] [money] NULL,
	[PaymentCount] [int] NULL,
	[InstallmentStatus] [varchar](20) NULL,
	[InstallmentFullStatus] [varchar](200) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[InstallmentsDue]  AS ([dbo].[BBBI_ReturnRange]('Due Months',datediff(month,getdate(),[InstallmentDate]))),
	[InstallmentsDueSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Due Months',datediff(month,getdate(),[InstallmentDate]))),
	[MonetaryInstallment]  AS ([dbo].[BBBI_ReturnRange]('Monetary Balance',[InstallmentAmount])),
	[MonetaryInstallmentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Balance',[InstallmentAmount])),
	[MonetaryBalance]  AS ([dbo].[BBBI_ReturnRange]('Monetary Balance',[InstallmentBalance])),
	[MonetaryBalanceSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Balance',[InstallmentBalance])),
	[LastWriteOffDate] [datetime] NULL,
	[WriteOffAmount] [money] NULL,
	[WriteOffCount] [int] NULL,
	[SplitRatioLastFY] [numeric](24, 10) NULL,
	[IsNPV] [bit] NULL,
	[InterestRate] [numeric](18, 10) NULL,
	[DueMonths] [int] NULL,
	[DueYears] [int] NULL,
	[InstallmentAdjustedDate] [datetime] NULL,
	[InstallmentAdjustedDateDimID] [int] NULL,
	[DueMonthsAdjusted] [int] NULL,
	[DueYearsAdjusted] [int] NULL,
	[InstallmentLastFYBalance] [money] NULL,
	[NPVDiscountRateApplied] [numeric](18, 10) NULL,
	[NPVDiscountAmount] [money] NULL,
	[DoubtfullAllowanceRate] [numeric](18, 10) NULL,
	[DoubtfullAllowanceAmount] [money] NULL,
	[NPVNetAmount] [money] NULL,
	[InstallmentsAdjustedDue]  AS ([dbo].[BBBI_ReturnRange]('Installments Due',[DueYearsAdjusted])),
	[InstallmentsAdjustedDueSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Installments Due',[DueYearsAdjusted])),
 CONSTRAINT [PK_FACT_GiftInstallmentByPaySplit] PRIMARY KEY CLUSTERED 
(
	[GiftInstallmentByPaySplitFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftInstallmentPayment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftInstallmentPayment](
	[GiftInstallmentPaymentFactID] [int] IDENTITY(1,1) NOT NULL,
	[GiftCommitmentSystemID] [int] NULL,
	[GiftCommitmentConstituentDimID] [int] NULL,
	[GiftCommitmentDateDimID] [int] NULL,
	[GiftCommitmentTypeDimID] [int] NULL,
	[GiftCommitmentSubTypeDimID] [int] NULL,
	[GiftCommitmentCodeDimID] [int] NULL,
	[GiftPaymentFactID] [int] NULL,
	[GiftPaymentSystemID] [int] NULL,
	[GiftSplitPaymentSystemID] [int] NULL,
	[GiftInstallmentSystemID] [int] NULL,
	[GiftInstallmentDateDimID] [int] NULL,
	[GiftPaymentConstituentDimID] [int] NULL,
	[GiftPaymentDateDimID] [int] NULL,
	[GiftPaymentTypeDimID] [int] NULL,
	[GiftPaymentSubTypeDimID] [int] NULL,
	[GiftPaymentCodeDimID] [int] NULL,
	[GiftPaymentPaymentTypeDimID] [int] NULL,
	[GiftPaymentPostStatusDimID] [int] NULL,
	[InstallmentToPaymentDays] [int] NULL,
	[CommitmentToPaymentDays] [int] NULL,
	[SplitRatio] [decimal](24, 10) NULL,
	[CommitmentAmount] [money] NULL,
	[InstallmentAmount] [money] NULL,
	[PaymentAmount] [money] NULL,
	[InstallmentBalance] [money] NULL,
	[PaymentStatus] [varchar](20) NULL,
	[FundDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[IsNPV] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[MonetaryInstallment]  AS ([dbo].[BBBI_ReturnRange]('Monetary Balance',[InstallmentAmount])),
	[MonetaryInstallmentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Balance',[InstallmentAmount])),
	[MonetaryBalance]  AS ([dbo].[BBBI_ReturnRange]('Monetary Balance',[InstallmentBalance])),
	[MonetaryBalanceSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary Balance',[InstallmentBalance])),
	[MonetaryPayment]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[PaymentAmount])),
	[MonetaryPaymentSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[PaymentAmount])),
 CONSTRAINT [PK_FACT_GiftInstallmentPayment_GiftInstallmentPaymentFactID] PRIMARY KEY CLUSTERED 
(
	[GiftInstallmentPaymentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftInstallmentPayment_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftInstallmentPayment_Stage](
	[GiftCommitmentSystemID] [int] NULL,
	[GiftCommitmentConstituentDimID] [int] NULL,
	[GiftCommitmentDateDimID] [int] NULL,
	[GiftCommitmentTypeDimID] [int] NULL,
	[GiftCommitmentSubTypeDimID] [int] NULL,
	[GiftCommitmentCodeDimID] [int] NULL,
	[GiftPaymentFactID] [int] NULL,
	[GiftPaymentSystemID] [int] NULL,
	[GiftSplitPaymentSystemID] [int] NULL,
	[GiftInstallmentSystemID] [int] NULL,
	[GiftInstallmentDateDimID] [int] NULL,
	[GiftPaymentConstituentDimID] [int] NULL,
	[GiftPaymentDateDimID] [int] NULL,
	[GiftPaymentTypeDimID] [int] NULL,
	[GiftPaymentSubTypeDimID] [int] NULL,
	[GiftPaymentCodeDimID] [int] NULL,
	[GiftPaymentPaymentTypeDimID] [int] NULL,
	[GiftPaymentPostStatusDimID] [int] NULL,
	[InstallmentToPaymentDays] [int] NULL,
	[CommitmentToPaymentDays] [int] NULL,
	[SplitRatio] [decimal](24, 10) NULL,
	[CommitmentAmount] [money] NULL,
	[InstallmentAmount] [money] NULL,
	[PaymentAmount] [money] NULL,
	[InstallmentBalance] [money] NULL,
	[PaymentStatus] [varchar](20) NULL,
	[FundDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[IsNPV] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftLatency]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GiftLatency](
	[GiftFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[Rank] [int] NULL,
	[Latency] [int] NULL,
	[Latency1] [int] NULL,
	[Latency2] [int] NULL,
	[Latency3] [int] NULL,
	[Latency4] [int] NULL,
	[Latency5] [int] NULL,
	[Latency6] [int] NULL,
	[Latency7] [int] NULL,
	[Latency8] [int] NULL,
	[Latency9] [int] NULL,
	[Latency10] [int] NULL,
 CONSTRAINT [PK_FACT_GiftLatency_GiftFactID] PRIMARY KEY CLUSTERED 
(
	[GiftFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

/****** Object:  Table [dbo].[FACT_GiftOther]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftOther](
	[GiftOtherFactID] [int] NOT NULL,
	[GiftOtherSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ProspectSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[FuzzyDateGiven] [varchar](8) NULL,
	[DateGiven] [datetime] NULL,
	[DateGivenDimID] [int] NULL,
	[Amount] [numeric](30, 6) NULL,
	[OrganizationName] [varchar](255) NULL,
	[OrganizationID] [int] NULL,
	[OrganizationType] [varchar](100) NULL,
	[Comments] [varchar](1000) NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChangedDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[Reason] [varchar](50) NULL,
	[GiftOtherImportID] [varchar](20) NULL,
	[WealthFinderHash] [varchar](255) NULL,
	[WealthFinderPartialHash] [varchar](255) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftOther] PRIMARY KEY CLUSTERED 
(
	[GiftOtherFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftPlanned]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftPlanned](
	[GiftSystemID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[PlannedGiftStatus] [varchar](100) NULL,
	[PolicyType] [varchar](100) NULL,
	[PolicyNumber] [varchar](128) NULL,
	[PolicyFaceAmount] [money] NULL,
	[GiftVehicle] [varchar](100) NULL,
	[GiftTermType] [varchar](100) NULL,
	[InstallmentFrequency] [varchar](100) NULL,
	[InstallmentFrequencyDimID] [int] NULL,
	[RemainderValue] [numeric](20, 4) NULL,
	[RemainderAsOfDateDimID] [int] NULL,
	[ExpectedMaturityYear] [smallint] NULL,
	[PayoutPercent] [numeric](20, 4) NULL,
	[PayoutAmount] [money] NULL,
	[DiscountRate] [numeric](20, 4) NULL,
	[NetPresentValue] [numeric](20, 4) NULL,
	[TermEndDate] [datetime] NULL,
	[TermEndDateDimID] [int] NULL,
	[YearsInTerm] [smallint] NULL,
	[IsFlexibleDeferred] [bit] NULL,
	[IsRealized] [bit] NULL,
	[IsRevocable] [bit] NULL,
	[NPVAsOfDateDimID] [int] NULL,
	[ConstituentIsBeneficiary] [bit] NULL,
	[ConstituentIsPolicyOwner] [bit] NULL,
	[ConsitutentPaysPremium] [bit] NULL,
	[PremiumFullyPaid] [bit] NULL,
	[InsuranceCarrier] [varchar](255) NULL,
	[DateOfFirstPremium] [datetime] NULL,
	[DateOfFirstPremiumDimID] [int] NULL,
	[PremiumStartDate] [datetime] NULL,
	[PremiumStartDateDimID] [nchar](10) NULL,
	[PremiumEndDate] [datetime] NULL,
	[PremiumEndDateDimID] [int] NULL,
	[PremiumMonth] [smallint] NULL,
	[PremiumDayOfMonth] [smallint] NULL,
	[PremiumFrequency] [varchar](25) NULL,
	[PremiumSpacing] [smallint] NULL,
	[PIFName] [varchar](100) NULL,
	[PIFNumberOfUnits] [int] NULL,
	[PIFTotalUnits] [int] NULL,
	[TrustTaxID] [varchar](20) NULL,
	[ConstituentAddressSystemID] [int] NULL,
	[ConstituentAddressDimID] [int] NULL,
	[FirstPaymentDate] [datetime] NULL,
	[FirstPaymentDateDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftPlanned] PRIMARY KEY CLUSTERED 
(
	[GiftSystemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftPlannedAsset]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftPlannedAsset](
	[GiftPlannedAssetFactID] [int] NOT NULL,
	[GiftPlannedAssetSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[AssetType] [varchar](100) NULL,
	[AssetDescription] [varchar](100) NULL,
	[AssetAmount] [money] NULL,
	[AssetCostBasis] [money] NULL,
	[Sequence] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftPlannedAsset] PRIMARY KEY CLUSTERED 
(
	[GiftPlannedAssetFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftPlannedBeneficiary]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftPlannedBeneficiary](
	[GiftPlannedBeneficiaryFactID] [int] NOT NULL,
	[GiftPlannedBeneficiarySystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[BeneficiaryConstituentDimID] [int] NULL,
	[ConstituentEducationDimID] [int] NULL,
	[ConstituentRelationshipDimID] [int] NULL,
	[BeneficiaryFullName] [varchar](110) NULL,
	[BeneficiaryRelationship] [varchar](100) NULL,
	[BeneficiaryRelationshipType] [varchar](100) NULL,
	[BeneficiaryAge] [int] NULL,
	[BeneficiaryType] [varchar](100) NULL,
	[BeneficiaryBank] [varchar](100) NULL,
	[BeneficiaryEducationSystemID] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftPlannedBeneficiary] PRIMARY KEY CLUSTERED 
(
	[GiftPlannedBeneficiaryFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftPlannedRelationship]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftPlannedRelationship](
	[GiftPlannedRelationshipFactID] [int] NOT NULL,
	[GiftPlannedRelationshipSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[RelatedConstituentDimID] [int] NULL,
	[RelatedFullName] [varchar](110) NULL,
	[Relationship] [varchar](100) NULL,
	[RelationshipType] [varchar](100) NULL,
	[Position] [varchar](100) NULL,
	[PrimaryBusiness] [varchar](100) NULL,
	[RelatedBank] [varchar](100) NULL,
	[RelatedEducationSystemID] [int] NULL,
	[SourceID] [int] NULL,
	[ETLControlID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftPlannedRelationship] PRIMARY KEY CLUSTERED 
(
	[GiftPlannedRelationshipFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftProposal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GiftProposal](
	[GiftProposalFactID] [int] NOT NULL,
	[GiftProposalSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ProposalDimID] [int] NULL,
	[ProposalConstituentDimID] [int] NULL,
	[ProposalAskToGiftDays] [int] NULL,
	[ProposalExpectedToGiftDays] [int] NULL,
	[GiftAmount] [numeric](20, 4) NULL,
	[GiftSplitAmount] [numeric](20, 4) NULL,
	[SplitRatio] [numeric](38, 18) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftProposal] PRIMARY KEY CLUSTERED 
(
	[GiftProposalFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_GiftRank]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GiftRank](
	[GiftFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[AllFirstSequence] [int] NULL,
	[AllLastSequence] [int] NULL,
	[AllHighestSequence] [int] NULL,
	[AllFirstRank] [int] NULL,
	[AllLastRank] [int] NULL,
	[AllHighestRank] [int] NULL,
	[AllHighestRankSplit] [int] NULL,
	[AllHighestSequenceSplit] [int] NULL,
	[Filter1HasSequence] [bit] NULL,
	[Filter1FirstSequence] [int] NULL,
	[Filter1LastSequence] [int] NULL,
	[Filter1HighestSequence] [int] NULL,
	[Filter1HighestSequenceSplit] [int] NULL,
	[Filter1FirstRank] [int] NULL,
	[Filter1LastRank] [int] NULL,
	[Filter1HighestRank] [int] NULL,
	[Filter1HighestRankSplit] [int] NULL,
	[Filter2FirstSequence] [int] NULL,
	[Filter2LastSequence] [int] NULL,
	[Filter2HighestSequence] [int] NULL,
	[Filter2HighestSequenceSplit] [int] NULL,
	[Filter2HasSequence] [bit] NULL,
	[Filter2FirstRank] [int] NULL,
	[Filter2LastRank] [int] NULL,
	[Filter2HighestRank] [int] NULL,
	[Filter3HasSequence] [bit] NULL,
	[Filter3FirstSequence] [int] NULL,
	[Filter3LastSequence] [int] NULL,
	[Filter3HighestSequence] [int] NULL,
	[Filter3HighestSequenceSplit] [int] NULL,
	[Filter3FirstRank] [int] NULL,
	[Filter3LastRank] [int] NULL,
	[Filter3HighestRank] [int] NULL,
	[Filter3HighestRankSplit] [int] NULL,
	[Filter4HasSequence] [bit] NULL,
	[Filter4FirstSequence] [int] NULL,
	[Filter4LastSequence] [int] NULL,
	[Filter4HighestSequence] [int] NULL,
	[Filter4HighestSequenceSplit] [int] NULL,
	[Filter4FirstRank] [int] NULL,
	[Filter4LastRank] [int] NULL,
	[Filter4HighestRank] [int] NULL,
	[Filter4HighestRankSplit] [int] NULL,
	[Filter5HasSequence] [bit] NULL,
	[Filter5FirstSequence] [int] NULL,
	[Filter5LastSequence] [int] NULL,
	[Filter5HighestSequence] [int] NULL,
	[Filter5HighestSequenceSplit] [int] NULL,
	[Filter5FirstRank] [int] NULL,
	[Filter5LastRank] [int] NULL,
	[Filter5HighestRank] [int] NULL,
	[Filter5HighestRankSplit] [int] NULL,
	[Filter6HasSequence] [bit] NULL,
	[Filter6FirstSequence] [int] NULL,
	[Filter6LastSequence] [int] NULL,
	[Filter6HighestSequence] [int] NULL,
	[Filter6HighestSequenceSplit] [int] NULL,
	[Filter6FirstRank] [int] NULL,
	[Filter6LastRank] [int] NULL,
	[Filter6HighestRank] [int] NULL,
	[Filter6HighestRankSplit] [int] NULL,
	[Filter7FirstSequence] [int] NULL,
	[Filter7LastSequence] [int] NULL,
	[Filter7HighestSequence] [int] NULL,
	[Filter7HighestSequenceSplit] [int] NULL,
	[Filter7HasSequence] [bit] NULL,
	[Filter7FirstRank] [int] NULL,
	[Filter7LastRank] [int] NULL,
	[Filter7HighestRank] [int] NULL,
	[Filter8HasSequence] [bit] NULL,
	[Filter8FirstSequence] [int] NULL,
	[Filter8LastSequence] [int] NULL,
	[Filter8HighestSequence] [int] NULL,
	[Filter8HighestSequenceSplit] [int] NULL,
	[Filter8FirstRank] [int] NULL,
	[Filter8LastRank] [int] NULL,
	[Filter8HighestRank] [int] NULL,
	[Filter8HighestRankSplit] [int] NULL,
	[Filter9HasSequence] [bit] NULL,
	[Filter9FirstSequence] [int] NULL,
	[Filter9LastSequence] [int] NULL,
	[Filter9HighestSequence] [int] NULL,
	[Filter9HighestSequenceSplit] [int] NULL,
	[Filter9FirstRank] [int] NULL,
	[Filter9LastRank] [int] NULL,
	[Filter9HighestRank] [int] NULL,
	[Filter9HighestRankSplit] [int] NULL,
	[Filter10HasSequence] [bit] NULL,
	[Filter10FirstSequence] [int] NULL,
	[Filter10LastSequence] [int] NULL,
	[Filter10HighestSequence] [int] NULL,
	[Filter10HighestSequenceSplit] [int] NULL,
	[Filter10FirstRank] [int] NULL,
	[Filter10LastRank] [int] NULL,
	[Filter10HighestRank] [int] NULL,
	[Filter10HighestRankSplit] [int] NULL,
 CONSTRAINT [PK_FACT_GiftRank] PRIMARY KEY CLUSTERED 
(
	[GiftFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_GiftSoftCredit]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GiftSoftCredit](
	[GiftSoftCreditFactID] [int] NOT NULL,
	[GiftSoftCreditSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[SplitRatio] [numeric](18, 4) NULL,
	[Amount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceId] [int] NULL,
 CONSTRAINT [PK_FACT_GiftSoftCredit_GiftSoftCreditFactID] PRIMARY KEY CLUSTERED 
(
	[GiftSoftCreditFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_GiftTribute]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftTribute](
	[GiftTributeFactID] [int] NOT NULL,
	[GiftFactID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[GiftTributeSystemID] [int] NULL,
	[TributeSystemID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[TributeConstituentSystemID] [int] NULL,
	[TributeConstituentDimID] [int] NULL,
	[TributeType] [varchar](100) NULL,
	[GiftTributeAcknowledgeeSystemID] [int] NULL,
	[Acknowledgement] [varchar](50) NULL,
	[Letter] [varchar](100) NULL,
	[SelfAcknowledge] [varchar](3) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftTribute_GiftTributeFactID] PRIMARY KEY CLUSTERED 
(
	[GiftTributeFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GiftWriteOff]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GiftWriteOff](
	[GiftWriteOffFactID] [int] NOT NULL,
	[GiftFactID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[GiftCommitmentSystemID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[Amount] [money] NULL,
	[SplitRatio] [decimal](24, 10) NULL,
	[IsAnonymous] [bit] NULL,
	[ConstituentDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[ConstituentCodeDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[GiftDate] [datetime] NULL,
	[WriteOffDateDimID] [int] NULL,
	[WriteOffDate] [datetime] NULL,
	[GiftCodeDimID] [int] NULL,
	[LetterCodeDimID] [int] NULL,
	[PaymentTypeDimID] [int] NULL,
	[PostStatusDimID] [smallint] NULL,
	[PostDateDimID] [int] NULL,
	[ChangedDateDimID] [int] NULL,
	[AddedDateDimID] [int] NULL,
	[AppealWeek] [int] NULL,
	[TotalGiftAmount] [money] NULL,
	[MGConstituentDimID] [int] NULL,
	[FinderNumber] [varchar](50) NULL,
	[MailingID] [varchar](50) NULL,
	[SourceCode] [varchar](50) NULL,
	[UserFilter1] [tinyint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GiftWriteOff_GiftWriteOffFactID] PRIMARY KEY CLUSTERED 
(
	[GiftWriteOffFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GLTransactionDistribution]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_GLTransactionDistribution](
	[TransactionDistributionFactID] [int] NOT NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[AccountCode] [varchar](30) NULL,
	[AccountNumber] [varchar](30) NULL,
	[AccountDescription] [varchar](60) NULL,
	[AccountCategorySystemID] [int] NULL,
	[AccountCategory] [varchar](20) NULL,
	[FundDimID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[ProjectID] [varchar](12) NULL,
	[ProjectDescription] [varchar](60) NULL,
	[FundID] [varchar](50) NULL,
	[FundDescription] [varchar](60) NULL,
	[PostDateDimID] [int] NULL,
	[PostDate] [datetime] NULL,
	[PostStatusDimID] [smallint] NULL,
	[IsEncumbrance] [varchar](3) NULL,
	[TransactionTypeDimID] [smallint] NULL,
	[TransactionType] [varchar](7) NULL,
	[TransactionCode1DimID] [int] NULL,
	[TransactionCode2DimID] [int] NULL,
	[TransactionCode3DimID] [int] NULL,
	[TransactionCode4DimID] [int] NULL,
	[TransactionCode5DimID] [int] NULL,
	[Amount] [money] NULL,
	[NaturalAmount] [money] NULL,
	[TransactionNumber] [varchar](18) NULL,
	[TransactionDistributionSystemID] [int] NULL,
	[TransactionSystemID] [int] NULL,
	[FiscalPeriodsSystemID] [int] NULL,
	[PostStatus] [varchar](20) NULL,
	[JournalDimID] [int] NULL,
	[Journal] [varchar](60) NULL,
	[JournalReference] [varchar](100) NULL,
	[BatchNumber] [varchar](50) NULL,
	[BatchDescription] [varchar](100) NULL,
	[BatchStatus] [varchar](20) NULL,
	[SourceRecordsID] [int] NULL,
	[GLSourceTypeDimID] [int] NULL,
	[SourceNumber] [varchar](20) NULL,
	[SourceType] [int] NULL,
	[SourceTypeName] [varchar](50) NULL,
	[SourceTypeGroup] [varchar](50) NULL,
	[TransactionDateAdded] [datetime] NULL,
	[TransactionDateChanged] [datetime] NULL,
	[ProjectPeriodSequence] [int] NULL,
	[ClassDimID] [int] NULL,
	[ClassSystemID] [int] NULL,
	[ClassDescription] [varchar](60) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_GLTransactionDistribution] PRIMARY KEY CLUSTERED 
(
	[TransactionDistributionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_GLTransactionDistributionBalance]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GLTransactionDistributionBalance](
	[TransactionDistributionFactID] [int] NULL,
	[Amount] [money] NULL,
	[Balance] [money] NULL
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_GLTransactionDistributionRank]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_GLTransactionDistributionRank](
	[TransactionDistributionFactID] [int] NULL,
	[Amount] [money] NULL,
	[AccountProjectRank] [int] NULL,
	[AccountProjectTCodeID] [int] NULL
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_Invoice]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[FACT_Invoice](
	[InvoiceFactID] [int] NOT NULL,
	[InvoiceSystemID] [int] NULL,
	[InvoiceNumber] [varchar](20) NULL,
	[PONumber] [varchar](20) NULL,
	[InvoiceDescription] [varchar](60) NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceDateDimID] [int] NULL,
	[DueDate] [datetime] NULL,
	[DueDateDimID] [int] NULL,
	[PostDate] [datetime] NULL,
	[PostDateDimID] [int] NULL,
	[PostStatus] [varchar](19) NULL,
	[Status] [varchar](14) NULL,
	[ConstituentDimID] [int] NULL,
	[VendorDimID] [int] NULL,
	[VendorSystemID] [int] NULL,
	[VendorName] [varchar](60) NULL,
	[ReversePostStatus] [varchar](14) NULL,
	[ReversePostDate] [datetime] NULL,
	[ReversePostDateDimID] [int] NULL,
	[PostStatusDimID] [smallint] NULL,
	[ReversePostStatusDimID] [smallint] NULL,
	[InvoiceAmount] [numeric](19, 4) NULL,
	[InvoiceBalance] [numeric](19, 4) NULL,
	[TaxAmount] [numeric](19, 4) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_Invoice] PRIMARY KEY CLUSTERED 
(
	[InvoiceFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_InvoicePayment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_InvoicePayment](
	[InvoicePaymentFactID] [int] NOT NULL,
	[InvoicePaymentSystemID] [int] NULL,
	[InvoicePaymentStatus] [varchar](25) NULL,
	[PaidByCreditMemo] [varchar](3) NULL,
	[InvoicePaymentDate] [datetime] NULL,
	[InvoicePaymentDateDimID] [int] NULL,
	[Amount] [money] NULL,
	[DiscountAmount] [money] NULL,
	[CheckAmount] [money] NULL,
	[InvoiceFactID] [int] NULL,
	[InvoiceNumber] [varchar](20) NULL,
	[InvoiceSystemID] [int] NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceDateDimID] [int] NULL,
	[InvoicePaymentDays] [int] NULL,
	[VendorDimID] [int] NULL,
	[VendorSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[CheckDate] [datetime] NULL,
	[CheckDateDimID] [int] NULL,
	[CheckPostDate] [datetime] NULL,
	[CheckPostDateDimID] [int] NULL,
	[CheckType] [varchar](14) NULL,
	[CheckNumber] [int] NULL,
	[CheckNotes] [varchar](95) NULL,
	[CheckPayeeName] [varchar](100) NULL,
	[CheckContactName] [varchar](60) NULL,
	[CheckPostStatusDimID] [smallint] NULL,
	[CheckIsCleared] [varchar](3) NULL,
	[CheckClearedDate] [datetime] NULL,
	[CheckClearedDateDimID] [int] NULL,
	[CheckIsVoided] [varchar](3) NULL,
	[CheckVoidedDate] [datetime] NULL,
	[CheckVoidedDateDimID] [int] NULL,
	[CheckIsReconciled] [varchar](3) NULL,
	[CheckReconciledDate] [datetime] NULL,
	[CheckReconciledDateDimID] [int] NULL,
	[BankSystemID] [int] NULL,
	[BankAccountNumber] [varchar](20) NULL,
	[BankName] [varchar](70) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_InvoicePayment_InvoicePaymentFactID] PRIMARY KEY CLUSTERED 
(
	[InvoicePaymentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_LTMembership]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[FACT_LTMembership](
	[MembershipAnnualConsecutiveFactID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[MembershipDimID] [int] NULL,
	[ConsecutiveYears] [int] NULL,
	[TotalAmount] [money] NULL,
	[AvgAmount] [money] NULL,
	[MaxAmount] [money] NULL,
	[MinAmount] [money] NULL,
	[TransactionCount] [int] NULL,
	[FirstTransactionDate] [datetime] NULL,
	[FirstTransactionDateDimID] [int] NULL,
	[LastTransactionDate] [datetime] NULL,
	[LastTransactionDateDimID] [int] NULL,
	[TransactionYears] [smallint] NULL,
	[TransactionMonths] [int] NULL,
	[TransactionDays] [int] NULL,
	[YearsSinceTransaction] [smallint] NULL,
	[MonthsSinceTransaction] [int] NULL,
	[DaysSinceTransaction] [int] NULL,
	[DistinctTransactionYears] [smallint] NULL,
	[DistinctTransactionFiscalYears] [smallint] NULL,
	[AnnualRenewer] [varchar](1) NULL,
	[AnnualFiscalRenewer] [varchar](1) NULL,
	[FrequencySM] [varchar](20) NULL,
	[LastTransactionAmount] [money] NULL,
	[FirstTransactionAmount] [money] NULL,
	[MaxTransactionDate] [datetime] NULL,
	[MaxTransactionDateDimID] [int] NULL,
	[AvgLatency] [int] NULL,
	[MaxLatency] [int] NULL,
	[MinLatency] [int] NULL
) ON [BBPM_FACTGROUP]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[FACT_LTMembership] ADD [Recency]  AS ([dbo].[BBBI_ReturnRange]('Recency',[MonthsSinceTransaction]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [Frequency]  AS ([dbo].[BBBI_ReturnRange]('Frequency',[TransactionCount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [MonetaryTotal]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[TotalAmount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [MonetaryMax]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[MaxAmount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [MonetaryLast]  AS ([dbo].[BBBI_ReturnRange]('Monetary',[LastTransactionAmount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [RecencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Recency',[MonthsSinceTransaction]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [FrequencySequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Frequency',[TransactionCount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [MonetaryTotalSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[TotalAmount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [MonetaryMaxSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[MaxAmount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [MonetaryLastSequence]  AS ([dbo].[BBBI_ReturnRangeSequence]('Monetary',[LastTransactionAmount]))
ALTER TABLE [dbo].[FACT_LTMembership] ADD [ETLControlID] [int] NULL
ALTER TABLE [dbo].[FACT_LTMembership] ADD [SourceID] [int] NULL

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_LTMembershipLatency]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_LTMembershipLatency](
	[TransactionFactID] [int] NOT NULL,
	[MembershipDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[Rank] [int] NULL,
	[Latency] [int] NULL,
 CONSTRAINT [PK_FACT_LTMembershipLatency_TransactionFactID] PRIMARY KEY CLUSTERED 
(
	[TransactionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_LTMembershipRank]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_LTMembershipRank](
	[TransactionFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[MembershipDimID] [int] NULL,
	[TransactionDate] [datetime] NULL,
	[TransactionDateDimID] [int] NULL,
	[AllFirstSequence] [int] NULL,
	[AllLastSequence] [int] NULL,
	[AllHighestSequence] [int] NULL,
	[AllFirstRank] [int] NULL,
	[AllLastRank] [int] NULL,
	[AllHighestRank] [int] NULL,
	[Filter1FirstSequence] [int] NULL,
	[Filter1LastSequence] [int] NULL,
	[Filter1HighestSequence] [int] NULL,
	[Filter1HasSequence] [bit] NULL,
	[Filter1FirstRank] [int] NULL,
	[Filter1LastRank] [int] NULL,
	[Filter1HighestRank] [int] NULL,
	[Filter2HasSequence] [bit] NULL,
	[Filter2FirstSequence] [int] NULL,
	[Filter2LastSequence] [int] NULL,
	[Filter2HighestSequence] [int] NULL,
	[Filter2FirstRank] [int] NULL,
	[Filter2LastRank] [int] NULL,
	[Filter2HighestRank] [int] NULL,
 CONSTRAINT [PK_FACT_LTMembershipRank] PRIMARY KEY CLUSTERED 
(
	[TransactionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_MembershipAnnualType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_MembershipAnnualType](
	[MembershipAnnualTypeFactID] [int] NULL,
	[MembershipDimID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[Type] [varchar](25) NULL,
	[FiscalYear] [smallint] NULL,
	[FiscalYearPrior] [smallint] NULL,
	[FiscalYearNext] [smallint] NULL,
	[RelativeYear] [smallint] NULL,
	[Rank] [int] NULL,
	[MinActivityDateDimID] [int] NULL,
	[MinActivityDate] [datetime] NULL,
	[MaxActivityDate] [datetime] NULL,
	[ExpiresOnDateDimID] [int] NULL,
	[MaxExpiresOnDate] [datetime] NULL,
	[Count] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_MembershipBenefit]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_MembershipBenefit](
	[MembershipBenefitFactID] [int] NOT NULL,
	[MembershipsBenefitSystemID] [int] NULL,
	[MembershipTransactionSystemID] [int] NULL,
	[MembershipTransactionFactID] [int] NULL,
	[MembershipDimID] [int] NULL,
	[MembershipCategoryDimID] [int] NULL,
	[MembershipSubCategoryDimID] [int] NULL,
	[Benefit] [varchar](100) NULL,
	[BenefitFulfilledDate] [datetime] NULL,
	[BenefitFulfilledDateDimID] [int] NULL,
	[TotalBenefitValue] [money] NULL,
	[Quantity] [smallint] NULL,
	[UnitCost] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_MembershipBenefit_MembershipBenefitFactID] PRIMARY KEY CLUSTERED 
(
	[MembershipBenefitFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_MembershipLatency]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_MembershipLatency](
	[TransactionFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[Rank] [int] NULL,
	[Latency] [int] NULL,
 CONSTRAINT [PK_FACT_MembershipLatency_TransactionFactID] PRIMARY KEY CLUSTERED 
(
	[TransactionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_MembershipLinkedGift]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_MembershipLinkedGift](
	[MembershipLinkedGiftFactID] [int] NOT NULL,
	[MembershipLinkedGiftSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[MembershipTransactionSystemID] [int] NULL,
	[MembershipTransactionFactID] [int] NULL,
	[AppliedSplitAmount] [money] NULL,
	[AppliedAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_MembershipLinkedGift_MembershipLinkedGiftFactID] PRIMARY KEY CLUSTERED 
(
	[MembershipLinkedGiftFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_MembershipRank]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_MembershipRank](
	[TransactionFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[MembershipDimID] [int] NULL,
	[TransactionDate] [datetime] NULL,
	[TransactionDateDimID] [int] NULL,
	[AllFirstSequence] [int] NULL,
	[AllLastSequence] [int] NULL,
	[AllHighestSequence] [int] NULL,
	[AllFirstRank] [int] NULL,
	[AllLastRank] [int] NULL,
	[AllHighestRank] [int] NULL,
	[Filter1FirstSequence] [int] NULL,
	[Filter1LastSequence] [int] NULL,
	[Filter1HighestSequence] [int] NULL,
	[Filter1HasSequence] [bit] NULL,
	[Filter1FirstRank] [int] NULL,
	[Filter1LastRank] [int] NULL,
	[Filter1HighestRank] [int] NULL,
	[Filter2FirstSequence] [int] NULL,
	[Filter2LastSequence] [int] NULL,
	[Filter2HighestSequence] [int] NULL,
	[Filter2HasSequence] [bit] NULL,
	[Filter2FirstRank] [int] NULL,
	[Filter2LastRank] [int] NULL,
	[Filter2HighestRank] [int] NULL,
 CONSTRAINT [PK_FACT_MembershipTransactionFactID] PRIMARY KEY CLUSTERED 
(
	[TransactionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_MembershipTransaction]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_MembershipTransaction](
	[MembershipTransactionFactID] [int] NOT NULL,
	[MembershipTransactionSystemID] [int] NULL,
	[MembershipDimID] [int] NULL,
	[JointMembershipDimID] [int] NULL,
	[MembershipSystemID] [int] NULL,
	[MembershipCategoryDimID] [int] NULL,
	[MembershipCategorySystemID] [int] NULL,
	[MembershipSubCategoryDimID] [int] NULL,
	[MembershipSubCategorySystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[ActivityDate] [datetime] NULL,
	[CardsPrinted] [varchar](3) NULL,
	[ProgramName] [varchar](100) NULL,
	[Reason] [varchar](100) NULL,
	[Dues] [numeric](19, 4) NULL,
	[ExpiresOn] [datetime] NULL,
	[SendNoticeTo] [varchar](25) NULL,
	[RenewalType] [varchar](25) NULL,
	[WaiveBenefits] [varchar](3) NULL,
	[SendBenefitsTo] [varchar](25) NULL,
	[LifeTimeMembership] [varchar](3) NULL,
	[PrintRenewals] [varchar](3) NULL,
	[Type] [varchar](25) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[ExpirationDateDimID] [int] NULL,
	[ActivityDateDimID] [int] NULL,
 CONSTRAINT [PK_FACT_MembershipTransaction_MembershipTransactionFactID] PRIMARY KEY CLUSTERED 
(
	[MembershipTransactionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_OnlineDonationTransaction]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_OnlineDonationTransaction](
	[OnlineDonationTransactionFactID] [int] NOT NULL,
	[ConstituentDimID] [int] NULL,
	[LastName] [varchar](100) NULL,
	[DonationTransactionSystemID] [int] NULL,
	[GiftPaymentMethod] [int] NULL,
	[ClientDonationsID] [int] NULL,
	[ClientsID] [int] NULL,
	[DateAdded] [datetime] NULL,
	[Status] [tinyint] NULL,
	[BackOfficeID] [varchar](100) NULL,
	[LastNameXML] [varchar](100) NULL,
	[PaymentMethod] [varchar](100) NULL,
	[FundID] [varchar](100) NULL,
	[Phone] [varchar](100) NULL,
	[ConstituentCodeID] [varchar](100) NULL,
	[Comments] [varchar](100) NULL,
	[GiftDateChar] [varchar](100) NULL,
	[EMailAddress] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[Amount] [varchar](100) NULL,
	[GiftAmount] [numeric](30, 6) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
	[EMailSystemID] [int] NULL,
	[EMailStatsFactID] [int] NULL,
	[EMailSentName] [varchar](100) NULL,
	[EMailSentSubject] [varchar](100) NULL,
	[EMailSentSendAfterDate] [datetime] NULL,
	[EMailSentSendAfterDateDimID] [int] NULL,
	[EMailSentType] [varchar](25) NULL,
 CONSTRAINT [PK_FACT_OnlineDonationTransaction] PRIMARY KEY CLUSTERED 
(
	[OnlineDonationTransactionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ProjectAccountBalance]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_ProjectAccountBalance](
	[ProjectAccountBalanceFactID] [int] IDENTITY(1,1) NOT NULL,
	[FundDimID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountCategorySystemID] [int] NULL,
	[TransactionCode1DimID] [int] NULL,
	[TransactionCode2DimID] [int] NULL,
	[TransactionCode3DimID] [int] NULL,
	[TransactionCode4DimID] [int] NULL,
	[TransactionCode5DimID] [int] NULL,
	[BalanceDate] [datetime] NULL,
	[BalanceDateDimID] [int] NULL,
	[Balance] [money] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_ProjectAccountBalance] PRIMARY KEY CLUSTERED 
(
	[ProjectAccountBalanceFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_ProjectBalance]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ProjectBalance](
	[ProjectBalanceFactID] [int] NOT NULL,
	[FundDimID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[ProjectID] [varchar](20) NULL,
	[AccountCategory] [int] NULL,
	[TransactionCode1DimID] [int] NULL,
	[TransactionCode2DimID] [int] NULL,
	[TransactionCode3DimID] [int] NULL,
	[TransactionCode4DimID] [int] NULL,
	[TransactionCode5DimID] [int] NULL,
	[BalanceDate] [datetime] NULL,
	[BalanceDateDimID] [int] NULL,
	[Balance] [money] NULL,
	[BalanceSubType] [varchar](50) NULL,
	[BalanceType] [varchar](1) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_ProjectBalance] PRIMARY KEY CLUSTERED 
(
	[ProjectBalanceFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_ProjectBudget]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_ProjectBudget](
	[ProjectBudgetFactID] [int] NOT NULL,
	[ProjectBudgetDetailSystemID] [int] NOT NULL,
	[ProjectBudgetSystemID] [int] NULL,
	[ScenarioDimID] [int] NULL,
	[ScenarioSystemID] [int] NULL,
	[ScenarioID] [int] NULL,
	[ScenarioDescription] [varchar](60) NULL,
	[ScenarioSequence] [smallint] NULL,
	[AccountBudgetID] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[FiscalPeriodSystemID] [int] NULL,
	[Amount] [money] NULL,
	[NaturalAmount] [money] NULL,
	[Percent] [decimal](19, 4) NULL,
	[PeriodStartDate] [datetime] NULL,
	[PeriodStartDateDimID] [int] NULL,
	[PeriodEndDate] [datetime] NULL,
	[PeriodEndDateDimID] [int] NULL,
	[IsFiscalYearClosed] [varchar](3) NULL,
	[PeriodSequence] [smallint] NULL,
	[FiscalYearDesc] [varchar](60) NULL,
	[FiscalYearSequence] [int] NULL,
	[FiscalPeriods] [smallint] NULL,
	[FiscalYearStatus] [smallint] NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_ProjectBudget] PRIMARY KEY CLUSTERED 
(
	[ProjectBudgetFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_SolicitorAction]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_SolicitorAction](
	[SolicitorActionFactID] [int] NOT NULL,
	[SolicitorActionSystemID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[SolicitorSystemID] [int] NULL,
	[ActionFactID] [int] NULL,
	[ActionSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ActionDateDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_SolicitorAction_SolicitorActionFactID] PRIMARY KEY CLUSTERED 
(
	[SolicitorActionFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

/****** Object:  Table [dbo].[DIM_ConstituentPhone_Stage]    Script Date: 12/08/2011 14:09:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_SolicitorAction_Stage](
	[SolicitorActionSystemID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[SolicitorSystemID] [int] NULL,
	[ActionFactID] [int] NULL,
	[ActionSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ActionDateDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

/****** Object:  Table [dbo].[FACT_SolicitorActivityGoal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_SolicitorActivityGoal](
	[SolicitorActivityGoalFactID] [int] IDENTITY(1,1) NOT NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[GoalType] [varchar](50) NULL,
	[ActivityDate] [datetime] NULL,
	[ActivityDateDimID] [int] NULL,
	[Value] [decimal](12, 4) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_SolicitorActivityGoal] PRIMARY KEY CLUSTERED 
(
	[SolicitorActivityGoalFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_SolicitorAssignment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_SolicitorAssignment](
	[SolicitorAssignmentFactID] [int] NOT NULL,
	[ConstituentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[SolicitorConstituentSystemID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[SolicitorType] [varchar](100) NULL,
	[Amount] [money] NULL,
	[FuzzyDateFrom] [varchar](8) NULL,
	[FuzzyDateTo] [varchar](8) NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[DateFromDimID] [int] NULL,
	[DateToDimID] [int] NULL,
	[DateFromIsBlank] [bit] NULL,
	[DateToIsBlank] [bit] NULL,
	[IsCurrent] [bit] NULL,
	[DateAddedDimID] [int] NULL,
	[DateChangedDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_SolicitorAssignment_SolicitorAssignmentFactID] PRIMARY KEY CLUSTERED 
(
	[SolicitorAssignmentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_SolicitorGift]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_SolicitorGift](
	[SolicitorGiftFactID] [int] NOT NULL,
	[SolicitorGiftSystemID] [int] NULL,
	[GiftFactID] [int] NULL,
	[GiftSplitSystemID] [int] NULL,
	[GiftSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[GiftTypeDimID] [int] NULL,
	[GiftSubTypeDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[GiftDateDimID] [int] NULL,
	[SolicitorSystemID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[Amount] [money] NULL,
	[TotalGiftAmount] [money] NULL,
	[SolicitorSplitRatio] [numeric](24, 10) NULL,
	[ProposalAskToGiftDays] [int] NULL,
	[ProposalExpectedToGiftDays] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_SolicitorGift_SolicitorGiftFactID] PRIMARY KEY CLUSTERED 
(
	[SolicitorGiftFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_SolicitorGoal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_SolicitorGoal](
	[SolicitorGoalFactID] [int] NOT NULL,
	[SolicitorGoalSystemID] [int] NULL,
	[SolicitorConstituentSystemID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[Category] [varchar](100) NULL,
	[CampaignDimID] [int] NULL,
	[CampaignSystemID] [int] NULL,
	[CampaignID] [varchar](20) NULL,
	[FundDimID] [int] NULL,
	[FundSystemID] [int] NULL,
	[FundID] [varchar](20) NULL,
	[Goal] [numeric](30, 6) NULL,
	[Type] [varchar](12) NULL,
	[TeamID] [int] NULL,
	[TeamName] [varchar](50) NULL,
	[TeamCaptainID] [int] NULL,
	[TeamReportsToID] [int] NULL,
	[Notes] [text] NULL,
	[TeamCampaignSystemID] [int] NULL,
	[TeamCampaignDimID] [int] NULL,
	[TeamFundSystemID] [int] NULL,
	[TeamFundDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_SolicitorGoal_SolicitorGoalFactID] PRIMARY KEY CLUSTERED 
(
	[SolicitorGoalFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP] TEXTIMAGE_ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_SolicitorProposal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FACT_SolicitorProposal](
	[SolicitorProposalFactID] [int] NOT NULL,
	[SolicitorProposalSystemID] [int] NULL,
	[SolicitorSystemID] [int] NULL,
	[SolicitorConstituentDimID] [int] NULL,
	[ProposalDimID] [int] NULL,
	[ProposalSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[Sequence] [smallint] NULL,
	[OriginalDateAskedDimID] [int] NULL,
	[DateAskedDimID] [int] NULL,
	[DateExpectedDimID] [int] NULL,
	[DateFundedDimID] [int] NULL,
	[SolicitorProposalSplitRatio] [numeric](24, 10) NULL,
	[Amount] [numeric](20, 4) NULL,
	[AmountAsked] [money] NULL,
	[AmountExpected] [money] NULL,
	[AmountFunded] [money] NULL,
	[ProposalAmountAsked] [money] NULL,
	[ProposalAmountExpected] [money] NULL,
	[CampaignDimID] [int] NULL,
	[FundDimID] [int] NULL,
	[AskToFundedDays] [int] NULL,
	[ExpectedToFundedDays] [int] NULL,
	[TotalActionCount] [int] NULL,
	[TotalProposalActionCount] [int] NULL,
	[TotalProposalPreFundedActionCount] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_SolicitorProposal_SolicitorProposalFactID] PRIMARY KEY CLUSTERED 
(
	[SolicitorProposalFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO


GO

/****** Object:  Table [dbo].[FACT_VCOFilter]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_VCOFilter](
	[VCOFilterFactID] [int] NOT NULL,
	[TemplateDetailSystemID] [int] NULL,
	[TemplateSystemID] [int] NULL,
	[TemplateID] [varchar](50) NULL,
	[TemplateName] [varchar](100) NULL,
	[Category] [smallint] NULL,
	[ParentKey] [int] NULL,
	[AccountNumberKeyValue] [int] NULL,
	[AccountNumberFrom] [varchar](30) NULL,
	[AccountNumberTo] [varchar](30) NULL,
	[AccountKeyValue] [int] NULL,
	[AccountCodeFrom] [varchar](30) NULL,
	[AccountCodeTo] [varchar](30) NULL,
	[SegmentKeyValue] [int] NULL,
	[FundKeyValue] [int] NULL,
	[ProjectKeyValue] [int] NULL,
	[Filter] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_VCOFilter] PRIMARY KEY CLUSTERED 
(
	[VCOFilterFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_VolunteerAssignment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_VolunteerAssignment](
	[VolunteerAssignmentFactID] [int] NOT NULL,
	[VolunteerAssignmentSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[VolunteerJobDimID] [int] NULL,
	[VolunteerJobSystemID] [int] NULL,
	[VolunteerTypeDimID] [int] NULL,
	[VolunteerType] [varchar](100) NULL,
	[VolunteerJobCategoryDimID] [int] NULL,
	[AssignmentCategory] [varchar](100) NULL,
	[VolunteerDepartmentSystemID] [int] NULL,
	[VolunteerDepartmentDimID] [int] NULL,
	[VolunteerDepartment] [varchar](100) NULL,
	[VolunteerTaskDimID] [int] NULL,
	[VolunteerTask] [varchar](100) NULL,
	[VolunteerPosition] [varchar](50) NULL,
	[Location] [varchar](100) NULL,
	[AssignmentStartDate] [datetime] NULL,
	[AssignmentStartDateDimID] [int] NULL,
	[AssignmentStartTime] [datetime] NULL,
	[AssignmentEndDate] [datetime] NULL,
	[AssignmentEndDateDimID] [int] NULL,
	[AssignmentEndTime] [datetime] NULL,
	[Supervisor] [varchar](50) NULL,
	[Status] [varchar](15) NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_VolunteerAssignment_VolunteerAssignmentFactID] PRIMARY KEY CLUSTERED 
(
	[VolunteerAssignmentFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_VolunteerTimeSheet]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_VolunteerTimeSheet](
	[VolunteerTimeSheetFactID] [int] NOT NULL,
	[VolunteerTimeSheetSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[VolunteerJobDimID] [int] NULL,
	[VolunteerJobSystemID] [int] NULL,
	[VolunteerTypeDimID] [int] NULL,
	[VolunteerType] [varchar](100) NULL,
	[VolunteerJobCategoryDimID] [int] NULL,
	[AssignmentCategory] [varchar](100) NULL,
	[VolunteerTaskDimID] [int] NULL,
	[VolunteerTask] [varchar](100) NULL,
	[VolunteerPosition] [varchar](50) NULL,
	[VolunteerDepartmentDimID] [int] NULL,
	[VolunteerDepartmentSystemID] [int] NULL,
	[VolunteerDepartment] [varchar](100) NULL,
	[TimeSheetSubmissionDate] [datetime] NULL,
	[TimeSheetSubmissionDateDimID] [int] NULL,
	[HourlyWage] [numeric](30, 6) NULL,
	[Hours] [numeric](30, 6) NULL,
	[TimeSheetValue] [numeric] (30, 6) NULL,
	[Location] [varchar](100) NULL,
	[ApplyToMandate] [smallint] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_VolunteerTimeSheet_VolunteerTimeSheetFactID] PRIMARY KEY CLUSTERED 
(
	[VolunteerTimeSheetFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_WebStats]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_WebStats](
	[WebStatsFactID] [int] NOT NULL,
	[WebStatsSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[RequestDateDimID] [int] NULL,
	[RequestDate] [datetime] NULL,
	[UserName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleInitial] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[URL] [varchar](500) NULL,
	[EMail] [varchar](100) NULL,
	[ReferringPageURL] [nvarchar](1000) NULL,
	[TCPIPAddress] [varchar](30) NULL,
	[PageName] [varchar](100) NULL,
	[BaseURL] [varchar](100) NULL,
	[Category] [varchar](100) NULL,
	[DisplayName] [varchar](100) NULL,
	[ClientSiteName] [varchar](100) NULL,
	[InternalUser] [bit] NULL,
	[IsAdmin] [bit] NULL,
	[UserIsSupervisor] [bit] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[ClientSystemID] [int] NULL,
	[SourceTypeSystemID] [int] NULL,
	[FRTeamSystemID] [int] NULL,
	[FRSolicitorSystemID] [int] NULL,
	[WebSiteDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL,
 CONSTRAINT [PK_FACT_WebStats] PRIMARY KEY CLUSTERED 
(
	[WebStatsFactID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_FACTGROUP]
) ON [BBPM_FACTGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[FACT_WebStats_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FACT_WebStats_Stage](
	[WebStatsSystemID] [int] NULL,
	[ConstituentDimID] [int] NULL,
	[ConstituentSystemID] [int] NULL,
	[RequestDateDimID] [int] NULL,
	[RequestDate] [datetime] NULL,
	[UserName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleInitial] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[URL] [varchar](500) NULL,
	[EMail] [varchar](100) NULL,
	[ReferringPageURL] [nvarchar](1000) NULL,
	[TCPIPAddress] [varchar](30) NULL,
	[PageName] [varchar](100) NULL,
	[BaseURL] [varchar](100) NULL,
	[Category] [varchar](100) NULL,
	[DisplayName] [varchar](100) NULL,
	[ClientSiteName] [varchar](100) NULL,
	[InternalUser] [bit] NULL,
	[IsAdmin] [bit] NULL,
	[UserIsSupervisor] [bit] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[ClientSystemID] [int] NULL,
	[SourceTypeSystemID] [int] NULL,
	[FRTeamSystemID] [int] NULL,
	[FRSolicitorSystemID] [int] NULL,
	[WebSiteDimID] [int] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[STATIC_GiftTypeLookup]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STATIC_GiftTypeLookup](
	[GiftTypeLookupID] [int] NOT NULL,
	[GiftType] [varchar](100) NOT NULL,
	[GiftTypeUK] [varchar](100) NOT NULL,
	[GiftTypeVariant] [varchar](100) NOT NULL,
	[GiftTypeSystemID] [int] NOT NULL,
 CONSTRAINT [PK_STATIC_GiftTypeLookup_GiftTypeLookupID] PRIMARY KEY CLUSTERED 
(
	[GiftTypeLookupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_BeginningBalances]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_BeginningBalances](
	[ProjectDimID] [int] NULL,
	[AccountDimID] [int] NULL,
	[BalanceDate] [datetime] NULL,
	[TransactionCode1DimID] [int] NULL,
	[TransactionCode2DimID] [int] NULL,
	[TransactionCode3DimID] [int] NULL,
	[TransactionCode4DimID] [int] NULL,
	[TransactionCode5DimID] [int] NULL,
	[TransactionTypeID] [int] NULL,
	[TransactionType] [varchar](6) NULL,
	[Balance] [numeric](38, 4) NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_CodeTable]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_CodeTable](
	[TableEntrySystemID] [int] NOT NULL,
	[CodeTableSystemID] [int] NULL,
	[CodeTableName] [varchar](30) NULL,
	[LongDescription] [varchar](100) NULL,
	[ShortDescription] [varchar](60) NULL,
	[Active] [bit] NULL,
	[TableEntrySequence] [int] NULL,
	[NumericValue] [numeric](15, 3) NULL,
	[DateAdded] [datetime] NULL,
	[DateChanged] [datetime] NULL,
	[CodeTableDateAdded] [datetime] NULL,
	[CodeTableDateChanged] [datetime] NULL,
 CONSTRAINT [PK_TEMP_CodeTable] PRIMARY KEY CLUSTERED 
(
	[TableEntrySystemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_STAGEGROUP]
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_CodeTable]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_FECodeTable](
	[TableEntrySystemID] [int] NULL,
	[CodeTableSystemID] [int] NULL,
	[CodeTableName] [varchar](30) NULL,
	[Sequence] [smallint] NULL,
	[EntryID] [varchar](6) NULL,
	[TableEntryName] [varchar](60) NULL,
	[NumericValue] [numeric](15, 3) NULL,
	[ParentSystemID] [int] NULL,
	[SystemEntry] [smallint] NULL,
	[ShortUpperCaseTranslation] [varchar](55) NULL,
	[LongUpperCaseTranslation] [varchar](255) NULL,
	[Active] [varchar](3) NULL,
	[CodeTableUserDefined] [varchar](3) NULL,
	[ShortProperCaseTranslation] [varchar](55) NULL,
	[LongProperCaseTranslation] [varchar](255) NULL,
	[TableSequence] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_ConstituentMatch]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_ConstituentMatch](
	[ConstituentDimID] [int] NULL,
	[FEConstituentDimID] [int] NULL,
	[FullName] [varchar](110) NULL,
	[FullNameMatch] [varchar](110) NULL,
	[Address] [varchar](150) NULL,
	[AddressMatch] [varchar](150) NULL,
	[City] [varchar](100) NULL,
	[CityMatch] [varchar](100) NULL,
	[PostCode] [varchar](12) NULL,
	[PostCodeMatch] [varchar](12) NULL,
	[KeyIndicator] [varchar](1) NULL,
	[SimilarityBest] [real] NULL,
	[ConfidenceBest] [real] NULL,
	[Confidence] [real] NULL,
	[Similarity] [real] NULL,
	[SimilarityFullName] [real] NULL,
	[SimilarityAddress] [real] NULL,
	[SimilarityCity] [real] NULL,
	[SimilarityPostCode] [real] NULL,
	[IsMatch] [bit] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TEMP_CustomGroup]    Script Date: 05/09/2013 12:27:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_CustomGroup](
	[ShortCode] [varchar](20) NULL,
	[LongDescription] [varchar](100) NULL,
	[CustomGroup] [varchar](100) NULL,
	[Flag] [bit] NULL,
	[DateAdded] [datetime] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TEMP_Deletions]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TEMP_Deletions](
	[ID] [int] NOT NULL,
 CONSTRAINT [PK_TEMP_Deletions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_STAGEGROUP]
) ON [BBPM_STAGEGROUP]

GO


GO

/****** Object:  Table [dbo].[TEMP_DonationTransaction]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TEMP_DonationTransaction](
	[DonationTransactionSystemID] [int] NULL,
	[XMLObjectData] [ntext] NULL
) ON [BBPM_STAGEGROUP] TEXTIMAGE_ON [BBPM_STAGEGROUP]

GO


GO

/****** Object:  Table [dbo].[TEMP_FEFiscalPeriod]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_FEFiscalPeriod](
	[FiscalPeriodSystemID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartDateDimID] [int] NULL,
	[EndDateDimID] [int] NULL,
	[IsPeriodClosed] [int] NULL,
	[FiscalPeriodSequence] [smallint] NULL,
	[FiscalYearShortDescription] [varchar](12) NULL,
	[FiscalYearDescription] [varchar](60) NULL,
	[FiscalYearStatus] [varchar](10) NULL,
	[IsSummarized] [int] NULL,
	[NoFiscalPeriods] [smallint] NULL,
	[FiscalYearSequence] [int] NULL,
	[CloseLastRunDate] [datetime] NULL,
	[SummarizedLastRunDate] [datetime] NULL,
	[PurgeLastRunDate] [datetime] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_GiftCommitment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TEMP_GiftCommitment](
	[GiftSystemID] [int] NULL,
	[FundDimID] [int] NULL,
	[AppealDimID] [int] NULL,
	[CampaignDimID] [int] NULL,
	[PackageDimID] [int] NULL,
	[PaymentAmount] [money] NULL,
	[PaymentCount] [int] NULL,
	[GiftCount] [int] NULL
) ON [BBPM_STAGEGROUP]

GO


GO

/****** Object:  Table [dbo].[TEMP_GiftInstallmentPayment]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_GiftInstallmentPayment](
	[InstallmentPaymentSystemID] [int] NULL,
	[InstallmentSystemID] [int] NULL,
	[CommitmentSystemID] [int] NULL,
	[PaymentSystemID] [int] NULL,
	[CommitmentDate] [datetime] NULL,
	[InstallmentDate] [datetime] NULL,
	[PayDate] [datetime] NULL,
	[CommitmentTypeSystemID] [int] NULL,
	[PayTypeSystemID] [int] NULL,
	[PayInstallmentDays] [int] NULL,
	[PayCommitmentDays] [int] NULL,
	[PaymentErrorFlag] [int] NULL,
	[InstallmentStatus] [varchar](14) NULL,
	[CommitmentAmount] [money] NULL,
	[InstallmentAmount] [money] NULL,
	[PayAmount] [money] NULL,
	[InstallmentPayAmount] [money] NULL,
	[BalanceAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[IsRG] [bit] NULL,
	[IsPayment] [bit] NULL,
	[IsMissedPayment] [bit] NULL,
	[IsFuturePayment] [bit] NULL,
	[IsWriteOff] [bit] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_GiftInstallmentPayment_Stage]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_GiftInstallmentPayment_Stage](
	[InstallmentPaymentSystemID] [int] NULL,
	[InstallmentSystemID] [int] NULL,
	[CommitmentSystemID] [int] NULL,
	[PaymentSystemID] [int] NULL,
	[CommitmentDate] [datetime] NULL,
	[InstallmentDate] [datetime] NULL,
	[PayDate] [datetime] NULL,
	[CommitmentTypeSystemID] [int] NULL,
	[PayTypeSystemID] [int] NULL,
	[PayInstallmentDays] [int] NULL,
	[PayCommitmentDays] [int] NULL,
	[PaymentErrorFlag] [int] NULL,
	[InstallmentStatus] [varchar](14) NULL,
	[CommitmentAmount] [money] NULL,
	[InstallmentAmount] [money] NULL,
	[PayAmount] [money] NULL,
	[InstallmentPayAmount] [money] NULL,
	[BalanceAmount] [money] NULL,
	[ETLControlID] [int] NULL,
	[IsRG] [bit] NULL,
	[IsPayment] [bit] NULL,
	[IsMissedPayment] [bit] NULL,
	[IsFuturePayment] [bit] NULL,
	[IsWriteOff] [bit] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_GLSourceType]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_GLSourceType](
	[SourceTypeSystemID] [int] NOT NULL,
	[SourceTypeName] [varchar](60) NULL,
	[BatchType] [varchar](60) NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_GLSummary]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_GLSummary](
	[Status] [varchar](10) NULL,
	[GL7SummaryID] [int] NULL,
	[ProjectDimID] [int] NULL,
	[ProjectSystemID] [int] NULL,
	[ProjectID] [varchar](12) NULL,
	[AccountDimID] [int] NULL,
	[AccountSystemID] [int] NULL,
	[AccountNumber] [varchar](30) NULL,
	[AccountCategoryCode] [smallint] NULL,
	[BeginningBalance] [numeric](19, 4) NULL,
	[Credit] [numeric](19, 4) NULL,
	[Debit] [numeric](19, 4) NULL,
	[TransactionCode1DimID] [int] NULL,
	[TransactionCode2DimID] [int] NULL,
	[TransactionCode3DimID] [int] NULL,
	[TransactionCode4DimID] [int] NULL,
	[TransactionCode5DimID] [int] NULL,
	[Class] [int] NULL,
	[YearID] [varchar](12) NULL,
	[YearDescription] [varchar](60) NULL,
	[FiscalCloseStatus] [varchar](11) NULL,
	[FiscalSummarized] [smallint] NULL,
	[FiscalPeriods] [smallint] NULL,
	[FiscalYearSequence] [int] NULL,
	[FiscalPeriodSequence] [smallint] NULL,
	[FiscalPeriodClosed] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartDateDimID] [int] NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_SolicitorActivityGoal]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_SolicitorActivityGoal](
	[SolicitorConstituentDimID] [int] NULL,
	[ActivityDateDimID] [int] NULL,
	[GoalType] [varchar](50) NULL,
	[Value] [decimal](12, 4) NULL
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_TotGivingYearsCTE]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TEMP_TotGivingYearsCTE](
	[ConstituentDimID] [int] NULL,
	[ConsecutiveYears] [int] NULL
) ON [BBPM_STAGEGROUP]

GO


GO

/****** Object:  Table [dbo].[TEMP_VCO]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_VCO](
	[VCODimID] [int] IDENTITY(1,1) NOT NULL,
	[ChartTemplateSystemID] [int] NULL,
	[TemplateDetailSystemID] [int] NULL,
	[FilterSystemID] [int] NULL,
	[Sequence] [int] NULL,
	[Caption] [varchar](100) NULL,
	[AccountCodeSystemID] [int] NULL,
	[AccountCode] [varchar](30) NOT NULL,
	[AccountSystemID] [int] NULL,
	[AccountDimID] [int] NULL,
	[AccountNumber] [varchar](100) NULL,
	[AccountCategory] [varchar](9) NULL,
	[ParentKey] [varchar](20) NULL,
	[ParentVCODimID] [int] NULL,
	[Filter] [bit] NULL,
	[Include] [bit] NULL,
	[Segment1SystemID] [int] NULL,
	[Segment2SystemID] [int] NULL,
 CONSTRAINT [PK_TEMP_VCO] PRIMARY KEY CLUSTERED 
(
	[VCODimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_STAGEGROUP]
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[TEMP_VCOFilter]    Script Date: 06/15/2012 18:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_VCOFilter](
	[VCOFilterDimID] [int] NULL,
	[TemplateDetailSystemID] [int] NULL,
	[FilterSystemID] [int] NULL,
	[TemplateSystemID] [int] NULL,
	[TemplateID] [varchar](50) NULL,
	[TemplateName] [varchar](100) NULL,
	[RecordType] [smallint] NULL,
	[Level] [smallint] NULL,
	[Caption] [varchar](100) NULL,
	[ParentKey] [int] NULL,
	[Category] [smallint] NULL,
	[FilterType] [varchar](20) NULL,
	[IncludeType] [varchar](20) NULL,
	[Action] [varchar](20) NULL,
	[VALUE1] [varchar](1000) NULL,
	[VALUE2] [varchar](1000) NULL,
	[VALUE1Original] [varchar](1000) NULL,
	[VALUE2Original] [varchar](1000) NULL,
	[Include] [bit] NULL,
	[ETLControlID] [int] NULL,
	[SourceID] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TEMP_VCOToFromFilter]    Script Date: 12/05/2011 12:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TEMP_VCOToFromFilter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FilterSystemID] [int] NULL,
	[TemplateDetailSystemID] [int] NULL,
	[TemplateSystemID] [int] NULL,
	[TemplateID] [varchar](50) NULL,
	[TemplateName] [varchar](100) NULL,
	[FilterType] [varchar](20) NULL,
	[IncludeType] [varchar](20) NULL,
	[Caption] [varchar](100) NULL,
	[FilterKey] [int] NULL,
	[FilterCode] [varchar](100) NULL,
	[Filter] [bit] NULL,
	[ParentKey] [int] NULL,
	[ToFromType] [varchar](10) NULL,
	[Sequence] [int] NULL,
	[ToFromList] [varchar](2000) NULL,
 CONSTRAINT [PK_TEMP_VCOToFromFilter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [BBPM_STAGEGROUP]
) ON [BBPM_STAGEGROUP]

GO

SET ANSI_PADDING OFF
GO

PRINT 'Tables Created'

/********************************************************************************************************************************************************************************
		Main View Creates
********************************************************************************************************************************************************************************/

/****** Object:  View [dbo].[CTL_TestDataReport]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CTL_TestDataReport]
AS
SELECT 
m.TargetTableName, m.TargetColumnName,
m.SourceTableName, m.SourceColumnName,
t.Value AS TargetCount, t.ValueNull AS TargetNullCount,
s.Value AS SourceCount, s.ValueNull AS SourceNullCount,
ABS(t.Value - s.Value) AS Variance
FROM
CTL_SourceToTargetMapping m
INNER JOIN CTL_DWTableStats t ON m.TargetTableName = t.ParentObjectName AND m.TargetColumnName = t.ObjectName
INNER JOIN CTL_SourceTableStats s ON m.SourceTableName = s.ParentObjectName AND m.SourceColumnName = s.ObjectName


GO


/****** Object:  View [dbo].[vw_DIM_Proposal]    Script Date: 08/01/2013 11:39:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_DIM_Proposal]
AS
SELECT 
	p.ProposalDimID,
	p.ProposalSystemID,
	p.ConstituentDimID,
	p.ConstituentSystemID,
	p.ConstituentProspectSystemID,
	p.CampaignDimID,
	p.CampaignSystemID,
	p.FundDimID,
	p.FundSystemID,
	p.OriginalAmountAsked,
	p.AmountAsked,
	p.AmountExpected,
	p.AmountFunded,
	p.DateAdded,
	p.DateChanged,
	p.DateAddedDimID,
	p.DateChangedDimID,
	p.OriginalDateAsked,
	p.DateAsked,
	p.DateExpected,
	p.DateFunded,
	p.DateRated,
	p.DateDeadline,
	p.DateConfirmation,
	p.OriginalDateAskedDimID,
	p.DateAskedDimID,
	p.DateExpectedDimID,
	p.DateFundedDimID,
	p.DateRatedDimID,
	p.DateDeadlineDimID,
	p.DateConfirmationDimID,
	p.PrimaryAssignedSolicitorSystemID,
	p.PrimaryAssignedSolicitorDimID,
	p.ContactSystemID,
	p.ContactConstituentDimID,
	p.InstrumentType,
	p.Purpose,
	p.Instrument,
	p.Rating,
	p.Status,
	p.ProposalName,
	p.IsInactive,
	p.Reason,
	p.Notes,
	p.AskToFundedDays,
	p.ExpectedToFundedDays,
	p.ETLControlID,
	p.SourceID,
	pr.CreateFirstSequence,
	pr.ChangeFirstSequence,
	pr.OriginalAskFirstSequence,
	pr.AskFirstSequence,
	pr.ExpectedFirstSequence,
	pr.FundedFirstSequence,
	pr.CreateLastSequence,
	pr.ChangeLastSequence,
	pr.OriginalAskLastSequence,
	pr.AskLastSequence,
	pr.ExpectedLastSequence,
	pr.FundedLastSequence,
	pr.AskHighestSequence,
	pr.ExpectedHighestSequence,
	pr.FundedHighestSequence,
	p.MonetaryAsked,
	p.MonetaryAskedSequence,
	p.RecencyAsked,
	p.RecencyAskedSequence,
	p.MonetaryExpected,
	p.MonetaryExpectedSequence,
	p.RecencyExpected,
	p.RecencyExpectedSequence,
	p.MonetaryFunded,
	p.MonetaryFundedSequence,
	p.RecencyFunded,
	p.RecencyFundedSequence,
	p.IsExpected,
	p.IsFunded,
	p.HasCloseDays,
	p.TotalActionCount,
	p.TotalProposalActionCount,
	p.TotalProposalPreFundedActionCount
FROM   
DIM_Proposal as p
LEFT OUTER JOIN DIM_ProposalRank as pr on p.ProposalDimID = pr.ProposalDimID 

GO

/****** Object:  View [dbo].[vw_DIM_Solicitor]    Script Date: 08/01/2013 11:39:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_DIM_Solicitor]
AS
select 
	s.SolicitorDimID as ConstituentDimID,
	s.ConstituentID,
	s.ConstituentImportID,
	s.FullName,
	s.StaffSolicitorName,
	s.PrimaryConstituentCode,
	s.KeyIndicator,
	s.LatestSolicitorType,
	s.PrimarySolicitorType,
	s.IsStaffSolicitor,
	s.IsPrimarySolicitor,
	s.IsASolicitor,
	s.IsSolicitorInactive,
	s.IsDeceased,
	s.HasGift,
	s.HasGoal,
	s.HasAction,
	s.HasProposal,
	s.HasAssignment,
	s.HasActivityGoal,
	s.LastSolicitorGiftDateDimID,
	s.LastSolicitorActionDateDimID,
	s.LastSolicitorProposalAskDateDimID,
	s.LastSolicitorProposalFundedDateDimID,
	s.LastAssignmentDateDimID,
	s.TotalSolicitorGiftAmount,
	s.TotalProposalFundedAmount,
	s.TotalActiveAssignmentCount,
	s.TotalRelationshipCount,
	s.TotalActionCount,
	s.ETLControlID,
	s.SourceID,
	c.City,
	c.State,
	c.PostCode,
	c.Country
FROM   
DIM_Constituent as c
INNER JOIN DIM_Solicitor as s on c.ConstituentDimID = s.SolicitorDimID 

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification1]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification1]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 1)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification2]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification2]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 2)


GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification3]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification3]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 3)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification4]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification4]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 4)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification5]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification5]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 5)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification6]    Script Date: 07/31/2013 19:07:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification6]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 6)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification7]    Script Date: 07/31/2013 19:07:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification7]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 7)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification8]    Script Date: 07/31/2013 19:07:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification8]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 8)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification9]    Script Date: 07/31/2013 19:07:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification9]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 9)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualClassification10]    Script Date: 07/31/2013 19:07:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualClassification10]
AS
SELECT     ConstituentAnnualClassificationFactID, ConstituentAnnualGivingFactID, ConstituentDimID, FiscalYear, Sequence, Classification, SummaryDimID
FROM         dbo.FACT_ConstituentAnnualClassification
WHERE     (SummaryDimID = 10)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving1]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving1]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 1)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving2]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving2]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 2)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving3]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving3]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 3)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving4]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving4]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 4)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving5]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving5]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 5)


GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving6]    Script Date: 07/31/2013 19:09:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving6]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 6)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving7]    Script Date: 07/31/2013 19:09:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving7]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 7)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving8]    Script Date: 07/31/2013 19:09:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving8]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 8)



GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving9]    Script Date: 07/31/2013 19:09:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving9]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 9)

GO

/****** Object:  View [dbo].[FACT_ConstituentAnnualGiving10]    Script Date: 07/31/2013 19:09:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentAnnualGiving10]
AS
SELECT     ConstituentAnnualGivingFactID, ConstituentDimID, RelativeYear, TotalGiftAmount, TotalNumberGifts, ConstituentSystemID, HighestGiftAmount, 
                      FiscalYear, FiscalYearPrior, FiscalYearNext, FiscalYearStartDateDimID, YearAgo, LowestGiftAmount, AvgGiftAmount, FirstGiftDate, LastGiftDate, 
                      UpgradeType, ETLControlID, SourceID, Frequency, MonetaryTotal, MonetaryMax, FrequencySequence, MonetaryTotalSequence, 
                      MonetaryMaxSequence, SummaryDimID, GivingSequence
FROM         dbo.FACT_ConstituentAnnualGiving
WHERE     (SummaryDimID = 10)

GO

/****** Object:  View [dbo].[FACT_ConstituentConsecutiveGivingYears]    Script Date: 04/30/2012 12:00:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW
[dbo].[FACT_ConstituentConsecutiveGivingYears]
AS
-- Handles all Constituents that had more than 1 streak of consecutive giving
SELECT cte.ConstituentDimID, MAX(TotalGivingYears) - MAX(cte.ConsecutiveYears) ConsecutiveYears   
FROM TEMP_TotGivingYearsCTE cte
	LEFT JOIN (
		SELECT ConstituentDimID, MAX(ConsecutiveYears) TotalGivingYears 
		FROM TEMP_TotGivingYearsCTE 
		GROUP BY ConstituentDimID) t ON cte.ConstituentDimID = t.ConstituentDimID 
WHERE cte.ConsecutiveYears < t.TotalGivingYears 
GROUP BY cte.ConstituentDimID

UNION ALL

-- Handles all constituents that had only 1 streak of consecutive giving
SELECT ConstituentDimID, ConsecutiveYears
FROM TEMP_TotGivingYearsCTE 
WHERE ConstituentDimID in 
(
	SELECT ConstituentDimID  
	FROM(  
		SELECT ConstituentDimID, Count(*) RecordCount 
		FROM TEMP_TotGivingYearsCTE 
		GROUP BY ConstituentDimID) a
	WHERE RecordCount = 1
)

GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary1]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentGivingSummary1]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 1)



GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary2]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[FACT_ConstituentGivingSummary2]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 2)




GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary3]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentGivingSummary3]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 3)
GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary4]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[FACT_ConstituentGivingSummary4]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 4)




GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary5]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[FACT_ConstituentGivingSummary5]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 5)

GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary6]    Script Date: 07/31/2013 19:12:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentGivingSummary6]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 6)



GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary7]    Script Date: 07/31/2013 19:12:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentGivingSummary7]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 7)



GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary8]    Script Date: 07/31/2013 19:12:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentGivingSummary8]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 8)



GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary9]    Script Date: 07/31/2013 19:12:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentGivingSummary9]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 9)

GO

/****** Object:  View [dbo].[FACT_ConstituentGivingSummary10]    Script Date: 07/31/2013 19:12:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentGivingSummary10]
AS
SELECT     ConstituentGivingSummaryFactID, ConstituentDimID, ConstituentSystemID, DateAddedDimID, Amt0, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Amt7, Amt8, 
                      Amt9, Amt10, Amt11, Amt12, Amt13, Amt14, Amt15, Amt16, Amt17, Amt18, Amt19, Amt20, Count0, Count1, Count2, Count3, Count4, Count5, Count6, 
                      Count7, Count8, Count9, Count10, Count11, Count12, Count13, Count14, Count15, Count16, Count17, Count18, Count19, Count20, Count20Plus, 
                      AcquisitionAgo, MaxAmt0, MaxAmt1, MaxAmt2, MaxAmt3, MaxAmt4, MaxAmt5, MaxAmt6, MaxAmt7, MaxAmt8, MaxAmt9, MaxAmt10, MaxAmt11, 
                      MaxAmt12, MaxAmt13, MaxAmt14, MaxAmt15, MaxAmt16, MaxAmt17, MaxAmt18, MaxAmt19, MaxAmt20, MaxAmt20Plus, Amt0Plus, Amt1Plus, 
                      Amt2Plus, Amt3Plus, Amt4Plus, Amt5Plus, Amt6Plus, Amt7Plus, Amt8Plus, Amt9Plus, Amt10Plus, Amt11Plus, Amt12Plus, Amt13Plus, Amt14Plus, 
                      Amt15Plus, Amt16Plus, Amt17Plus, Amt18Plus, Amt19Plus, Amt20Plus, Amt21Plus, [Column], Flag, LYBUNT, SYBUNT, NewDMDonor, 
                      LifetimeGiftAmount, LifetimeGiftCount, LifetimeGiftAverage, ConsecutiveRenewal, TenYearRenewal, ETLControlID, SourceID, SummaryDimID
FROM         dbo.FACT_ConstituentGivingSummary
WHERE     (SummaryDimID = 10)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving1]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving1]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 1)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving2]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving2]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 2)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving3]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[FACT_ConstituentLTGiving3]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 3)


GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving4]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving4]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 4)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving5]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving5]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 5)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving6]    Script Date: 07/31/2013 19:14:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving6]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 6)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving7]    Script Date: 07/31/2013 19:14:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving7]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 7)



GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving8]    Script Date: 07/31/2013 19:14:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving8]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 8)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving9]    Script Date: 07/31/2013 19:14:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving9]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 9)

GO

/****** Object:  View [dbo].[FACT_ConstituentLTGiving10]    Script Date: 07/31/2013 19:14:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FACT_ConstituentLTGiving10]
AS
SELECT     ConstituentLTGivingFactID, ConstituentDimID, FirstGiftFactID, LastGiftFactID, TotalAmount, AvgAmount, MaxAmount, MinAmount, TotalMaxAmount, 
                      TotalMinAmount, Count, TotalCount, FirstGiftDate, FirstGiftDateDimID, LastGiftDate, LastGiftDateDimID, LastGiftTypeDimID, GivingYears, GivingMonths,
                       GivingDays, YearsSinceGiven, MonthsSinceGiven, DaysSinceGiven, ConsecutiveYears, DistinctGivingYears, DistinctGivingFiscalYears, 
                      AnnualRenewer, AnnualFiscalRenewer, FrequencySM, LastGiftAmount, FirstGiftAmount, FirstFundDimID, LastFundDimID, FirstCampaignDimID, 
                      LastCampaignDimID, FirstAppealDimID, LastAppealDimID, FirstPackageDimID, LastPackageDimID, MaxGiftDate, MaxGiftDateDimID, MaxGiftSplitDate, 
                      MaxGiftSplitDateDimID, FirstGiftSplitAmount, LastGiftSplitAmount, AvgLatency, MaxLatency, MinLatency, SourceID, ETLControlID, Recency, Frequency, 
                      MonetaryTotal, MonetaryMax, MonetaryLast, RecencySequence, FrequencySequence, MonetaryTotalSequence, MonearyMaxSequence, 
                      MonetaryLastSequence, SummaryDimID
FROM         dbo.FACT_ConstituentLTGiving
WHERE     (SummaryDimID = 10)

GO

/****** Object:  View [dbo].[vw_DWSchemaColumns]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_DWSchemaColumns]
as
  select
    A.[NAME] COLUMN_NAME,
    B.[NAME] TABLE_NAME,
    C.[NAME] SCHEMA_NM,
    (select
       COUNT(*)
     from
       INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Z
       inner join INFORMATION_SCHEMA.TABLE_CONSTRAINTS Y on Z.TABLE_NAME = Y.TABLE_NAME
     where  Y.CONSTRAINT_TYPE = 'PRIMARY KEY'
            and Z.COLUMN_NAME = A.[NAME]
            and Z.TABLE_NAME = B.[NAME]
            and Z.TABLE_SCHEMA = C.[NAME]) IS_PK,
    A.IS_IDENTITY IS_IDENT
  from
    sys.COLUMNS A
    left join sys.TABLES B on A.[OBJECT_ID] = B.[OBJECT_ID]
    left join sys.SCHEMAS C on B.[SCHEMA_ID] = C.[SCHEMA_ID]
  where  A.IS_COMPUTED = 0
         --AND A.IS_IDENTITY = 0
         and B.TYPE_DESC = 'USER_TABLE' 




GO

/****** Object:  View [dbo].[vw_DWSchemaTables]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_DWSchemaTables]
as
  select
    B.[name] TABLE_NAME,
    C.[name] SCHEMA_NM
  from
    sys.tables B
    left join sys.schemas C on B.[schema_id] = C.[schema_id]
  where  B.type_desc = 'USER_TABLE'
         and B.[name] like 'DIM_%'
          or B.[name] like 'FACT_%' 
          or B.[name] like 'TEMP_%'


GO

/****** Object:  View [dbo].[vw_DIM_Constituent]    Script Date: 08/01/2013 11:39:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_DIM_Constituent]
AS
SELECT
	c.ConstituentDimID,
	c.ConstituentSystemID,
	c.UniqueConstituentID,
	c.ConstituentID,
	c.ImportID,
	c.KeyIndicator,
	c.FullName,
	c.Title,
	c.FirstName,
	case
	 when c.ConstituentID is null then c.FullName
	 else c.FullName + ' (' + c.ConstituentID + ')'
	end                               as ConstituentIDAndName,
	c.MiddleName,
	c.LastName,
	c.Suffix,
	c.Birthday,
	c.Addressee,
	c.Salutation,
	c.Gender,
	c.MaritalStatus,
	c.IsAConstituent,
	c.IsDeceased,
	c.IsAnonymous,
	c.IsInactive,
	c.IsASolicitor,
	c.IsSolicitorInactive,
	c.IsHonorMemorial,
	c.DeceasedDate,
	c.Income,
	c.Industry,
	c.NoValidAddress,
	c.NoEMail,
	c.Address,
	c.Address1,
	c.Address2,
	c.Address3,
	c.Address4,
	c.Address5,
	c.City,
	c.State,
	c.County,
	c.PostCode,
	c.Country,
	c.Region,
	c.Target,
	c.EmailAddress,
	c.HomePhone,
	c.BusinessPhone,
	c.SendMail,
	c.PaysTax,
	c.Ethnicity,
	c.Religion,
	c.SocialSecurityNumber,
	c.DateAdded,
	c.DateChanged,
	c.EmployerConstituentSystemID,
	c.SpouseConstituentSystemID,
	c.PrimaryContactConstituentSystemID,
	c.ProspectStatus,
	c.ProspectClassification,
	c.IsHeadOfHousehold,
	c.HouseholdID,
	c.HouseholdSystemID,
	c.Age,
	c.ETLControlID,
	c.SourceID,
	sg.TotalAmount                    as LTGivingTotalAmount,
	sg.AvgAmount                      as LTGivingAvgAmount,
	sg.MaxAmount                      as LTGivingMaxAmount,
	sg.MinAmount                      as LTGivingMinAmount,
	sg.TotalMaxAmount                 as LTGivingTotalMaxAmount,
	sg.TotalMinAmount                 as LTGivingTotalMinAmount,
	sg.[Count]                        as LTGivingCount,
	sg.TotalCount                     as LTGivingTotalCount,
	sg.FirstGiftDate                  as LTGivingFirstGiftDate,
	sg.LastGiftDate                   as LTGivingLastGiftDate,
	sg.GivingYears                    as LTGivingGivingYears,
	sg.GivingMonths                   as LTGivingGivingMonths,
	sg.GivingDays                     as LTGivingGivingDays,
	sg.YearsSinceGiven                as LTGivingYearsSinceGiven,
	sg.MonthsSinceGiven               as LTGivingMonthsSinceGiven,
	sg.DaysSinceGiven                 as LTGivingDaysSinceGiven,
	sg.DistinctGivingYears            as LTGivingDistinctGivingYears,
	sg.DistinctGivingFiscalYears      as LTGivingDistinctGivingFiscalYears,
	sg.AnnualRenewer                  as LTGivingAnnualRenewer,
	sg.AnnualFiscalRenewer            as LTGivingAnnualFiscalRenewer,
	sg.LastGiftAmount                 as LTGivingLastGiftAmount,
	sg.FirstGiftAmount                as LTGivingFirstGiftAmount,
	sg.MaxGiftSplitDate               as LTGivingMaxGiftSplitDate,
	sg.MaxGiftDate                    as LTGivingMaxGiftDate,
	sg.FirstGiftSplitAmount           as LTGivingFirstGiftSplitAmount,
	sg.LastGiftSplitAmount            as LTGivingLastGiftSplitAmount,
	sg.AvgLatency                     as LTGivingAvgLatency,
	sg.MaxLatency                     as LTGivingMaxLatency,
	sg.MinLatency                     as LTGivingMinLatency,
	sg.Recency                        as LTGivingRecency,
	sg.FrequencySM                    as LTGivingFrequencySM,
	sg.Frequency                      as LTGivingFrequency,
	sg.MonetaryTotal                  as LTGivingMonetaryTotal,
	sg.MonetaryMax                    as LTGivingMonetaryMax,
	sg.MonetaryLast                   as LTGivingMonetaryLast,
	sg.RecencySequence                as LTGivingRecencySequence,
	sg.FrequencySequence              as LTGivingFrequencySequence,
	sg.MonetaryTotalSequence          as LTGivingMonetaryTotalSequence,
	sg.MonearyMaxSequence             as LTGivingMonetaryMaxSequence,
	sg.MonetaryLastSequence           as LTGivingMonetaryLastSequence,
	sap.FirstAppealDate               as LTAppealFirstAppealDate,
	sap.LastAppealDate                as LTAppealLastAppealDate,
	sap.DistinctCalendarYears         as LTAppealDistinctCalendarYears,
	sap.DistinctFiscalYears           as LTAppealDistinctFiscalYears,
	sap.YearsSinceAppeal              as LTAppealYearsSinceAppeal,
	sap.MonthsSinceAppeal             as LTAppealMonthsSinceAppeal,
	sap.DaysSinceAppeal               as LTAppealDaysSinceAppeal,
	sap.AppealYears                   as LTAppealAppealYears,
	sap.AppealCount                   as LTAppealAppealCount,
	sap.Recency                       as LTAppealRecency,
	sap.Frequency                     as LTAppealFrequency,
	sap.RecencySequence               as LTAppealRecencySequence,
	sap.FrequencySequence             as LTAppealFrequencySequence,
	sac.FirstActionDate               as LTActionFirstActionDate,
	sac.LastActionDate                as LTActionLastActionDate,
	sac.TotalNumberActions            as LTActionActionCount,
	sac.Recency                       as LTActionRecency,
	sac.Frequency                     as LTActionFrequency,
	sac.RecencySequence               as LTActionRecencySequence,
	sac.FrequencySequence             as LTActionFrequencySequence,
	sm.TotalAmount                    as LTMembershipTotalAmount,
	sm.AvgAmount                      as LTMembershipAvgAmount,
	sm.MaxAmount                      as LTMembershipMaxAmount,
	sm.MinAmount                      as LTMembershipMinAmount,
	sm.TransactionCount               as LTMembershipTransactionCount,
	sm.FirstTransactionDate           as LTMembershipFirstTransactionDate,
	sm.LastTransactionDate            as LTMembershipLastTransactionDate,
	sm.TransactionYears               as LTMembershipTransactionYears,
	sm.YearsSinceTransaction          as LTMembershipYearsSinceTransaction,
	sm.MonthsSinceTransaction         as LTMembershipMonthsSinceTransaction,
	sm.DistinctTransactionYears       as LTMembershipDistinctTransactionYears,
	sm.DistinctTransactionFiscalYears as LTMembershipDistrinctTransactionFiscalYears,
	sm.LastTransactionAmount          as LTMembershipLastTransactionAmount,
	sm.FirstTransactionAmount         as LTMembershipFirstTransactionAmount,
	sm.Recency                        as LTMembershipRecency,
	sm.Frequency                      as LTMembershipFrequency,
	sm.MonetaryTotal                  as LTMembershipMonetaryTotal,
	sm.MonetaryMax                    as LTMembershipMonetaryMax,
	sm.MonetaryLast                   as LTMembershipMonetaryLast,
	sm.RecencySequence                as LTMembershipRecencySequence,
	sm.FrequencySequence              as LTMembershipFrequencySequence,
	sm.MonetaryTotalSequence          as LTMembershipMonetaryTotalSequence,
	sm.MonetaryMaxSequence            as LTMembershipMonetaryMaxSequence,
	sm.MonetaryLastSequence           as LTMembershipMonetaryLastSequence,
	sac.DistinctCalendarYears         as LTActionDistinctCalendarYears,
	sac.DistinctFiscalYears           as LTActionDistinctFiscalYears,
	sac.YearsSinceAction              as LTActionYearsSinceAction,
	sac.MonthsSinceAction             as LTActionMonthsSince,
	sac.ActionYears                   as LTActionActionYears,
	sac.FirstActionDateDimID          as LTActionFirstDateDimID,
	sac.LastActionDateDimID           as LTActionLastDateDimID,
	sg.FirstGiftDateDimID             as LTGivingFirstDateDimID,
	sg.LastGiftDateDimID              as LTGivingLastDateDimID,
	sap.FirstAppealDateDimID          as LTAppealFirstDateDimID,
	sap.LastAppealDateDimID           as LTAppealLastDateDimID,
	sm.FirstTransactionDateDimID      as LTMembershipFirstDateDimID,
	sm.LastTransactionDateDimID       as LTMembershipLastDateDimID,
	sg.MaxGiftDateDimID               as LTGivingMaxDateDimID,
	se.FirstEventDateDimID            as LTEventFirstDateDimID,
	se.LastEventDateDimID             as LTEventLastDateDimID,
	se.DistinctCalendarYears          as LTEventDistinctCalendarYears,
	se.DistinctFiscalYears            as LTEventDistinctFiscalYears,
	se.YearsSinceEvent                as LTEventYearsSince,
	se.MonthsSinceEvent               as LTEventMonthsSince,
	se.DaysSinceEvent                 as LTEventDaysSince,
	se.EventYears                     as LTEventYears,
	se.EventCount                     as LTEventCount,
	se.Recency                        as LTEventRecency,
	se.Frequency                      as LTEventFrequency,
	se.RecencySequence                as LTEventRecencySequence,
	se.FrequencySequence              as LTEventFrequencySequence,
	df.CalendarYear                   as LTGivingFirstCalendarYear,
	df.FiscalYear                     as LTGivingFirstFiscalYear,
	dl.CalendarYear                   as LTGivingLastCalendarYear,
	dl.FiscalYear                     as LTGivingLastFiscalYear
FROM   
DIM_Date as dl
INNER JOIN DIM_Date as df
INNER JOIN FACT_ConstituentLTGiving1 as sg on df.DateDimID = sg.FirstGiftDateDimID on dl.DateDimID = sg.LastGiftDateDimID
RIGHT OUTER JOIN DIM_Constituent as c
LEFT OUTER JOIN FACT_ConstituentLTEvent as se on c.ConstituentDimID = se.ConstituentDimID on sg.ConstituentDimID = c.ConstituentDimID
LEFT OUTER JOIN FACT_ConstituentLTMembership as sm on c.ConstituentDimID = sm.ConstituentDimID
LEFT OUTER JOIN FACT_ConstituentLTAppeal as sap on c.ConstituentDimID = sap.ConstituentDimID
LEFT OUTER JOIN FACT_ConstituentLTAction as sac on c.ConstituentDimID = sac.ConstituentDimID 

GO

/****** Object:  View [dbo].[vw_FACT_Gift]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_FACT_Gift]
as
SELECT
  c.ConstituentID,
  c.FullName,
  g.GiftDate,
  g.Amount,
  gt.GiftType,
  f.FundIdentifier,
  cp.CampaignIdentifier,
  a.AppealIdentifier,
  p.PackageIdentifier
FROM
  FACT_Gift as g
  inner join DIM_Appeal as a on g.AppealDimID = a.AppealDimID
  inner join DIM_Campaign as cp on g.CampaignDimID = cp.CampaignDimID
  inner join DIM_Constituent as c on g.ConstituentDimID = c.ConstituentDimID
  inner join DIM_ConstituentCode as cc on g.ConstituentCodeDimID = cc.ConstituentCodeDimID
  inner join DIM_Date as gd on g.GiftDateDimID = gd.DateDimID
  inner join DIM_Fund as f on g.FundDimID = f.FundDimID
  inner join DIM_GiftCode as gc on g.GiftCodeDimID = gc.GiftCodeDimID
  inner join DIM_GiftSubType as gst on g.GiftSubTypeDimID = gst.GiftSubTypeDimID
  inner join DIM_GiftType as gt on g.GiftTypeDimID = gt.GiftTypeDimID
  inner join DIM_LetterCode as lc on g.LetterCodeDimID = lc.LetterCodeDimID
  inner join DIM_Package as p on g.PackageDimID = p.PackageDimID
  inner join DIM_PaymentType as pt on g.PaymentTypeDimID = pt.PaymentTypeDimID
  inner join DIM_PostStatus as gps on gps.PostStatusDimID = g.PostStatusDimID


GO

/****** Object:  View [dbo].[vw_FACT_GiftInstallment_Load]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view
[dbo].[vw_FACT_GiftInstallment_Load]
as
SELECT
gi.GiftInstallmentSystemID,
gi.GiftSystemID,
gi.InstallmentAmount,
gi.InstallmentAmount * g.SplitRatio as InstallmentSplitAmount,
gi.InstallmentCount,
gi.InstallmentDate,
g.GiftFactID,
g.ConstituentDimID,
g.FundDimID,
g.AppealDimID,
g.CampaignDimID,
g.PackageDimID,
g.GiftTypeDimID,
g.GiftSubTypeDimID,
g.GiftCodeDimID,
g.LetterCodeDimID,
g.GiftDateDimID,
g.SplitRatio,
gip.FirstPaymentDate,
gip.LastPaymentDate,
gip.PaymentCount,
gip.InstallmentPayAmount,
giw.PaymentCount as WriteOffCount,
giw.InstallmentWriteOffAmount,
COALESCE(gi.ETLControlID, COALESCE(gip.ETLControlID, giw.ETLControlID)) as ETLControlID
FROM
(
SELECT  
p.InstallmentSystemID AS GiftInstallmentSystemID,
p.CommitmentSystemID AS GiftSystemID,
p.InstallmentDate,
p.InstallmentAmount,
p.ETLControlID, 
Count(*) AS InstallmentCount
FROM
TEMP_GiftInstallmentPayment p 
GROUP BY
p.InstallmentSystemID,
p.CommitmentSystemID, p.InstallmentDate, p.InstallmentAmount, p.ETLControlID
) gi
LEFT JOIN
FACT_Gift g on g.GiftSystemID = gi.GiftSystemID
LEFT JOIN
(
SELECT  
p.CommitmentSystemID,
p.InstallmentSystemID AS GiftInstallmentSystemID,
Count(p.InstallmentPaymentSystemID) AS PaymentCount,
SUM(p.InstallmentPayAmount) AS InstallmentPayAmount,
p.ETLControlID,
MIN(p.PayDate) AS FirstPaymentDate,
MAX(p.PayDate) AS LastPaymentDate
FROM
TEMP_GiftInstallmentPayment p 
WHERE 
IsPayment = 1
GROUP BY
p.CommitmentSystemID, p.InstallmentSystemID, p.ETLControlID
) gip on gip.CommitmentSystemID = gi.GiftSystemID and gip.GiftInstallmentSystemID = gi.GiftInstallmentSystemID
LEFT JOIN
(
SELECT  
p.CommitmentSystemID,
p.InstallmentSystemID AS GiftInstallmentSystemID,
Count(p.InstallmentPaymentSystemID) AS PaymentCount,
SUM(p.InstallmentPayAmount) AS InstallmentWriteOffAmount,
p.ETLControlID,
MIN(p.PayDate) AS FirstPaymentDate,
MAX(p.PayDate) AS LastPaymentDate
FROM
TEMP_GiftInstallmentPayment p 
WHERE 
IsWriteOff = 1
GROUP BY
p.CommitmentSystemID, p.InstallmentSystemID, p.ETLControlID
) giw on giw.CommitmentSystemID = gi.GiftSystemID and giw.GiftInstallmentSystemID = gi.GiftInstallmentSystemID
WHERE g.GiftTypeDimID IN (8, 27, 30)



GO

/****** Object:  View [dbo].[vw_FACT_GiftInstallmentPayment_Load]    Script Date: 12/05/2011 12:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW
[dbo].[vw_FACT_GiftInstallmentPayment_Load]
as
select
  gip.InstallmentPaymentSystemID,
  gip.InstallmentSystemID,
  gip.CommitmentSystemID,
  gip.PaymentSystemID,
  gip.InstallmentDate,
  gip.CommitmentDate,
  gip.CommitmentTypeSystemID,
  gip.PayTypeSystemID,
  gip.PayInstallmentDays,
  gip.PayCommitmentDays,
  gip.PaymentErrorFlag,
  gip.InstallmentStatus,
  gip.InstallmentAmount,
  gip.PayAmount,
  gip.InstallmentPayAmount,
  gip.BalanceAmount,
  g.GiftFactID,
  g.GiftSystemID,
  g.GiftSplitSystemID,
  g.Amount,
  g.TotalGiftAmount,
  g.SplitRatio,
  g.ReceiptAmount,
  g.IsAnonymous,
  g.PostStatusDimID,
  g.ConstituentDimID,
  g.PaymentTypeDimID,
  g.GiftCodeDimID,
  g.LetterCodeDimID,
  g.GiftTypeDimID,
  g.GiftSubTypeDimID,
  g.CampaignDimID,
  g.FundDimID,
  g.AppealDimID,
  g.PackageDimID,
  g.ConstituentCodeDimID,
  g.GiftDateDimID,
  g.GiftDate,
  g.PostDateDimID,
  g.ChangedDateDimID,
  g.AddedDateDimID,
  g.MGConstituentDimID,
  gc.ConstituentDimID as CommitmentConstituentDimID,
  gc.GiftCodeDimID as CommitmentGiftCodeDimID,
  gc.GiftDateDimID as CommitmentGiftDateDimID,
  gc.GiftSubTypeDimID as CommitmentGiftSubTypeDimID,
  gc.LetterCodeDimID as CommitmentLetterCodeDimID,
  gip.ETLControlID
from
  TEMP_GiftInstallmentPayment as gip
  left outer join FACT_Gift as g on g.GiftSystemID = gip.PaymentSystemID
  left outer join 
  (
    SELECT
tg.GiftSystemID,
tg.ConstituentDimID,
tg.GiftDateDimID,
tg.GiftCodeDimID,
tg.LetterCodeDimID,
tg.GiftSubTypeDimID
FROM
FACT_Gift tg
WHERE
tg.GiftTypeDimID IN (8, 27, 30)
GROUP BY
tg.GiftSystemID,
tg.ConstituentDimID,
tg.GiftDateDimID,
tg.GiftCodeDimID,
tg.LetterCodeDimID,
tg.GiftSubTypeDimID
  ) gc on gc.GiftSystemID = gip.CommitmentSystemID
where
  ( gip.IsPayment = 1 )
  and ( g.GiftTypeDimID in ( 2, 3, 4, 11,
                             12, 13, 14, 16,
                             17, 19, 20, 31 ) )

GO

/****** Object:  View [dbo].[vw_FACT_GiftParent]    Script Date: 08/01/2013 12:04:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_FACT_GiftParent]
AS
SELECT 
	g.GiftSystemID,
	gst.GiftStatus           as GiftStatusDescription,
	inf.InstallmentFrequency as InstallmentFrequencyDescription,
	lc.Description           as LetterCodeDescription,
	gc.Description           as GiftCodeDescription,
	pty.PaymentType          as PaymentTypeDescription,
	ps.PostStatus            as PostStatusDescription
FROM
FACT_GiftCommitmentByPaySplit as g
INNER JOIN dbo.DIM_GiftStatus as gst on g.GiftStatusDimID = gst.GiftStatusDimID
INNER JOIN dbo.DIM_InstallmentFrequency as inf on g.InstallmentFrequencyDimID = inf.InstallmentFrequencyDimID
INNER JOIN dbo.DIM_LetterCode as lc on g.LetterCodeDimID = lc.LetterCodeDimID
INNER JOIN dbo.DIM_GiftCode as gc on g.GiftCodeDimID = gc.GiftCodeDimID
INNER JOIN dbo.DIM_PostStatus as ps on g.PostStatusDimID = ps.PostStatusDimID
INNER JOIN dbo.DIM_PaymentType as pty on g.PaymentTypeDimID = pty.PaymentTypeDimID
GROUP BY
	gst.GiftStatus,
	inf.InstallmentFrequency,
	g.GiftSystemID,
	lc.Description,
	gc.Description,
	pty.PaymentType,
	ps.PostStatus 
GO

/****** Object:  View [dbo].[vw_FACT_GiftParentAttribute]    Script Date: 08/01/2013 12:04:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_FACT_GiftParentAttribute]
AS
SELECT 
	GiftSystemID,
	AttributeDescription,
	AttributeCategory,
	GiftAttributeSystemID,
	Sequence,
	Comments,
	AttributeDate,
	AttributeDateDimID,
	TypeOfData,
	Required,
	MustBeUnique
FROM   
FACT_GiftAttribute as ga
WHERE  
(
GiftSystemID IN (SELECT GiftSystemID
                          FROM   dbo.FACT_Gift
                          WHERE  GiftTypeDimID in ( 8, 27, 30 ) 
                          GROUP BY GiftSystemID) 
)
GROUP BY 
	GiftSystemID,
	AttributeDescription,
	AttributeCategory,
	GiftAttributeSystemID,
	Sequence,
	Comments,
	AttributeDate,
	AttributeDateDimID,
	TypeOfData,
	Required,
	MustBeUnique 
GO


/****** Object:  View [dbo].[vw_FACT_GiftRank]    Script Date: 08/01/2013 11:41:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_FACT_GiftRank]
AS
SELECT     
g.GiftFactID, g.ConstituentDimID, g.GiftDateDimID, g.GiftDate, 
	CASE WHEN gr.Filter1FirstSequence > 25 THEN 25 ELSE gr.Filter1FirstSequence END AS OldestSequence, 
	CASE WHEN gr.Filter1LastSequence > 25 THEN 25 ELSE gr.Filter1LastSequence END AS NewestSequence, 
	CASE WHEN gr.Filter1HighestSequence > 25 THEN 25 ELSE gr.Filter1HighestSequence END AS HighestSequence, 
	CASE WHEN gr.Filter1FirstRank > 25 THEN 25 ELSE gr.Filter1FirstRank END AS OldestRank, 
	CASE WHEN gr.Filter1LastRank > 25 THEN 25 ELSE gr.Filter1LastRank END AS NewestRank, 
	CASE WHEN gr.Filter1HighestRank > 25 THEN 25 ELSE gr.Filter1HighestRank END AS HighestRank, 
	CASE WHEN gr.Filter2FirstSequence > 25 THEN 25 ELSE gr.Filter2FirstSequence END AS Oldest2Sequence, 
	CASE WHEN gr.Filter2LastSequence > 25 THEN 25 ELSE gr.Filter2LastSequence END AS Newest2Sequence, 
	CASE WHEN gr.Filter2HighestSequence > 25 THEN 25 ELSE gr.Filter2HighestSequence END AS Highest2Sequence, 
	CASE WHEN gr.Filter2FirstRank > 25 THEN 25 ELSE gr.Filter2FirstRank END AS Oldest2Rank, 
	CASE WHEN gr.Filter2LastRank > 25 THEN 25 ELSE gr.Filter2LastRank END AS Newest2Rank, 
	CASE WHEN gr.Filter2HighestRank > 25 THEN 25 ELSE gr.Filter2HighestRank END AS Highest2Rank, 
	CASE WHEN gr.Filter3FirstSequence > 25 THEN 25 ELSE gr.Filter3FirstSequence END AS Oldest3Sequence, 
	CASE WHEN gr.Filter3LastSequence > 25 THEN 25 ELSE gr.Filter3LastSequence END AS Newest3Sequence, 
	CASE WHEN gr.Filter3HighestSequence > 25 THEN 25 ELSE gr.Filter3HighestSequence END AS Highest3Sequence, 
	CASE WHEN gr.Filter3FirstRank > 25 THEN 25 ELSE gr.Filter3FirstRank END AS Oldest3Rank, 
	CASE WHEN gr.Filter3LastRank > 25 THEN 25 ELSE gr.Filter3LastRank END AS Newest3Rank, 
	CASE WHEN gr.Filter3HighestRank > 25 THEN 25 ELSE gr.Filter3HighestRank END AS Highest3Rank, 
	CASE WHEN gr.Filter4FirstSequence > 25 THEN 25 ELSE gr.Filter4FirstSequence END AS Oldest4Sequence, 
	CASE WHEN gr.Filter4LastSequence > 25 THEN 25 ELSE gr.Filter4LastSequence END AS Newest4Sequence, 
	CASE WHEN gr.Filter4HighestSequence > 25 THEN 25 ELSE gr.Filter4HighestSequence END AS Highest4Sequence, 
	CASE WHEN gr.Filter4FirstRank > 25 THEN 25 ELSE gr.Filter4FirstRank END AS Oldest4Rank, 
	CASE WHEN gr.Filter4LastRank > 25 THEN 25 ELSE gr.Filter4LastRank END AS Newest4Rank, 
	CASE WHEN gr.Filter4HighestRank > 25 THEN 25 ELSE gr.Filter4HighestRank END AS Highest4Rank, 
	CASE WHEN gr.Filter5FirstSequence > 25 THEN 25 ELSE gr.Filter5FirstSequence END AS Oldest5Sequence, 
	CASE WHEN gr.Filter5LastSequence > 25 THEN 25 ELSE gr.Filter5LastSequence END AS Newest5Sequence, 
	CASE WHEN gr.Filter5HighestSequence > 25 THEN 25 ELSE gr.Filter5HighestSequence END AS Highest5Sequence, 
	CASE WHEN gr.Filter5FirstRank > 25 THEN 25 ELSE gr.Filter5FirstRank END AS Oldest5Rank, 
	CASE WHEN gr.Filter5LastRank > 25 THEN 25 ELSE gr.Filter5LastRank END AS Newest5Rank, 
	CASE WHEN gr.Filter5HighestRank > 25 THEN 25 ELSE gr.Filter5HighestRank END AS Highest5Rank, 
	CASE WHEN gr.Filter6FirstSequence > 25 THEN 25 ELSE gr.Filter6FirstSequence END AS Oldest6Sequence, 
	CASE WHEN gr.Filter6LastSequence > 25 THEN 25 ELSE gr.Filter6LastSequence END AS Newest6Sequence, 
	CASE WHEN gr.Filter6HighestSequence > 25 THEN 25 ELSE gr.Filter6HighestSequence END AS Highest6Sequence, 
	CASE WHEN gr.Filter6FirstRank > 25 THEN 25 ELSE gr.Filter6FirstRank END AS Oldest6Rank, 
	CASE WHEN gr.Filter6LastRank > 25 THEN 25 ELSE gr.Filter6LastRank END AS Newest6Rank, 
	CASE WHEN gr.Filter6HighestRank > 25 THEN 25 ELSE gr.Filter6HighestRank END AS Highest6Rank, 
	CASE WHEN gr.Filter7FirstSequence > 25 THEN 25 ELSE gr.Filter7FirstSequence END AS Oldest7Sequence, 
	CASE WHEN gr.Filter7LastSequence > 25 THEN 25 ELSE gr.Filter7LastSequence END AS Newest7Sequence, 
	CASE WHEN gr.Filter7HighestSequence > 25 THEN 25 ELSE gr.Filter7HighestSequence END AS Highest7Sequence, 
	CASE WHEN gr.Filter7FirstRank > 25 THEN 25 ELSE gr.Filter7FirstRank END AS Oldest7Rank, 
	CASE WHEN gr.Filter7LastRank > 25 THEN 25 ELSE gr.Filter7LastRank END AS Newest7Rank, 
	CASE WHEN gr.Filter7HighestRank > 25 THEN 25 ELSE gr.Filter7HighestRank END AS Highest7Rank, 
	CASE WHEN gr.Filter8FirstSequence > 25 THEN 25 ELSE gr.Filter8FirstSequence END AS Oldest8Sequence, 
	CASE WHEN gr.Filter8LastSequence > 25 THEN 25 ELSE gr.Filter8LastSequence END AS Newest8Sequence, 
	CASE WHEN gr.Filter8HighestSequence > 25 THEN 25 ELSE gr.Filter8HighestSequence END AS Highest8Sequence, 
	CASE WHEN gr.Filter8FirstRank > 25 THEN 25 ELSE gr.Filter8FirstRank END AS Oldest8Rank, 
	CASE WHEN gr.Filter8LastRank > 25 THEN 25 ELSE gr.Filter8LastRank END AS Newest8Rank, 
	CASE WHEN gr.Filter8HighestRank > 25 THEN 25 ELSE gr.Filter8HighestRank END AS Highest8Rank, 
	CASE WHEN gr.Filter9FirstSequence > 25 THEN 25 ELSE gr.Filter9FirstSequence END AS Oldest9Sequence, 
	CASE WHEN gr.Filter9LastSequence > 25 THEN 25 ELSE gr.Filter9LastSequence END AS Newest9Sequence, 
	CASE WHEN gr.Filter9HighestSequence > 25 THEN 25 ELSE gr.Filter9HighestSequence END AS Highest9Sequence, 
	CASE WHEN gr.Filter9FirstRank > 25 THEN 25 ELSE gr.Filter9FirstRank END AS Oldest9Rank, 
	CASE WHEN gr.Filter9LastRank > 25 THEN 25 ELSE gr.Filter9LastRank END AS Newest9Rank, 
	CASE WHEN gr.Filter9HighestRank > 25 THEN 25 ELSE gr.Filter9HighestRank END AS Highest9Rank, 
	CASE WHEN gr.Filter10FirstSequence > 25 THEN 25 ELSE gr.Filter10FirstSequence END AS Oldest10Sequence, 
	CASE WHEN gr.Filter10LastSequence > 25 THEN 25 ELSE gr.Filter10LastSequence END AS Newest10Sequence, 
	CASE WHEN gr.Filter10HighestSequence > 25 THEN 25 ELSE gr.Filter10HighestSequence END AS Highest10Sequence, 
	CASE WHEN gr.Filter10FirstRank > 25 THEN 25 ELSE gr.Filter10FirstRank END AS Oldest10Rank, 
	CASE WHEN gr.Filter10LastRank > 25 THEN 25 ELSE gr.Filter10LastRank END AS Newest10Rank, 
	CASE WHEN gr.Filter10HighestRank > 25 THEN 25 ELSE gr.Filter10HighestRank END AS Highest10Rank, 
	CASE WHEN gr.AllFirstSequence > 25 THEN 25 ELSE gr.AllFirstSequence END AS AllOldestSequence, 
	CASE WHEN gr.AllLastSequence > 25 THEN 25 ELSE gr.AllLastSequence END AS AllNewestSequence, 
	CASE WHEN gr.AllHighestSequence > 25 THEN 25 ELSE gr.AllHighestSequence END AS AllHighestSequence, 
	CASE WHEN gr.AllFirstRank > 25 THEN 25 ELSE gr.AllFirstRank END AS AllOldestRank, 
	CASE WHEN gr.AllLastRank > 25 THEN 25 ELSE gr.AllLastRank END AS AllNewestRank, 
	CASE WHEN gr.AllHighestRank > 25 THEN 25 ELSE gr.AllHighestRank END AS AllHighestRank, gr.Filter1HasSequence, gr.Filter1HighestRankSplit, 
	gr.Filter1HighestSequenceSplit
FROM         
FACT_Gift AS g 
INNER JOIN FACT_GiftRank AS gr ON g.GiftFactID = gr.GiftFactID

GO

/****** Object:  View [dbo].[vw_FACT_GiftSoftCredit]    Script Date: 08/01/2013 11:41:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_FACT_GiftSoftCredit]
AS
SELECT 
	gsc.GiftSoftCreditFactID,
	gsc.GiftFactID,
	gsc.GiftSplitSystemID,
	gsc.GiftSystemID,
	gsc.ConstituentSystemID,
	gsc.ConstituentDimID,
	gsc.SplitRatio,
	gsc.Amount,
	gsc.ETLControlID,
	gsc.SourceId,
	g.GiftCodeDimID,
	g.GiftTypeDimID,
	g.GiftSubTypeDimID,
	g.CampaignDimID,
	g.FundDimID,
	g.AppealDimID,
	g.PackageDimID,
	g.LetterCodeDimID,
	g.ConstituentCodeDimID,
	g.GiftDateDimID,
	g.PostDateDimID,
	g.AcknowledgeDateDimID,
	g.Issuer,
	g.IssuerNumberUnits,
	g.IssuerSymbol,
	g.StockSaleDate,
	g.Reference,
	g.MGConstituentDimID,
	g.SourceCode,
	g.DoNotAcknowledge,
	g.DoNotReceipt,
	g.IsPlannedGiftPayment,
	g.IsTribute,
	g.IsOnline,
	g.IsIncluded,
	g.IsAnonymous,
	g.PostStatusDimID,
	g.ConstituentDimID as OriginatingGiftConstituentDimID,
	g.Recency,
	g.RecencySequence,
	g.Monetary,
	g.MonetarySequence
FROM   
FACT_GiftSoftCredit as gsc
INNER JOIN dbo.FACT_Gift as g on gsc.GiftFactID = g.GiftFactID 

GO


PRINT 'Views Created'


/********************************************************************************************************************************************************************************
		UDF Creates
********************************************************************************************************************************************************************************/

/****** Object:  UserDefinedFunction [dbo].[BBBI_Split]    Script Date: 12/05/2011 12:24:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[BBBI_Split] (

@str_in VARCHAR(8000),
@separator VARCHAR(4) )
RETURNS @strtable TABLE ([ID] [int] IDENTITY(1,1) NOT NULL, strval VARCHAR(8000))

AS
BEGIN
DECLARE
@Occurrences INT,
@Counter INT,
@tmpStr VARCHAR(8000)
SET @Counter = 0
IF SUBSTRING(@str_in,LEN(@str_in),1) <> @separator 
SET @str_in = @str_in + @separator
SET @Occurrences = (DATALENGTH(REPLACE(@str_in,@separator,@separator+'#')) - DATALENGTH(@str_in))/ DATALENGTH(@separator)
SET @tmpStr = @str_in

WHILE @Counter <= @Occurrences 
BEGIN
SET @Counter = @Counter + 1

INSERT INTO @strtable
VALUES ( SUBSTRING(@tmpStr,1,CHARINDEX(@separator,@tmpStr)-1))
SET @tmpStr = SUBSTRING(@tmpStr,CHARINDEX(@separator,@tmpStr)+1,8000)
IF DATALENGTH(@tmpStr) = 0

BREAK

END

RETURN 
END

GO


/****** Object:  UserDefinedFunction [dbo].[BBBI_445AccountingYear]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* Takes in a Fiscal Year and a Date Returns a string representing the year in which the date falls based on a 4-4-5 accounting method.*/

CREATE FUNCTION [dbo].[BBBI_445AccountingYear]
( 
@Fiscal datetime, -- The Month and Day are the relevant pieces to determine the fiscal period start date 
@TestDate datetime, 
@Weekday int-- The day of the week to start accounting: 1-Sunday, 2-Monday, etc.
) 
RETURNS varchar(50) AS
BEGIN 
DECLARE @FiscalStart DateTime
DECLARE @FiscalStartAdjusted DateTime
-- Determine the Fiscal Start Date for the year of the given date.
SELECT @FiscalStart = DateAdd(year, datediff(year, IsNull(@Fiscal,'1/1/1900'), @TestDate) , IsNull(@Fiscal,'1/1/1900'))
-- Adjust the Fiscal Start to the first @Weekday in the Fiscal year.
SELECT @FiscalStartAdjusted = 
CASE 
  WHEN DatePart(dw, @FiscalStart) = @Weekday THEN @FiscalStart  
  WHEN DatePart(dw, @FiscalStart) < @Weekday THEN DateAdd(d, @Weekday - DatePart(dw, @FiscalStart), @FiscalStart) 
ELSE DateAdd(d, @Weekday - DatePart(dw, @FiscalStart) + 7, @FiscalStart) 
END
-- Determine the year for the date
IF (@TestDate < @FiscalStartAdjusted)
BEGIN -- Have to find the starting date on the previous year 
SELECT @FiscalStartAdjusted = DateAdd(year, -1, @FiscalStart) -- Adjust the Fiscal Start to the first @Weekday again because the same day the previous year is not the same day of the week and leap years -- change it by two days. 
SELECT @FiscalStartAdjusted = 
CASE 
  WHEN DatePart(dw, @FiscalStartAdjusted) = @Weekday THEN @FiscalStartAdjusted  
  WHEN DatePart(dw, @FiscalStartAdjusted) < @Weekday THEN DateAdd(d, @Weekday - DatePart(dw, @FiscalStartAdjusted), @FiscalStartAdjusted) 
ELSE DateAdd(d, @Weekday - DatePart(dw, @FiscalStartAdjusted) + 7, @FiscalStartAdjusted) END
END

RETURN 'Year ' + Convert(varchar,Year(@FiscalStartAdjusted)) + ' (Starting ' + Convert(varchar,@FiscalStartAdjusted, 101) + ')'

END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_CalcFiscalPeriod]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[BBBI_CalcFiscalPeriod] (@FiscalYearStart Varchar(5), @FiscalYearEnd Varchar(5) = '', @CurrentDate DateTime, @PeriodRequired Int = 0)  
RETURNS DateTime AS  
BEGIN 
-- Arguments @PeriodRequired = 0 Current Fiscal Year Start, 1 Current Fiscal Year End
-- Arguments @PeriodRequired = 2 Next Fiscal Year Start, 3 Next Fiscal Year End
-- Arguments @PeriodRequired = 4 Prev Fiscal Year Start, 5 Prev Fiscal Year End
DECLARE @FiscalMonthStart Int
DECLARE @CurrentYear Int
DECLARE @CurrentMonth Int
DECLARE @CurrentFiscalYear Int
DECLARE @FiscalPeriod DateTime
DECLARE @YearEndDate DateTime
DECLARE @YearEnd Varchar(5)
DECLARE @StartDate DateTime
DECLARE @EndDate DateTime
SET @CurrentYear = Year(@CurrentDate)
SET @CurrentMonth = Month(@CurrentDate)
SET @FiscalMonthStart = 
(SELECT
CASE 
WHEN LEN(@FiscalYearStart) = 1 THEN Convert(Int, Left(@FiscalYearStart,1))
ELSE Convert(Int, Left(@FiscalYearStart,2))
END)

SET @CurrentFiscalYear = 
(SELECT
CASE 
WHEN @FiscalMonthStart = 1 THEN @CurrentYear + 1
WHEN @FiscalMonthStart > 1 AND @CurrentMonth >= @FiscalMonthStart THEN @CurrentYear + 1
WHEN @CurrentMonth < @FiscalMonthStart THEN @CurrentYear
END)

SET @YearEndDate =  
(SELECT CASE WHEN @FiscalMonthStart = 1 THEN
Convert(DateTime, @FiscalYearStart + '/'+Convert(Varchar(4),@CurrentFiscalYear - 1)) - 1
ELSE
Convert(DateTime, @FiscalYearStart + '/'+Convert(Varchar(4),@CurrentFiscalYear)) - 1
END)

IF @FiscalYearEnd = ''
	SET @YearEnd = Convert(Varchar(5), @YearEndDate,101)
ELSE
	SET @YearEnd = @FiscalYearEnd

SET @StartDate = Convert(DateTime, @FiscalYearStart+'/'+Convert(Varchar(4),@CurrentFiscalYear - 1)) 
SET @EndDate = Convert(DateTime, @YearEnd+'/'+Convert(Varchar(4),@CurrentFiscalYear))

IF @FiscalMonthStart = 1 -- Adjustment to Year of Month starts in Jan 
	SET @EndDate = DATEADD(yy, -1, @EndDate)
IF @PeriodRequired = 0    -- Current Fiscal Start Date 
	SET @FiscalPeriod = @StartDate
IF @PeriodRequired = 1    -- Current Fiscal Start Date 
	SET @FiscalPeriod = @EndDate
IF @PeriodRequired = 2  -- Next Fiscal Start Date 
	SET @FiscalPeriod = DATEADD(yy,1,@StartDate)
IF @PeriodRequired = 3  -- Next Fiscal End Date 
	SET @FiscalPeriod = DATEADD(yy,1,@EndDate)
IF @PeriodRequired = 4  -- Previous Fiscal Start Date 
	SET @FiscalPeriod = DATEADD(yy,-1,@StartDate)
IF @PeriodRequired = 5  -- Previous Fiscal End Date 
	SET @FiscalPeriod = DATEADD(yy,-1,@EndDate)

RETURN (@FiscalPeriod)

END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_CalcQuarter]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_CalcQuarter] (@InputDate Varchar(20), @QuarterStartMonth Int, @QuarterOnly Int = 0)  
RETURNS Varchar(10) AS  
BEGIN 
-- UDF which returns the Quarter Number and Year based on being passed a Data and Month when a fiscal year starts
-- Arguments @QuartOnly = 0 for Qn-YYYY format for this Qtr, 1 for Qn only format, 2 for Qtr YYYY only format
-- Arguments @QuartOnly = 3 for Qn-YYYY format for Next Qtr, 4 for Next Qn only format, 5 for Next Qtr YYYY only format
-- Arguments @QuartOnly = 6 for Qn-YYYY format for Next Qtr, 7 for Next Qn only format, 8 for Next Qtr YYYY only format

DECLARE @FiscalMonth As Varchar(2)
DECLARE @FiscalQuarter As Varchar(2)
DECLARE @FiscalYear As Varchar(4)
DECLARE @FiscalQuarterYear As Varchar(20)
DECLARE @FiscalNextQuarter As Varchar(2)
DECLARE @FiscalNextYear As Varchar(4)
DECLARE @FiscalNextQuarterYear As Varchar(20)
DECLARE @FiscalPrevQuarter As Varchar(2)
DECLARE @FiscalPrevYear As Varchar(4)
DECLARE @FiscalPrevQuarterYear As Varchar(20)
DECLARE @ThisYear As Varchar(4)
DECLARE @NextYear As Varchar(4)
DECLARE @PrevYear As Varchar(4)
DECLARE @FiscalMonthNum As Int
DECLARE @NextQuarterStartMonth As Int
DECLARE @QuarterNum AS Int
DECLARE @iPos AS Int
DECLARE @FiscalQuarterMonth As Int

SET @iPos = PATINDEX('%/%', @InputDate)
SET @FiscalMonth = LEFT(@InputDate,@iPos - 1)
SET @FiscalMonthNum = Convert(Int, @FiscalMonth)
SET @NextQuarterStartMonth = @QuarterStartMonth
SET @ThisYear = RIGHT(@InputDate, 4)
SET @NextYear = Convert(Varchar(4), Convert(Int, RIGHT(@InputDate, 4)) + 1)
SET @PrevYear = Convert(Varchar(4), Convert(Int, RIGHT(@InputDate, 4)) - 1)


IF @QuarterStartMonth = 1
BEGIN
	SET @QuarterNum = ((@FiscalMonthNum+2) / 3)
	SET @FiscalQuarter = 'Q'+Convert(Varchar(1), @QuarterNum)
	SET @FiscalYear = @ThisYear
	
	IF @QuarterNum < 4
	BEGIN
		SET @FiscalNextYear = @ThisYear
		SET @FiscalNextQuarter = 'Q' + Convert(Varchar(1), @QuarterNum + 1)
	END
	IF @QuarterNum = 4
	BEGIN
		SET @FiscalNextYear = @NextYear
		SET @FiscalNextQuarter = 'Q1'
	END
	IF @QuarterNum = 1
	BEGIN
		SET @FiscalPrevYear = @PrevYear
		SET @FiscalPrevQuarter = 'Q4'
	END
	IF @QuarterNum > 1
	BEGIN
		SET @FiscalPrevYear = @ThisYear
		SET @FiscalPrevQuarter = 'Q'+Convert(Varchar(1), @QuarterNum - 1)
	END

END
ELSE
BEGIN
IF
(@QuarterStartMonth = 1 AND @FiscalMonthNum in (1, 2, 3)) OR
(@QuarterStartMonth = 2 AND @FiscalMonthNum in (2, 3, 4)) OR
(@QuarterStartMonth = 3 AND @FiscalMonthNum in (3, 4, 5)) OR
(@QuarterStartMonth = 4 AND @FiscalMonthNum in (4 ,5 ,6)) OR
(@QuarterStartMonth = 5 AND @FiscalMonthNum in (5 ,6 ,7)) OR
(@QuarterStartMonth = 6 AND @FiscalMonthNum in (6 ,7 ,8)) OR
(@QuarterStartMonth = 7 AND @FiscalMonthNum in (7 ,8 ,9)) OR
(@QuarterStartMonth = 8 AND @FiscalMonthNum in (8, 9 , 10)) OR
(@QuarterStartMonth = 9 AND @FiscalMonthNum in (9, 10 , 11)) OR
(@QuarterStartMonth = 10 AND @FiscalMonthNum in (10, 11 , 12)) OR
(@QuarterStartMonth = 11 AND @FiscalMonthNum in (11, 12 ,1)) OR
(@QuarterStartMonth = 12 AND @FiscalMonthNum in (12, 1 , 2))
BEGIN 
	-------------------------------   Q1  ------------------------------------
	SET @FiscalPrevQuarter = 'Q4'
	SET @FiscalQuarter = 'Q1'
	SET @FiscalNextQuarter = 'Q2'

	SET @FiscalPrevYear = @ThisYear
	SET @FiscalYear = @NextYear
	SET @FiscalNextYear = @NextYear

	SET @FiscalQuarterMonth = @QuarterStartMonth
END

IF 
(@QuarterStartMonth = 1 AND @FiscalMonthNum in (4,5,6)) OR
(@QuarterStartMonth = 2 AND @FiscalMonthNum in (5,6,7)) OR
(@QuarterStartMonth = 3 AND @FiscalMonthNum in (6,7,8)) OR
(@QuarterStartMonth = 4 AND @FiscalMonthNum in (7 ,8 ,9)) OR
(@QuarterStartMonth = 5 AND @FiscalMonthNum in (8 ,9 ,10)) OR
(@QuarterStartMonth = 6 AND @FiscalMonthNum in (9 ,10 ,11)) OR
(@QuarterStartMonth = 7 AND @FiscalMonthNum in (10 ,11 ,12)) OR
(@QuarterStartMonth = 8 AND @FiscalMonthNum in ( 11,12 ,1)) OR
(@QuarterStartMonth = 9 AND @FiscalMonthNum in ( 12,1 ,2)) OR
(@QuarterStartMonth = 10 AND @FiscalMonthNum in ( 1,2 ,3)) OR
(@QuarterStartMonth = 11 AND @FiscalMonthNum in ( 2,3 ,4)) OR
(@QuarterStartMonth = 12 AND @FiscalMonthNum in ( 3,4 ,5))
BEGIN 
	-------------------------------   Q2  ------------------------------------
	SET @FiscalPrevQuarter = 'Q1'
	SET @FiscalQuarter = 'Q2'
	SET @FiscalNextQuarter = 'Q3'

	SET @FiscalYear = @NextYear
	SET @FiscalNextYear = @NextYear
	SET @FiscalPrevYear = @NextYear
	SET @FiscalQuarterMonth = @QuarterStartMonth + 3
END

IF 
(@QuarterStartMonth = 1 AND @FiscalMonthNum in (7, 8, 9)) OR
(@QuarterStartMonth = 2 AND @FiscalMonthNum in (8, 9, 10)) OR
(@QuarterStartMonth = 3 AND @FiscalMonthNum in (9, 10, 11)) OR
(@QuarterStartMonth = 4 AND @FiscalMonthNum in (10 ,11 ,12)) OR
(@QuarterStartMonth = 5 AND @FiscalMonthNum in (11 ,12 ,1)) OR
(@QuarterStartMonth = 6 AND @FiscalMonthNum in (12 ,1 ,2)) OR
(@QuarterStartMonth = 7 AND @FiscalMonthNum in (1 ,2 ,3)) OR
(@QuarterStartMonth = 8 AND @FiscalMonthNum in (2, 3 ,4)) OR
(@QuarterStartMonth = 9 AND @FiscalMonthNum in (3,  4 , 5)) OR
(@QuarterStartMonth = 10 AND @FiscalMonthNum in ( 4, 5 , 6)) OR
(@QuarterStartMonth = 11 AND @FiscalMonthNum in ( 5, 6 ,7)) OR
(@QuarterStartMonth = 12 AND @FiscalMonthNum in ( 6, 7 , 8))
BEGIN 
	-------------------------------   Q3  ------------------------------------
	SET @FiscalPrevQuarter = 'Q2'
	SET @FiscalQuarter = 'Q3'
	SET @FiscalNextQuarter = 'Q4'

	IF @QuarterStartMonth < 5
	BEGIN
		SET @FiscalPrevYear = @NextYear
		SET @FiscalYear = @NextYear
		SET @FiscalNextYear = @NextYear
	END
	IF @QuarterStartMonth > 4
	BEGIN
		SET @FiscalPrevYear = @ThisYear
		SET @FiscalYear = @ThisYear
		SET @FiscalNextYear = @ThisYear
	END
	SET @FiscalQuarterMonth = @QuarterStartMonth + 6
END
IF
(@QuarterStartMonth = 1 AND @FiscalMonthNum in (10, 11, 12)) OR
(@QuarterStartMonth = 2 AND @FiscalMonthNum in (11, 12, 1)) OR
(@QuarterStartMonth = 3 AND @FiscalMonthNum in (12, 1, 2)) OR
(@QuarterStartMonth = 4 AND @FiscalMonthNum in (1, 2, 3)) OR
(@QuarterStartMonth = 5 AND @FiscalMonthNum in (2, 3, 4)) OR
(@QuarterStartMonth = 6 AND @FiscalMonthNum in (3, 4, 5)) OR
(@QuarterStartMonth = 7 AND @FiscalMonthNum in (4 ,5 ,6)) OR
(@QuarterStartMonth = 8 AND @FiscalMonthNum in (5 ,6 ,7)) OR
(@QuarterStartMonth = 9 AND @FiscalMonthNum in (6 ,7 ,8)) OR
(@QuarterStartMonth = 10 AND @FiscalMonthNum in (7 ,8 ,9)) OR
(@QuarterStartMonth = 11 AND @FiscalMonthNum in (8, 9 , 10)) OR
(@QuarterStartMonth = 12 AND @FiscalMonthNum in (9, 10 , 11))
BEGIN 
	-------------------------------   Q4  ------------------------------------
	SET @FiscalPrevQuarter = 'Q3'
	SET @FiscalQuarter = 'Q4'
	SET @FiscalNextQuarter = 'Q1'

	SET @FiscalPrevYear = @ThisYear
	SET @FiscalYear = @ThisYear
	SET @FiscalNextYear = @NextYear
	SET @FiscalQuarterMonth = @QuarterStartMonth + 9
END

END

SET @FiscalQuarterYear = @FiscalQuarter + '-' + @FiscalYear
SET @FiscalNextQuarterYear = @FiscalNextQuarter + '-' + @FiscalNextYear

IF @QuarterOnly = 1 
	SET @FiscalQuarterYear = @FiscalQuarter
IF @QuarterOnly = 2 
	SET @FiscalQuarterYear = @FiscalYear
IF @QuarterOnly = 3 
	SET @FiscalQuarterYear = @FiscalNextQuarter + '-' + @FiscalNextYear
IF @QuarterOnly = 4 
	SET @FiscalQuarterYear = @FiscalNextQuarter
IF @QuarterOnly = 5 
	SET @FiscalQuarterYear = @FiscalNextYear
IF @QuarterOnly = 6 
	SET @FiscalQuarterYear = @FiscalPrevQuarter + '-' + @FiscalPrevYear
IF @QuarterOnly = 7 
	SET @FiscalQuarterYear = @FiscalPrevQuarter
IF @QuarterOnly = 8 
	SET @FiscalQuarterYear = @FiscalPrevYear
IF @QuarterOnly = 9
BEGIN
	IF @FiscalQuarterMonth > 12
		SET @FiscalQuarterMonth = @FiscalQuarterMonth - 12
	SET @FiscalQuarterYear = Convert(Varchar(2), @FiscalQuarterMonth) +'/01/' + @ThisYear
END
	
IF @QuarterOnly = 10 
	SET @FiscalQuarterYear = RIGHT(@FiscalQuarter, 1)

RETURN (@FiscalQuarterYear)

END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_CalcQuarterDate]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_CalcQuarterDate] (@InputDate AS DateTime, @QuarterStartMonth Int, @QuarterOnly Int = 0)  
RETURNS Varchar(20) 
AS  
BEGIN 
DECLARE @QuarterDate As Varchar(20)
SET @QuarterDate = Convert(Varchar(20), @InputDate, 101)
RETURN dbo.BBBI_CalcQuarter (@QuarterDate, @QuarterStartMonth, @QuarterOnly)  
END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ConvertFuzzyDate]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[BBBI_ConvertFuzzyDate](@FuzzyDate varchar(8))
/*--------------------------------------------------------------------------------------------------------------------------------------
	Takes a String of format YYYY, YYYYMM YYYYMMMDD, verifies it and converts to full YYYYMMDD.
	Change 
	@DefaultMonthDay
	@DefaultDay 
	to put in the MM or DD if the Month or Day is missing
-------------------------------------------------------------------------------------------------------------------------------------*/
RETURNS Varchar(8) AS
BEGIN
DECLARE @CurrentDate AS DateTime
DECLARE @CurrentYear As Int
DECLARE @FuzzyDateInt AS Int
DECLARE @FuzzyDateStr AS Varchar(8)
DECLARE @ConvertedDate AS DateTime
DECLARE @IsLeapYear As SmallInt
DECLARE @Year AS Int
DECLARE @DefaultMonthDay Int
DECLARE @DefaultDay Int

-------------- Change these default values ---------------------------------
SET @DefaultMonthDay = 715 -- 0715 for July 15th - Middle of Year
SET @DefaultDay = 15 -- 15 for Middle of the month
-----------------------------------------------------------------------------------
SET @FuzzyDate = REPLACE(@FuzzyDate, ' ', '0')

IF NOT ASCII(SUBSTRING(@FuzzyDate, 1, 1)) BETWEEN 48 AND 57
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 2, 1)) BETWEEN 48 AND 57
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 3, 1)) BETWEEN 48 AND 57
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 4, 1)) BETWEEN 48 AND 57
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 5, 1)) BETWEEN 48 AND 57 AND LEN(@FuzzyDate) > 4
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 6, 1)) BETWEEN 48 AND 57 AND LEN(@FuzzyDate) > 5
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 7, 1)) BETWEEN 48 AND 57 AND LEN(@FuzzyDate) > 6
	RETURN NULL
IF NOT ASCII(SUBSTRING(@FuzzyDate, 8, 1)) BETWEEN 48 AND 57 AND LEN(@FuzzyDate) > 7
	RETURN NULL

IF NOT Convert(Int, SUBSTRING(@FuzzyDate, 7, 2)) BETWEEN 1 AND 31 AND LEN(@FuzzyDate) > 6
	SET @FuzzyDate = LEFT(@FuzzyDate, 6)

IF Convert(Int, LEFT(@FuzzyDate, 2)) < 17 OR Convert(Int, LEFT(@FuzzyDate, 2)) > 20
	RETURN NULL

IF LEN(@FuzzyDate) < 4
	RETURN NULL

IF (NOT Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) BETWEEN 1 AND 12) AND LEN(@FuzzyDate) > 4
	RETURN NULL

SET @CurrentDate = GetDate()
SET @CurrentYear = Year(@CurrentDate)
SET @FuzzyDateInt = Convert(Int, @FuzzyDate)

IF LEN(@FuzzyDate) BETWEEN 5 AND 6
BEGIN
	IF Convert(Int, LEFT(@FuzzyDate, 4)) < 1700 AND Convert(Int, LEFT(@FuzzyDate, 4)) > @CurrentYear
		RETURN NULL
	ELSE
		BEGIN
			SET @FuzzyDateInt = Convert(Int, LEFT(@FuzzyDate, 4)) * 10000 + Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) * 100 + @DefaultDay
			SET @FuzzyDateStr = Convert(Varchar(8), @FuzzyDateInt)
			RETURN @FuzzyDateStr
		END
END

IF LEN(@FuzzyDate) = 4
BEGIN
	IF @FuzzyDateInt < 1700 AND @FuzzyDateInt > @CurrentYear
	BEGIN
		RETURN NULL
	END
	ELSE
		BEGIN
			SET @FuzzyDateInt = Convert(Int, @FuzzyDate) * 10000 + @DefaultMonthDay
			SET @FuzzyDateStr = Convert(Varchar(8), @FuzzyDateInt)
			RETURN @FuzzyDateStr
		END
END
ELSE
BEGIN
IF LEN(@FuzzyDate) > 6
BEGIN
	SET @Year = Convert(Int, LEFT(@FuzzyDate, 4))
	IF @year % 400 = 0
		SET @IsLeapYear = 1
  ELSE
  BEGIN
    IF @year % 100 = 0
		SET @IsLeapYear = 0
    ELSE
    BEGIN
      IF @year % 4 = 0
		SET @IsLeapYear = 1
      ELSE
		SET @IsLeapYear = 0
    END
  END

IF Convert(Int, SUBSTRING(@FuzzyDate, 7, 2)) > 31
BEGIN
	SET @FuzzyDateInt = Convert(Int, LEFT(@FuzzyDate, 4)) * 10000 + Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) * 100 + @DefaultDay
	SET @FuzzyDateStr = Convert(Varchar(8), @FuzzyDateInt)
	RETURN @FuzzyDateStr
END

IF Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) = 2 AND Convert(Int, SUBSTRING(@FuzzyDate, 7, 2)) > 28 AND @IsLeapYear = 1
BEGIN
	SET @FuzzyDateStr = LEFT(@FuzzyDate, 6) + '29'
	RETURN @FuzzyDateStr
END

IF Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) = 2 AND Convert(Int, SUBSTRING(@FuzzyDate, 7, 2)) > 28 AND @IsLeapYear = 0
BEGIN
	SET @FuzzyDateStr = LEFT(@FuzzyDate, 6) + '28'
	RETURN @FuzzyDateStr
END

-- September, April, June and November
IF Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) IN (4, 6, 9, 11) AND Convert(Int, SUBSTRING(@FuzzyDate, 7, 2)) > 30
BEGIN
	SET @FuzzyDateStr = LEFT(@FuzzyDate, 6) + '30'
	RETURN @FuzzyDateStr
END

SET @FuzzyDateInt = Convert(Int, LEFT(@FuzzyDate, 4)) * 10000 + Convert(Int, SUBSTRING(@FuzzyDate, 5, 2)) * 100 + Convert(Int, SUBSTRING(@FuzzyDate, 7, 2))
SET @FuzzyDateStr = Convert(Varchar(8), @FuzzyDateInt)

END -- LEN(@FuzzyDate) > 6

END

RETURN @FuzzyDateStr

END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_IndexCreateCmd]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[BBBI_IndexCreateCmd] (@TableID int, @IndexID int, @IndexColumnPosition int)
returns varchar(255)
as
BEGIN

	declare @retval varchar(255)
	declare @separator varchar(1)

	set @retval=''
	IF @IndexColumnPosition = 1
	BEGIN
	SELECT @retval = @retval + CASE WHEN Len(@retval)=0 THEN '' 
				ELSE ',' end + ColumnName
	FROM CTL_IndexHistory 
	WHERE   TableSystemID = @TableID AND 
					IndexSystemID=@IndexID AND IndexColumnPosition = 0
	ORDER BY TableSystemID ASC, IndexSystemID ASC, IndexColumnPosition DESC
	END
	ELSE
	BEGIN
	SELECT @retval = ColumnName + CASE WHEN Len(@retval)=0 THEN '' 
				ELSE ',' end + @retval
	FROM CTL_IndexHistory 
	WHERE   TableSystemID = @TableID AND 
					IndexSystemID=@IndexID --AND IndexColumnPosition = 0
	ORDER BY TableSystemID ASC, IndexSystemID ASC, IndexColumnPosition DESC

	END

	return @retval

END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_IndexFindInclude]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[BBBI_IndexFindInclude] (@TableID int, @IndexID int)
returns varchar(255)
as
BEGIN

	declare @retval varchar(255)
	declare @separator varchar(1)

	set @retval=''
--	SELECT @retval = Convert(Int, @retval + CASE WHEN Len(@retval)=0 THEN '' 
--				ELSE '' end + Convert(Varchar(3), IndexColumnPosition))
	SELECT @retval = 1
	FROM CTL_IndexHistory  
	WHERE   TableSystemID = @TableID AND 
					IndexSystemID=@IndexID AND IndexColumnPosition = 0
	ORDER BY TableSystemID ASC, IndexSystemID ASC, IndexColumnPosition DESC

	return @retval

END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_IsAlpha]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[BBBI_IsAlpha] 
               (@Input     VARCHAR(255), 
                @Position   INT)
                 
RETURNS TINYINT 
AS 
  BEGIN 
  DECLARE @IsTrue TINYINT
	SET @IsTrue = CASE WHEN ASCII(SUBSTRING(UPPER(@Input), @Position, 1)) BETWEEN 65 AND 90 THEN 1 ELSE 0 END
RETURN @IsTrue
  END



GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_LowerUpperRange]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[BBBI_LowerUpperRange]
               (
               @Value int,
               @LowerRange int = 0,
               @UpperRange  int  = 0,
               @Interval int = 3,
               @IsNull bit = 1,
               @IsOverlap bit = 0
                )
returns nvarchar(25)
as
  begin
    declare  @LowerBound int
    declare  @UpperBound int
    declare  @RangeOutput nvarchar(25)
    declare @Overlap int
   
    set @Overlap = @IsOverlap   

	if @UpperRange = 0 
		set @UpperRange = @Interval * 20

	set @LowerBound = (ROUND(@Value / @Interval, 0, 1) * @Interval) 
	set @UpperBound = (ROUND(@Value / @Interval, 0, 1) * @Interval) + @Interval


	if @value < @LowerRange  
			if @IsNull = 1
				set @RangeOutput = NULL
			else
				set @RangeOutput = 'Less than ' + convert(nvarchar(8), @LowerRange)
	if @value >= @UpperRange  
			if @IsNull = 1
				set @RangeOutput = NULL
			else
				set @RangeOutput = convert(nvarchar(8), @UpperRange) + '+' 
	
	if @value >= @LowerRange and @value < @LowerRange + @Interval
		   set @RangeOutput = convert(nvarchar(8), @LowerBound) + ' - ' + convert(nvarchar(8), @UpperBound - 1)
	if @value >= @LowerRange + @Interval and @value < @UpperRange
		   set @RangeOutput = convert(nvarchar(8), @LowerBound + @Overlap - 1) + ' - ' + convert(nvarchar(8), @UpperBound - 1)
	 

    return (@RangeOutput)
  end


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_LowerUpperRangeSequence]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[BBBI_LowerUpperRangeSequence]
               (
               @Value int,
               @LowerRange int = 0,
               @UpperRange  int  = 0,
               @Interval int = 3,
               @StartSequence int = 0
                )
returns int
as
  begin
    declare  @LowerBound int
    declare  @UpperBound int
    declare  @RangeOutput int

	if @UpperRange = 0 
		set @UpperRange = @Interval * 20
	
	set @LowerBound = (ROUND(@LowerRange / @Interval, 0, 1) * @Interval)
	set @UpperBound = (ROUND(@UpperRange / @Interval, 0, 1) * @Interval) + (@Interval * 2)

	if @value < @LowerRange  
				set @RangeOutput = @LowerBound + @StartSequence
	if @value > @UpperRange  
				set @RangeOutput = @UpperBound + @StartSequence
	if @value >= @LowerRange and @value <= @UpperRange
		   set @RangeOutput = (ROUND(@Value / @Interval, 0, 1) * @Interval) + @Interval - 1
	 

    return (@RangeOutput)
  end


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_MonthEnd]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_MonthEnd] (@CurrentDate DateTime, @NoPeriods Int = 0)  
RETURNS DateTime AS  
BEGIN 
-- Arguments @PeriodRequired = 0 Current Fiscal Year Start, 1 Current Fiscal Year End
-- Arguments @PeriodRequired = 2 Next Fiscal Year Start, 3 Next Fiscal Year End
-- Arguments @PeriodRequired = 4 Prev Fiscal Year Start, 5 Prev Fiscal Year End
DECLARE @CurrentMonth Int
DECLARE @CurrentYear VarChar(4)
DECLARE @PrevMonth Int
DECLARE @LastMonthEnd DateTime
SET @CurrentYear = Convert(Varchar(4), Year(@CurrentDate))
SET @CurrentMonth = Month(@CurrentDate)
SET @PrevMonth = @CurrentMonth - 1
SET @LastMonthEnd = DateAdd(m, @NoPeriods, Convert(DateTime, Convert(Varchar(2),@CurrentMonth)+'/01/'+@CurrentYear) - 1)
RETURN (@LastMonthEnd)
END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_NthIndex]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[BBBI_NthIndex] 
               (@Input     VARCHAR(255), 
                @Delimiter CHAR(1), 
                @Ordinal   INT) 
RETURNS INT 
AS 
  BEGIN 
    DECLARE  @Pointer INT, 
             @Last    INT, 
             @Count   INT 
    SET @Pointer = 1 
    SET @Last = 0 
    SET @Count = 1 
    WHILE (2 > 1) 
      BEGIN 
        SET @Pointer = CHARINDEX(@Delimiter,@Input,@Pointer) 
        IF @Pointer = 0 
          BREAK 
        IF @Count = @Ordinal 
          BEGIN 
            SET @Last = @Pointer 
            BREAK 
          END 
        SET @Count = @Count + 1 
        SET @Pointer = @Pointer + 1 
      END 
    RETURN @Last 
  END



GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_NthString]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[BBBI_NthString] 
               (@Input     VARCHAR(255), 
                @Delimiter CHAR(1), 
                @Ordinal   INT,
                @Before BIT = 1)
RETURNS VARCHAR(255) 
AS 
  BEGIN 
    DECLARE @Output  VARCHAR(255)
    DECLARE  @Pointer INT, 
             @Last    INT, 
             @Count   INT 
    SET @Pointer = 1 
    SET @Last = 0 
    SET @Count = 1 
    WHILE (2 > 1) 
      BEGIN 
        SET @Pointer = CHARINDEX(@Delimiter,@Input,@Pointer) 
        IF @Pointer = 0 
          BREAK 
        IF @Count = @Ordinal 
          BEGIN 
            SET @Last = @Pointer 
            BREAK 
          END 
        SET @Count = @Count + 1 
        SET @Pointer = @Pointer + 1 
      END 
     IF @Before = 1
		set @Output = LEFT(@Input, @Last)
	ELSE
		set @Output = SUBSTRING(@Input, @Last + 1, LEN(@Input) + 1 - @Pointer)
		
	RETURN @Output	
  END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ParseFileName]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[BBBI_ParseFileName] 
               (@FileName     NVARCHAR(500), 
                @Extension NVARCHAR(100), 
                @ReturnLogicalName BIT = 1)
                
RETURNS NVARCHAR(500) 
AS 
BEGIN 
declare @FileFullName nvarchar(500)
declare @FileLogicalName nvarchar(500)
declare @OutputName nvarchar(500)

set @FileLogicalName = (SELECT CASE WHEN CHARINDEX(@Extension, @FileName) > 0 THEN LEFT(@FileName, CHARINDEX(@Extension, @FileName) - 1) ELSE @FileName END)
set @FileFullName = (SELECT CASE WHEN CHARINDEX(@Extension, @FileName) > 0 THEN @FileName ELSE @FileName + @Extension END)
set @FileName = @FileLogicalName

IF @ReturnLogicalName = 1
	set @OutputName = @FileLogicalName
ELSE
	set @OutputName = @FileFullName
	
RETURN @OutputName	
END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ReturnFiscalYear]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_ReturnFiscalYear] (@InputDate DateTime, @FiscalMonthStart Int = 1)  
RETURNS Varchar(4) AS  
BEGIN 
-- UDF which returns the Fiscal year based on the input date and fiscal month start
DECLARE @CurrentYear Varchar(4)
DECLARE @NextYear Varchar(4)
DECLARE @CurrentMonth Int
DECLARE @FiscalYear Varchar(4)

SET @CurrentYear = Convert(Varchar(4), Year(@InputDate))
SET @NextYear = Convert(Varchar(4), Year(@InputDate) + 1)
SET @CurrentMonth = Month(@InputDate)

IF @FiscalMonthStart = 1 
BEGIN
	SET @FiscalYear = @CurrentYear
END
ELSE
BEGIN
	IF @CurrentMonth < @FiscalMonthStart
         		SET @FiscalYear = @CurrentYear  
	ELSE
         		SET @FiscalYear = @NextYear             
END

RETURN (@FiscalYear)

END

GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_ReturnXMLValue]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[BBBI_ReturnXMLValue] (@Tag Varchar(50), @XML NVarchar(4000))  
RETURNS Varchar(100) AS  
BEGIN 
DECLARE @StartPos AS Int
DECLARE @EndPos As Int
DECLARE @Len AS Int
DECLARE @TagValue AS Varchar(100)
SET @StartPos = CharIndex('<' + @Tag + '>', @XML)
SET @EndPos = CharIndex('</' + @Tag + '>', @XML)
IF @EndPos < 2
	SET @TagValue = ''
ELSE
BEGIN
	SET @Len = LEN(@Tag)
	SET @TagValue = SUBSTRING(LEFT(@XML, @EndPos - 1), @StartPos + @Len + 2, 100)
END

RETURN (@TagValue)
END


GO

/****** Object:  UserDefinedFunction [dbo].[BBBI_YearMonthDescription]    Script Date: 12/05/2011 12:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[BBBI_YearMonthDescription] (@Year Int, @Month Int)  
RETURNS Varchar(25) AS  
BEGIN 
DECLARE @YearMonthDescription Varchar(25)
DECLARE @YearOutput Varchar(5)
DECLARE @MonthOutput Varchar(5)
DECLARE @MonthTag Varchar(10)
DECLARE @YearTag Varchar(10)
DECLARE @PastFuture Varchar(10)

IF @Year = 0 AND @Month = 0 
	SET @YearMonthDescription = 'Current'
ELSE
BEGIN

	SET @YearOutput = Convert(Varchar(5), ABS(@Year))
	SET @MonthOutput = Convert(Varchar(5), ABS(@Month))

	IF @Year >= 0 AND @Month >=0 
		SET @PastFuture = ' Past'
	ELSE
		SET @PastFuture = ' Future'

	IF ABS(@Year) = 1  
		SET @YearTag = ' Year '
	ELSE IF @Year = 0 
	BEGIN
		SET @YearTag = ''
		SET @YearOutput = ''
	END
	ELSE IF ABS(@Year) > 1 
		SET @YearTag = ' Years '

	IF ABS(@Month) = 1  
		SET @MonthTag = ' Month'
	ELSE IF @Month = 0 
	BEGIN
		SET @MonthTag = ''
		SET @MonthOutput = ''
		SET @YearTag = RTRIM(@YearTag)
	END
	ELSE IF ABS(@Month) > 1 
		SET @MonthTag = ' Months'	

	SET @YearMonthDescription = @YearOutput + @YearTag + @MonthOutput + @MonthTag + @PastFuture

END -- @Year = 0 AND @Month = 0

RETURN (@YearMonthDescription)
END


GO

PRINT 'UDFs Created'

/********************************************************************************************************************************************************************************
		USP Creates
********************************************************************************************************************************************************************************/

USE [BBPM_DW]
GO

/****** Object:  StoredProcedure [dbo].[BBBI_ConstituentAnnualGivingUpgrade]    Script Date: 06/21/2013 18:38:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_ConstituentAnnualGivingUpgrade]
    	@SummaryDimID Int = 1
AS
BEGIN
SELECT
		mc.ConstituentDimID,
		--mc.FiscalYear,
		COALESCE(mn.FiscalYear, mc.FiscalYear + 1) AS FiscalYear,
		--rank() over (partition by mc.ConstituentDimID order by mc.FiscalYear) as ConsecutiveYears,
		--mc.TotalGiftAmount AS CYTotalGiftAmount,
		--mn.TotalGiftAmount AS NYTotalGiftAmount,
		CASE 
			WHEN COALESCE(mn.TotalGiftAmount, 0) > COALESCE(mc.TotalGiftAmount, 0) THEN 'Upgrade'
			WHEN COALESCE(mn.TotalGiftAmount, 0) < COALESCE(mc.TotalGiftAmount, 0)  THEN 'Downgrade'
			WHEN COALESCE(mn.TotalGiftAmount, 0) = COALESCE(mc.TotalGiftAmount, 0)  THEN 'Same'
		END AS TotalUpgradeType,
		CASE 
			WHEN COALESCE(mn.HighestGiftAmount, 0) > COALESCE(mc.HighestGiftAmount, 0) THEN 'Upgrade'
			WHEN COALESCE(mn.HighestGiftAmount, 0) < COALESCE(mc.HighestGiftAmount, 0)  THEN 'Downgrade'
			WHEN COALESCE(mn.HighestGiftAmount, 0) = COALESCE(mc.HighestGiftAmount, 0)  THEN 'Same'
		END AS MaxUpgradeType,
		CASE 
			WHEN COALESCE(mn.LowestGiftAmount, 0) > COALESCE(mc.LowestGiftAmount, 0) THEN 'Upgrade'
			WHEN COALESCE(mn.LowestGiftAmount, 0) < COALESCE(mc.LowestGiftAmount, 0)  THEN 'Downgrade'
			WHEN COALESCE(mn.LowestGiftAmount, 0) = COALESCE(mc.LowestGiftAmount, 0)  THEN 'Same'
		END AS MinUpgradeType,
		CASE 
			WHEN COALESCE(mn.AvgGiftAmount, 0) > COALESCE(mc.AvgGiftAmount, 0) THEN 'Upgrade'
			WHEN COALESCE(mn.AvgGiftAmount, 0) < COALESCE(mc.AvgGiftAmount, 0)  THEN 'Downgrade'
			WHEN COALESCE(mn.AvgGiftAmount, 0) = COALESCE(mc.AvgGiftAmount, 0)  THEN 'Same'
		END AS AvgUpgradeType,
		CASE 
			WHEN COALESCE(mn.TotalNumberGifts, 0) > COALESCE(mc.TotalNumberGifts, 0) THEN 'Upgrade'
			WHEN COALESCE(mn.TotalNumberGifts, 0) < COALESCE(mc.TotalNumberGifts, 0)  THEN 'Downgrade'
			WHEN COALESCE(mn.TotalNumberGifts, 0) = COALESCE(mc.TotalNumberGifts, 0)  THEN 'Same'
		END AS FrequencyUpgradeType
		FROM
		FACT_ConstituentAnnualGiving mc
		LEFT JOIN FACT_ConstituentAnnualGiving mn ON 
		mc.ConstituentDimID = mn.ConstituentDimID AND 
		COALESCE(mc.FiscalYearNext, 0) = COALESCE(mn.FiscalYear, 0)
WHERE mc.SummaryDimID = @SummaryDimID AND mn.SummaryDimID = @SummaryDimID

END


GO


/****** Object:  StoredProcedure [dbo].[BBBI_RaiseError]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  				 
CREATE PROCEDURE [dbo].[BBBI_RaiseError]
as
  --Call this from a catch block to rethrow an error.
  set nocount on;

  declare 
    @ERRORMESSAGE nvarchar(1000),
    @ERRORNUMBER int,
    @ERRORSEVERITY int,
    @ERRORSTATE int,
    @ERRORLINE int,
    @ERRORPROCEDURE nvarchar(400);

  -- Assign variables to error-handling functions to 
  -- capture information for raiserror.
  select 
    @ERRORNUMBER = ERROR_NUMBER(),
    @ERRORSEVERITY = ERROR_SEVERITY(),
    @ERRORSTATE = ERROR_STATE(),
    @ERRORLINE = ERROR_LINE();

  if @ERRORSTATE = 0 
    set @ERRORSTATE = 13;
  
  if @ERRORSEVERITY = 0 
    set @ERRORSEVERITY = 13;


  if(ERROR_PROCEDURE() is null)
    select @ERRORPROCEDURE = '';
  else
    select @ERRORPROCEDURE = RTRIM(ERROR_PROCEDURE()) ;

  select @ERRORMESSAGE = ERROR_MESSAGE();


  /* Raise non-throwing errors to provide info to the client about
     the original error message
     Use the System.Data.SQLClient.SQLException.Errors collection to get to these
     These errors will not cause a jump to a catch block, but just fill the
     return buffer with this information.	
  */

  if @ERRORNUMBER < 50000 --This is a built-in SQL error (FK violation, etc.)
  begin
    declare @INFOMSG nvarchar(100)
    --Raise additional errors to provide original error codes back to client.
    set @INFOMSG='BBERR_ORIGINAL_ERROR:' + cast(@ERRORNUMBER as nvarchar(10));
    raiserror (@INFOMSG,1,11);
    end

    if @ERRORPROCEDURE <> 'USP_RAISE_ERROR' --This was raised explicitly by a raiserror statement
    begin
    set @INFOMSG='BBERR_ORIGINAL_PROCEDURE:' + @ERRORPROCEDURE;
    raiserror (@INFOMSG,2,22);

    set @INFOMSG='BBERR_ORIGINAL_LINE_NUMBER:' + cast(@ERRORLINE as nvarchar(10));
    raiserror (@INFOMSG,3,33);
  end

  /*
   Now raise real error.  This will be caught by any catch blocks or the client.
  */
  raiserror 
  (
    @ERRORMESSAGE, 
    @ERRORSEVERITY, 
    @ERRORSTATE 
  );

  return 0;
      

GO

/****** Object:  StoredProcedure [dbo].[BBBI_Create_DWMergeCommand]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_Create_DWMergeCommand] @TABLENAME as VARCHAR(100)
/*
Create Date: 19 MAR 2010
Author: Alan Eager
Purpose: Create syntax for SSIS T-SQL Merge Command and gives parameter numbers to columns
Execution Sample: exec BBDW.USP_CREATE_DWMERGECOMMAND 'FACT_REVENUE'
*/
as
  begin
      -- DECLARE VARIABLES
      declare @col      VARCHAR(100),
              @pkcol    VARCHAR(100),
              @tab      VARCHAR(100),
              @TabNoExt      VARCHAR(100),
              @isPK     TINYINT,
              @isIdent  TINYINT,
              @view     VARCHAR(100),
              @syscol   VARCHAR(100),
              @schema   VARCHAR(100),
              @trgStmt  VARCHAR(1000),
              @setLine  VARCHAR(1000),
              @content  VARCHAR(6000) = '',
              @content2 VARCHAR(6000) = '',
              @content3 VARCHAR(6000) = '',
              @frmLine  VARCHAR(1000),
              @stmt     VARCHAR(7000),
              @i        INT
      -- GET ALL TABLES IN THE DATABASE
      declare GET_TAB_DATA cursor for
        select
          TABLE_NAME,
          SCHEMA_NM
        from
          vw_DWSCHEMATABLES
        where
          TABLE_NAME = @TABLENAME

      -- FOR EACH TABLE...
      open GET_TAB_DATA

      fetch NEXT from GET_TAB_DATA into @tab, @schema

      while @@fetch_status = 0
        begin
			set @TabNoExt = REPLACE(@tab, '_EXT', '')
            -- SET THE VIEW NAME
            set @syscol = substring(@TabNoExt, charindex('_', @TabNoExt, 1) + 1, len(@TabNoExt)) + 'SystemID'
            -- SET THE BEGINNING OF THE MERGE STATEMENT
            set @trgStmt = 'declare @COUNTS table([ACTION] varchar(28), [INSERTED] int, [UPDATED] int);' + char(10) + 'merge ' + @schema + '.[' + @tab + '] as t' + char(10) + 'using ' + @schema + '.[' + @tab + '_Stage] as s' + char(10) + 'on (t.[' + @syscol + '] = s.[' + @syscol + '])' + char(10) + 'when not matched by target' + char(10) + '  then insert('
            -- SET THE UPDATE LINE WITH THE CORRECT TABLE NAME
            set @setLine = ''

            -- GET ALL COLUMNS IN THE TABLE THAT ARE NOT COMPUTED OR THE PRIMARY KEY
            declare GET_COL_DATA cursor for
              select
                COLUMN_NAME,
                IS_PK,
                IS_IDENT
              from
                vw_DWSCHEMACOLUMNS
              where
                TABLE_NAME = @tab
                and SCHEMA_NM = @schema

            set @i = 1

            -- FOR EACH COLUMN...
            open GET_COL_DATA

            fetch NEXT from GET_COL_DATA into @col, @isPK, @isIdent

            while @@fetch_status = 0
              begin
                  -- IF THIS COLUMN IS NOT A PRIMARY KEY AND IS NOT AN IDENTITY FIELD
                  if @isPK = 0
                    begin
                        set @content = @content + '   [' + @col + '],' + char(10)
                        set @content2 = @content2 + '    s.[' + @col + '],' + char(10)
                        set @content3 = @content3 + '    t.[' + @col + '] = s.[' + @col + '],' + char(10)
                        set @i = @i + 1
                    end

                  fetch NEXT from GET_COL_DATA into @col, @isPK, @isIdent
              end

            close GET_COL_DATA

            deallocate GET_COL_DATA

            -- BUILD THE STATEMENT
            set @content = left(@content, len(@content) - 2)
            set @content2 = left(@content2, len(@content2) - 2)
            set @content3 = left(@content3, len(@content3) - 2)
            set @stmt = @trgStmt + char(10) + @content + char(10) + '    )' + char(10) + '  	values(' + char(10) + @content2 + char(10) + '    )' + char(10) + 'when matched' + char(10) + '   then update' + char(10) + '    set ' + char(10) + @content3 + 'output ' + char(10) + '   $action, ' + char(10) + '  case when deleted.[ETLCONTROLID] is null then 1 else 0 end,' + char(10) + '  case when deleted.[ETLCONTROLID] is not null then 1 else 0 end into @COUNTS;' + char(10) + 'select  count(*) as [TOTAL], isnull(sum([INSERTED]),0) as [INSERTED], isnull(sum([UPDATED]),0) as [UPDATED]' + char(10) + 'from @COUNTS'

            print @stmt

            begin TRY
                if @content <> '	SET '
                   and @frmLine <> ''
                  begin
                      -- CREATE THE TRIGGER
                      print char(10)
                  --EXECUTE(@stmt)
                  end
            end TRY

            begin CATCH
                print 'ERROR: ' + error_message() + char(10) + 'STATEMENT: ' + @stmt
            end CATCH

            -- RESET T-SQL VARIABLES
            set @view = ''
            set @trgStmt = ''
            set @setLine = ''
            set @content = '	'
            set @frmLine = ''

            fetch NEXT from GET_TAB_DATA into @tab, @schema
        end

      close GET_TAB_DATA

      deallocate GET_TAB_DATA
  end 

GO

/****** Object:  StoredProcedure [dbo].[BBBI_Create_DWOLEDBCommand]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_Create_DWOLEDBCommand] @TABLENAME as VARCHAR(100)
/*
Create Date: 22 FEB 2010
Author: Alan Eager
Purpose: Create syntax for SSIS OLE DB Update Command and gives parameter numbers to columns
Execution Sample: exec BBDW.USP_CREATE_DWOLEDBCOMMAND 'DIM_APPEAL'
*/
as
  begin
      -- DECLARE VARIABLES
      declare @col      VARCHAR(100),
              @pkcol    VARCHAR(100),
              @tab      VARCHAR(100),
              @isPK     TINYINT,
              @isIdent  TINYINT,
              @view     VARCHAR(100),
              @schema   VARCHAR(100),
              @trgStmt  VARCHAR(1000),
              @setLine  VARCHAR(1000),
              @content  VARCHAR(6000) = '',
              @content2 VARCHAR(6000) = '',
              @frmLine  VARCHAR(1000),
              @stmt     VARCHAR(7000),
              @i        INT
      -- GET ALL TABLES IN THE DATABASE
      declare GET_TAB_DATA cursor for
        select
          TABLE_NAME,
          SCHEMA_NM
        from
          vw_DWSCHEMATABLES
        where
          TABLE_NAME = @TABLENAME

      -- FOR EACH TABLE...
      open GET_TAB_DATA

      fetch NEXT from GET_TAB_DATA into @tab, @schema

      while @@fetch_status = 0
        begin
            -- SET THE VIEW NAME
            set @view = '[' + @schema + '].[' + @tab + ']'
            -- SET THE BEGINNING OF THE CREATE TRIGGER STATEMENT
            set @trgStmt = 'update ' + @view + char(10) + 'set'
            -- SET THE UPDATE LINE WITH THE CORRECT TABLE NAME
            set @setLine = ''

            -- GET ALL COLUMNS IN THE TABLE THAT ARE NOT COMPUTED OR THE PRIMARY KEY
            declare GET_COL_DATA cursor for
              select
                COLUMN_NAME,
                IS_PK,
                IS_IDENT
              from
                vw_DWSCHEMACOLUMNS
              where
                TABLE_NAME = @tab
                and SCHEMA_NM = @schema

            set @i = 1

            -- FOR EACH COLUMN...
            open GET_COL_DATA

            fetch NEXT from GET_COL_DATA into @col, @isPK, @isIdent
			------------------------
			--print @col
			------------------------

            while @@fetch_status = 0
              begin
                  -- IF THIS COLUMN IS NOT A PRIMARY KEY AND IS NOT AN IDENTITY FIELD
 			------------------------
			--print @col
			------------------------
                 if @isPK = 0
                     and @isIdent = 0
                    begin
                        if @i = 1
                          begin
                              set @content = @content + '    [' + @col + '] =?' + char(10)
                          end
                        else
                          begin
                              set @content = @content + '   ,[' + @col + '] =?' + char(10)
                          end

                        --SELECT RIGHT(SPACE(30) + RTRIM(APPEALDIMID), 5) FROM BBDW.DIM_APPEAL
                        set @content2 = @content2 + char(10) + @col + space(50 - len(@col)) + ' Param_' + convert(VARCHAR(3), @i - 1)
                        set @i = @i + 1
                    end

                  -- IF THIS COLUMN IS THE PRIMARY KEY
                  if @isPK = 1
                    begin
                        set @frmLine = '	where [' + @col + '] = ?'
                        set @pkcol = @col
                    end

                  fetch NEXT from GET_COL_DATA into @col, @isPK, @isIdent
              end

            close GET_COL_DATA

            deallocate GET_COL_DATA

            -- BUILD THE CREATE TRIGGER STATEMENT
            set @stmt = @trgStmt + char(10) + @content + @frmLine + char(10)

            print @stmt

            print char(10)

            print '--------------------------------------------------------------------------------------------------------'

            print @content2 + char(10) + @pkcol + space(44 - len(@col)) + ' Param_' + convert(VARCHAR(3), @i - 1)

            begin TRY
                if @content <> '	SET '
                   and @frmLine <> ''
                  begin
                      -- CREATE THE TRIGGER
                      print char(10)
                  --EXECUTE(@stmt)
                  end
            end TRY

            begin CATCH
                print 'ERROR: ' + error_message() + char(10) + 'STATEMENT: ' + @stmt
            end CATCH

            -- RESET T-SQL VARIABLES
            set @view = ''
            set @trgStmt = ''
            set @setLine = ''
            set @content = '	'
            set @frmLine = ''

            fetch NEXT from GET_TAB_DATA into @tab, @schema
        end

      close GET_TAB_DATA

      deallocate GET_TAB_DATA
  end 

GO

/****** Object:  StoredProcedure [dbo].[BBBI_CreateDWIndexes]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_CreateDWIndexes]
-- Creates Non-clustered indexes for all foreign key fields of DimID and FactID
AS
BEGIN

DECLARE @SQL NVarchar(2000)
DECLARE @TableName Varchar(100)
DECLARE @OutputField Varchar(200)
DECLARE @ColumnName Varchar(100)
DECLARE @i Int
DECLARE @iPos Int
DECLARE @Command Varchar(100)

DECLARE ColumnCursor CURSOR FOR 
--------------------------------------------------------------------------------
SELECT
DISTINCT
c.TABLE_NAME, c.COLUMN_NAME
FROM
( 
SELECT
Tab.TABLE_NAME, Col.COLUMN_NAME, sc.IsComputed
FROM         
INFORMATION_SCHEMA.TABLES AS Tab 
INNER JOIN INFORMATION_SCHEMA.COLUMNS AS Col ON Tab.TABLE_NAME = Col.TABLE_NAME
INNER JOIN 
(
SELECT stc.name AS COLUMN_NAME, so.name AS TABLE_NAME, stc.iscomputed AS IsComputed
FROM
syscolumns stc 
INNER JOIN sysobjects so ON so.id = stc.id 
) sc 
ON sc.COLUMN_NAME = Col.COLUMN_NAME AND
sc.TABLE_NAME = Tab.TABLE_NAME
) c
LEFT JOIN 
(
SELECT  cu.TABLE_NAME, cu.COLUMN_NAME
FROM 
INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE cu
INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc ON cu.TABLE_NAME = tc.TABLE_NAME
WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
) cc ON cc.TABLE_NAME = c.TABLE_NAME AND cc.COLUMN_NAME = c.COLUMN_NAME
WHERE 
(c.TABLE_NAME LIKE 'DIM_%' OR c.TABLE_NAME LIKE 'FACT_%') AND 
(c.COLUMN_NAME LIKE '%DimID' OR c.COLUMN_NAME LIKE '%FactID' OR c.COLUMN_NAME LIKE '%GiftSystemID%' OR 
c.COLUMN_NAME LIKE '%AttributeCategory%' OR c.COLUMN_NAME LIKE '%AttributeDescription%' OR c.COLUMN_NAME LIKE '%Sequence%' OR 
c.COLUMN_NAME LIKE '%Rank%' OR c.COLUMN_NAME LIKE '%Year' OR c.COLUMN_NAME LIKE '%Identifier') AND
cc.COLUMN_NAME IS NULL AND
c.IsComputed = 0
ORDER BY c.TABLE_NAME
-------------------------------------------------------------------------------------------------------

OPEN ColumnCursor
FETCH ColumnCursor INTO @TableName, @ColumnName

WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = 'EXEC dbo.BBBI_IndexCreate ''' + @TableName + ''',''' + @ColumnName + ''''
		EXEC sp_executesql @SQL
		PRINT @SQL
		FETCH ColumnCursor INTO @TableName, @ColumnName
	END

CLOSE ColumnCursor
DEALLOCATE ColumnCursor

END 


GO

/****** Object:  StoredProcedure [dbo].[BBBI_CreateVCODetail]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_CreateVCODetail]

AS
BEGIN

	DECLARE @List Varchar(1000)
	DECLARE @ListItemString Varchar(10)
	DECLARE @ListItem Int
	--DECLARE @ItemTable TABLE(FilterSystemID Int, TemplateDetailSystemID Int, TemplateSystemID Int, TemplateID Varchar(60), TemplateName Varchar(100), ItemValue Varchar(10))
	DECLARE @TemplateSystemID Int
	DECLARE @TemplateDetailSystemID Int
	DECLARE @FilterSystemID Int
	DECLARE @TemplateID Varchar(60)
	DECLARE @TemplateName Varchar(100)
	DECLARE @Category Varchar(60)
	DECLARE @ParentKey Int
	DECLARE @AccountNumberKeyValue Int
	DECLARE @AccountNumberFrom Varchar(50)
	DECLARE @AccountNumberTo Varchar(50)
	DECLARE @AccountKeyValue Int
	DECLARE @AccountCodeFrom Varchar(50)
	DECLARE @AccountCodeTo Varchar(50)
	DECLARE @SegmentKeyValue Int
	DECLARE @FundKeyValue Int
	DECLARE @ProjectKeyValue Int
	DECLARE @i Int
	DECLARE @SQL NVarchar(2000)
	DECLARE @Where NVarchar(1000)
	DECLARE @InsertSQL NVarchar(500)
	
	SET @InsertSQL = 'INSERT INTO TEMP_VCO 
			(ChartTemplateSystemID, TemplateDetailSystemID, 
			ParentKey, AccountCategory, AccountDimID, AccountSystemID, AccountCodeSystemID, AccountCode, AccountNumber, Caption,
			Sequence) '

	DECLARE IndexCursor CURSOR FOR 
		SELECT   DISTINCT TemplateDetailSystemID, Category, ParentKey, 
						AccountNumberKeyValue, AccountNumberFrom, 	AccountNumberTo, 
						AccountKeyValue, AccountCodeFrom, AccountCodeTo, 
						SegmentKeyValue, FundKeyValue, ProjectKeyValue
		FROM		FACT_VCOFilter

		OPEN IndexCursor
		FETCH IndexCursor INTO 
						@TemplateDetailSystemID, @Category, @ParentKey, 
						@AccountNumberKeyValue, @AccountNumberFrom, @AccountNumberTo, 
						@AccountKeyValue, @AccountCodeFrom, @AccountCodeTo, 
						@SegmentKeyValue, @FundKeyValue, @ProjectKeyValue

	PRINT 'ID, AccNumKey, AccNumFrom, AccNumTo, AccKey, AccFrom, AccTo, Seg, Fund, Proj'
	SET @i = 1
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = 'SELECT v.TemplateSystemID, v.TemplateDetailSystemID, v.ParentKey, a.AccountCategoryDescription, a.AccountDimID, a.AccountSystemID, a.AccountCodeSystemID, a.AccountCode, a.AccountNumber, a.AccountNumber + '' '' + a.AccountDescription AS Caption,' + Convert(Varchar(3), @i) +  'AS Sequence
								FROM FACT_VCOFilter v INNER JOIN DIM_Account a ON '

		-- Initial INNER JOIN Clause
		IF @AccountCodeFrom IS NOT NULL
				SET @Where = 'a.AccountCode BETWEEN v.AccountCodeFrom  AND v.AccountCodeTo'
		ELSE IF @AccountNumberFrom IS NOT NULL
				SET @Where = 'a.AccountNumber BETWEEN v.AccountNumberFrom  AND v.AccountNumberTo'
		ELSE IF @AccountKeyValue IS NOT NULL
				SET @Where = 'a.AccountCodeSystemID = v.AccountKeyValue'
		ELSE IF @AccountNumberKeyValue IS NOT NULL
				SET @Where = 'a.AccountSystemID = v.AccountNumberKeyValue'
		ELSE IF @SegmentKeyValue IS NOT NULL
				SET @Where = 'a.Segment1SystemID = v.SegmentKeyValue OR a.Segment2SystemID = v.SegmentKeyValue'
		ELSE IF @FundKeyValue IS NOT NULL
				SET @Where = 'a.FundSystemID = v.FundKeyValue'
	
		-- Now Add the ANDs
		IF @SegmentKeyValue IS NOT NULL
				SET @Where = @Where + ' AND (a.Segment1SystemID = v.SegmentKeyValue OR a.Segment2SystemID = v.SegmentKeyValue)'
		IF @FundKeyValue IS NOT NULL
				SET @Where = @Where + ' AND a.FundSystemID = v.FundKeyValue'

		SET @SQL = @InsertSQL + @SQL +  @Where
		PRINT 		Convert(Varchar(50), @TemplateDetailSystemID) + ', '  + 
						Convert(Varchar(50), COALESCE(@AccountNumberKeyValue, '')) + ', ' + 
						COALESCE(@AccountNumberFrom, '') + ', ' + COALESCE(@AccountNumberTo, '') + ', ' + 
						Convert(Varchar(50), COALESCE(@AccountKeyValue, '')) + ', ' + 
						COALESCE(@AccountCodeFrom, '') + ', ' + COALESCE(@AccountCodeTo, '') + ', ' +
						Convert(Varchar(50), COALESCE(@SegmentKeyValue, '')) + ', ' + 
						Convert(Varchar(50), COALESCE(@FundKeyValue, '')) + ', ' + 
						Convert(Varchar(50), COALESCE(@ProjectKeyValue, ''))

		PRINT @SQL
		EXEC sp_executesql @SQL

		FETCH IndexCursor INTO 
						@TemplateDetailSystemID, @Category, @ParentKey, 
						@AccountNumberKeyValue, @AccountNumberFrom, 
						@AccountNumberTo, @AccountKeyValue, @AccountCodeFrom, @AccountCodeTo, 
						@SegmentKeyValue, @FundKeyValue, @ProjectKeyValue
		SET @i = @i + 1
	END

CLOSE IndexCursor
DEALLOCATE IndexCursor

END






GO

/****** Object:  StoredProcedure [dbo].[BBBI_DenormalizeAttribute]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[BBBI_DenormalizeAttribute]    Script Date: 06/02/2011 16:58:09 ******/

CREATE PROCEDURE [dbo].[BBBI_DenormalizeAttribute]
    	@ParentTableName Varchar(200) ,
		@ParentColumnName Varchar(200),
		@AttributeTableName Varchar(200),
		@AttributeValue Varchar(200),
		@ParentKeyColumnName Varchar(200)
		--  e.g. De-normalizing a Fund Attribute called Fund Group onto the DIM_Fund table with column called FundGroup
		--  EXEC dbo.BBBI_DenormalizeAttribute 'DIM_Fund', 'FundGroup', 'DIM_FundAttribute', 'Fund Group', 'FundDimID'
AS
BEGIN
DECLARE @SQL NVarchar(2000)
DECLARE @ErrorNumber AS Varchar(10)
DECLARE @ErrorMessage AS Varchar(500)

SET @SQL = 'UPDATE ' + @ParentTableName + ' SET ' + @ParentColumnName + ' = a.AttributeDescription FROM ' + @AttributeTableName + ' a ' +
							'WHERE a.AttributeCategory = ''' + @AttributeValue + ''' AND a.' + @ParentKeyColumnName + ' = '  + @ParentTableName + '.' + @ParentKeyColumnName

BEGIN TRY 
	EXEC sp_executesql @SQL
	--PRINT @SQL
END TRY
BEGIN CATCH
	--SELECT ERROR_NUMBER() as ErrorNumber, ERROR_MESSAGE() as ErrorMessage
			SET @ErrorNumber = Convert(Varchar(10), ERROR_NUMBER())
			SET @ErrorMessage = Convert(Varchar(500), ERROR_MESSAGE())
			PRINT @ErrorNumber + ' : ' + @ErrorMessage
END CATCH

--PRINT @SQL
END


GO

/****** Object:  StoredProcedure [dbo].[BBBI_DonorClassification]    Script Date: 12/09/2011 12:13:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[BBBI_DonorClassification]    Script Date: 03/27/2008 17:59:09 ******/
CREATE PROCEDURE [dbo].[BBBI_DonorClassification] 
	@StartYear SmallInt,
	@NoYears SmallInt,
    @FilterNumber SmallInt = 0,
    @InsertTableName Varchar(100) = 'FACT_ConstituentAnnualClassification'
AS
--------------------------------------------------------------------------------------------------------------------------
-- Code Generator for Donor Classification
-- Consecutive, C2..C10, C2 is 2 Year Consecutive, C3 is 3 Year Consecutive, etc
-- Reactivation
-- Lapsed
-- Non Contributor
-- Not Exists (In Database)
-----------------------------------------------------------------------------------------------------------------------------
BEGIN

DECLARE @h INT
DECLARE @i INT
DECLARE @j INT
DECLARE @SQLMain NVarchar(4000)
DECLARE @SQL NVarchar(4000)
DECLARE @SQL1 NVarchar(1000)
DECLARE @SQL2 NVarchar(4000)
DECLARE @SQLStart NVarchar(1000)
DECLARE @SQLMiddle NVarchar(4000)
DECLARE @SQLEnd NVarchar(1000)
DECLARE @NewSQL NVarchar(1000)
DECLARE @Operator NVarchar(1)
DECLARE @CurrentFiscalYear int
DECLARE @LoopEnd Int
DECLARE @OuterLoopEnd Int
DECLARE @is NVarchar(3)
DECLARE @js NVarchar(3)
DECLARE @FilterNumberStr Varchar(2)
DECLARE @SummaryDimIDStr Varchar(2)

IF @FilterNumber = 0
BEGIN
	SET @FilterNumberStr = ''
	SET @SummaryDimIDStr = '1'
END
ELSE
BEGIN
	SET @FilterNumberStr = CONVERT(Varchar(2), @FilterNumber)
	SET @SummaryDimIDStr = CONVERT(Varchar(2), @FilterNumber)
END

SET @i = @StartYear
SET @is = Convert(Varchar(3), @i)
SET @j = @i + 1
SET @js = Convert(Varchar(3), @j)

SET @SQLStart = 'SELECT ConstituentDimID, @YA AS YearsAgo, @CFY AS FiscalYear, CASE WHEN AcquisitionAgo IS NOT NULL AND AcquisitionAgo = @YA THEN ''F'''   --d.FiscalYear FiscalYearCreated, ' + --@CurrentFiscalYear - d.FiscalYear AS CreatedYearsAgo, ' +
				
--IF @StartYear = 0
	SET @SQLStart = @SQLStart + ' WHEN Amt0 = 0 AND ' + ' Amt' + @js + 'Plus = 0 THEN ''N''' + '  '
--ELSE
	--SET @SQLStart = @SQLStart + ' WHEN Amt' + @is + 'Plus = 0 THEN ''N''' + ' WHEN Amt' + @is + ' > 0 AND Amt' + @js + 'Plus = 0 THEN ''F''' 

SET @SQLStart = @SQLStart + ' WHEN (@CFY - d.FiscalYear) < 0 AND Amt' + @js + 'Plus = 0 THEN ''X'''

-- Loop for Reactivations
SET @SQL = ' '
SET @i = @StartYear
SET @h = @StartYear
SET @LoopEnd = @NoYears 
SET @OuterLoopEnd = @NoYears 
WHILE (@h < @OuterLoopEnd)
BEGIN
	SET @i = @StartYear
	SET @SQL2 = ' WHEN '
	WHILE (@i < @LoopEnd)
	BEGIN
		SET @h = @StartYear + 1
		SET @is = Convert(Varchar(3), @i)
		IF @i = @StartYear
			SET @Operator = '>'
		ELSE
			SET @Operator = '='
		IF (@LoopEnd - 1 - @StartYear) > 0
			SET @NewSQL = 'Amt'  + @is + '' + @Operator + '0 AND '
		SET @SQL2 = @SQL2 + @NewSQL
		SET @i = @i + 1
	END -- @i < @LoopEnd
	IF (@LoopEnd - 1 - @StartYear) > 0
		SET @SQL2 = @SQL2 + 'Amt' + Convert(Varchar(3), @LoopEnd) + '>0 THEN ''R' + Convert(Varchar(3), @LoopEnd - 1 - @StartYear) + ''''
	SET @LoopEnd = @LoopEnd - 1
	SET @OuterLoopEnd = @LoopEnd + 1
	SET @h = @h + 1
	SET @SQL = @SQL + @SQL2
END -- @h < @LoopEnd
SET @SQL = @SQL + ' WHEN Amt' + Convert(Varchar(3), @StartYear) + '>0 AND Amt' + Convert(Varchar(3), @StartYear + 1) + 'Plus=' +
'Amt' + Convert(Varchar(3), @NoYears + 1) + 'Plus THEN ''R' + Convert(Varchar(3), @NoYears - @StartYear) + ''''
--WHEN Amt9>0 AND Amt10Plus = Amt20Plus THEN 'R10'
--SET @SQL = @SQL + @SQL2

--PRINT @SQL

SET @SQLMiddle = @SQL

-- Loop for Continuous
SET @SQL = ' '
SET @i = @StartYear
SET @h = @StartYear
SET @LoopEnd = @NoYears 
SET @OuterLoopEnd = @NoYears
WHILE (@h < @OuterLoopEnd)
BEGIN
	SET @i = @StartYear
	SET @SQL2 = ' WHEN '
	WHILE (@i < @LoopEnd)
	BEGIN
		SET @h = @StartYear
		SET @is = Convert(Varchar(3), @i)
		SET @NewSQL = 'Amt'  + @is + '>0 AND '
		SET @SQL2 = @SQL2 + @NewSQL
		SET @i = @i + 1
	END -- @i < @LoopEnd
	SET @SQL2 = @SQL2 + 'Amt' + Convert(Varchar(3), @LoopEnd) + '>0 THEN ''C' + Convert(Varchar(3), @LoopEnd + 1 - @StartYear) + ''''
	SET @LoopEnd = @LoopEnd - 1
	SET @OuterLoopEnd = @LoopEnd + 1
	SET @h = @h + 1
	SET @SQL = @SQL + @SQL2
END -- @h < @LoopEnd
--PRINT @SQL
SET @SQLMiddle = @SQLMiddle + @SQL

-- Lapsed Donors
SET @SQL = ' '
SET @i = @StartYear
SET @h = @StartYear
SET @LoopEnd = @NoYears + 1
SET @OuterLoopEnd = @NoYears
WHILE (@h < @OuterLoopEnd)
BEGIN
	SET @i = @StartYear
	SET @SQL2 = ' WHEN '
	WHILE (@i < @LoopEnd)
	BEGIN
		SET @h =  @StartYear - 1
		SET @is = Convert(Varchar(3), @i)
		SET @NewSQL = 'Amt'  + @is + '=0 AND '
		SET @SQL2 = @SQL2 + @NewSQL
		SET @i = @i + 1
	END -- @i < @LoopEnd
	IF @LoopEnd > 20
		SET @SQL2 = @SQL2 + 'Amt' + Convert(Varchar(3), @LoopEnd - 1) + 'Plus>0 THEN ''L' + Convert(Varchar(3), @LoopEnd - @StartYear) + ''''
	ELSE
		IF (@LoopEnd - @StartYear) > 0
			SET @SQL2 = @SQL2 + 'Amt' + Convert(Varchar(3), @LoopEnd) + '>0 THEN ''L' + Convert(Varchar(3), @LoopEnd - @StartYear) + ''''
		ELSE
			SET @SQL2 = ''
	SET @LoopEnd = @LoopEnd - 1
	SET @OuterLoopEnd = @LoopEnd + 1
	SET @h = @h + 1
	SET @SQL = @SQL + @SQL2
END -- @h < @LoopEnd
-- Output
--PRINT @SQL
SET @SQLMiddle = @SQLMiddle + @SQL

SET @SQLEnd = ' ELSE ''L' + Convert(Varchar(2), @NoYears - @StartYear + 1) + ''' END AS Status, ' + @SummaryDimIDStr + 'AS SummaryDimID FROM FACT_ConstituentGivingSummary' + @FilterNumberStr + ' gs LEFT JOIN DIM_Date d ON gs.DateAddedDimID = d.DateDimID'

SET @SQLMain = 'DECLARE @YA Smallint DECLARE @StartYear Smallint DECLARE @CFY Smallint SET @YA =' + Convert(Varchar(3), @StartYear) + 
' SET @CFY = (SELECT FiscalYear FROM DIM_Date WHERE CONVERT(VARCHAR, GETDATE(), 101) = CONVERT(VARCHAR, ActualDate, 101)) SET @CFY = @CFY - ' + Convert(Varchar(3), @StartYear) + ' ' + 
' INSERT INTO ' + @InsertTableName + '(ConstituentDimID,Sequence,FiscalYear,Classification, SummaryDimID) ' +
@SQLStart + @SQLMiddle + @SQLEnd

--SET @CurrentFiscalYear = (SELECT FiscalYear FROM DIM_Date WHERE CONVERT(VARCHAR, GETDATE(), 101) = CONVERT(VARCHAR, ActualDate, 101))
--SET @CurrentFiscalYear = @CurrentFiscalYear - @StartYear
--SET @YearsAgo = @StartYear

--PRINT @SQLMain
EXEC sp_executesql @SQLMain

END

GO

/****** Object:  StoredProcedure [dbo].[BBBI_ETLBeginLoad]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[BBBI_ETLBeginLoad]
@CURRENTDATAWINDOWCLOSE datetimeoffset=NULL, --should be relative to the source system, use SELECT SYSDATETIMEOFFSET() from the source system.
                                             --for child packages will use the DATAWINDOWCLOSE of the parent.
@ISPARENT bit,
@SSISPACKAGENAME nvarchar(255),
@SSISPACKAGEPATH nvarchar(255) ='',
@SSISSTARTTIME datetimeoffset =null,
@SSISUSERNAME nvarchar(255) ='',
@SSISMACHINENAME nvarchar(255) ='',
@SSISEXECUTIONINSTANCEGUID uniqueidentifier =null,
@CLEAR_INCOMPLETE_LOADS bit=1, --true to delete where LOADISCOMPLETE=0;
@INSERTINTOLOG bit=1,
@PACKAGEFILENAMEEXT nvarchar(10) = '.dtsx',
@USELOGICALPACKAGENAME bit = 1
as
  set nocount on;

declare @SSISPACKAGEFULLNAME nvarchar(100)
declare @SSISPACKAGELOGICALNAME nvarchar(100)
set @SSISPACKAGELOGICALNAME = dbo.BBBI_ParseFileName(@SSISPACKAGENAME, @PACKAGEFILENAMEEXT, 1)
set @SSISPACKAGEFULLNAME = dbo.BBBI_ParseFileName(@SSISPACKAGENAME, @PACKAGEFILENAMEEXT, 0)
set @SSISPACKAGENAME = @SSISPACKAGELOGICALNAME

  declare @CURRENTDATAWINDOWOPEN datetimeoffset;
  declare @ETLCONTROLID int;

  begin try

    declare @PARENTETLCONTROLID int;

    if @ISPARENT =0
    begin
      select top 1 @PARENTETLCONTROLID = ETLControlID
      from CTL_ETLHistory where LoadIsComplete = 0 and IsParent=1 order by DataWindowClose desc, ETLControlID desc;

      if @PARENTETLCONTROLID is null
      begin
        raiserror('Unable to find an executing parent ETLCONTROLID', 16, 1);
        return 68;
      end
    end


    if (@CURRENTDATAWINDOWCLOSE IS NULL) AND (@ISPARENT=1)
      begin
        raiserror ('@CURRENTDATAWINDOWCLOSE cannot be NULL.  Select GETDATE() from the source system and use that value for @CURRENTDATAWINDOWCLOSE', 16, 1);
        return 70;
      end

    if (@CURRENTDATAWINDOWCLOSE IS NULL) AND (@ISPARENT = 0)
      begin
        select @CURRENTDATAWINDOWCLOSE = max(DataWindowClose) 
        from CTL_ETLHistory 
        where ETLControlID = @PARENTETLCONTROLID
      end

    begin transaction;

    if @CLEAR_INCOMPLETE_LOADS = 1 and (@ISPARENT=1)
      delete from CTL_ETLHistory
        where LoadIsComplete = 0;
    
    if @USELOGICALPACKAGENAME = 1
	begin  
		select @CURRENTDATAWINDOWOPEN = max([DATAWINDOWCLOSE])
		from CTL_ETLHistory
		where LoadIsComplete = 1 and SSISPackageName = @SSISPACKAGELOGICALNAME;
	end 
	else
	begin
		select @CURRENTDATAWINDOWOPEN = max([DATAWINDOWCLOSE])
		from CTL_ETLHistory
		where LoadIsComplete = 1 and SSISPackageName = @SSISPACKAGENAME;	
	end

    if @CURRENTDATAWINDOWOPEN is null
      set @CURRENTDATAWINDOWOPEN='1776-07-04 00:00:00.0000000 +00:00'


if @INSERTINTOLOG = 1
    -- Set load parameters - use the last load and the current datetime to determine load window
begin
    insert into CTL_ETLHistory (
      [DATAWINDOWOPEN],
      [DATAWINDOWCLOSE],
      [LOADISCOMPLETE],
      [ETLSTARTTIME],
      [SSISPACKAGENAME],
      [SSISPACKAGEPATH],
      [SSISSTARTTIME],
      [SSISUSERNAME],
      [SSISMACHINENAME],
      [PARENTETLCONTROLID],
      [ISPARENT],
      [SSISEXECUTIONINSTANCEGUID],
      [NUMROWSADDED],
      [NUMROWSUPDATED]
      )
    values(
      @CURRENTDATAWINDOWOPEN,
      @CURRENTDATAWINDOWCLOSE,
      0,
      SYSDATETIMEOFFSET(),
      @SSISPACKAGENAME ,
      @SSISPACKAGEPATH,
      @SSISSTARTTIME ,
      @SSISUSERNAME ,
      @SSISMACHINENAME ,
      @PARENTETLCONTROLID,
      @ISPARENT,
      @SSISEXECUTIONINSTANCEGUID,
      0,
      0
      );

end
    set @ETLCONTROLID = @@IDENTITY
    if @ETLCONTROLID is null
				set @ETLCONTROLID = (select MAX(ETLCONTROLID) from CTL_ETLHistory) + 0  

    commit transaction;

    --RETURN a result set to avoid the SSIS parameter hassle.
    select @ETLCONTROLID as [ETLCONTROLID],
    cast(@CURRENTDATAWINDOWOPEN as datetime) as [DATAWINDOWOPEN],
    cast(@CURRENTDATAWINDOWCLOSE as datetime) as [DATAWINDOWCLOSE],
    @CURRENTDATAWINDOWOPEN as [DATAWINDOWOPEN_DATETIMEOFFSET],
    @CURRENTDATAWINDOWCLOSE as [DATAWINDOWCLOSE_DATETIMEOFFSET];

  end try
  begin catch
    if @@TRANCOUNT > 0 rollback transaction;
    exec dbo.BBBI_RaiseError;
    raiserror('There was an error in procedure BBBI_ETL_BEGINLOAD while setting the load parameters.', 16, 1);
    return 50;
  end catch

      


GO

/****** Object:  StoredProcedure [dbo].[BBBI_ETLEndLoad]    Script Date: 12/05/2011 12:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[BBBI_ETLEndLoad]
@SSISEXECUTIONINSTANCEGUID uniqueidentifier =null,
@SSISPACKAGENAME nvarchar(255),
@PACKAGEFILENAMEEXT nvarchar(10) = '.dtsx'
as
  set nocount on;

declare @SSISPACKAGEFULLNAME nvarchar(100)
declare @SSISPACKAGELOGICALNAME nvarchar(100)
set @SSISPACKAGELOGICALNAME = dbo.BBBI_ParseFileName(@SSISPACKAGENAME, @PACKAGEFILENAMEEXT, 1)
set @SSISPACKAGEFULLNAME = dbo.BBBI_ParseFileName(@SSISPACKAGENAME, @PACKAGEFILENAMEEXT, 0)
set @SSISPACKAGENAME = @SSISPACKAGELOGICALNAME

declare @CURRENTDATAWINDOWOPEN datetimeoffset;
declare @ETLCONTROLID int;

UPDATE 
CTL_ETLHistory
SET 
ETLEndTime =GETDATE(),
LoadIsComplete=1
WHERE 
SSISEXECUTIONINSTANCEGUID = @SSISEXECUTIONINSTANCEGUID
AND
SSISPACKAGENAME=@SSISPACKAGELOGICALNAME
      

GO

/****** Object:  StoredProcedure [dbo].[BBBI_ETLUpdateRowStats]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[BBBI_ETLUpdateRowStats]
    	@NumRowsAdded Int,
		@NumRowsUpdated Int,
		@DataWindowOpen DateTime = NULL,
		@ETLControlID Int=0,
		@UpdateDataWindow bit = 0
AS
BEGIN
	
IF @ETLControlID = 0
	SET @ETLControlID = (select max(ETLCONTROLID) from CTL_ETLHistory)

--IF @DataWindowOpen IS NULL
--	SET @DataWindowOpen = (select top 1 DataWindowClose from CTL_ETLHistory where ETLControlID = @ETLControlID)
	
UPDATE
	CTL_ETLHistory
SET
	NumRowsAdded=@NumRowsAdded,
	NumRowsUpdated=@NumRowsUpdated
WHERE
	ETLControlID = @ETLControlID

IF @UpdateDataWindow > 0
BEGIN
	UPDATE
		CTL_ETLHistory
	SET
		DataWindowOpen=@DataWindowOpen
	WHERE
		ETLControlID = @ETLControlID
END

END


GO

/****** Object:  StoredProcedure [dbo].[BBBI_IndexCreate]    Script Date: 12/05/2011 13:27:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BBBI_IndexCreate]
    	@TableName Varchar(200) ,
		@ColumnName Varchar(200),
		@FileGroup Varchar(25) = 'BBPM_IDXGROUP',
		@IndexPrefix Varchar(25) = 'BBPM_IX_',
		@TableSize Int = 30,
		@FieldSize Int = 30
AS
BEGIN
IF @FileGroup = ''
	SET @FileGroup = 'PRIMARY'
	
DECLARE @IndexName Varchar(500)
DECLARE @SQL NVarchar(2000)
DECLARE @SQL1 NVarchar(1000)
DECLARE @SQL2 NVarchar(1000)
DECLARE @SQL3 NVarchar(1000)
DECLARE @ErrorNumber AS Varchar(10)
DECLARE @ErrorMessage AS Varchar(500)
DECLARE @FileGroupStmt AS Varchar(50) = 'ON [' + @FileGroup + ']'

SET @IndexName = @IndexPrefix + LEFT(@TableName, @TableSize) + '_' + LEFT(@ColumnName, @FieldSize)

SET @SQL1 = 'IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''' + @TableName + ''') AND name = N''' + @IndexName + ''') '
SET @SQL2 = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + '(' + @ColumnName + ') ' + @FileGroupStmt 
SET @SQL = @SQL1 + @SQL2

BEGIN TRY 
	EXEC sp_executesql @SQL
END TRY
BEGIN CATCH
	--SELECT ERROR_NUMBER() as ErrorNumber, ERROR_MESSAGE() as ErrorMessage
			SET @ErrorNumber = Convert(Varchar(10), ERROR_NUMBER())
			SET @ErrorMessage = Convert(Varchar(500), ERROR_MESSAGE())
			PRINT @ErrorNumber + ' : ' + @ErrorMessage
END CATCH

--PRINT @SQL
END

GO

/****** Object:  StoredProcedure [dbo].[BBBI_IndexDrop]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[BBBI_IndexDrop]
    	@TableName Varchar(200) ,
		@ColumnName Varchar(200),
		@IndexPrefix Varchar(25) = 'BBPM_IX_',
		@TableSize Int = 30,
		@FieldSize Int = 30
AS
BEGIN
DECLARE @IndexName Varchar(500)
DECLARE @SQL NVarchar(2000)
DECLARE @SQL1 NVarchar(1000)
DECLARE @SQL2 NVarchar(1000)
DECLARE @SQL3 NVarchar(1000)
DECLARE @ErrorNumber AS Varchar(10)
DECLARE @ErrorMessage AS Varchar(500)

SET @IndexName = @IndexPrefix + LEFT(@TableName, @TableSize) + '_' + LEFT(@ColumnName, @FieldSize)

SET @SQL1 = 'IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''' + @TableName + ''') AND name = N''' + @IndexName + ''') '
SET @SQL2 = 'DROP INDEX ' + @TableName + '.'+ @IndexName 
SET @SQL = @SQL1 + @SQL2

BEGIN TRY 
	EXEC sp_executesql @SQL
END TRY
BEGIN CATCH
	--SELECT ERROR_NUMBER() as ErrorNumber, ERROR_MESSAGE() as ErrorMessage
			SET @ErrorNumber = Convert(Varchar(10), ERROR_NUMBER())
			SET @ErrorMessage = Convert(Varchar(500), ERROR_MESSAGE())
			PRINT @ErrorNumber + ' : ' + @ErrorMessage
END CATCH

--PRINT @SQL
END


GO

/****** Object:  StoredProcedure [dbo].[BBBI_IndexHistory]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_IndexHistory]
    	@TaskType Varchar(15) = 'HISTORY',
		@TableFilter Varchar(50) = ''
AS
BEGIN
-- @TaskType = 'DROP' To Drop all current NONCLUSTERED INDEXES
-- @TaskType = 'ADD' To Recreate all NONCLUSTERED INDEXES from History file
-- @TaskType = 'NOW' to show Current Indexes in existance
-- @TaskType = 'HISTORY' to show indexes stored in History table for re-creation

DECLARE @SQL Varchar(2000)
DECLARE @IndexName Varchar(200)
DECLARE @TableName Varchar(100)
DECLARE @OutputField Varchar(200)
DECLARE @ColumnName Varchar(100)
DECLARE @IncludeNames Varchar(1000)
DECLARE @ObjectName Varchar(1000)
DECLARE @i Int
DECLARE @iPos Int
DECLARE @Command Varchar(100)
DECLARE @IndexInclude SmallInt
DECLARE @NoColumns SmallInt

IF @TaskType <> 'ADD' AND @TaskType <> 'DROP' AND @TaskType <> 'NOW' AND @TaskType <> 'REFRESH'
	SET @TaskType = 'HISTORY'

if NOT exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CTL_IndexHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[CTL_IndexHistory](
	[TableSystemID] int NULL,
	[IndexSystemID] int NULL,
	[ColumnSystemID] int NULL,
	[IndexColumnPosition] smallint NULL,
	[IndexType] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TableName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IndexName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ColumnName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IndexInclude] SmallInt NULL,
	[NoColumns] SmallInt NULL
) ON [PRIMARY]
END

IF @TaskType = 'DROP' OR @TaskType = 'REFRESH'
BEGIN
	TRUNCATE TABLE [CTL_IndexHistory]

INSERT INTO [dbo].[CTL_IndexHistory]
(
			[TableSystemID]
           ,[IndexSystemID]
           ,[ColumnSystemID]
           ,[IndexColumnPosition]
           ,[IndexType]
           ,[TableName]
           ,[IndexName]
           ,[ColumnName]
		   --,[IndexInclude]
)
SELECT     
O.id AS TableSystemID, 
I.indid AS IndexSystemID, 
IK.colid AS ColumnSystemID, 
IK.keyno AS IndexColumnPosition,
CASE I.indid WHEN 0 THEN 'Heap' WHEN 1 THEN 'Clustered' ELSE 'Nonclustered' END AS IndexType,
O.name AS TableName, 
I.name AS IndexName,
TC.name AS ColumnName
FROM         
syscolumns AS TC INNER JOIN
sysobjects AS O INNER JOIN
sysindexes AS I ON O.id = I.id INNER JOIN
sysindexkeys AS IK ON I.indid = IK.indid AND I.id = IK.id ON TC.colid = IK.colid AND TC.id = IK.id
WHERE 
O.xtype = 'U' AND
(O.name LIKE 'DIM_%' OR O.name LIKE 'FACT_%' OR O.name LIKE 'TEMP_%')
ORDER BY O.id ASC, I.indid ASC, IK.keyno ASC

UPDATE [CTL_IndexHistory]
SET IndexInclude =  dbo.BBBI_IndexFindInclude(TableSystemID, IndexSystemID)  

UPDATE [CTL_IndexHistory]
SET Includecolumns = dbo.BBBI_IndexCreateCmd(TableSystemID, IndexSystemID, IndexInclude) 

UPDATE [CTL_IndexHistory]
SET NoColumns = h.NoColumns
FROM 
(
SELECT TableSystemID, IndexSystemID, Count(*) NoColumns FROM CTL_IndexHistory GROUP BY TableSystemID, IndexSystemID
) h
WHERE CTL_IndexHistory.TableSystemID = h.TableSystemID AND CTL_IndexHistory.IndexSystemID = h.IndexSystemID

END -- @TaskType = 'DROP'

------------------------------------------------------------------------------------------------------------------------
IF @TaskType = 'DROP'
BEGIN
	DECLARE IndexCursor CURSOR FOR SELECT DISTINCT TableName, IndexName, IncludeColumns
	FROM CTL_IndexHistory WHERE IndexSystemID > 1 AND IndexColumnPosition = 1
	OPEN IndexCursor
	FETCH IndexCursor INTO @TableName, @IndexName, @IncludeNames

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @ObjectName = @TableName + '.' + @IndexName
		SET @SQL = 'IF Exists (select * from sysindexes where name = ''' + @IndexName + ''')' + ' DROP INDEX ' + @ObjectName 
		EXEC(@SQL)
		PRINT 'Dropping Index ' + @ObjectName
		FETCH IndexCursor INTO @TableName, @IndexName, @IncludeNames
END

CLOSE IndexCursor
DEALLOCATE IndexCursor

END --@TaskType = 'DROP'

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF @TaskType = 'ADD'
BEGIN
	DECLARE IndexCursor CURSOR FOR SELECT DISTINCT TableName, IndexName, IncludeColumns, ColumnName, IndexInclude, NoColumns 
	FROM CTL_IndexHistory WHERE IndexSystemID > 1 AND IndexColumnPosition = 1 AND TableName LIKE '%' + @TableFilter + '%'
	OPEN IndexCursor
	FETCH IndexCursor INTO @TableName, @IndexName, @IncludeNames, @ColumnName, @IndexInclude, @NoColumns

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @ObjectName = @TableName + '.' + @IndexName
		IF @NoColumns = 1
			SET @SQL = 'IF NOT Exists (select * from sysindexes where name = ''' + @IndexName + ''')' +
			'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + '(' + @ColumnName + ')'
		ELSE
		BEGIN
			IF @IndexInclude = 1
				SET @SQL = 'IF NOT Exists (select * from sysindexes where name = ''' + @IndexName + ''')' +
				'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + '(' + @ColumnName + ')  INCLUDE (' + @IncludeNames + ')' 
			IF @IndexInclude <> 1
				SET @SQL = 'IF NOT Exists (select * from sysindexes where name = ''' + @IndexName + ''')' +
				'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + '(' + @IncludeNames + ')' 
		END
		EXEC(@SQL)
		PRINT 'Adding Index ' + @IndexName + ' : ' + @SQL 
		--PRINT @SQL
   FETCH IndexCursor INTO @TableName, @IndexName, @IncludeNames, @ColumnName, @IndexInclude, @NoColumns
END

CLOSE IndexCursor
DEALLOCATE IndexCursor

END -- @TaskType = 'ADD'

IF @TaskType = 'HISTORY'
	SELECT * FROM CTL_IndexHistory

IF @TaskType = 'NOW'
BEGIN
SELECT     
O.id AS TableSystemID, 
I.indid AS IndexSystemID, 
IK.colid AS ColumnSystemID, 
IK.keyno AS IndexColumnPosition,
CASE I.indid WHEN 0 THEN 'Heap' WHEN 1 THEN 'Clustered' ELSE 'Nonclustered' END AS IndexType,
O.name AS TableName, 
I.name AS IndexName,
TC.name AS ColumnName
FROM         
syscolumns AS TC INNER JOIN
sysobjects AS O INNER JOIN
sysindexes AS I ON O.id = I.id INNER JOIN
sysindexkeys AS IK ON I.indid = IK.indid AND I.id = IK.id ON TC.colid = IK.colid AND TC.id = IK.id
WHERE 
O.xtype = 'U' AND
(O.name LIKE 'DIM_%' OR O.name LIKE 'FACT_%' OR O.name LIKE 'TEMP_%' OR O.name LIKE 'SUMMARY_%')
ORDER BY O.id ASC, I.indid ASC, IK.keyno ASC
END -- @TaskType = 'NOW'

END



GO

/****** Object:  StoredProcedure [dbo].[BBBI_ManageFuzzyDates]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BBBI_ManageFuzzyDates]
@UpdateTable Varchar(50) 
-- Manage Fuzzy Dates - Pass Parameter of Table you want to update
AS
BEGIN

DECLARE @SQL NVarchar(2000)
DECLARE @UpdateCommand NVarchar(10) = 'UPDATE '
DECLARE @SetCommand NVarchar(10) = ' SET '
DECLARE @WhereCommand NVarchar(10) = ' WHERE '
DECLARE @DeclareClause NVarchar(1000)
DECLARE @WhereClause NVarchar(1000)
DECLARE @SetClause NVarchar(1000)

SET @DeclareClause = 
'DECLARE @CurrentDate DateTime' + CHAR(10) + 
'DECLARE @CurrentDateDimID Int' + CHAR(10) + 
'SET @CurrentDate = GetDate()' + CHAR(10) + 
'SET @CurrentDateDimID = YEAR(@CurrentDate) * 10000 + MONTH(@CurrentDate) * 100 + DAY(@CurrentDate)' + CHAR(10)

SET @SetClause = 'DateFrom = dbo.BBBI_ConvertFuzzyDate(FuzzyDateFrom)'
SET @WhereClause = 'DateFrom IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'DateTo = dbo.BBBI_ConvertFuzzyDate(FuzzyDateTo)'
SET @WhereClause = 'DateTo IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'DateFromDimID = YEAR(DateFrom) * 10000 + MONTH(DateFrom) * 100 + DAY(DateFrom)'
SET @WhereClause = 'DateFromDimID IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'DateToDimID = YEAR(DateTo) * 10000 + MONTH(DateTo) * 100 + DAY(DateTo)'
SET @WhereClause = 'DateToDimID IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'DateFromDimID = 19000101'
SET @WhereClause = 'DateFromDimID IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'DateToDimID = 19000101'
SET @WhereClause = 'DateToDimID IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'IsCurrent = 0'
SET @WhereClause = '(DateFromDimID <> 19000101 and DateFromDimID > @CurrentDateDimID) or (DateToDimID <> 19000101 and DateToDimID < @CurrentDateDimID)'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL

SET @SetClause = 'IsCurrent = 1'
SET @WhereClause ='IsCurrent IS NULL'
SET @SQL = @DeclareClause + @UpdateCommand + @UpdateTable + @SetCommand + @SetClause + @WhereCommand + @WhereClause
EXEC sp_executesql @SQL


END



GO

/****** Object:  StoredProcedure [dbo].[BBBI_ParseVCOFilters]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_ParseVCOFilters]
    	@GetFrom Bit = 1

AS
BEGIN

	DECLARE @List Varchar(1000)
	DECLARE @ListItemString Varchar(10)
	DECLARE @ListItem Int
	--DECLARE @ItemTable TABLE(FilterSystemID Int, TemplateDetailSystemID Int, TemplateSystemID Int, TemplateID Varchar(60), TemplateName Varchar(100), ItemValue Varchar(10))
	DECLARE @TemplateSystemID Int
	DECLARE @TemplateDetailSystemID Int
	DECLARE @AccountFilterSystemID Int
	DECLARE @TemplateID Varchar(60)
	DECLARE @TemplateName Varchar(100)
	DECLARE @ParentKey Int
	DECLARE @ToFromType Varchar(10)
	DECLARE @IncludeType Varchar(20)
	DECLARE @FilterType Varchar(20)
	DECLARE @ToFromList Varchar(2000)
	DECLARE @i Int
	
	IF @GetFrom = 1
	BEGIN
		DECLARE IndexCursor CURSOR FOR 
			SELECT FilterSystemID, TemplateDetailSystemID, TemplateSystemID, TemplateID, 
			TemplateName, ParentKey AS ParentKey, IncludeType, FilterType, 
			COALESCE(VALUE1, '') + ':'+COALESCE(VALUE2, ''), VALUE1 
			FROM TEMP_VCOFilter WHERE VALUE1 LIKE '%,%' AND VALUE1 IS NOT NULL
			SET @ToFromType = 'From'
	END
	ELSE
	BEGIN
		DECLARE IndexCursor CURSOR FOR 
			SELECT FilterSystemID, TemplateDetailSystemID, TemplateSystemID, TemplateID, 
			TemplateName , 
			--TemplateDetailSystemID AS ParentKey,	
			ParentKey AS ParentKey,	
			IncludeType, FilterType, COALESCE(VALUE1, '') + ':' + COALESCE(VALUE2, '') AS FromToList, VALUE2
			FROM TEMP_VCOFilter WHERE VALUE2 LIKE '%,%' AND VALUE2 IS NOT NULL
			SET @ToFromType = 'To'
	END

	OPEN IndexCursor
	FETCH IndexCursor INTO @AccountFilterSystemID, @TemplateDetailSystemID, @TemplateSystemID, @TemplateID, @TemplateName, @ParentKey, 
	@IncludeType, @FilterType, @ToFromList, @List

	SET @i = 1
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @List
		INSERT INTO TEMP_VCOToFromFilter (FilterSystemID, TemplateDetailSystemID, TemplateSystemID, TemplateID, TemplateName, ParentKey,IncludeType, FilterType, ToFromList, Sequence, ToFromType, FilterKey)
			SELECT DISTINCT @AccountFilterSystemID AS AccountFilterSystemID, 
			--NULL AS TemplateDetailSystemID, 
		    @TemplateDetailSystemID AS TemplateDetailSystemID, 
			@TemplateSystemID AS TemplateSystemID, @TemplateID AS TemplateID, @TemplateName AS TemplateName, 
			@ParentKey AS ParentKey, @IncludeType AS IncludeType, @FilterType AS FilterType, @ToFromList As ToFromList, ls.ID AS Sequence, @ToFromType AS ToFromType, Convert(Int, strval) AS FilterKey
			FROM dbo.BBBI_Split(@List ,',') ls
		FETCH IndexCursor INTO @AccountFilterSystemID, @TemplateDetailSystemID, @TemplateSystemID, @TemplateID, @TemplateName, @ParentKey, 
		@IncludeType, @FilterType, @ToFromList, @List
		SET @i = @i + 1
	END

CLOSE IndexCursor
DEALLOCATE IndexCursor

SELECT * FROM TEMP_VCOToFromFilter

END







GO

/****** Object:  StoredProcedure [dbo].[BBBI_PopulateDateDim]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_PopulateDateDim] 
	@StartDate DateTime,
	@EndDate DateTime,
	@QuarterStartMonth Int
AS
BEGIN
--declare variables
DECLARE @DT DATETIME

DECLARE @i INT
DECLARE @DateKey INT
DECLARE @CalendarYear SMALLINT
DECLARE @CalendarHalf TINYINT
DECLARE @CalendarHalfName CHAR(2)
DECLARE @CalendarQuarter TINYINT
DECLARE @CalendarQuarterName CHAR(2)
DECLARE @CalendarMonth  TINYINT
DECLARE @CalendarMonthName CHAR(10)
DECLARE @CalendarWeek  TINYINT
DECLARE @CalendarDayofYear SMALLINT

DECLARE @FiscalYear SMALLINT
DECLARE @FiscalHalf TINYINT
DECLARE @FiscalHalfName CHAR(2)
DECLARE @FiscalQuarter TINYINT
DECLARE @FiscalQuarterName CHAR(2)
DECLARE @FiscalMonth  TINYINT
DECLARE @FiscalMonthName CHAR(10)
DECLARE @FiscalWeek  TINYINT
DECLARE @FiscalDayofYear SMALLINT

DECLARE @DayofMonth INT
DECLARE @DayofWeek INT
DECLARE @DayName VARCHAR(12)

DECLARE @IsWeekend BIT
DECLARE @IsWeekday BIT
DECLARE @IsHoliday BIT
DECLARE @HolidayName VARCHAR(50)
DECLARE @MonthName VARCHAR(20)
DECLARE @IsLeapYear BIT
DECLARE @CurrentDateDimID Int
DECLARE @CurrentDate DateTime

SET @CurrentDate = GetDate()
SET @CurrentDateDimID = Year(@CurrentDate) * 10000 + Month(@CurrentDate) * 100 + Day(@CurrentDate)

--initialize variables
SELECT @IsHoliday 	= 0
SELECT @IsWeekend 	= 0
SELECT @FiscalWeek 	= 1
SELECT @CalendarWeek 	= 1
SELECT @IsLeapYear 	= 0

--the starting date for the date dimension
SELECT @DT = @StartDate

SET @i = 1
--start looping, stop at ending date
WHILE (@DT <= @EndDate)
BEGIN
--get information about the data
	SELECT @IsWeekend  		= 0
	SELECT @IsWeekDay			= 1
	SELECT @CalendarYear 		= DATEPART (yyyy , @DT)
	SELECT @CalendarHalf		= CASE DATEPART(q , @DT)
						WHEN 1 THEN 1
						WHEN 2 THEN 1
						WHEN 3 THEN 2
						WHEN 4 THEN 2
					  END 
	SELECT @CalendarHalfName	= 'H' + CAST(@CalendarHalf AS CHAR)
	SELECT @CalendarQuarter 	= DATEPART (q , @DT)
	SELECT @CalendarQuarterName	= 'Q' + CAST(@CalendarQuarter AS CHAR)
	SELECT @CalendarMonth		= DATEPART (MONTH , @DT)
	SELECT @CalendarMonthName	= DATENAME (mm , @DT)
	SELECT @CalendarWeek		= DATEPART (ww , @DT)
	SELECT @CalendarDayofYear	= DATEPART (dy , @DT)
	SELECT @DayofMonth		= DATEPART (dd , @DT)
	SELECT @DayofWeek		= DATEPART (dw , @DT)
	SELECT @DayName			= DATENAME (dw , @DT)

--SELECT DATEPART(DW, '8/24/2005')
--SELECT DATENAME(DW, '8/24/2005')

--note if weeknd or not
IF ( @DayofWeek = 1 OR  @DayofWeek = 7 )  
BEGIN
	SELECT @IsWeekend	= 1
	SELECT @IsWeekDay = 0
END

--add business rule (need to know complete weeks in a year, so a partial week in new year set to 0)
IF ( @DayofWeek != 1 AND @FiscalDayofYear = 1)
BEGIN
	SELECT @FiscalWeek 	= 0
END


IF ( @DayofWeek = 1)
BEGIN
	SELECT @FiscalWeek 	= @FiscalWeek + 1
END

--add business rule (start counting business weeks with first complete week)
IF (@FiscalWeek = 53)
BEGIN
	SELECT @FiscalWeek	= 1
END

--check for leap year
IF ((@CalendarYear % 4 = 0)  AND (@CalendarYear % 100 != 0 OR @CalendarYear % 400 = 0))
	SELECT @IsLeapYear	= 1
	ELSE SELECT @IsLeapYear 	= 0

--insert values into Date Dimension table

INSERT DIM_Date (
	DateDimID,
	--DateKey,
	CalendarYear,
	CalendarHalf,
	CalendarHalfName,
	CalendarQuarter,
	CalendarQuarterName,
	CalendarMonth,
	CalendarMonthName,
	CalendarWeek,
	CalendarDayofYear,

	FiscalYear,
	FiscalHalf,
	FiscalHalfName,
	FiscalQuarter,
	FiscalQuarterName,
	FiscalMonth ,
	FiscalMonthName,
	FiscalWeek,
	FiscalDayofYear,

	DayofMonth,
	DayofWeek,
	DayName,
	IsWeekend,
	IsWeekDay,
	IsHoliday,
	IsLeapYear,
	HolidayName,
	ActualDate )
VALUES (--@i,
	(@CalendarYear * 10000) + (@CalendarMonth * 100) + @DayofMonth,
	@CalendarYear, 
	@CalendarHalf, 
	@CalendarHalfName, 
	@CalendarQuarter, 
	@CalendarQuarterName, 
	@CalendarMonth, 
	@CalendarMonthName, 
	@CalendarWeek, 
	@CalendarDayofYear, 
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	@DayofMonth,
	@DayofWeek,
	@DayName,
	@IsWeekend,
	@IsWeekDay,
	@IsHoliday,
	@IsLeapYear,
	NULL, -- Holiday name (only one per date)
	CONVERT(DATETIME, CAST(@CalendarMonth AS VARCHAR) + '/' + CAST(@DayofMonth AS VARCHAR) + '/' + CAST(@CalendarYear AS VARCHAR), 101) )
	
--increment the date one day
SELECT @DT  = DATEADD(DAY, 1, @DT)

	SET @i = @i + 1

END

--***********************  Fiscal Stuff  *****************************************************************

UPDATE DIM_Date
SET 
FiscalQuarter = Convert(Int, dbo.BBBI_CalcQuarterDate (ActualDate, @QuarterStartMonth, 10)),
FiscalQuarterName = dbo.BBBI_CalcQuarterDate (ActualDate, @QuarterStartMonth, 1),
FiscalYear = dbo.BBBI_ReturnFiscalYear (ActualDate, @QuarterStartMonth) 
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate
 
UPDATE DIM_Date
SET 
FiscalHalf = CASE WHEN FiscalQuarter IN (1,2) THEN 1 ELSE 2 END,
FiscalHalfName = CASE WHEN FiscalQuarter IN (1,2) THEN 'H1' ELSE 'H2' END
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

IF @QuarterStartMonth > 1
BEGIN

-- Fiscal Year Start
UPDATE DIM_Date
SET 
FiscalYearStartDate = Convert(DateTime, Convert(Varchar(4), dbo.BBBI_ReturnFiscalYear(ActualDate, @QuarterStartMonth) -1) + '/'+
Convert(Varchar(2), @QuarterStartMonth)+ '/01',101),
FiscalYearEndDate = Convert(DateTime, Convert(Varchar(4), dbo.BBBI_ReturnFiscalYear(ActualDate, @QuarterStartMonth)) + '/'+
Convert(Varchar(2), @QuarterStartMonth)+ '/01',101) -1
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET 
FiscalDayofYear = DATEDIFF([Day], FiscalYearStartDate, ActualDate) + 1,
FiscalWeek = DATEDIFF([Week], FiscalYearStartDate, ActualDate) + 1,
FiscalMonth = DATEDIFF([Month], FiscalYearStartDate, ActualDate) + 1,
FiscalMonthName = DATENAME (mm , ActualDate)
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

END -- IF @QuarterStartMonth > 1
ELSE
IF @QuarterStartMonth = 1
BEGIN
UPDATE DIM_Date
SET 
FiscalDayofYear = CalendarDayofYear,
FiscalWeek = CalendarWeek,
FiscalMonth = CalendarMonth,
FiscalMonthName = DATENAME (mm , ActualDate)
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET 
FiscalYearStartDate = Convert(DateTime, Convert(Varchar(4), dbo.BBBI_ReturnFiscalYear(ActualDate, @QuarterStartMonth)) + '/'+
Convert(Varchar(2), @QuarterStartMonth)+ '/01',101),
FiscalYearEndDate = Convert(DateTime, Convert(Varchar(4), dbo.BBBI_ReturnFiscalYear(ActualDate, @QuarterStartMonth) + 1) + '/'+
Convert(Varchar(2), @QuarterStartMonth)+ '/01',101) -1
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

END  -- IF @QuarterStartMonth = 1

-- Period Start & End
UPDATE 
DIM_Date
SET
PeriodStartDate = Convert(DateTime, Convert(Varchar(4), CalendarYear) + '-' + Convert(Varchar(2), CalendarMonth) + '-1', 101),
PeriodEndDate = DateAdd(mm, 1, Convert(DateTime, Convert(Varchar(4), CalendarYear) + '-' + Convert(Varchar(2), CalendarMonth) + '-1', 101)) - 1
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Fiscal Quarter Start & End
UPDATE 
DIM_Date
SET
FiscalQuarterStartDate = t.QuarterStartDate,
FiscalQuarterEndDate = t.QuarterEndDate
FROM
(
SELECT
FiscalYear, FiscalQuarter,
Min(ActualDate) AS QuarterStartDate, Max(ActualDate) AS QuarterEndDate
FROM
DIM_Date
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate
GROUP BY
FiscalYear, FiscalQuarter
) t
WHERE
t.FiscalYear = DIM_Date.FiscalYear AND
t.FiscalQuarter = DIM_Date.FiscalQuarter

-- Calendar Quarter Start & End
UPDATE 
DIM_Date
SET
CalendarQuarterStartDate = t.QuarterStartDate,
CalendarQuarterEndDate = t.QuarterEndDate
FROM
(
SELECT
CalendarYear, CalendarQuarter,
Min(ActualDate) AS QuarterStartDate, Max(ActualDate) AS QuarterEndDate
FROM
DIM_Date
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate
GROUP BY
CalendarYear, CalendarQuarter
) t
WHERE
t.CalendarYear = DIM_Date.CalendarYear AND
t.CalendarQuarter = DIM_Date.CalendarQuarter

-- Fiscal Period
UPDATE
DIM_Date
SET FiscalPeriod = FiscalMonth
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Fiscal Week Start & End
UPDATE 
DIM_Date
SET
FiscalWeekStartDate = t.StartDate,
FiscalWeekEndDate = t.EndDate
FROM
(
SELECT
FiscalYear, FiscalWeek,
Min(ActualDate) AS StartDate, Max(ActualDate) AS EndDate
FROM
DIM_Date
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate
GROUP BY
FiscalYear, FiscalWeek
) t
WHERE
t.FiscalYear = DIM_Date.FiscalYear AND
t.FiscalWeek = DIM_Date.FiscalWeek

-- Calendar Week Start & End
UPDATE 
DIM_Date
SET
CalendarWeekStartDate = t.StartDate,
CalendarWeekEndDate = t.EndDate
FROM
(
SELECT
CalendarYear, CalendarWeek,
Min(ActualDate) AS StartDate, Max(ActualDate) AS EndDate
FROM
DIM_Date
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate
GROUP BY
CalendarYear, CalendarWeek
) t
WHERE
t.CalendarYear = DIM_Date.CalendarYear AND
t.CalendarWeek = DIM_Date.CalendarWeek

-- Date Long Descriptions
UPDATE DIM_Date
SET
ActualDateString = Convert(Varchar(12), ActualDate, 102),
ActualDateStringMDY = LEFT(CalendarMonthName, 3) + ' ' + Convert(Varchar(2), DayofMonth) + ' ' + Convert(Varchar(4), CalendarYear),
ActualDateStringDMY = Convert(Varchar(2), DayofMonth) + ' ' + LEFT(CalendarMonthName, 3) + ' ' + Convert(Varchar(4), CalendarYear) ,
CalendarHalfYearName = CalendarHalfName + ' ' + Convert(Varchar(4), CalendarYear),
CalendarFullHalfYearName = CalendarHalfName + ' CY ' + Convert(Varchar(4), CalendarYear),
CalendarQuarterYearName = CalendarQuarterName + ' ' + Convert(Varchar(4), CalendarYear),
CalendarFullQuarterYearName = CalendarQuarterName + ' CY ' + Convert(Varchar(4), CalendarYear),
CalendarMonthYearName = LEFT(CalendarMonthName, 3) + ' ' + Convert(Varchar(4), CalendarYear),
CalendarFullMonthYearName = LEFT(CalendarMonthName, 3) + ' CY ' + Convert(Varchar(4), CalendarYear),
CalendarWeekYearName = 'W' + CASE WHEN CalendarWeek < 10 THEN '0' + Convert(Varchar(2), CalendarWeek) ELSE Convert(Varchar(2), CalendarWeek) END + ' ' + Convert(Varchar(4), CalendarYear),
CalendarFullWeekYearName = 'W' + CASE WHEN CalendarWeek < 10 THEN '0' + Convert(Varchar(2), CalendarWeek) ELSE Convert(Varchar(2), CalendarWeek) END + ' CY  ' + Convert(Varchar(4), CalendarYear),
FiscalHalfYearName = FiscalHalfName + ' ' + Convert(Varchar(4), FiscalYear),
FiscalFullHalfYearName = FiscalHalfName + ' FY ' + Convert(Varchar(4), FiscalYear),
FiscalQuarterYearName = FiscalQuarterName + ' ' + Convert(Varchar(4), FiscalYear),
FiscalFullQuarterYearName = FiscalQuarterName + ' FY ' + Convert(Varchar(4), FiscalYear),
FiscalPeriodYearName = 'Period ' + Convert(Varchar(2), FiscalPeriod) + ' ' + Convert(Varchar(4), FiscalYear),
FiscalMonthYearName = LEFT(FiscalMonthName, 3) + ' ' + Convert(Varchar(4), FiscalYear),
FiscalFullMonthYearName = LEFT(FiscalMonthName, 3) + ' FY ' + Convert(Varchar(4), FiscalYear),
FiscalWeekYearName = 'W' + CASE WHEN FiscalWeek < 10 THEN '0' + Convert(Varchar(2), FiscalWeek) ELSE Convert(Varchar(2), FiscalWeek) END + ' ' + Convert(Varchar(4), FiscalYear),
FiscalFullWeekYearName = 'W' + CASE WHEN FiscalWeek < 10 THEN '0' + Convert(Varchar(2), FiscalWeek) ELSE Convert(Varchar(2), FiscalWeek) END + ' FY ' + Convert(Varchar(4), FiscalYear) 
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Set defaults for nth day of week
UPDATE DIM_Date
SET
MonSequence = 0,
TueSequence = 0,
WedSequence = 0,
ThuSequence = 0,
FriSequence = 0,
SatSequence = 0,
SunSequence = 0
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

-- nth Day of Week
UPDATE DIM_Date
SET
MonSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Monday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

UPDATE DIM_Date
SET
TueSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Tuesday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

UPDATE DIM_Date
SET
WedSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Wednesday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

UPDATE DIM_Date
SET
ThuSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Thursday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

UPDATE DIM_Date
SET
FriSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Friday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

UPDATE DIM_Date
SET
SatSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Saturday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

UPDATE DIM_Date
SET
SunSequence = t.Sequence
FROM
(
SELECT 
DateDimID, 
RANK() OVER (PARTITION BY CalendarMonth, CalendarYear ORDER BY DateDimID) as Sequence
FROM DIM_Date
WHERE DayName = 'Sunday'
AND ActualDate >= @StartDate AND ActualDate <= @EndDate
) t
WHERE
t.DateDimID = DIM_Date.DateDimID

-- Holidays
UPDATE DIM_Date
SET
HolidayName = '4th July'
WHERE DayofMonth = 4 AND CalendarMonth = 7
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Christmas Day'
WHERE DayofMonth = 25 AND CalendarMonth = 12
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Christmas Eve'
WHERE DayofMonth = 24 AND CalendarMonth = 12
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'New Years Day'
WHERE DayofMonth = 1 AND CalendarMonth = 1
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'New Years Eve'
WHERE DayofMonth = 31 AND CalendarMonth = 12
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Valentine''s Day'
WHERE DayofMonth = 14 AND CalendarMonth = 2
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Thanksgiving Day'
WHERE CalendarMonth = 11 AND ThuSequence = 4 
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Labor Day'
WHERE CalendarMonth = 9 AND MonSequence = 1
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Martin Luther King Day'
WHERE CalendarMonth = 1 AND MonSequence = 3 AND CalendarYear >= 1971
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Columbus Day'
WHERE CalendarMonth = 10 AND DayofMonth = 12 AND CalendarYear < 1971
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Columbus Day'
WHERE CalendarMonth = 10 AND MonSequence = 2 AND CalendarYear >= 1971
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Washington''s Birthday'
WHERE CalendarMonth = 2 AND DayofMonth = 22 AND CalendarYear < 1971
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Washington''s Birthday'
WHERE CalendarMonth = 2 AND MonSequence = 3 AND CalendarYear >= 1971
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Mother''s Day'
WHERE CalendarMonth = 5 AND SunSequence = 2
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Father''s Day'
WHERE CalendarMonth = 6 AND SunSequence = 3
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Veterans Day'
WHERE CalendarMonth = 11 AND DayofMonth = 11
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Armed Forces Day'
WHERE CalendarMonth = 5 AND SatSequence = 3
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Halloween'
WHERE DayofMonth = 31 AND CalendarMonth = 10
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'St Patrick''s Day'
WHERE DayofMonth = 17 AND CalendarMonth = 3
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
HolidayName = 'Groundhog Day'
WHERE DayofMonth = 2 AND CalendarMonth = 2
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
IsHoliday = 1
WHERE HolidayName IS NOT NULL
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

/* ------------------------------------------------------------------------------------------------------------------
	Seasons - Spring, Summer, Autumn, Winter
------------------------------------------------------------------------------------------------------------------*/
-- Spring Time
DECLARE @SeasonName Varchar(20)
DECLARE @SeasonStartDate Varchar(8)
DECLARE @SeasonEndDate Varchar(8)
SET @SeasonName = 'Spring'
SET @SeasonStartDate = '3/21/'
SET @SeasonEndDate = '6/20/'
UPDATE DIM_Date
SET
SeasonName = @SeasonName,
SeasonStartDate = Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)),
SeasonEndDate = Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Summer
SET @SeasonName = 'Summer'
SET @SeasonStartDate = '6/21/'
SET @SeasonEndDate = '9/20/'
UPDATE DIM_Date
SET
SeasonName = @SeasonName,
SeasonStartDate = Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)),
SeasonEndDate = Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Autumn
SET @SeasonName = 'Autumn'
SET @SeasonStartDate = '9/21/'
SET @SeasonEndDate = '12/20/'
UPDATE DIM_Date
SET
SeasonName = @SeasonName,
SeasonStartDate = Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)),
SeasonEndDate = Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Winter
SET @SeasonName = 'Winter'
SET @SeasonStartDate = '12/21/'
SET @SeasonEndDate = '3/20/'
UPDATE DIM_Date
SET
SeasonName = @SeasonName,
SeasonStartDate = Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)),
SeasonEndDate = Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  '12/31/' + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate


-- Winter Part 2
UPDATE DIM_Date
SET
SeasonName = @SeasonName,
SeasonStartDate = Convert(DateTime,  @SeasonStartDate + Convert(Varchar(4), CalendarYear)),
SeasonEndDate = Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  '1/1/' + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SeasonEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

/* ------------------------------------------------------------------------------------------------------------------
	Academic Semesters - Note This is default and would normally be CUSTOM to each client
------------------------------------------------------------------------------------------------------------------*/
-- Fall Semester
DECLARE @Semester SmallInt
DECLARE @SemesterName Varchar(15)
DECLARE @SemesterStartDate Varchar(8)
DECLARE @SemesterEndDate Varchar(8)
SET @Semester = 1
SET @SemesterName = 'Fall'
SET @SemesterStartDate = '9/1/'
SET @SemesterEndDate = '12/31/'
UPDATE DIM_Date
SET
AcademicSemester = @Semester,
AcademicSemesterName = @SemesterName,
SemesterStartDate = Convert(DateTime,  @SemesterStartDate + Convert(Varchar(4), CalendarYear)),
SemesterEndDate = Convert(DateTime,  @SemesterEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SemesterStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SemesterEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Spring Semester
SET @Semester = 2
SET @SemesterName = 'Spring'
SET @SemesterStartDate = '1/1/'
SET @SemesterEndDate = '4/30/'
UPDATE DIM_Date
SET
AcademicSemester = @Semester,
AcademicSemesterName = @SemesterName,
SemesterStartDate = Convert(DateTime,  @SemesterStartDate + Convert(Varchar(4), CalendarYear)),
SemesterEndDate = Convert(DateTime,  @SemesterEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SemesterStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SemesterEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Summer Semester
SET @Semester = 2
SET @SemesterName = 'Summer'
SET @SemesterStartDate = '5/1/'
SET @SemesterEndDate = '8/31/'
UPDATE DIM_Date
SET
AcademicSemester = @Semester,
AcademicSemesterName = @SemesterName,
SemesterStartDate = Convert(DateTime,  @SemesterStartDate + Convert(Varchar(4), CalendarYear)),
SemesterEndDate = Convert(DateTime,  @SemesterEndDate + Convert(Varchar(4), CalendarYear))
WHERE
ActualDate >= Convert(DateTime,  @SemesterStartDate + Convert(Varchar(4), CalendarYear)) AND
ActualDate <= Convert(DateTime,  @SemesterEndDate + Convert(Varchar(4), CalendarYear))
--AND ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Academic Years
UPDATE DIM_Date
SET
AcademicYearStartDate = SemesterStartDate
WHERE
AcademicSemester = 1
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
AcademicYearStartDate = SemesterEndDate
WHERE
AcademicSemester = 3
AND ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
AcademicYear = Year(SemesterEndDate)
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

-- Academic Year Descriptions
UPDATE DIM_Date
SET
AcademicMonthYearName = LEFT(CalendarMonthName, 3) + ' ' + Convert(Varchar(4), AcademicYear),
AcademicSemesterYearName = AcademicSemesterName + ' ' + Convert(Varchar(4), AcademicYear)
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

------------------------------------------------------------------------------------------
-- Keys for Fiscal/Calendar Weeks, Months and Quarters
------------------------------------------------------------------------------------------
UPDATE DIM_Date
SET
CalendarYearHalf = CalendarYear * 100 + CalendarHalf,
CalendarYearQuarter = CalendarYear * 100 + CalendarQuarter,
CalendarYearMonth = CalendarYear * 100 + CalendarMonth,
CalendarYearWeek = CalendarYear * 100 + CalendarWeek,
FiscalYearHalf = FiscalYear * 100 + FiscalHalf,
FiscalYearQuarter = FiscalYear * 100 + FiscalQuarter,
FiscalYearPeriod = FiscalYear * 100 + FiscalPeriod,
FiscalYearMonth = FiscalYear * 100 + FiscalMonth,
FiscalYearWeek = FiscalYear * 100 + FiscalWeek,
AcademicYearSemester = AcademicYear * 100 + AcademicSemester
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

------------------------------------------------------------------------------------------
-- Current Date & Filter
------------------------------------------------------------------------------------------
UPDATE DIM_Date
SET
Filter = 1
WHERE ActualDate >= @StartDate AND ActualDate <= @EndDate

UPDATE DIM_Date
SET
IsCurrentDate = 0

UPDATE DIM_Date
SET
IsCurrentDate = 1
WHERE 
DateDimID = @CurrentDateDimID

END  --- Of Stored Proc


GO

/****** Object:  StoredProcedure [dbo].[BBBI_PopulateTimeDim]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BBBI_PopulateTimeDim] 
AS
BEGIN
--declare variables
DECLARE @i Int
DECLARE @j Int
DECLARE @Hour24Inc SmallInt
DECLARE @Hour12Inc SmallInt
DECLARE @MinChar Varchar(2)
DECLARE @Hour12Char Varchar(2)
DECLARE @Hour24Char Varchar(2)
DECLARE @MinInc SmallInt
DECLARE @HourMax SmallInt
DECLARE @MinMax SmallInt
DECLARE @AMorPM Varchar(2)
DECLARE @TimeDescription Varchar(25)
DECLARE @TimeDescription24 Varchar(25)
DECLARE @TimeDimID Int
DECLARE @TimeFull Varchar(8)

SET @i = 1
SET @j = 0
SET @HourMax = 24
SET @MinMax = 60
SET @Hour24Inc = 0
SET @Hour12Inc = 12
SET @MinInc = 0

TRUNCATE TABLE DIM_Time

WHILE (@Hour24Inc < @HourMax)
BEGIN
	IF @Hour24Inc < 12 SET @AMorPM = 'AM' 
	ELSE SET @AMorPM = 'PM'
	IF @Hour24Inc < 1 SET @Hour12Inc = 12
	IF @Hour24Inc > 12 SET @Hour12Inc = @Hour24Inc - 12
	IF @Hour24Inc <= 12 AND @Hour24Inc > 0 SET @Hour12Inc = @Hour24Inc

	IF @Hour12Inc < 10 SET @Hour12Char = '0' + Convert(Varchar(1), @Hour12Inc) ELSE SET @Hour12Char = Convert(Varchar(2), @Hour12Inc)
	IF @Hour24Inc < 10 SET @Hour24Char = '0' + Convert(Varchar(1), @Hour24Inc) ELSE SET @Hour24Char = Convert(Varchar(2), @Hour24Inc)

	WHILE (@MinInc < @MinMax)
	BEGIN
		SET @TimeDimID = @Hour24Inc * 100 + @MinInc + 1
		IF Len(@MinInc) < 2 SET @MinChar = '0' + Convert(Varchar(1), @MinInc) ELSE SET @MinChar = Convert(Varchar(2), @MinInc)
		SET @TimeDescription = Convert(Varchar(2), @Hour12Inc) + ':' + @MinChar + ' ' + @AMorPM
		SET @TimeDescription24 = @Hour24Char + ':' + @MinChar
		SET @TimeFull = @Hour12Char + ':' + @MinChar + ' ' + @AMorPM
		-- Insert into Time Table
		INSERT INTO DIM_Time (TimeDimID, TimeFull, TimeDescription, TimeDescription24, Hour, HourFull, Hour24, Hour24Full, Minute, MinuteFull, AMorPM)
		VALUES (@j, @TimeFull, @TimeDescription, @TimeDescription24, @Hour12Inc, @Hour12Char, @Hour24Inc, @Hour24Char, @MinInc, @MinChar, @AMorPM)
		-- Increment Values
		SET @MinInc = @MinInc + 1
		SET @i = @i + 1
		SET @j = @i - 1
	END
	SET @Hour24Inc = @Hour24Inc + 1
	--SET @Hour12Inc = @Hour12Inc + 1
	SET @MinInc = 0
END

-- Performance Time
UPDATE DIM_Time
SET PerformanceTime = 'Morning'
WHERE Hour24 >= 5 AND Hour24 <= 11
UPDATE DIM_Time
SET PerformanceTime = 'Matinee'
WHERE Hour24 >=12  AND Hour24 <= 16
UPDATE DIM_Time
SET PerformanceTime = 'Evening'
WHERE Hour24 >=17  AND Hour24 <= 23
UPDATE DIM_Time
SET PerformanceTime = 'Night time'
WHERE Hour24 = 0 OR (Hour24 >=1  AND Hour24 <= 4)

-- PerformanceTimeGroup
UPDATE DIM_Time
SET PerformanceTimeGroup = 'Matinee'
WHERE Hour24 < 17
UPDATE DIM_Time
SET PerformanceTimeGroup = 'Evening'
WHERE Hour24 >= 17 AND Hour24 < 23
UPDATE DIM_Time
SET PerformanceTimeGroup = 'Night'
WHERE Hour24 >= 23 OR (Hour24 >= 0 AND Hour24 <= 5)

-- Nearest Meal Time
UPDATE DIM_Time
SET NearestMealTime = 'Breakfast'
WHERE Hour24 >= 5 AND Hour24 <=10
UPDATE DIM_Time
SET NearestMealTime = 'Lunch'
WHERE Hour24 >= 11 AND Hour24 <=14
UPDATE DIM_Time
SET NearestMealTime = 'Afternoon Tea'
WHERE Hour24 >= 15 AND Hour24 <=16
UPDATE DIM_Time
SET NearestMealTime = 'Dinner'
WHERE Hour24 >= 17 AND Hour24 <=23
UPDATE DIM_Time
SET NearestMealTime = 'Supper'
WHERE Hour24 >= 23 OR (Hour24 >= 0 AND Hour24 < 5) 
END

-- Time Group Description
UPDATE DIM_Time
SET TimeGroupDescription = 'Early Morning'
WHERE Hour24 >= 5 AND Hour24 <= 7
UPDATE DIM_Time
SET TimeGroupDescription = 'Mid Morning'
WHERE Hour24 >= 8 AND Hour24 <= 9
UPDATE DIM_Time
SET TimeGroupDescription = 'Late Morning'
WHERE Hour24 >=10  AND Hour24 <= 11
UPDATE DIM_Time
SET TimeGroupDescription = 'Early Afternoon'
WHERE Hour24 >=12  AND Hour24 <= 13
UPDATE DIM_Time
SET TimeGroupDescription = 'Mid Afternoon'
WHERE Hour24 >=14  AND Hour24 <= 15
UPDATE DIM_Time
SET TimeGroupDescription = 'Late Afternoon'
WHERE Hour24 >=16  AND Hour24 <= 17
UPDATE DIM_Time
SET TimeGroupDescription = 'Early Evening'
WHERE Hour24 >=18  AND Hour24 <= 19
UPDATE DIM_Time
SET TimeGroupDescription = 'Mid Evening'
WHERE Hour24 >=20  AND Hour24 <= 21
UPDATE DIM_Time
SET TimeGroupDescription = 'Late Evening'
WHERE Hour24 >=22  AND Hour24 <= 23
UPDATE DIM_Time
SET TimeGroupDescription = 'Night time'
WHERE Hour24 = 0 OR (Hour24 >=1  AND Hour24 <= 4)


GO

/****** Object:  StoredProcedure [dbo].[BBBI_ProposalSummaryUpdates]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BBBI_ProposalSummaryUpdates]
    	@TargetAmountField Varchar(30) ,
		@TargetDimIDField Varchar(30),
		@SourceAmountField Varchar(30),
		@SequenceField Varchar(30)
AS
BEGIN
DECLARE @SQL1 NVarchar(2000)
DECLARE @SQL2 NVarchar(2000)
DECLARE @SQL NVarchar(2000)


SET @SQL1 ='UPDATE FACT_ConstituentLTProposal SET ' + @TargetDimIDField +' = p.ProposalDimID' 

IF @TargetAmountField = ''
	SET @SQL1 = @SQL1
ELSE
	SET @SQL1 = @SQL1 + ', ' + @TargetAmountField + '= p.Amount'  
	
	
SET @SQL2 = ' FROM (SELECT tp.ConstituentDimID, tp.ProposalDimID, tp.' + @SourceAmountField + 
' AS Amount FROM DIM_Proposal tp INNER JOIN DIM_ProposalRank tpr ON tpr.ProposalDimID = tp.ProposalDimID WHERE ' + 
@SequenceField + 
' = 1 ) p WHERE p.ConstituentDimID = FACT_ConstituentLTProposal.ConstituentDimID'

SET @SQL = @SQL1 + @SQL2

BEGIN TRY 
	EXEC sp_executesql @SQL
	PRINT @SQL
END TRY
BEGIN CATCH
	SELECT ERROR_NUMBER() as ErrorNumber
	, ERROR_MESSAGE() as ErrorMessage
END CATCH

END


GO

/****** Object:  StoredProcedure [dbo].[BBBI_TableCountsSource]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_TableCountsSource]
    	@DatabaseName Varchar(500)  = ''
AS
BEGIN
DECLARE @SQL NVarchar(2000)
DECLARE @TableCount TABLE ( Rowid int primary key identity , TableName varchar(150) , [RowCount] int )

SET @SQL = 'DECLARE @TableCount TABLE ( Rowid int primary key identity , TableName varchar(150) , [RowCount] int ) INSERT INTO @TableCount ( TableName , [RowCount] ) '
IF @DatabaseName = ''
	SET @SQL = @SQL + 'EXEC dbo.sp_MSforeachtable @command1="SELECT ''?'', Count(*) FROM ?" '
ELSE
	SET @SQL = @SQL + 'EXEC ' + @DatabaseName + '.dbo.sp_MSforeachtable @command1="SELECT ''?'', Count(*) FROM ?" '

--PRINT @SQL
SET @SQL = @SQL + ' SELECT TableName, [RowCount] FROM @TableCount WHERE TableName NOT LIKE ''%Temp%'' ORDER BY TableName, [RowCount]'

EXEC sp_executesql @SQL

END

GO

/****** Object:  StoredProcedure [dbo].[BBBI_TestScript]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_TestScript]
	@PrefixName Varchar(50) = '',
    	@TaskType Varchar(15) = 'TABLE',
		@ExcludeType Varchar(10) = 'LIKE',
		@MetaDataTableName Varchar(100) = 'CTL_DWTableStats',
		@SourceName Varchar(25) = 'DW'
/********************* Arguments ******************************************************************************************************************************
@PrefixName - Wildcard, first part of table name to either include or exclude, e.g. put in 'DIM_' and it will do all tables starting with that
@TaskType - This is TABLE to store table counts, COLUMN to store every column in each table and TRUNCATE to truncate all tables with the @Prefix
@ExcludeType - This is defaulted to LIKE but you can put in 'NOT LIKE' to exclude all tables starting with the @PrefixName value
@MetaDataTableName	-	This is the name of the table that the stats are stored in.  If it doesn't exist then it is created
@SourceName - This is to name the source such as 'DW' or 'RE'
*******************************************************************************************************************************************************************/
AS
BEGIN
DECLARE @SQL NVarchar(2000)
DECLARE @SQLCreate NVarchar(2000)
DECLARE @IndexName Varchar(100)
DECLARE @TableName Varchar(100)
DECLARE @OutputField Varchar(50)
DECLARE @ObjectName Varchar(100)
DECLARE @i Int
DECLARE @iPos Int
DECLARE @Command Varchar(100)
DECLARE @CountValue Int
DECLARE @ObjectType Varchar(50)
DECLARE @StatsType Varchar(50)
DECLARE @TableID Int
DECLARE  @IndexID  Int
DECLARE  @ColumnID Int
DECLARE @IndexPosition Int
DECLARE @IndexType Varchar(50)
DECLARE @ColumnTypeID Int
DECLARE @ColumnName Varchar(100)
DECLARE @PriorTableName Varchar(100)
DECLARE @HasIndex Int
DECLARE @IsPrimary Int
DECLARE @StartCol Varchar(1)
DECLARE @EndCol Varchar(1)

--if NOT exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CTL_TableStats]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
SET @SQLCreate = 
'if NOT exists (select * from dbo.sysobjects where id = object_id(N''' + @MetaDataTableName +  ''') and OBJECTPROPERTY(id, N''IsUserTable'') = 1)
BEGIN
CREATE TABLE ' + @MetaDataTableName + '(
    ParentObjectName Varchar(100) NULL,
    ObjectName Varchar(100) NULL,
	ObjectType Varchar(50) NULL,
	StatsType Varchar(50) NULL,
	[Value] Int,
	[ValueNull] Int,
	[Date] DateTime,
	[HasIndex] bit,
	[IsPrimary] bit,
	[Sequence] Int,
    SourceName Varchar(25) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_' + @MetaDataTableName + '] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END'

BEGIN TRY 
	EXEC sp_executesql @SQLCreate
END TRY
BEGIN CATCH
	SELECT ERROR_NUMBER() as ErrorNumber
	, ERROR_MESSAGE() as ErrorMessage
END CATCH

--PRINT @SQLCreate

SET @ObjectType = 'Table'
SET @StatsType = 'Count'

IF @TaskType = 'COLUMN'
BEGIN
SET @ObjectType = 'Column'
-----------------------------------------------------------------------------------
SET @SQL = 'DECLARE ObjectCursor CURSOR FOR SELECT     
O.id AS TableSystemID, 
IK.indid AS IndexSystemID, 
TC.colid AS ColumnSystemID, 
TC.xtype AS ColumnTypeSystemID,
IK.keyno AS IndexColumnPosition,
CASE WHEN COALESCE(IK.indid, 0) > 0 THEN 1 ELSE 0 END AS HasIndex,
CASE WHEN COALESCE(IK.indid,0) = 1 THEN 1 ELSE 0 END AS IsPrimary,
O.name AS TableName, 
TC.name AS ColumnName
FROM         
sys.sysobjects AS O
INNER JOIN sys.syscolumns AS TC ON TC.id = O.id 
LEFT OUTER JOIN sys.sysindexkeys AS IK ON TC.id = IK.id AND TC.colid = IK.colid
WHERE 
O.xtype = ''U'' AND
O.name ' + @ExcludeType +  ' ''' + @PrefixName +  '%'' ORDER BY O.name, TC.colid' 

BEGIN TRY 
	EXEC sp_executesql @SQL
END TRY
BEGIN CATCH
	SELECT ERROR_NUMBER() as ErrorNumber
	, ERROR_MESSAGE() as ErrorMessage
END CATCH
--PRINT @SQL

SET @i = 2
OPEN ObjectCursor
FETCH ObjectCursor INTO @TableID, @IndexID, @ColumnID, @ColumnTypeID, @IndexPosition, @HasIndex, @IsPrimary, @TableName, @ColumnName
SET @PriorTableName = @TableName
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @iPos = CHARINDEX('[',@ColumnName)
	IF @iPos > 0
	BEGIN
		SET @StartCol = ''
		SET @EndCol = ''
	END
	ELSE
	BEGIN
		SET @StartCol = '['
		SET @EndCol = ']'
	END
	SET @SQL = 'INSERT INTO ' + @MetaDataTableName + ' (ParentObjectName, ObjectName, ObjectType, StatsType, Sequence, HasIndex, IsPrimary,  [Date], [Value], [ValueNull], SourceName) 
SELECT ''' + @TableName + ''',''' + @ColumnName + ''',''' + @ObjectType + ''','''  + @StatsType + ''',' + Convert(Varchar(5), @i) + ', ' + Convert(Varchar(1), @HasIndex) + ', ' + Convert(Varchar(1), @IsPrimary) + 
', GetDate(), COUNT( DISTINCT ' + @StartCol + @ColumnName + @EndCol + ') AS CountValue, (SELECT COUNT(*) FROM [' + @TableName + '] WHERE ' + @StartCol + @ColumnName +  @EndCol + ' IS NULL) AS CountNull, ''' + @SourceName +  ''' FROM [' + @TableName + ']'
	IF @ColumnTypeID <> 35 AND @ColumnTypeID <> 34 -- Text and Media Objects
		BEGIN TRY 
			EXEC sp_executesql @SQL
		END TRY
		BEGIN CATCH
			SELECT ERROR_NUMBER() as ErrorNumber
	,		ERROR_MESSAGE() as ErrorMessage
		END CATCH
	--PRINT @SQL
	SET @PriorTableName = @TableName
	FETCH ObjectCursor INTO @TableID, @IndexID, @ColumnID, @ColumnTypeID, @IndexPosition, @HasIndex, @IsPrimary, @TableName, @ColumnName
	IF @PriorTableName <> @TableName
		SET @i = 1
	SET @i = @i + 1
END

-----------------------------------------------------------------------------------
END
ELSE
BEGIN
-----------------------------------------------------------------------------------
IF @TaskType = 'TRUNCATE' 
	SET @Command = 'TRUNCATE TABLE '

IF @TaskType = 'TABLE' 
	SET @Command = 'SELECT COUNT(*) FROM  '

SET @SQL = 'DECLARE ObjectCursor CURSOR FOR 
SELECT O.name AS TableName FROM sysobjects O WHERE (xtype = ''U'' OR xtype = ''V'') AND  O.name ' + @ExcludeType  + ' '''  + @PrefixName +  '%''' 
EXEC sp_executesql @SQL

OPEN ObjectCursor
FETCH ObjectCursor INTO @TableName

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @TaskType = 'TABLE' 
	BEGIN
		SET @SQL = 'INSERT INTO ' + @MetaDataTableName + ' (ParentObjectName, ObjectName, ObjectType, StatsType, Sequence, [Date], [Value], HasIndex, IsPrimary, SourceName) 
		SELECT ''' + @TableName + ''',''' + @ObjectType + ''',''' + @ObjectType + ''','''  + @StatsType + ''', 1, GetDate(), COUNT(*) AS CountValue, 0, 0, ''' + @SourceName + ''' FROM [' + @TableName + ']'
		BEGIN TRY 
			EXEC sp_executesql @SQL
		END TRY
		BEGIN CATCH
			SELECT ERROR_NUMBER() as ErrorNumber
	,		ERROR_MESSAGE() as ErrorMessage
		END CATCH
		--PRINT @TableName + ' ' + @SQL
	END
	IF @TaskType = 'TRUNCATE' 
	BEGIN
		SET @SQL = @Command + '[' + @TableName + ']'
		EXEC sp_executesql @SQL
		PRINT 'Truncated Table ' + @TableName
	END
   FETCH ObjectCursor INTO @TableName
END

-------------------------------------------------------------------------------------------
END

SET @SQL = 'SELECT * FROM ' + @MetaDataTableName + ' ORDER BY [Date], ObjectName, Sequence'
--EXEC @SQL

CLOSE ObjectCursor
DEALLOCATE ObjectCursor


END


GO

/****** Object:  StoredProcedure [dbo].[BBBI_TruncateDWTables]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[BBBI_TruncateDWTables]
@TruncateType Varchar(15) = 'NONSTATIC'
/*---------------------------------------------------------------------------------------------------------
Parameters:
NONSTATIC - Truncates all user tables except Static Dimension tables
ALL - Truncates all tables
NONHISTORY - Truncates all user tables except Archive/History Tables
----------------------------------------------------------------------------------------------------------*/
AS
BEGIN

DECLARE @SQL NVarchar(2000)
DECLARE @TableName Varchar(100)
DECLARE @ErrorNumber AS Varchar(10)
DECLARE @ErrorMessage AS Varchar(500)

IF @TruncateType IS NULL OR @TruncateType = ''
	SET @TruncateType = 'NONSTATIC'
ELSE
	SET @TruncateType = UPPER(@TruncateType)

-------------------------------------------------------------------------------------------
IF @TruncateType = 'NONSTATIC'
BEGIN
	DECLARE TableCursor CURSOR FOR 
	SELECT
	t.TABLE_NAME
	FROM         
	INFORMATION_SCHEMA.TABLES AS t
	WHERE t.TABLE_TYPE = 'BASE TABLE' AND
	(t.TABLE_NAME LIKE 'CTL_%' OR t.TABLE_NAME LIKE 'DIM_%' OR t.TABLE_NAME LIKE 'FACT_%' OR t.TABLE_NAME LIKE 'TEMP_%')
	AND t.TABLE_NAME NOT IN 
	(
	SELECT 'DIM_ProposalStatusHistory' UNION
	SELECT 'DIM_ConstituentProspect' UNION
	SELECT 'CTL_DWTableStats'  UNION
	SELECT 'CTL_SourceToTargetMapping'  UNION
	SELECT 'DIM_ActionCategory' UNION
	SELECT 'DIM_ActionPriority' UNION
	SELECT 'DIM_AnnualClassification' UNION
	SELECT 'DIM_Date' UNION
	SELECT 'DIM_GiftStatus' UNION
	SELECT 'DIM_InstallmentFrequency' UNION
	SELECT 'DIM_PaymentType' UNION
	SELECT 'DIM_PostStatus' UNION
	SELECT 'DIM_Range' UNION
	SELECT 'DIM_Time' UNION
	SELECT 'STATIC_GiftTypeLookup'
	)
	ORDER BY t.TABLE_NAME
END
ELSE
BEGIN
-------------------------------------------------------------------------------------------
IF @TruncateType = 'NONHISTORY'
BEGIN
	DECLARE TableCursor CURSOR FOR 
	SELECT
	t.TABLE_NAME
	FROM         
	INFORMATION_SCHEMA.TABLES AS t
	WHERE t.TABLE_TYPE = 'BASE TABLE' AND
	(t.TABLE_NAME LIKE 'CTL_%' OR t.TABLE_NAME LIKE 'DIM_%' OR t.TABLE_NAME LIKE 'FACT_%' OR t.TABLE_NAME LIKE 'TEMP_%' OR t.TABLE_NAME LIKE 'STATIC_%')
	AND t.TABLE_NAME NOT IN 
	(
	SELECT 'DIM_ProposalStatusHistory' UNION
	SELECT 'DIM_ConstituentProspect' 
	)
	ORDER BY t.TABLE_NAME
END
-------------------------------------------------------------------------------------------
IF @TruncateType = 'ALL'
BEGIN
	DECLARE TableCursor CURSOR FOR 
	SELECT
	t.TABLE_NAME
	FROM         
	INFORMATION_SCHEMA.TABLES AS t
	WHERE t.TABLE_TYPE = 'BASE TABLE' AND
	(t.TABLE_NAME LIKE 'CTL_%' OR t.TABLE_NAME LIKE 'DIM_%' OR t.TABLE_NAME LIKE 'FACT_%' OR t.TABLE_NAME LIKE 'TEMP_%' OR t.TABLE_NAME LIKE 'STATIC_%')
	ORDER BY t.TABLE_NAME
END

END -- ELSE @TruncateType = 'NONSTATIC'

-------------------------------------------------------------------------------------------------------

OPEN TableCursor
FETCH TableCursor INTO @TableName

WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = 'TRUNCATE TABLE ' +  @TableName 
		BEGIN TRY 
			EXEC sp_executesql @SQL
			PRINT @SQL
		END TRY
		BEGIN CATCH
			SET @ErrorNumber = Convert(Varchar(10), ERROR_NUMBER())
			SET @ErrorMessage = Convert(Varchar(500), ERROR_MESSAGE())
			PRINT @ErrorNumber + ' : ' + @ErrorMessage
			--SELECT ERROR_NUMBER() as ErrorNumber, ERROR_MESSAGE() as ErrorMessage
		END CATCH
		FETCH TableCursor INTO @TableName
	END

CLOSE TableCursor
DEALLOCATE TableCursor

END 


GO

/****** Object:  StoredProcedure [dbo].[BBBI_WarehouseTableTask]    Script Date: 12/05/2011 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_WarehouseTableTask]
	@PrefixName Varchar(50),
    	@TaskType Varchar(15)
AS
BEGIN
DECLARE @SQL Varchar(2000)
DECLARE @IndexName Varchar(100)
DECLARE @TableName Varchar(100)
DECLARE @OutputField Varchar(50)
DECLARE @ObjectName Varchar(100)
DECLARE @i Int
DECLARE @iPos Int
DECLARE @Command Varchar(100)
DECLARE @CountValue Int
DECLARE @ObjectType Varchar(50)
DECLARE @StatsType Varchar(50)

if NOT exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CTL_TableStats]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[CTL_TableStats] (
    ObjectName Varchar(50) NULL,
	ObjectType Varchar(50) NULL,
	StatsType Varchar(50) NULL,
	[Value] Int,
	[Date] DateTime,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_CTL_TableStats] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END

SET @ObjectType = 'Table'
SET @StatsType = 'Count'

IF @TaskType = 'TRUNCATE' 
	SET @Command = 'TRUNCATE TABLE '

IF @TaskType = 'COUNT' 
	SET @Command = 'SELECT COUNT(*) FROM  '

SET @SQL = 'DECLARE DonorCursor CURSOR FOR SELECT O.name AS TableName FROM sysobjects O WHERE xtype = ''U'' AND  O.name LIKE '''  + @PrefixName +  '%''' 
EXEC(@SQL)

OPEN DonorCursor
FETCH DonorCursor INTO @TableName

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @TaskType = 'COUNT' 
	BEGIN
		SET @SQL = 'INSERT INTO CTL_TableStats (ObjectName, ObjectType, StatsType, [Date], [Value]) SELECT ''' + @TableName + ''',''' + @ObjectType + ''','''  + @StatsType + ''', GetDate(), COUNT(*) AS CountValue ' + ' FROM ' + @TableName
		EXEC (@SQL)
		--PRINT @TableName + ' ' + @SQL
	END
	IF @TaskType = 'TRUNCATE' 
	BEGIN
		SET @SQL = @Command + @TableName
		EXEC(@SQL)
		PRINT 'Truncated Table ' + @TableName
	END
   FETCH DonorCursor INTO @TableName
END

IF @TaskType = 'COUNT' 
	SELECT * FROM [CTL_TableStats] ORDER BY [Date], ObjectName

CLOSE DonorCursor
DEALLOCATE DonorCursor

END

GO

/****** Object:  StoredProcedure [dbo].[BBBI_SetColumnComment]    Script Date: 10/11/2012 17:59:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BBBI_SetColumnComment]
@TABLENAME nvarchar(128),
@COLUMNNAME nvarchar(128),
@COMMENT nvarchar(4000),
@TYPE nvarchar(128) = 'MS_Description'
as
  declare @Schema nvarchar(20) = 'dbo'
  declare @VALUE sql_variant

  --select @VALUE = value from fn_listextendedproperty(@TYPE, N'SCHEMA', N'BBDW', N'table', @TABLENAME, N'COLUMN', @COLUMNNAME)
  select @VALUE = value from fn_listextendedproperty(@TYPE, N'SCHEMA', @Schema, N'table', @TABLENAME, N'COLUMN', @COLUMNNAME)

if @VALUE is null
  -- property doesn't exist, so add it
  exec sys.sp_addextendedproperty @TYPE, @COMMENT, N'SCHEMA', @Schema, N'table', @TABLENAME, N'COLUMN', @COLUMNNAME
else
  -- property already exists, so update it
  exec sys.sp_updateextendedproperty @TYPE, @COMMENT, N'SCHEMA', @Schema, N'table', @TABLENAME, N'COLUMN', @COLUMNNAME
GO

/****** Object:  StoredProcedure [dbo].[BBBI_SetTableComment]    Script Date: 10/11/2012 17:59:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[BBBI_SetTableComment]
@TABLENAME nvarchar(128),
@COMMENT nvarchar(4000),
@TYPE nvarchar(128) = 'MS_Description'
as
  declare @Schema nvarchar(20) = 'dbo'
  declare @VALUE sql_variant

  select @VALUE = value from fn_listextendedproperty(@TYPE, N'SCHEMA', @Schema, N'table', @TABLENAME, default, default)

if @VALUE is null
  -- property doesn't exist, so add it
  exec sys.sp_addextendedproperty @TYPE, @COMMENT, N'SCHEMA', @Schema, N'table', @TABLENAME
else
  -- property already exists, so update it
  exec sys.sp_updateextendedproperty @TYPE, @COMMENT, N'SCHEMA', @Schema, N'table', @TABLENAME       

GO

/****** Object:  StoredProcedure [dbo].[BBBI_Create_DWDefaultMember]    Script Date: 10/30/2012 17:12:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
	
CREATE procedure [dbo].[BBBI_Create_DWDefaultMember]
@TABLENAME as VARCHAR(100),
@DEFAULTVALUE as VARCHAR(100) = ''
/*
Create Date: 29 OCT 2012
Author: Alan Eager
Purpose: Create syntax insert of default member
Execution Sample: exec BBDW.USP_USR_CREATE_DWDEFAULTMEMBER 'DIM_APPEAL_EXT', 'No Appeal'
*/
as
  begin
      -- DECLARE VARIABLES
      declare @col      VARCHAR(100),
              @DataType VARCHAR(100), 
              @DataTypeLen INT, 
              @Value VARCHAR(100), 
              @pkcol    VARCHAR(100),
              @tab      VARCHAR(100),
              @isPK     TINYINT,
              @isIdent  TINYINT,
              @view     VARCHAR(100),
              @schema   VARCHAR(100),
              @StmtHeader VARCHAR(1000), 
              @trgStmt  VARCHAR(1000),
              @setLine  VARCHAR(1000),
              @content  VARCHAR(6000) = '',
              @content2 VARCHAR(6000) = '',
              @ObjectName VARCHAR(100), 
              @DefaultFriendlyValueLen INT, 
              @DefaultFriendlyValue VARCHAR(100), 
              @frmLine  VARCHAR(1000),
              @stmt     VARCHAR(7000), 
              @iPos  INT, 
              @i        INT

	  set @iPos = CHARINDEX('_', @TABLENAME)        
	 if @iPos > 0
		set @ObjectName =  SUBSTRING(@TABLENAME, @iPos + 1, LEN(@TABLENAME))     
		
	  set @ObjectName = REPLACE(@ObjectName, '_EXT', '')
	  set @ObjectName = REPLACE(@ObjectName, '_USR_', '')
	  set @ObjectName = REPLACE(@ObjectName, '_USR', '')
 	  set @ObjectName = REPLACE(@ObjectName, 'USR_', '')
 	  
	if @DEFAULTVALUE = '' and @iPos > 0
		set @DefaultFriendlyValue = 'No ' + LOWER(@ObjectName)  
	else
		set @DefaultFriendlyValue = @DEFAULTVALUE
	
	set @DefaultFriendlyValueLen = LEN(@DefaultFriendlyValue)
	
      -- GET REQUIRED TABLE IN THE DATABASE
      declare GET_TAB_DATA cursor for
        select
          TABLE_NAME,
          SCHEMA_NM
        from
          vw_DWSCHEMATABLES
        where
          TABLE_NAME = @TABLENAME

      -- FOR REQUIRED TABLE...
      open GET_TAB_DATA

      fetch NEXT from GET_TAB_DATA into @tab, @schema

		set @StmtHeader = 'if not exists (' + CHAR(10) + 
				'select * from ' + @schema + '.' + @tab + CHAR(10) + 
				'where ' + @ObjectName + 'SystemID = -1' + CHAR(10) + ')' + CHAR(10) + 
				'begin' + CHAR(10) + 
				'set identity_insert ' + @schema + '.' + @tab + ' on' + CHAR(10) + 
				'insert into ' + @schema + '.' + @tab + CHAR(10) + '(' 

	
      while @@fetch_status = 0
        begin
            -- SET THE VIEW NAME
            set @view = ''
            -- SET THE BEGINNING OF THE STATEMENT
            set @trgStmt = ''
            -- SET THE UPDATE LINE WITH THE CORRECT TABLE NAME
            set @setLine = ''

            -- GET ALL COLUMNS IN THE TABLE THAT ARE NOT COMPUTED OR THE PRIMARY KEY
            declare GET_COL_DATA cursor for
				select
				COLUMN_NAME,
				DATA_TYPE,
				CHARACTER_MAXIMUM_LENGTH
				from
				INFORMATION_SCHEMA.COLUMNS
              where
                TABLE_NAME = @tab
                and TABLE_SCHEMA = @schema
			order by TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION

            set @i = 1
			set @content2 = ''
            -- FOR EACH COLUMN...
            open GET_COL_DATA

            fetch NEXT from GET_COL_DATA into @col, @DataType, @DataTypeLen
			------------------------
			--print @col
			------------------------

            while @@fetch_status = 0
              begin
                  -- IF THIS COLUMN IS NOT A PRIMARY KEY AND IS NOT AN IDENTITY FIELD
 				------------------------
				--print @col
				------------------------
                       set @content = @content + '    [' + @col + '],' + char(10)
                        -- Do Insert Values
                       set @Value = 'NULL'
                       if @DataType = 'int' or @DataType = 'bit' --or @DataType = 'datetime' or @DataType = 'date'
	                      set @Value = '0'
                       if @DataType = 'int' and (@col like '%SystemID' or @col like '%DimID' or @col like '%FactID')
	                      set @Value = '-1'
                        if @DataType = 'int' and @col like '%Date%' and @col like '%DimID'
	                      set @Value = '19000101'
                       if @col = 'IsIncluded' 
	                      set @Value = '1'
                      if @DataType = 'nvarchar' or @DataType = 'varchar' or @DataType = 'char'
                       begin 
                          if @DataTypeLen >=  @DefaultFriendlyValueLen
							set @Value = '''' + @DefaultFriendlyValue + ''''
						   else
							set @Value = 'NULL'					   
	                    end 
                       if @DataType = 'uniqueidentifier'
	                      set @Value = '''00000000-0000-0000-0000-000000000000'''
	                      
                        set @content2 = @content2 + char(10) + '    ' + @Value + ' as ' + @col + ','
                        set @i = @i + 1

                        set @frmLine = ''

					fetch NEXT from GET_COL_DATA into @col, @DataType, @DataTypeLen
              end

            close GET_COL_DATA

            deallocate GET_COL_DATA

            -- BUILD THE CREATE TRIGGER STATEMENT
            set @content = LEFT(RTRIM(@content), LEN(RTRIM(@content)) - 2)
            set @content2 = LEFT(RTRIM(@content2), LEN(RTRIM(@content2)) - 1)
            set @stmt = @trgStmt + char(10) + @content + @frmLine + char(10)

            print @StmtHeader + @stmt + char(10) + ')' + char(10) + 'select'  + @content2 +   char(10) + '' + char(10) + 'set identity_insert ' + @schema + '.' + @tab + ' off' + CHAR(10) + 'end'
			--print @content
            --print char(10)

            begin TRY
                if @content <> '	SET '
                   and @frmLine <> ''
                  begin
                      -- CREATE THE TRIGGER
                      print char(10)
                  --EXECUTE(@stmt)
                  end
            end TRY

            begin CATCH
                print 'ERROR: ' + error_message() + char(10) + 'STATEMENT: ' + @stmt
            end CATCH

            -- RESET T-SQL VARIABLES
            set @view = ''
            set @trgStmt = ''
            set @setLine = ''
            set @content = '	'
            set @frmLine = ''

            fetch NEXT from GET_TAB_DATA into @tab, @schema
        end

      close GET_TAB_DATA

      deallocate GET_TAB_DATA
  end 
	
GO

/****** Object:  StoredProcedure [dbo].[BBBI_SchemaCompareVariance]    Script Date: 01/23/2013 09:08:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BBBI_SchemaCompareVariance] (
	-- parameters
	@Database1_Name varchar(100) , 
	@Database2_Name varchar(100) 
)
-- =============================================
-- Author:		Blackbaud Professional Services
-- Create date: 09/05/07
-- Modified date: 01/22/13
-- Description:	Compare schemas of 2 different databases
-- =============================================
AS
BEGIN

SET NOCOUNT ON;

DECLARE @sql nvarchar(4000)
SET @sql =

N'SELECT 
	 REF2.TABLE_SCHEMA
	 , REF2.TABLE_NAME
	 , REF2.COLUMN_NAME
	 , REF2.TABLE_SCHEMA + ''.'' + REF2.TABLE_NAME + ''.'' + REF2.COLUMN_NAME
	 , REF2.IS_NULLABLE
	 , REF2.DATA_TYPE
	 , REF2.CHARACTER_MAXIMUM_LENGTH
	 , REF2.NUMERIC_PRECISION
	 , REF2.NUMERIC_SCALE
	 , REF2.DATETIME_PRECISION

FROM [' + @Database1_Name + '].INFORMATION_SCHEMA.COLUMNS REF2

WHERE REF2.TABLE_SCHEMA + REF2.TABLE_NAME + REF2.COLUMN_NAME NOT IN 

	(
	SELECT 

		REF.TABLE_SCHEMA + REF.TABLE_NAME + REF.COLUMN_NAME

		FROM 
			(
			SELECT TABLE_SCHEMA
			 , TABLE_NAME
			 , COLUMN_NAME
			 , IS_NULLABLE
			 , DATA_TYPE
			 , CHARACTER_MAXIMUM_LENGTH
			 , NUMERIC_PRECISION
			 , NUMERIC_SCALE
			 , DATETIME_PRECISION
			FROM [' + @Database1_Name + '].INFORMATION_SCHEMA.COLUMNS
			)  REF 

		INNER JOIN

			(
			SELECT TABLE_SCHEMA
			 , TABLE_NAME
			 , COLUMN_NAME
			 , IS_NULLABLE
			 , DATA_TYPE
			 , CHARACTER_MAXIMUM_LENGTH
			 , NUMERIC_PRECISION
			 , NUMERIC_SCALE
			 , DATETIME_PRECISION
			FROM [' + @Database2_Name + '].INFORMATION_SCHEMA.COLUMNS
			) COMP 

		---- Find the X DW schema information that does match 
		----   the Y DW schema
		ON REF.TABLE_SCHEMA = COMP.TABLE_SCHEMA  
		  AND REF.TABLE_NAME = COMP.TABLE_NAME 
		  ---- schema , table, and column are the same
		 AND REF.COLUMN_NAME = COMP.COLUMN_NAME 
	)

ORDER BY REF2.TABLE_NAME
 , REF2.COLUMN_NAME '



--- debug
--PRINT @sql  

EXEC sp_executesql @sql 

END

GO

PRINT 'USPs Created'

/********************************************************************************************************************************************************************************
		Other Processing
********************************************************************************************************************************************************************************/
PRINT 'Creating Indexes'

EXEC dbo.BBBI_CreateDWIndexes

PRINT 'Script Deployment Successful'
