DECLARE @Year AS Int
DECLARE @Month AS Int
DECLARE @Day AS Int

SET @Year = (SELECT Year(MAX(starttime)) FROM sysdtslog90)
SET @Month = (SELECT Month(MAX(starttime)) FROM sysdtslog90)
SET @Day = (SELECT Day(MAX(starttime)) FROM sysdtslog90)

SELECT 
--*
sl.ID, sl.Event, sl.Source, sl.starttime, sl.endtime, sl.message,
DATEDIFF(ss, 
(SELECT MIN(starttime) FROM sysdtslog90 WHERE Year(sl.starttime) = @Year AND Month(sl.starttime) = @Month AND Day(sl.starttime) = @Day),  
(SELECT MAX(endtime) FROM sysdtslog90 WHERE Year(sl.starttime) = @Year AND Month(sl.starttime) = @Month AND Day(sl.starttime) = @Day)
)/60 AS TimeLapsed
FROM sysssislog sl
WHERE
Year(starttime) = @Year AND
Month(starttime) = @Month AND
Day(starttime) = @Day
ORDER BY ID DESC


--DELETE sysdtslog90 WHERE ID = 97
--SELECT TOP 1000 * FROM FACT_Action
--TRUNCATE TABLE sysdtslog90

