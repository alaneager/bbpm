There are two types of reports to deploy.  The first is Excel Spreadsheets one for Fundraising (RE) and the other for Finance (FE).  
The second type of reports are SQL Reporting Services reports (.rdl) that need to be deployed to the report server.
Deploy to the report server with the following naming:

Blackbaud/BBPM/<App Name>/Reports/Standard Reports/Fundraising
Blackbaud/BBPM/<App Name>/Data Sources
Blackbaud/BBPM/<App Name>/Datasets

Where <App Name> is the name of the BBPM Application such as "BBPM Demo", "BBPM OSU"
Therefore, you must, in this report project:
1. Change your Data Sources in the SSRS "BBPM Standard Reports" project to reflect the SQL and OLAP Dbs you are connecting to
2. Change your deploy folder as specified above for Reports, Data Source and Datasets
2. Deploy the Data Sources to your SSRS instance in the Data Sources folder
3. Deploy any share Datasets to your SSRS instance in the Dataset folder
4. Deploy your rdls to your SSRS instance
5. Create a directory called Blackbaud/BBPM/<App Name>/Reports/Standard Reports/System and move the following reports to them:
	BBPM Refresh Log
	Sys SSIS Refresh Log
6. Create a directory called Blackbaud/BBPM/<App Name>/Reports/Standard Reports/Finance and move the following FE reports to them:


